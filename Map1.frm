VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{51C0A9CA-F7B7-4F5A-96F4-43927C6FA50F}#1.0#0"; "MapPointControl.ocx"
Begin VB.Form Form1 
   Caption         =   "Mapping Project"
   ClientHeight    =   10035
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14415
   LinkTopic       =   "Form1"
   ScaleHeight     =   10035
   ScaleWidth      =   14415
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdDirections 
      Caption         =   "Directions"
      Height          =   375
      Left            =   13020
      TabIndex        =   29
      Top             =   5460
      Width           =   975
   End
   Begin VB.ComboBox cmbLocation 
      Height          =   315
      Left            =   4800
      Sorted          =   -1  'True
      TabIndex        =   28
      Top             =   3960
      Width           =   5235
   End
   Begin VB.CommandButton cmdFind 
      Caption         =   "Find"
      Height          =   315
      Left            =   10080
      TabIndex        =   27
      Top             =   3960
      Width           =   495
   End
   Begin VB.TextBox txtLocation 
      Height          =   315
      Left            =   4800
      TabIndex        =   25
      Top             =   3960
      Visible         =   0   'False
      Width           =   5235
   End
   Begin VB.CommandButton cmdShowPins 
      Caption         =   "Show Pins"
      Height          =   375
      Left            =   11940
      TabIndex        =   24
      Top             =   5460
      Width           =   975
   End
   Begin VB.CommandButton cmdEnd 
      Caption         =   "Add End"
      Height          =   375
      Left            =   10860
      TabIndex        =   15
      Top             =   5460
      Width           =   975
   End
   Begin VB.CommandButton cmdStart 
      Caption         =   "Add Start"
      Height          =   375
      Left            =   9780
      TabIndex        =   14
      Top             =   5460
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Caption         =   "Parameters"
      Height          =   1935
      Left            =   3780
      TabIndex        =   9
      Top             =   1920
      Width           =   6015
      Begin VB.TextBox txtLineWeight 
         Height          =   285
         Left            =   1320
         TabIndex        =   12
         Text            =   "1"
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox txtDriveTime 
         Height          =   285
         Left            =   1320
         TabIndex        =   11
         Text            =   "6.5"
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label7 
         Caption         =   "Pixel Size:"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   960
         Width           =   975
      End
      Begin VB.Label lblPix 
         Height          =   255
         Left            =   1080
         TabIndex        =   22
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label3 
         Caption         =   "Line Weight:"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Drive Time List:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   1155
      End
   End
   Begin VB.CommandButton cmdZone 
      Caption         =   "Show Zone"
      Height          =   375
      Left            =   8700
      TabIndex        =   8
      Top             =   5460
      Width           =   975
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "Clear Route"
      Height          =   375
      Left            =   7500
      TabIndex        =   7
      Top             =   5460
      Width           =   1095
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "City View"
      Height          =   375
      Left            =   6300
      TabIndex        =   4
      Top             =   5460
      Width           =   1095
   End
   Begin VB.CommandButton cmdOut 
      Caption         =   "Out"
      Height          =   375
      Left            =   5700
      TabIndex        =   2
      Top             =   5460
      Width           =   495
   End
   Begin VB.CommandButton cmdIn 
      Caption         =   "In"
      Height          =   375
      Left            =   5100
      TabIndex        =   1
      Top             =   5460
      Width           =   495
   End
   Begin VB.CommandButton cmdRoute 
      Caption         =   "Route"
      Height          =   375
      Left            =   3900
      TabIndex        =   0
      Top             =   5460
      Width           =   1095
   End
   Begin MSComctlLib.ListView lvList 
      Height          =   1815
      Left            =   12180
      TabIndex        =   3
      Top             =   480
      Width           =   2115
      _ExtentX        =   3731
      _ExtentY        =   3201
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   6
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Item"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Dist"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "LocX"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "LocY"
         Object.Width           =   1235
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Latitude"
         Object.Width           =   3087
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Longitude"
         Object.Width           =   3087
      EndProperty
   End
   Begin MSWinsockLib.Winsock sckTCP 
      Left            =   120
      Top             =   9000
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.ListBox lstDebug 
      Height          =   2010
      Left            =   11520
      TabIndex        =   34
      Top             =   7320
      Width           =   2715
   End
   Begin MSComctlLib.ListView lvMain 
      Height          =   2055
      Left            =   9180
      TabIndex        =   33
      Top             =   7320
      Width           =   2235
      _ExtentX        =   3942
      _ExtentY        =   3625
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MapPointCtl.MappointControl mapMain 
      Height          =   2355
      Left            =   120
      TabIndex        =   32
      Top             =   540
      Width           =   2775
      BorderStyle     =   0
      MousePointer    =   0
      Object.TabStop         =   0   'False
      Appearance      =   1
      PaneState       =   3
      UnitsOfMeasure  =   0
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   435
      Left            =   0
      TabIndex        =   31
      Top             =   9600
      Width           =   14415
      _ExtentX        =   25426
      _ExtentY        =   767
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   2775
      Left            =   60
      TabIndex        =   30
      Top             =   6840
      Width           =   14355
      _ExtentX        =   25321
      _ExtentY        =   4895
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Label Label6 
      Caption         =   "Route Time:"
      Height          =   255
      Left            =   3900
      TabIndex        =   20
      Top             =   5100
      Width           =   975
   End
   Begin VB.Label Label8 
      Caption         =   "Location:"
      Height          =   315
      Left            =   3900
      TabIndex        =   26
      Top             =   3960
      Width           =   855
   End
   Begin VB.Label lblRouteTime 
      Height          =   255
      Left            =   4980
      TabIndex        =   21
      Top             =   5100
      Width           =   2535
   End
   Begin VB.Label lblEnd 
      Height          =   255
      Left            =   4860
      TabIndex        =   19
      Top             =   4740
      Width           =   5175
   End
   Begin VB.Label lblStart 
      Height          =   255
      Left            =   4860
      TabIndex        =   18
      Top             =   4380
      Width           =   5175
   End
   Begin VB.Label Label5 
      Caption         =   "Route End:"
      Height          =   255
      Left            =   3900
      TabIndex        =   17
      Top             =   4740
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Route Start:"
      Height          =   255
      Left            =   3900
      TabIndex        =   16
      Top             =   4380
      Width           =   975
   End
   Begin VB.Label lblRouteDist 
      Height          =   255
      Left            =   8820
      TabIndex        =   6
      Top             =   5100
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Route Distance:"
      Height          =   255
      Left            =   7620
      TabIndex        =   5
      Top             =   5100
      Width           =   1335
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'=====================================================================================
'
'   Application Name:   Simulator Experiment Phase 1
'
'   Module Name:        Map1.frm
'
'   Date Written:       01/10/03
'
'   Developed by:       Brian McGrath
'                       Brimac Systems Inc.
'                       123 Geoffrey Cres.,
'                       Stouffville, ON
'
'   Description:
'
'
'
'   Copyright:          Brimac Systems Inc., 2003
'
'   Developed for:      University of Toronto,
'                       Mechanical and Industrial Engineering Department,
'                       5 King's College Road,
'                       Toronto, ON
'
'                       with the cooperation of:
'
'                       Toronto EMS
'                       4330 Dufferin Street,
'                       Toronto, ON
'
'=====================================================================================

Const TR_Lat = 43.89413         '   top right of view
Const TR_Long = -79.14296
Const BL_Lat = 43.55867         '   bottom left of view
Const BL_Long = -79.626

Dim TR_Anchor As Object         '   global TR location
Dim BL_Anchor As Object         '   global BL location

Dim WP_Start As Waypoint        '   Route Start
Dim WP_End As Waypoint          '   Route End

Private Type XMLStatus
    UnitID As Long
    FromStat As Integer
    ToStat As Integer
    CallID As Long
    StatTime As Variant
    StationID As Long
End Type
    
Private Sub cmdClear_Click()
Dim n As Integer

    mapMain.ActiveMap.ActiveRoute.Clear
    If mapMain.ActiveMap.DataSets.Count > 0 Then
        mapMain.ActiveMap.DataSets.Item(1).Delete
    End If
    
    While mapMain.ActiveMap.Shapes.Count > 0
        mapMain.ActiveMap.Shapes.Item(1).Delete
    Wend

    lvList.ListItems.Clear
    lblStart.Caption = ""
    lblEnd.Caption = ""
    lblRouteTime.Caption = ""
    cmdRoute.Enabled = False
End Sub

Private Sub GoFindLocation(ByVal sLocation As String)
    Dim objLoc As mappointctl.Location
    Dim objPin As mappointctl.Pushpin
    
    Set objLoc = mapMain.ActiveMap.FindResults(sLocation).Item(1)
    
    Set objPin = mapMain.ActiveMap.AddPushpin(objLoc)
    
    objLoc.GoTo
    objPin.Select
End Sub

Private Sub cmdFind_Click()
    If txtLocation.Text <> "" Then
        Call GoFindLocation(txtLocation.Text)
    Else
        Call GoFindLocation(cmbLocation.Text)
    End If
End Sub

Private Sub cmdIn_Click()
    mapMain.ActiveMap.ZoomIn
End Sub

Private Sub cmdDirections_Click()
    If Not mapMain.ItineraryVisible Then
        mapMain.ItineraryVisible = True
    Else
        mapMain.ItineraryVisible = False
    End If
End Sub

Private Sub cmdOut_Click()
    mapMain.ActiveMap.ZoomOut
End Sub

Private Sub cmdReset_Click()
    mapMain.ActiveMap.Union(Array(TR_Anchor, BL_Anchor)).GoTo
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Call ExitApplication
End Sub

Private Sub cmdRoute_Click()
    Dim Route As mappointctl.Route
    
    Set Route = mapMain.ActiveMap.ActiveRoute
    
    Route.DriverProfile.Speed(geoRoadLimitedAccess) = Route.DriverProfile.Speed(geoRoadLimitedAccess) / 6
    Route.DriverProfile.Speed(geoRoadInterstate) = Route.DriverProfile.Speed(geoRoadInterstate) / 6
    Route.DriverProfile.Speed(geoRoadArterial) = Route.DriverProfile.Speed(geoRoadArterial) / 6
    Route.DriverProfile.Speed(geoRoadOtherHighway) = Route.DriverProfile.Speed(geoRoadOtherHighway) / 6
    Route.DriverProfile.Speed(geoRoadStreet) = Route.DriverProfile.Speed(geoRoadStreet) / 6
    
    Route.DriverProfile.TimeBetweenRests = 1 * geoOneMinute
    Route.DriverProfile.RestStopDuration = 1 * geoOneMinute
    Route.DriverProfile.IncludeRestStops = True
    
    Route.Calculate
    
    mapMain.ItineraryVisible = False

    Call CalculateRouteCoordinates(Route)
    
    Route.DriverProfile.ResetSpeeds
    Route.DriverProfile.TimeBetweenRests = 1 * geoOneDay
    Route.DriverProfile.IncludeRestStops = False
    
    Route.Calculate
    mapMain.ItineraryVisible = False
    
    lblRouteTime.Caption = Format(Route.DrivingTime / geoOneMinute, "##0.00 Minutes")
    lblRouteDist.Caption = Format(Route.Distance, "##0.00 Kms")

End Sub

Private Sub cmdShowPins_Click()
    Dim n As Integer
    Dim Item As MSComctlLib.ListItem
    Dim Latitude As Double, Longitude As Double
    Dim oLoc As Location
    Dim x As Long, Y As Long
    
    For n = 1 To lvList.ListItems.Count
    
        Set Item = lvList.ListItems(n)
        
        Latitude = Item.SubItems(4)
        Longitude = Item.SubItems(5)
        
        Set oLoc = mapMain.ActiveMap.GetLocation(Latitude, Longitude)
        
        mapMain.ActiveMap.AddPushpin oLoc, "Position Report " & Item.Text
        
    Next n
End Sub

Private Sub cmdStart_Click()
    Dim newWP As Waypoint
    Dim xyObj As Object
    
    Set xyObj = mapMain.ActiveMap.Selection
        
    If TypeOf xyObj Is Shape Then
        
        MsgBox "Cannot set WayPoint from a Shape", vbInformation, "Error setting WayPoint"
    
    Else
        
        Set newWP = mapMain.ActiveMap.ActiveRoute.Waypoints.Add(xyObj.Location)
        
        newWP.ListPosition = 1
        
        lblStart.Caption = newWP.Location.Name
    
        If mapMain.ActiveMap.ActiveRoute.Waypoints.Count > 1 Then
            cmdRoute.Enabled = True
        Else
            cmdRoute.Enabled = False
        End If
    
    End If
    
End Sub

Private Sub cmdEnd_Click()
    Dim newWP As Waypoint
    Dim xyObj As Object
    
    Set xyObj = mapMain.ActiveMap.Selection
        
    If TypeOf xyObj Is Shape Then
        
        MsgBox "Cannot set WayPoint from a Shape", vbInformation, "Error setting WayPoint"
    
    Else
        
        Set newWP = mapMain.ActiveMap.ActiveRoute.Waypoints.Add(xyObj.Location)
        
        newWP.ListPosition = 2
        
        lblEnd.Caption = newWP.Location.Name
        
        If mapMain.ActiveMap.ActiveRoute.Waypoints.Count > 1 Then
            cmdRoute.Enabled = True
        Else
            cmdRoute.Enabled = False
        End If
    
    End If
      
End Sub

Private Sub cmdZone_Click()
    Dim xyObj As Object
    Dim newObj As mappointctl.Shape
    Dim n As Integer
    Dim aZones() As String
    
    Set xyObj = mapMain.ActiveMap.Selection
    
'    If TypeOf xyObj Is Pushpin Then
'        Set newObj = mapMain.ActiveMap.Shapes.AddDrivetimeZone(xyObj.Location, Val(txtDriveTime.Text) * geoOneMinute)
'    ElseIf TypeOf xyObj Is Shape Then
'        Set newObj = mapMain.ActiveMap.Shapes.AddDrivetimeZone(xyObj.Location, Val(txtDriveTime.Text) * geoOneMinute)
'    ElseIf TypeOf xyObj Is Waypoint Then
'        Set newObj = mapMain.ActiveMap.Shapes.AddDrivetimeZone(xyObj.Location, Val(txtDriveTime.Text) * geoOneMinute)
'    ElseIf TypeOf xyObj Is Location Then
'        Set newObj = mapMain.ActiveMap.Shapes.AddDrivetimeZone(xyObj, Val(txtDriveTime.Text) * geoOneMinute)
'    End If

    aZones = Split(txtDriveTime.Text)
    
    For n = 0 To UBound(aZones)
        Set newObj = mapMain.ActiveMap.Shapes.AddDrivetimeZone(xyObj.Location, Val(aZones(n)) * geoOneMinute)
        newObj.Line.Weight = 1
        Select Case n
            Case 0
                newObj.Line.ForeColor = RGB(0, 200, 0)
            Case 1
                newObj.Line.ForeColor = RGB(255, 255, 0)
            Case 2
                newObj.Line.ForeColor = RGB(255, 200, 0)
            Case 3
                newObj.Line.ForeColor = RGB(255, 0, 0)
            Case Else
                newObj.Line.ForeColor = RGB(0, 0, 0)
        End Select
    Next n
End Sub

Private Sub Form_Load()
    mapMain.NewMap geoMapNorthAmerica
    
    Call InitialiseCalcPos
    
    Form1.Top = 0
    Form1.Left = 0
    Form1.Width = Screen.Width
    Form1.Height = Screen.Height
    
    cmdRoute.Top = Form1.ScaleHeight - cmdRoute.Height - 50
    cmdRoute.Left = Form1.ScaleLeft + 50
    cmdRoute.Enabled = False
    
    Label6.Top = cmdRoute.Top - Label1.Height - 30
    lblRouteTime.Top = Label6.Top
    lblRouteTime.Left = Label6.Left + Label6.Width
    
    Label1.Top = lblRouteTime.Top
    Label1.Left = lblRouteTime.Left + lblRouteTime.Width
    lblRouteDist.Top = Label1.Top
    lblRouteDist.Left = Label1.Left + Label1.Width
    
    Label5.Top = Label1.Top - Label5.Height - 30
    lblEnd.Top = Label5.Top
    lblEnd.Left = Label5.Left + Label5.Width
    
    Label4.Top = Label5.Top - Label4.Height - 30
    lblStart.Top = Label4.Top
    lblStart.Left = Label4.Left + Label4.Width
    
    Label8.Top = Label4.Top - Label4.Height - 50
    Label8.Left = Label4.Left
    txtLocation.Top = Label8.Top
    txtLocation.Left = Label8.Left + Label8.Width
    cmbLocation.Top = Label8.Top
    cmbLocation.Left = Label8.Left + Label8.Width
    cmdFind.Top = Label8.Top
    cmdFind.Left = txtLocation.Left + txtLocation.Width + 30
    
    cmdIn.Top = cmdRoute.Top
    cmdIn.Left = cmdRoute.Left + cmdRoute.Width + 30
    
    cmdOut.Top = cmdRoute.Top
    cmdOut.Left = cmdIn.Left + cmdIn.Width + 30
    
    cmdReset.Top = cmdRoute.Top
    cmdReset.Left = cmdOut.Left + cmdOut.Width + 30
    
    cmdClear.Top = cmdRoute.Top
    cmdClear.Left = cmdReset.Left + cmdReset.Width + 30
    
    cmdZone.Top = cmdRoute.Top
    cmdZone.Left = cmdClear.Left + cmdClear.Width + 30
    
    cmdStart.Top = cmdRoute.Top
    cmdStart.Left = cmdZone.Left + cmdZone.Width + 30
    
    cmdEnd.Top = cmdRoute.Top
    cmdEnd.Left = cmdStart.Left + cmdStart.Width + 30
    
    cmdShowPins.Top = cmdRoute.Top
    cmdShowPins.Left = cmdEnd.Left + cmdEnd.Width + 30
    
    cmdDirections.Top = cmdRoute.Top
    cmdDirections.Left = cmdShowPins.Left + cmdShowPins.Width + 30
    
    mapMain.Top = Form1.ScaleTop
    mapMain.Left = Form1.ScaleLeft + 50
    mapMain.Width = Form1.ScaleWidth * 0.66 - 50
    mapMain.Height = Form1.ScaleHeight * 0.66 - 50
    
    lvList.Top = mapMain.Top
    lvList.Left = mapMain.Left + mapMain.Width + 50
    lvList.Width = Form1.ScaleWidth - 50 - lvList.Left
    lvList.Height = mapMain.Height
    
    Frame1.Top = lvList.TabIndex + lvList.Height + 50
    Frame1.Left = lvList.Left
    Frame1.Height = cmdRoute.Top - Frame1.Top - 50
    Frame1.Width = lvList.Width
    
    mapMain.Units = geoKm
    
'    mapMain.ActiveMap.AllowEdgePan = False
    
    Set TR_Anchor = mapMain.ActiveMap.GetLocation(TR_Lat, TR_Long)
    Set BL_Anchor = mapMain.ActiveMap.GetLocation(BL_Lat, BL_Long)
    
    mapMain.ActiveMap.Union(Array(TR_Anchor, BL_Anchor)).GoTo
    
    lblPix.Caption = mapMain.ActiveMap.PixelSize
    
    Call ConnectToServer
    
End Sub

Private Function GetLatLong2(oLoc As mappointctl.Location) As Double()
    Dim aResults(2) As Double
    Dim oMap As mappointctl.Map
    
    Set oMap = mapMain.ActiveMap
    
    If CalcPos(oMap, oLoc, aResults(0), aResults(1)) Then
        GetLatLong2 = aResults
    End If

End Function

Private Function GetLatLong(ByVal x As Long, ByVal Y As Long) As Double()
    Dim aResults(2) As Double
    Dim degVert As Double, degHoriz As Double
    Dim xTR As Long, yTR As Long, xBL As Long, yBL As Long
    Dim pixVert As Long, pixHoriz As Long
    Dim dPerPixX As Double, dPerPixY As Double
    
    '   get the pixel coordinates of the reference locations
    
    xTR = mapMain.ActiveMap.LocationToX(TR_Anchor.Location)
    yTR = mapMain.ActiveMap.LocationToY(TR_Anchor.Location)
    xBL = mapMain.ActiveMap.LocationToX(BL_Anchor.Location)
    yBL = mapMain.ActiveMap.LocationToY(BL_Anchor.Location)
    
    '   work out the pixel delta between the points
    
    pixVert = yTR - yBL
    pixHoriz = xTR - xBL

    '   work out the geographic delta between the points
    
    degVert = TR_Lat - BL_Lat
    degHoriz = TR_Long - BL_Long
    
    '   work out the degrees per pixel
    
    dPerPixY = degVert / pixVert
    dPerPixX = degHoriz / pixHoriz
    
    '   work out the delta between the point in question and one of the reference locations
    
    pixVert = Y - yBL
    pixHoriz = x - xBL
    
    '   turn pixel coordinates into approximate Lat/Long (within variableness of the pixelsize)
    
    aResults(0) = BL_Lat + (pixVert * dPerPixY)
    aResults(1) = BL_Long + (pixHoriz * dPerPixX)
    
    GetLatLong = aResults
    
End Function

Private Sub lvList_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim Latitude As Double, Longitude As Double
    Dim oLoc As Location
    Dim x As Long, Y As Long
    
    Latitude = Item.SubItems(4)
    Longitude = Item.SubItems(5)
    
    Set oLoc = mapMain.ActiveMap.GetLocation(Latitude, Longitude)
    
    mapMain.ActiveMap.AddPushpin oLoc, "Position Report " & Item.Text
    
End Sub

Private Sub mapMain_AfterViewChange()
    If mapMain.Units = geoKm Then
        lblPix.Caption = Format(mapMain.ActiveMap.PixelSize * 1000, "####0.000 meters")
    Else
        lblPix.Caption = Format(mapMain.ActiveMap.PixelSize * 5280, "####0.000 feet")
    End If
End Sub

Private Sub CalculateRouteCoordinates(ByVal oRoute As mappointctl.Route)
    Dim itemX As ListItem
    Dim Point As mappointctl.Location
    Dim aLatLong As Variant
    Dim x As Long, Y As Long
    Dim n As Integer, counter As Integer
    
    lvList.ListItems.Clear
    counter = 0
    
    For n = 1 To oRoute.Directions.Count
        If oRoute.Directions.Item(n).Distance > 0 Then
            counter = counter + 1
            Set Point = oRoute.Directions.Item(n).Location
    
            Set itemX = lvList.ListItems.Add(, , counter)
            itemX.SubItems(1) = Format(oRoute.Directions.Item(n).Distance, "0.000")
    
            x = mapMain.ActiveMap.LocationToX(Point)
            Y = mapMain.ActiveMap.LocationToY(Point)
    
            itemX.SubItems(2) = x
            itemX.SubItems(3) = Y
    
            ' aLatLong = GetLatLong(x, Y)
            
            aLatLong = GetLatLong2(Point)
            
            itemX.SubItems(4) = Format(aLatLong(0), "##0.000000")
            itemX.SubItems(5) = Format(aLatLong(1), "##0.000000")
        End If
    
    Next n

End Sub

Private Sub txtLocation_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
        cmdFind.SetFocus
        Call GoFindLocation(txtLocation.Text)
    End If
End Sub

Private Sub cmbLocation_KeyPress(KeyAscii As Integer)
Dim n As Integer
Dim Found As Boolean

    Found = False

    If KeyAscii = vbKeyReturn Then
        For n = 0 To cmbLocation.ListCount
            If cmbLocation.Text = cmbLocation.List(n) Then
                Found = True
                Exit For
            End If
        Next n
        
        If Not Found Then
            cmbLocation.AddItem (cmbLocation.Text)
        End If
        
        If cmbLocation.ListCount > 10 Then
            cmbLocation.RemoveItem (10)
        End If
        
        cmdFind.SetFocus
        Call GoFindLocation(cmbLocation.Text)
    End If
End Sub

Private Sub TabStrip1_Click()

    '   Manage the tabs, making the appropriate frames visible
    
    Select Case TabStrip1.SelectedItem.Index
        Case 1      '   Main Tab
            fmDisplays.Visible = True
            fmSetup.Visible = False
            fmLogNotes.Visible = False
            lvMain.Visible = False
            lstDebug.Visible = False
            fmDisplays.Refresh
        Case 2      '   Setup Tab
            fmDisplays.Visible = False
            fmSetup.Visible = True
            fmLogNotes.Visible = False
            lvMain.Visible = False
            lstDebug.Visible = False
            fmSetup.Refresh
        Case 3      '   TCP Log Tab
            fmDisplays.Visible = False
            fmSetup.Visible = False
            fmLogNotes.Visible = False
            lstDebug.Visible = False
            lvMain.Visible = True
            lvMain.Refresh
        Case 4      '   Add Notes to Log Tab
            fmDisplays.Visible = False
            fmSetup.Visible = False
            fmLogNotes.Visible = True
            lvMain.Visible = False
            lstDebug.Visible = False
            lstDebug.Refresh
        Case 5      '   Debug Log Tab
            fmDisplays.Visible = False
            fmSetup.Visible = False
            fmLogNotes.Visible = False
            lvMain.Visible = False
            lstDebug.Visible = True
            lstDebug.Refresh
        Case 8
        Case 9
        Case 10
    End Select
End Sub

Private Sub txtIPCServer_Change()
    If cmbDB.Text <> "" And txtSQLLogin.Text <> "" And txtIPCServer.Text <> "" Then
        cmdTestSQL.Enabled = True
    Else
        cmdTestSQL.Enabled = False
    End If
End Sub

Private Sub txtLogNote_Change()
    cmdSaveNote.Enabled = True
End Sub

Private Sub txtSQLLogin_Change()
    If cmbDB.Text <> "" And txtSQLLogin.Text <> "" And txtIPCServer.Text <> "" Then
        cmdTestSQL.Enabled = True
    Else
        cmdTestSQL.Enabled = False
    End If
End Sub

Private Sub cmbDB_Change()
    If cmbDB.Text <> "" And txtSQLLogin.Text <> "" And txtIPCServer.Text <> "" Then
        cmdTestSQL.Enabled = True
    Else
        cmdTestSQL.Enabled = False
    End If
End Sub

Private Function ConnectToServer() As Boolean
    Dim LocalName As String
    Dim TimeOut As Variant
    
    LocalName = UCase(sckTCP.LocalHostName)
    sckTCP.RemoteHost = txtIPCServer.Text
    sckTCP.RemotePort = 121
    
    AddToList1 "[Connecting] " & sckTCP.RemoteHost & ":" & sckTCP.RemotePort
    lblConnect.Caption = "Connecting"
    
    If sckTCP.State = sckClosed Then
        sckTCP.Connect
        AddToList1 "[State]" & Str(sckTCP.State) & ", " & GetWinSockState(sckTCP)
        AddToList1 "[Local port]" & Str(sckTCP.LocalPort)
        
        TimeOut = Now
        
        While sckTCP.State <> sckConnected And DateDiff("S", TimeOut, Now) < 2
            DoEvents
        Wend
        
        If sckTCP.State <> sckConnected Then        '   Timeout
            AddToList1 "[Error] Failed to connect"
            AddToList1 "[State]" & Str(sckTCP.State) & ", " & GetWinSockState(sckTCP)
            AddToList1 "[Closing]"
            sckTCP.Close
            AddToList1 "[State]" & Str(sckTCP.State) & ", " & GetWinSockState(sckTCP)
            lblConnect.Caption = "Error"
            ConnectToServer = False
        Else                                        '   Success!
            AddToList1 "[State]" & Str(sckTCP.State) & ", " & GetWinSockState(sckTCP)
            AddToList1 "[Registering] " & LocalName & "_UofT_GRAPHS"
            sckTCP.SendData Chr(2) & Chr(200) & LocalName & "_UofT_GRAPHS" & Chr(3)
            lblConnect.Caption = "Connected"
            ConnectToServer = True
        End If
    Else
        AddToList1 "[Error] Cannot connect - invalid state" & Str(sckTCP.State) & ", " & GetWinSockState(sckTCP)
        AddToList1 "[State]" & Str(sckTCP.State) & ", " & GetWinSockState(sckTCP)
        lblConnect.Caption = "Error"
        ConnectToServer = False
    End If
End Function

Private Sub DisconnectServer()
    AddToList1 "[Disconnecting] " & sckTCP.RemoteHost & ":" & sckTCP.RemotePort
    sckTCP.Close
    AddToList1 "[State]" & Str(sckTCP.State) & ", " & GetWinSockState(sckTCP)
    lblConnect.Caption = "Disconnected"
End Sub

Private Function GetWinSockState(WinSock As Object)
    ' sckClosed             0 Default. Closed
    ' sckOpen               1 Open
    ' sckListening          2 Listening
    ' sckConnectionPending  3 Connection pending
    ' sckResolvingHost      4 Resolving host
    ' sckHostResolved       5 Host resolved
    ' sckConnecting         6 Connecting
    ' sckConnected          7 Connected
    ' sckClosing            8 Peer is closing the connection
    ' sckError              9 Error
    Select Case WinSock.State
        Case sckClosed                  ' 0
            GetWinSockState = "Closed"
        Case sckOpen                    ' 1
            GetWinSockState = "Open"
        Case sckListening               ' 2
            GetWinSockState = "Listening"
        Case sckConnectionPending       ' 3
            GetWinSockState = "Connection pending"
        Case sckResolvingHost           ' 4
            GetWinSockState = "Resolving host"
        Case sckHostResolved            ' 5
            GetWinSockState = "Host resolved"
        Case sckConnecting              ' 6
            GetWinSockState = "Connecting"
        Case sckConnected               ' 7
            GetWinSockState = "Connected"
        Case sckClosing                 ' 8
            GetWinSockState = "Peer is closing the connection"
        Case sckError                   ' 9
            GetWinSockState = "Error"
    End Select
End Function

Private Sub sckTCP_Close()
    AddToList1 "[Event] Socket Closed"
End Sub

Private Sub sckTCP_Connect()
    AddToList1 "[Event] Connected"
End Sub

Private Sub sckTCP_ConnectionRequest(ByVal requestID As Long)
    AddToList1 "[Event] Connection Request Received"
End Sub

Private Sub sckTCP_DataArrival(ByVal bytesTotal As Long)
    Dim sTemp As String
    Dim aMessage As Variant
    Dim msgLen As Integer
    Dim sReceived As String
    Dim cASCII As String
    Dim n As Integer, I As Integer
    
    AddToList1 "[Event] DataArrival" & Str(bytesTotal)
    
    sckTCP.GetData sReceived
    
    aMessage = Split(sReceived, Chr(3))     '   split buffer into separate messages at the termination character ASCII 3
    
    For n = 0 To UBound(aMessage) - 1
        msgLen = Len(aMessage(n))
        If msgLen > 3 Then
            aMessage(n) = Right(aMessage(n), msgLen - 1)
            Call AddToList2(aMessage(n), bytesTotal)
            Call ProcessCADMessage(aMessage(n))
        Else
'            TabStrip1.Tabs.Item(7).Selected = True                            '   show debug tab when error occurs
'            Call TabStrip1_Click
            aMessage(n) = Right(aMessage(n), msgLen - 1)
            cASCII = ""
            For I = 1 To Len(aMessage(n))
                cASCII = cASCII & "[" & Trim(Str(Asc(Mid(aMessage(n), I, 1)))) & "]"
            Next I
            AddToList1 "[ODD THING] >" & aMessage(n) & "< ASCII:" & cASCII
        End If
    Next n
    
End Sub

Private Sub sckTCP_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
' ByVal Number As Integer,
' Description As String,
' ByVal Scode As Long,
' ByVal Source As String,
' ByVal HelpFile As String,
' ByVal HelpContext As Long,
' CancelDisplay As Boolean
    TabStrip1.Tabs.Item(5).Selected = True                            '   show debug tab when error occurs
    Call TabStrip1_Click
    AddToList1 "[Event] Error" & Str(Number) & ": " & Description
    AddToList1 "[Info]  Source: " & Source & " Scode:" & Str(Scode)
    AddToList1 "[Info]  HelpFile: " & HelpFile & " HelpContext:" & Str(HelpContext)
    AddToList1 "[Info]  CancelDisplay: " & Switch(CancelDisplay, "TRUE", Not CancelDisplay, "FALSE")
    AddToList1 "[State]" & Str(sckTCP.State) & ", " & GetWinSockState(sckTCP)
End Sub

Private Sub AddToList1(sString As String)
    Dim logitem As String
    
    logitem = Format(Now, "hh:nn:ss ") & sString
    
    lstDebug.AddItem logitem
    If gbLogging Then
        Print #gDebugLog, logitem
    End If
    lstDebug.ListIndex = lstDebug.ListCount - 1
    lstDebug.Selected(lstDebug.ListIndex) = False
End Sub

Private Sub AddToList2(ByVal sString As String, Optional nBytes As Long = 0)
    Dim lvwItem As Object
    Dim msgType As String
    Dim msgTypeString As String
    Dim MsgData As String
    Dim logitem As String

    msgType = Left(sString, 4)
    msgTypeString = VisiCADMessageType(msgType)
    MsgData = Right(sString, Len(sString) - 4)
    
    If gbLogging Then
        logitem = Format(Now, "hh:nn:ss ")
        logitem = logitem & "[" & msgType & "]"
        logitem = logitem & "[" & Format(Len(MsgData), "000") & "]"
        logitem = logitem & "[" & msgTypeString & Space(40 - Len(msgTypeString)) & "]"
        logitem = logitem & ">" & MsgData & "<"
    
        Print #gTCPLog, logitem
    End If

    Set lvwItem = lvMain.ListItems.Add()
    lvwItem.Text = Format(Now, "hh:mm:ss")
    lvwItem.SubItems(1) = msgType
    lvwItem.SubItems(2) = Len(MsgData)
    lvwItem.SubItems(3) = VisiCADMessageType(msgType)
    lvwItem.SubItems(4) = MsgData
    
    lvMain.ListItems.Item(lvMain.ListItems.Count).Selected = True
    lvMain.SelectedItem.EnsureVisible

End Sub

Private Sub ProcessCADMessage(ByVal CADMessage As String)
    Dim MessageType As String
    Dim MessageBody As String
    Dim StatusXML As XMLStatus
    Dim cSQL As String
    Dim aArray As Variant
    Dim nCalls As Long
    
    MessageType = Left(CADMessage, 4)
    MessageBody = Right(CADMessage, Len(CADMessage) - 4)
    
    Select Case MessageType
        Case "0030"         '   NP_READ_MACHINESETTINGS
            Call GetLocalSettingsData(MessageBody)
        Case "0035"         '   NP_NEW_RESPONSE
            Call GetCurrentPerformanceData
        Case "0134"         '   NP_READ_SSMPLANS (Change to View Controller settings)
            Call GetViewSettingsData
            Call ForceUpdate
        Case "0244"         '   NP_READ_VEHICLE_STATUS
            StatusXML = ParseXMLMessage(MessageBody)
            Select Case StatusXML.ToStat
                Case STAT_OffDuty
                    Call GetCurrentUseData
                    Call GetCurrentOOSData
                    Call GetCurrentPostData
                    Call GetCurrentAvailData
                Case STAT_Avail
                    Call GetCurrentUseData
                    Call GetCurrentOOSData
                    Call GetCurrentPostData
                    Call GetCurrentAvailData
                Case STAT_AtStation
                    Call GetCurrentUseData
                    Call GetCurrentOOSData
                    Call GetCurrentPostData
                    Call GetCurrentAvailData
                Case STAT_LocalArea
                    Call GetCurrentUseData
                    Call GetCurrentOOSData
                    Call GetCurrentPostData
                    Call GetCurrentAvailData
                Case STAT_AvailOS
                    Call GetCurrentUseData
                    Call GetCurrentOOSData
                    Call GetCurrentPostData
                    Call GetCurrentAvailData
                Case STAT_OOS
                    Call GetCurrentUseData
                    Call GetCurrentOOSData
                    Call GetCurrentPostData
                    Call GetCurrentAvailData
                Case STAT_Disp
                    Call GetCurrentPerformanceData
                    Call GetCurrentUseData
                    Call GetCurrentOOSData
                    Call GetCurrentPostData
                    Call GetCurrentAvailData
                Case STAT_Resp
                Case STAT_Enr2Post
                    Call GetCurrentUseData
                    Call GetCurrentOOSData
                    Call GetCurrentPostData
                    Call GetCurrentAvailData
                Case STAT_Staged
                Case STAT_OnScene
                    Call GetCurrentPerformanceData
                    cSQL = "Select Priority_Description from Response_Master_Incident " & _
                                "Where ID = " & Str(StatusXML.CallID)
                                
                    nCalls = bsiSQLGet(cSQL, aArray, gSQLConnection)
                    
                    Select Case aArray(0, 0)
                        Case AlphaPrio
                            Call GetAlphaHistory
                        Case BravoPrio
                            Call GetBravoHistory
                        Case CharliePrio
                            Call GetCharlieHistory
                        Case DeltaPrio
                            Call GetDeltaHistory
                        Case EchoPrio
                            Call GetEchoHistory
                    End Select
                Case STAT_PtContact
                Case STAT_Transport
                Case STAT_AtDest
                Case STAT_DelayedAvail
                Case STAT_AssignEval
                Case STAT_ShiftPend
                Case STAT_ATP
                    Call GetCurrentUseData
                    Call GetCurrentOOSData
                    Call GetCurrentPostData
                    Call GetCurrentAvailData
                Case STAT_MultiAssign
                Case STAT_D2L
                Case STAT_R2L
                Case STAT_A2L
            End Select
    End Select
        
End Sub

Private Function ParseXMLMessage(Message As String) As XMLStatus
    Dim Result As XMLStatus
    Dim nStart As Integer
    Dim nStop As Integer
    
    '00:36:41 [0244][214][NP_READ_VEHICLE_STATUS                  ]
    '<Vehicle ID="166"  ReadVehicleSent= "-1">
    '    <FromStatus>13</FromStatus>
    '    <ToStatus>14</ToStatus>
    '    <Master_Incident_ID>2291</Master_Incident_ID>
    '    <TimeStamp>Apr 17 2003 00:36:38</TimeStamp>
    '    <StationID>34</StationID>
    '</Vehicle>
    
    'Type XMLStatus
    '    UnitID As Integer
    '    FromStat As Integer
    '    ToStat As Integer
    '    CallID As Integer
    '    StatTime As Variant
    '    StationID As Integer
    'End Type
    
    nStart = InStr(1, Message, "<Vehicle ID=") + Len("<Vehicle ID=") + 1
    nStop = InStr(nStart, Message, "ReadVehicleSent=")
    Result.UnitID = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<FromStatus>") + Len("<FromStatus>")
    nStop = InStr(nStart, Message, "</FromStatus>")
    Result.FromStat = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<ToStatus>") + Len("<ToStatus>")
    nStop = InStr(nStart, Message, "</ToStatus>")
    Result.ToStat = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<Master_Incident_ID>") + Len("<Master_Incident_ID>")
    nStop = InStr(nStart, Message, "</Master_Incident_ID>")
    Result.CallID = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<TimeStamp>") + Len("<TimeStamp>")
    nStop = InStr(nStart, Message, "</TimeStamp>")
    Result.StatTime = CDate(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<StationID>") + Len("<StationID>")
    nStop = InStr(nStart, Message, "</StationID>")
    Result.StationID = Val(Mid(Message, nStart, nStop - nStart))

    ParseXMLMessage = Result
    
End Function

Private Function VisiCADMessageType(ByVal txtMSG As String)
    Select Case txtMSG
        Case "8000"
            VisiCADMessageType = "NP_READ_SQLTABLE"
        Case "9000"
            VisiCADMessageType = "NP_USER_DEFINED"
        Case "0001"
            VisiCADMessageType = "NP_TIME_UPDATE"
        Case "0002"
            VisiCADMessageType = "NP_READ_RESOURCETYPE"
        Case "0094"
            VisiCADMessageType = "NP_READ_RESOURCEGROUPS"
        Case "0121"
            VisiCADMessageType = "NP_READ_RESOURCEGROUPLIST"
        Case "0003"
            VisiCADMessageType = "NP_READ_SYSTEM_PREFS"
        Case "0005"
            VisiCADMessageType = "NP_AVL_UPDATE"
        Case "0006"
            VisiCADMessageType = "NP_MESSAGE_BROADCAST"
        Case "0009"
            VisiCADMessageType = "NP_MAIL_WAITING"
        Case "0010"
            VisiCADMessageType = "NP_SWITCH_MIRROREDSERVER"
        Case "0011"
            VisiCADMessageType = "NP_MAP_TO_MIRROR"
        Case "0012"
            VisiCADMessageType = "NP_READ_PAGER_GROUPS"
        Case "0014"
            VisiCADMessageType = "NP_READ_AGENCYTYPES"
        Case "0015"
            VisiCADMessageType = "NP_READ_JURISDICTIONS"
        Case "0016"
            VisiCADMessageType = "NP_READ_DIVISIONS"
        Case "0017"
            VisiCADMessageType = "NP_READ_BATTALIONS"
        Case "0018"
            VisiCADMessageType = "NP_READ_RESPONSEAREAS"
        Case "0027"
            VisiCADMessageType = "NP_READ_PRIORITY"
        Case "0064"
            VisiCADMessageType = "NP_READ_STATUS"
        Case "0065"
            VisiCADMessageType = "NP_READ_STATUS_ACTION"
        Case "0028"
            VisiCADMessageType = "NP_READ_PAGERMESSAGE_TYPE"
        Case "0029"
            VisiCADMessageType = "NP_READ_PAGING_SERVICE"
        Case "0030"
            VisiCADMessageType = "NP_READ_MACHINESETTINGS"
        Case "0034"
            VisiCADMessageType = "NP_READ_LOCATIONTYPE"
        Case "0069"
            VisiCADMessageType = "NP_READ_SHIFTTYPE"
        Case "0070"
            VisiCADMessageType = "NP_READ_CALLDISPOSITION"
        Case "0071"
            VisiCADMessageType = "NP_READ_CANCELREASON"
        Case "0072"
            VisiCADMessageType = "NP_READ_FACILITYCHANGE"
        Case "0073"
            VisiCADMessageType = "NP_READ_INVENTORY"
        Case "0074"
            VisiCADMessageType = "NP_READ_FTETYPES"
        Case "0075"
            VisiCADMessageType = "NP_READ_FIRSTRESPONSE"
        Case "0076"
            VisiCADMessageType = "NP_READ_INCIDENTTYPE"
        Case "0077"
            VisiCADMessageType = "NP_READ_DELAY"
        Case "0068"
            VisiCADMessageType = "NP_READ_OUTOFSERVICEREASONS"
        Case "0078"
            VisiCADMessageType = "NP_READ_METHODOFCALLRCVD"
        Case "0079"
            VisiCADMessageType = "NP_READ_RADIOCHANNELS"
        Case "0080"
            VisiCADMessageType = "NP_READ_ROLODEXCATEGORIES"
        Case "0081"
            VisiCADMessageType = "NP_READ_PROTOCOL"
        Case "0082"
            VisiCADMessageType = "NP_READ_UPDNGRADE"
        Case "0083"
            VisiCADMessageType = "NP_READ_FUNCTIONALITYGROUPS"
        Case "0084"
            VisiCADMessageType = "NP_READ_VEHICLEBODYTYPE"
        Case "0085"
            VisiCADMessageType = "NP_READ_VEHICLECERTIFICATION"
        Case "0086"
            VisiCADMessageType = "NP_READ_UNITNAMES"
        Case "0087"
            VisiCADMessageType = "NP_READ_HYDRANTCLASS"
        Case "0088"
            VisiCADMessageType = "NP_READ_BURNPERMITTYPE"
        Case "0089"
            VisiCADMessageType = "NP_READ_ALARMTYPES"
        Case "0090"
            VisiCADMessageType = "NP_READ_CALLERTYPES"
        Case "0091"
            VisiCADMessageType = "NP_READ_CAPABILITIES"
        Case "0092"
            VisiCADMessageType = "NP_READ_EMPLOYEECERT"
        Case "0055"
            VisiCADMessageType = "NP_READ_EMPPOSITION"
        Case "0093"
            VisiCADMessageType = "NP_READ_AUDITREASONS"
        Case "0095"
            VisiCADMessageType = "NP_READ_TRANSPORTMODES"
        Case "0097"
            VisiCADMessageType = "NP_READ_USERTIMESTAMP"
        Case "0163"
            VisiCADMessageType = "NP_READ_USERDATESTAMP"
        Case "0100"
            VisiCADMessageType = "NP_READ_BURN_PERMITS"
        Case "0101"
            VisiCADMessageType = "NP_READ_SHIFTNAME"
        Case "0106"
            VisiCADMessageType = "NP_READ_LISTBOXCOLUMNSETUP"
        Case "0111"
            VisiCADMessageType = "NP_READ_LAYOUTASSIGNMENTS"
        Case "0117"
            VisiCADMessageType = "NP_READ_PROBLEMS"
        Case "0118"
            VisiCADMessageType = "NP_READ_MODULES"
        Case "0120"
            VisiCADMessageType = "NP_READ_INCIDENTNUMBERSETUP"
        Case "0130"
            VisiCADMessageType = "NP_READ_SHORTHAND_COMMENTS"
        Case "0213"
            VisiCADMessageType = "NP_READ_MA911PROBLEM"
        Case "0214"
            VisiCADMessageType = "NP_READ_MA911PROBXREF"
        Case "0212"
            VisiCADMessageType = "NP_READ_MULTIAGENCY"
        Case "0218"
            VisiCADMessageType = "NP_READ_TRANSPRIORITY"
        Case "0022"
            VisiCADMessageType = "NP_READ_LOCATIONS"
        Case "0033"
            VisiCADMessageType = "NP_CLEAR_LOCATIONS"
        Case "0023"
            VisiCADMessageType = "NP_READ_LOCATIONPERSONNEL"
        Case "0024"
            VisiCADMessageType = "NP_READ_LOCATIONRESPONSEPLAN"
        Case "0026"
            VisiCADMessageType = "NP_READ_LOCATIONHAZMAT"
        Case "0216"
            VisiCADMessageType = "NP_READ_LOCATIONRESPONSEAREAS"
        Case "0032"
            VisiCADMessageType = "NP_READ_RESPONSEPLAN"
        Case "0107"
            VisiCADMessageType = "NP_READ_RESPONSEPLAN_STATIC"
        Case "0108"
            VisiCADMessageType = "NP_READ_RESPONSEPLAN_STATICALARM"
        Case "0021"
            VisiCADMessageType = "NP_READ_ALLVEHICLEINFO"
        Case "0020"
            VisiCADMessageType = "NP_READ_VEHICLE"
        Case "0244"
            VisiCADMessageType = "NP_READ_VEHICLE_STATUS"
        Case "0245"
            VisiCADMessageType = "NP_READ_VEHICLE_POSITION"
        Case "0007"
            VisiCADMessageType = "NP_VEHICLE_LATE"
        Case "0165"
            VisiCADMessageType = "NP_READ_VEHICLEOUTOFSERVICE"
        Case "0176"
            VisiCADMessageType = "NP_READ_VEHICLERADIOS"
        Case "0109"
            VisiCADMessageType = "NP_NEW_VEHICLE_SHIFT"
        Case "0110"
            VisiCADMessageType = "NP_REMOVE_VEHICLE_SHIFT"
        Case "0102"
            VisiCADMessageType = "NP_READ_VEHICLE_SHIFT"
        Case "0103"
            VisiCADMessageType = "NP_READ_VEHICLE_SHIFT_PERSONNEL"
        Case "0035"
            VisiCADMessageType = "NP_NEW_RESPONSE"
        Case "0036"
            VisiCADMessageType = "NP_REMOVE_RESPONSE"
        Case "0123"
            VisiCADMessageType = "NP_READ_RESPONSE_FIELDS"
        Case "0037"
            VisiCADMessageType = "NP_READ_RESPONSE_MASTER_INCIDENT"
        Case "0038"
            VisiCADMessageType = "NP_READ_RESPONSE_ALARMS"
        Case "0039"
            VisiCADMessageType = "NP_READ_RESPONSE_USER_TIMESTAMPS"
        Case "0040"
            VisiCADMessageType = "NP_READ_RESPONSE_PRESCHEDULED"
        Case "0152"
            VisiCADMessageType = "NP_READ_RESPONSE_DESTINATION"
        Case "0041"
            VisiCADMessageType = "NP_READ_RESPONSE_AUDIT"
        Case "0042"
            VisiCADMessageType = "NP_READ_RESPONSE_CALLBACKS"
        Case "0043"
            VisiCADMessageType = "NP_READ_RESPONSE_CHANGEDADDRESS"
        Case "0001"
              VisiCADMessageType = "RESPONSE_CHANGEDADDRESS_PICK"
        Case "0002"
              VisiCADMessageType = "RESPONSE_CHANGEDADDRESS_DEST"
        Case "0003"
              VisiCADMessageType = "RESPONSE_CHANGEDADDRESS_BOTH"
        Case "0044"
            VisiCADMessageType = "NP_READ_RESPONSE_PRIORITYCHANGE"
        Case "0045"
            VisiCADMessageType = "NP_READ_RESPONSE_VEHICLESASSIGN"
        Case "0046"
            VisiCADMessageType = "NP_READ_RESPONSE_TRANSPORT"
        Case "0047"
            VisiCADMessageType = "NP_READ_RESPONSE_TRANSPORTLEGS"
        Case "0048"
            VisiCADMessageType = "NP_READ_RESPONSE_PATIENT"
        Case "0066"
            VisiCADMessageType = "NP_READ_RESPONSE_COMMENTS"
        Case "0150"
            VisiCADMessageType = "NP_READ_RESPONSE_COMMENTS_NOTIFICATION"
        Case "0153"
            VisiCADMessageType = "NP_REMOVE_COMMENTS_NOTIFICATION"
        Case "0162"
            VisiCADMessageType = "NP_READ_RESPONSE_USERDATAFIELDS"
        Case "0122"
            VisiCADMessageType = "NP_RESPONSE_LATE"
        Case "0125"
            VisiCADMessageType = "NP_READ_RESPONSE_EDIT"
        Case "0131"
            VisiCADMessageType = "NP_READ_RESPONSE_ANIALI"
        Case "0126"
            VisiCADMessageType = "NP_READ_COMMAND_CONFIG"
        Case "0235"
            VisiCADMessageType = "NP_CHANGED_RESPONSE_DESTINATION"
        Case "0236"
            VisiCADMessageType = "NP_CHANGED_RESPONSE_PROBLEMNATURECHANGE"
        Case "0049"
            VisiCADMessageType = "UPDATE_CONTROLLER_VIEW"
        Case "0098"
            VisiCADMessageType = "VISICAD_IS_TERMINATING"
        Case "0067"
            VisiCADMessageType = "NP_READ_ALLPERSONNEL"
        Case "0031"
            VisiCADMessageType = "NP_READ_PERSONNEL"
        Case "0050"
            VisiCADMessageType = "NP_READ_EMPALTPOSITION"
        Case "0051"
            VisiCADMessageType = "NP_READ_EMPBATTALION"
        Case "0053"
            VisiCADMessageType = "NP_READ_EMPDIVISION"
        Case "0054"
            VisiCADMessageType = "NP_READ_EMPPAGEGROUP"
        Case "0056"
            VisiCADMessageType = "NP_READ_EMPPHONE"
        Case "0057"
            VisiCADMessageType = "NP_READ_EMPPASSWORD"
        Case "0058"
            VisiCADMessageType = "NP_READ_EMPHISTORY"
        Case "0052"
            VisiCADMessageType = "NP_READ_EMPCERTIFICATION"
        Case "0061"
            VisiCADMessageType = "GIS_ZOOM_ON_VEHICLE"
        Case "0062"
            VisiCADMessageType = "GIS_ZOOM_ON_INCIDENT"
        Case "0063"
            VisiCADMessageType = "GIS_ZOOM_TO_CLOSEST_VEHICLES"
        Case "0096"
            VisiCADMessageType = "GIS_ZOOM_ON_LATLON"
        Case "0159"
            VisiCADMessageType = "GIS_CLEAR_ON_LATLON"
        Case "0210"
            VisiCADMessageType = "GIS_ZOOM_ON_SSM_PLAN"
        Case "0119"
            VisiCADMessageType = "GIS_CLEAR_ADDRESS_FLAGS"
        Case "0226"
            VisiCADMessageType = "GIS_FAX_PRINT"
        Case "0019"
            VisiCADMessageType = "NP_READ_STATIONS"
        Case "0059"
            VisiCADMessageType = "NP_READ_STATIONBATTALIONS"
        Case "0060"
            VisiCADMessageType = "NP_READ_STATIONPOSITIONS"
        Case "0177"
            VisiCADMessageType = "NP_READ_STATIONRADIOS"
        Case "0112"
            VisiCADMessageType = "NP_READ_RA_SEGMENTS"
        Case "0113"
            VisiCADMessageType = "NP_READ_PIORITY_RESPONSETIMES"
        Case "0114"
            VisiCADMessageType = "NP_READ_RESPONSE_PLAN_LINK"
        Case "0115"
            VisiCADMessageType = "NP_READ_REGION_TYPE"
        Case "0116"
            VisiCADMessageType = "NP_READ_REGION_SEGEMENTS"
        Case "0124"
            VisiCADMessageType = "NP_READ_GA_STREETS"
        Case "0127"
            VisiCADMessageType = "NP_READ_NEW_CENTROID"
        Case "0128"
            VisiCADMessageType = "NP_READ_RA_CHIEFLINK"
        Case "0129"
            VisiCADMessageType = "EDITMGR_VIEW_RESPONSE"
        Case "0133"
            VisiCADMessageType = "NP_READ_SSMGROUPS"
        Case "0134"
            VisiCADMessageType = "NP_READ_SSMPLANS"
        Case "0198"
            VisiCADMessageType = "NP_READ_SSMCHANGEPLAN"
        Case "0147"
            VisiCADMessageType = "NP_REMOVE_SSMGROUP"
        Case "0148"
            VisiCADMessageType = "NP_REMOVE_SSMPLAN"
        Case "0145"
            VisiCADMessageType = "NP_REMOVE_ACTIVE_SSMPLAN"
        Case "0146"
            VisiCADMessageType = "NP_READ_ACITVE_SSMPLAN"
        Case "0135"
            VisiCADMessageType = "NP_READ_RESETTIMERREASONS"
        Case "0136"
            VisiCADMessageType = "NP_READ_ALARM_ZONES"
        Case "0137"
            VisiCADMessageType = "NP_READ_CAUTION_NOTES"
        Case "0138"
            VisiCADMessageType = "NP_RESET_LATE_TIMER"
        Case "0139"
            VisiCADMessageType = "NP_READ_COMMENTS"
        Case "0141"
            VisiCADMessageType = "NP_READ_MGMT_RULES"
        Case "0142"
            VisiCADMessageType = "NP_READ_MGMT_PRIORITIES"
        Case "0143"
            VisiCADMessageType = "NP_READ_MGMT_STATUS"
        Case "0144"
            VisiCADMessageType = "NP_READ_MGMT_AREA"
        Case "0149"
            VisiCADMessageType = "NP_READ_UNSENT_RESOURCES"
        Case "0151"
            VisiCADMessageType = "NP_AVAILABLE_RESOURCE"
        Case "0154"
            VisiCADMessageType = "NP_READ_MAPBOOK"
        Case "0155"
            VisiCADMessageType = "NP_ADD_DISPLAY_COMMENT_NOTE"
        Case "0156"
            VisiCADMessageType = "NP_REMOVE_DISPLAY_COMMENT_NOTE"
        Case "0157"
            VisiCADMessageType = "NP_ADD_VEHICLE_DISPLAY_COMMENT_NOTE"
        Case "0158"
            VisiCADMessageType = "NP_REMOVE_VEHICLE_DISPLAY_COMMENT_NOTE"
        Case "0160"
            VisiCADMessageType = "NP_READ_CROSS_STREET"
        Case "0161"
            VisiCADMessageType = "NP_READ_RESPONSE_AREA"
        Case "0164"
            VisiCADMessageType = "NP_READ_RESPONSELEVELASSIGNMENT"
        Case "0167"
            VisiCADMessageType = "NP_READ_START_DETECT_VERSION"
        Case "0168"
            VisiCADMessageType = "NP_READ_END_DETECT_VERSION"
        Case "0169"
            VisiCADMessageType = "NP_READ_UPDATE_VERSION"
        Case "0170"
            VisiCADMessageType = "NP_READ_MUST_RESTART"
        Case "0171"
            VisiCADMessageType = "NP_READ_DISPATCHER_NOTES"
        Case "0172"
            VisiCADMessageType = "NP_READ_GRIDPACK"
        Case "0173"
            VisiCADMessageType = "NP_READ_GRIDNAMES"
        Case "0174"
            VisiCADMessageType = "NP_CADMONITOR_RUNNING"
        Case "0181"
            VisiCADMessageType = "NP_APP_HEARTBEAT"
        Case "0175"
            VisiCADMessageType = "NP_READ_GRID"
        Case "0178"
            VisiCADMessageType = "NP_READ_MESSAGE_CHANGE"
        Case "0179"
            VisiCADMessageType = "NP_REMOVE_MESSAGE_CHANGE"
        Case "0180"
            VisiCADMessageType = "NP_READ_TIMEBLOCK"
        Case "0182"
            VisiCADMessageType = "NP_REMOVE_PATIENT"
        Case "0183"
            VisiCADMessageType = "NP_ACTIVATE_VISICAD_MAIN"
        Case "0184"
            VisiCADMessageType = "NP_ACTIVATE_WAITING_QUEUE"
        Case "0185"
            VisiCADMessageType = "NP_ACTIVATE_ACTIVE_QUEUE"
        Case "0186"
            VisiCADMessageType = "NP_ACTIVATE_UNIT_QUEUE"
        Case "0187"
            VisiCADMessageType = "NP_ACTIVATE_INCIDENT_EDITOR"
        Case "0188"
            VisiCADMessageType = "NP_ACTIVATE_EMERGENCY_CALL"
        Case "0189"
            VisiCADMessageType = "NP_ACTIVATE_SCHEDULED_CALL"
        Case "0190"
            VisiCADMessageType = "NP_ACTIVATE_JUR_DIVISION_SELECT"
        Case "0191"
            VisiCADMessageType = "NP_ACTIVATE_PAGING"
        Case "0192"
            VisiCADMessageType = "NP_ACTIVATE_HELP"
        Case "0193"
            VisiCADMessageType = "NP_ACTIVATE_TOOLS_LAUNCHER"
        Case "0194"
            VisiCADMessageType = "NP_ACTIVATE_UNSENT_RESOURCES"
        Case "0267"
            VisiCADMessageType = "NP_ACTIVATE_REPORTS_LAUNCHER"
        Case "0268"
            VisiCADMessageType = "NP_ACTIVATE_CALL_LOADING"
        Case "0201"
            VisiCADMessageType = "NP_ACTIVATE_CANCEL_RESPONSE"
        Case "0202"
            VisiCADMessageType = "NP_ACTIVATE_DUPLICATE_CALL"
        Case "0204"
            VisiCADMessageType = "NP_ACTIVATE_GIS"
        Case "0205"
            VisiCADMessageType = "NP_ACTIVATE_STATION_VIEWER"
        Case "0206"
            VisiCADMessageType = "NP_ACTIVATE_CARDFILE"
        Case "0207"
            VisiCADMessageType = "NP_ACTIVATE_ACTIVITY_LOG_VIEWER"
        Case "0208"
            VisiCADMessageType = "NP_ACTIVATE_PERSONNEL_MANAGER"
        Case "0209"
            VisiCADMessageType = "NP_ACTIVATE_VIEW_CONTROLLER"
        Case "0215"
            VisiCADMessageType = "NP_ACTIVATE_MULTIAGENCY"
        Case "0193"
            VisiCADMessageType = "NP_READ_OOS_LOG"
        Case "0194"
            VisiCADMessageType = "NP_READ_DIVERTREASONS"
        Case "0195"
            VisiCADMessageType = "NP_READ_SNAPSHOT_DISPATCH"
        Case "0196"
            VisiCADMessageType = "NP_READ_SNAPSHOT_CLOCK_START"
        Case "0197"
            VisiCADMessageType = "NP_READ_SHAPSHOT_CLOCK_START_END"
        Case "0199"
            VisiCADMessageType = "NP_PAINT"
        Case "0217"
            VisiCADMessageType = "NP_SECONDARY_ROUTE_RESULTS"
        Case "0218"
            VisiCADMessageType = "NP_READ_ROSTER_EXCEPTION"
        Case "0219"
            VisiCADMessageType = "NP_CHATMESSAGE"
        Case "0220"
            VisiCADMessageType = "NP_READ_FACILITY_REASONS"
        Case "0221"
            VisiCADMessageType = "NP_READ_FACILITY_STATUS"
        Case "0222"
            VisiCADMessageType = "NP_UPDATE_FACILITY_STATUS"
        Case "0223"
            VisiCADMessageType = "NP_READ_MACHINEMODULE"
        Case "0224"
            VisiCADMessageType = "NP_SEND_MSGBOX_TO_MACHINE"
        Case "0225"
            VisiCADMessageType = "NP_READ_ROSTERSCHEDULE"
        Case "0230"
            VisiCADMessageType = "NP_READ_EMPLOYEESCHEDULE"
        Case "0227"
            VisiCADMessageType = "NP_ACTIVATE_ROSTER_SYSTEM"
        Case "0192"
            VisiCADMessageType = "NP_ACTIVATE_SHIFT_MANAGER"
        Case "0228"
            VisiCADMessageType = "NP_READ_ROSTERTEMPLATE"
        Case "0229"
            VisiCADMessageType = "NP_RECORDCHECK"
        Case "0231"
            VisiCADMessageType = "NP_TRANSFER_INCIDENT"
        Case "0232"
            VisiCADMessageType = "NP_TRANSFER_CONFIRM"
        Case "0233"
            VisiCADMessageType = "NP_READ_TRANSFERREASON"
        Case "0234"
            VisiCADMessageType = "NP_READ_MULTI_ASSIGN_VEHICLE"
        Case "0237"
            VisiCADMessageType = "NP_READ_ROSTER_EXCEPTION"
        Case "0238"
            VisiCADMessageType = "NP_FAILOVER_IN_PROGRESS"
        Case "0239"
            VisiCADMessageType = "NP_FAILOVER_COMPLETED"
        Case "0240"
            VisiCADMessageType = "NP_READ_RESPONSEPLANSTATION"
        Case "0241"
            VisiCADMessageType = "NP_CADMON_SCHEDULE_VEHICLE_LATE_OS"
        Case "0242"
            VisiCADMessageType = "GIS_ZOOM_AT_INITIAL_ASSIGN"
        Case "0243"
            VisiCADMessageType = "NP_READ_RESPONSE_LICENSE_INFO"
        Case "0246"
            VisiCADMessageType = "NP_ZOOM_GIS"
        Case "0247"
            VisiCADMessageType = "NP_TESTSTATIONTONE"
        Case "0248"
            VisiCADMessageType = "NP_READ_XML"
        Case "0249"
            VisiCADMessageType = "NP_DATABASE_FREE_SPACE"
        Case "0250"
            VisiCADMessageType = "NP_INERFACE_MANUAL_SEND"
        Case "0251"
            VisiCADMessageType = "NP_ACTIVATE_DEPARTSCENE"
        Case "0252"
            VisiCADMessageType = "NP_ACTIVATE_STATIONASSIGN"
        Case "0253"
            VisiCADMessageType = "NP_READ_RESPONSE_USERCUSTOMDATA"
        Case "0254"
            VisiCADMessageType = "NP_ACTIVATE_CHG_TRANSPORT"
        Case "0255"
            VisiCADMessageType = "NP_READ_GENERIC_XML"
        Case "0256"
            VisiCADMessageType = "NP_SHOW_NOTES_INCIDENT"
        Case "0257"
            VisiCADMessageType = "NP_SHOW_NOTES_VEHICLE"
        Case "0258"
            VisiCADMessageType = "NP_ACTIVATE_COMMANDLINEACTION"
        Case "0259"
            VisiCADMessageType = "NP_PLAY_INCIDENT_LATE_SOUND"
        Case "0260"
            VisiCADMessageType = "NP_RECORDCHECK_RESULT"
        Case "0261"
            VisiCADMessageType = "NP_RECORDCHECK_CLIENT_RESULT"
        Case "0262"
            VisiCADMessageType = "NP_READ_VEHICLE_PERSONNEL_XML"
        Case "0263"
            VisiCADMessageType = "NP_TESTSTATIONPRINT"
        Case "0264"
            VisiCADMessageType = "NP_READ_ACTIVATE_EXIT_CAD"
        Case "0265"
            VisiCADMessageType = "NP_READ_ACTIVATE_LOGOFF_CAD"
        Case "0266"
            VisiCADMessageType = "NP_READ_ACTIVATE_LOGOFFON_CAD"
        Case Else
            VisiCADMessageType = "UNKNOWN VisiCAD MESSAGE TYPE"
    End Select
End Function

Private Sub ExitApplication()
    mapMain.ActiveMap.Saved = True
    If sckTCP.State <> sckClosed Then
        Call DisconnectServer
    End If
    If gbLogging Then
        AddToList1 "[Application Shutdown]"
        AddToList2 "9000BRIMAC Application Shutdown"
        Close #gTCPLog
        Close #gDebugLog
    End If
    End
End Sub


