# README #

cnSimulator (Think Geo) Master Console application

### What is this repository for? ###

* Training Simulator for VisiCAD/InformCAD (all versions)
* Current release: v3.2b101

### How do I get set up? ###

* Call us
* Dependencies:
- ThinkGeo v8.0
- Note: cnSimulatorTG can run on a VisiCAD workstation
- Requires cnIPCServer to be running on a VisiCAD workstation
- Requires cnRoutingTG to be running on a VisiCAD workstation

* Database configuration
- requires ODBC System DSN's for VisiCAD Streets and System db
- USR: tritech
- PWD: europa

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Developer: Brian McGrath - brianmcg@cadnorth.com