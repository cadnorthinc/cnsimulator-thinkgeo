Simulator ToDo File - Lists current Work-In-Progress, Bug Reports, Feature Requests, etc.
Started 2017.05.04	Brian McGrath, CAD North Inc.

BugReport/Feature Refinement
2017.05.11	ASNSW
1. It appears that the client has checked some/all of the checkbox in the Stations/Post Speech Configuration list, creating a situation where
	these stations/posts become candidate destinations for patient transports in the ArrivedAtScene proceedure. This confusion has existed from 
	very early in the development because of the kludgey way I've got it organized (in the Speech Configuration form).
	
	IDEA:	Modify the UNIT BEHAVIOR tab to include one frame for transport destination configuration (but not speech configuration)
			and a second frame for Transport Unit configuration. Lose the road network speeds frame since this is legacy for MapPoint
			and this is all handled within the ESRI routing files now.
2. Long routes are still being calculated (one instance in testing was 300+kms). Suspect destination selection. Limit destinations based on Load Test Radius while in load testing.
3. Change logic so that Load Test respects the unit-behavior-at-destination setting from the normal operation so that units can be reassigned immediately rather than returning to base after each call.
4. Client reports calls are stacking/becoming multi-assigned. Suspect that this is because the user is not resetting the CAD to all units at base prior to starting a load test. Or, alternatively, this 
	is because the user is stopping/restarting the load test to add units rather than pausing/resuming to pause the load test. Change name on button to "END LOAD TEST" as a start and then attempt to
	reeducate the client.
	ADDITIONAL INFO: This now seems to be related to the user sending incidents to queue via running a script and using the [Send Inc] button.
5. As with #4, look at adding a feature to reset the system to no incidents and all units at base.
6. Add functionality to update list of units being used in the load test by allowing user to check off additional units in list to have them added to the test. Update the list of all units used in the load test.


Bug Reports
2017.05.04	ASNSW
1. Need to turn off logging of IPC message 0181 (NP_APP_HEARTBEAT) in all applications. (Simulator, cnRoutingTG, cnIPCServer?)	DONE
2. In Load Testing, do not send route requests with 0,0 Lat/Lon coordinates at destination.		Implemented in cnRoutingTG - Returns an error message	DONE
3. Further to #2, reassign unit from current location when Home Station has 0,0 coordinates.	DONE
	-	when unit receives a zero-length route (the error condition from #2) it will immediately be moved to the end status in its current action.
4. In Load Testing, the process of finding appropriate incidents to assign need to change from a bulk get at start-up to a unit-centric query that finds 
	appropriate incidents for each unit that is in the test or gets added later.	DONE
	-	this is done by selecting incidents from within a bounding box around the unit location when there are no candidate incidents in the current data set
	-	the new incidents are added to the incident data set so that the next time the  unit comes up for assignment, there will likely be a suitable incident
		otherwise, a new bounding box is created and new incidents are added.
5. For #4, will need to implement a function that returns a bounding box for a passed coordinate pair and radius/distance parameter.	DONE
