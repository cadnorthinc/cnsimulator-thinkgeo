Attribute VB_Name = "Startup"
'Public Type tLicenseInfo
'    AppName As String
'    AppMode As String
'    AppExpires As Variant
'    AppSerial As String
'    Workstations As Integer
'    ClientName As String
'    ClientCity As String
'    ClientProv As String
'    ClientSite As String
'    SiteLat As Double
'    SiteLong As Double
'End Type
'
Public Const INIPATH = "Q:\Tritech\VisiCAD\Data\System"
Public Const C_INIPATH = "C:\Tritech\VisiCAD\Data\System"
Public Const Q_INIPATH = "Q:\Tritech\VisiCAD\Data\System"

Public Const K_FLX_ROLL = 0
Public Const K_FLX_ID = 1
Public Const K_FLX_DateTime = 2
Public Const K_FLX_Address = 3
Public Const K_FLX_City = 4
Public Const K_FLX_Quad = 5
Public Const K_FLX_Problem = 6
Public Const K_FLX_Unit = 7
Public Const K_FLX_UnitType = 8
Public Const K_FLX_CallTaker = 9
Public Const K_FLX_Destination = 10
Public Const K_FLX_Assigned = 11
Public Const K_FLX_Enroute = 12
Public Const K_FLX_AtScene = 13
Public Const K_FLX_Transport = 14
Public Const K_FLX_AtDest = 15
Public Const K_FLX_AvailAtDest = 16
Public Const K_FLX_Available = 17
Public Const K_FLX_Latitude = 18
Public Const K_FLX_Longitude = 19
Public Const K_FLX_ForeColor = 20
Public Const K_FLX_BackColor = 21

Public KeyInfo As tLicenseInfo                  '   all info relating to the license

Public gSystemDirectory As String   '   Not used until 2022-11-12. This global contains the path to the system settings. With the release of Central Square Enterprise CAD v22.1.5, Q: share went away.
Public gSimScriptDir As String
Public gSoundByteDir As String

Public gLatitudeAdjust As Double
Public gLongitudeAdjust As Double
Public gOffsetFile As String

Public gUseESRIResources As Boolean
Public gESRINetworkDataset As String
Public gESRINetworkCost As String
Public gAVLUpdateLimit As Integer
Public gUseOneWayRestrictions As Boolean
Public gUseShareRoutingFolder As Boolean
Public gSharedRoutingFolder As String

Public gDetailedDebugging As Boolean
Public gTraceLogging As Boolean

Public gLoadTestMode As Boolean
Public gLoadTestEnabled As Boolean

Public Type RtPoint
    Distance As Double
    Time As Double
    AccumDist As Double
    AccumTime As Double
    Lat As Double
    Lon As Double
End Type

Public Type tRoute
    Route() As RtPoint
    RoutePoints As Integer
    TotalDistance As Double
    TotalTime As Double
End Type

Public Type Vehicle
    UnitName As Variant
    VehID As Long
    AVLID As Long
    MDTID As Long
    RMIID As Long
    RVAID As Long                       '   not used
    AVLEnabled As Boolean
    CurrentLat As Double
    CurrentLon As Double
    CurrentLoc As String
    CurrentCity As String
    SceneLoc As String
    SceneCity As String
    SceneLat As Double
    SceneLon As Double
    DestinationLoc As String
    DestinationCity As String
    DestinationCode As String
    DestinationLat As Double
    DestinationLon As Double
    Status As Integer
    LastStatus As Integer
    '   MapLocation As MapPointCtl.Location
    Route() As RtPoint
    RouteSegments As Integer
    QueueRow As Long
    Station As String
    StationLat As Double
    StationLon As Double
    CurrentDivision As String
    ShapeID As Long
    ResourceType As String
    TXApproved As Boolean
    StatusChangeTime As Date
    NextStatus As Integer
    EndStatus As Integer
    UpdatePending As Boolean
End Type

Private Sub Main()
    
    On Error GoTo StartupDAMMIT
    
    Dim bContinue As Boolean
    Dim aCommandArgs() As String
    Dim nArgc As Integer
    Dim aArgv() As String
    Dim aArg() As String
    Dim Test As Boolean
    Dim cShareLoc As String
    Dim n As Integer
    
    bContinue = False
    
    Call GetCommandlineArgs(Command$, nArgc, aArgv)
    
    If nArgc >= 0 Then                '   there are commandline arguments
        aArg = Split(aArgv(0), "=")
        If Trim(UCase(aArg(0))) = "SHARE" Then
            cShareLoc = Trim(UCase(aArg(1)))
            If cShareLoc = "Q:" Then
                gSystemDirectory = Q_INIPATH
            ElseIf cShareLoc = "C:" Then
                gSystemDirectory = C_INIPATH
            Else                                        '   incorrect parameter specified on command line
                Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP (1)")
            End If
        Else                                            '   incorrect parameter specified on command line
            Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP (2)")
        End If
    Else
        gSystemDirectory = Q_INIPATH                    '   default to Q: share
    End If
    
    If Dir(gSystemDirectory & "\System.INI") <> "" Then
        bContinue = True
    Else                                            '   incorrect parameter specified on command line
        Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP (3)")
    End If
    
    If bContinue Then               '   we're good, so let's go!
        Test = bsiPutSettings("MAIN", "SHARE", cShareLoc, App.Path & "\Share.INI")       '   save the SHARE location for the Simulator AVL/MDT Interface startup functions
        Load frmMain
        Load frmSplash
    End If
    
    Exit Sub
    
StartupDAMMIT:
    Call MsgBox("TriTech directory structure location missing." & vbCrLf & "Specify SHARE=Q: or SHARE=C: as a command line parameter." & vbCrLf & "Simulator cannot load. Exiting.", vbCritical + vbOKOnly, "CRITICAL ERROR on STARTUP (0)")
End Sub

Private Sub GetCommandlineArgs(cCommandline As String, ByRef nArgc As Integer, ByRef aArgv() As String)
    Dim aParameters() As String
    Dim n As Integer
    
    aParameters = Split(cCommandline, " ")
    
    nArgc = 0
    
    For n = 0 To UBound(aParameters, 1)
        If Len(aParameters(n)) > 0 Then
            ReDim Preserve aArgv(0 To nArgc)
            aArgv(nArgc) = aParameters(n)
            nArgc = nArgc + 1
        End If
    Next n
    
End Sub
