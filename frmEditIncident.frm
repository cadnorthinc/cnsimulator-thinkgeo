VERSION 5.00
Begin VB.Form frmEditIncident 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   1830
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   6990
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1830
   ScaleWidth      =   6990
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdAddIncSpeech 
      Caption         =   "RadioEvents"
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   1320
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancelIncEdits 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   4560
      TabIndex        =   2
      Top             =   1320
      Width           =   1095
   End
   Begin VB.CommandButton cmdSaveIncEdits 
      Caption         =   "&Save"
      Height          =   375
      Left            =   5760
      TabIndex        =   1
      Top             =   1320
      Width           =   1095
   End
   Begin VB.TextBox txtEditIncidentTime 
      Height          =   285
      Left            =   840
      TabIndex        =   0
      Text            =   "txtEditIncidentTime"
      Top             =   1320
      Width           =   1815
   End
   Begin VB.Label lblIncProblem 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblIncProblem"
      Height          =   285
      Left            =   2880
      TabIndex        =   14
      Top             =   840
      Width           =   3975
   End
   Begin VB.Label lblIncDivision 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblIncDivision"
      Height          =   285
      Left            =   5520
      TabIndex        =   13
      Top             =   480
      Width           =   1335
   End
   Begin VB.Label lblIncCity 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblIncCity"
      Height          =   285
      Left            =   2880
      TabIndex        =   12
      Top             =   480
      Width           =   1455
   End
   Begin VB.Label lblIncAddress 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblIncAddress"
      Height          =   285
      Left            =   2880
      TabIndex        =   11
      Top             =   120
      Width           =   3975
   End
   Begin VB.Label lblIncID 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblIncID"
      Height          =   285
      Left            =   840
      TabIndex        =   10
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Label7 
      Caption         =   "Division:"
      Height          =   255
      Left            =   4680
      TabIndex        =   9
      Top             =   480
      Width           =   735
   End
   Begin VB.Label Label6 
      Caption         =   "City:"
      Height          =   255
      Left            =   2160
      TabIndex        =   8
      Top             =   480
      Width           =   735
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   6840
      Y1              =   1200
      Y2              =   1200
   End
   Begin VB.Label Label5 
      Caption         =   "InQueue:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1320
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "Problem:"
      Height          =   255
      Left            =   2160
      TabIndex        =   6
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Address:"
      Height          =   255
      Left            =   2160
      TabIndex        =   5
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Inc ID:"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "frmEditIncident"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAddIncSpeech_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdAddIncSpeech_Click of frmEditIncident")
    End If
    Call MsgBox("You will be able to add Incident Speech Events in this module", vbOKOnly + vbInformation, "Under Construction")
End Sub

Private Sub cmdCancelIncEdits_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdCancelIncEdits_Click of frmEditIncident")
    End If
    frmEditIncident.Hide
End Sub

Private Sub cmdSaveIncEdits_Click()
    Dim r As Integer
    Dim c As Integer
    Dim fc As Long
    Dim bc As Long
    Dim IncStr As String
    Dim aInc As Variant
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSaveIncEdits_Click of frmEditIncident")
    End If
    
    r = frmMain.flxBrowse.Row
    
    IncStr = frmMain.flxBrowse.TextMatrix(r, K_FLX_ID)
    For c = 2 To frmMain.flxBrowse.Cols - 1
        IncStr = IncStr & "|" & frmMain.flxBrowse.TextMatrix(r, c)
    Next c
    
    frmMain.flxBrowse.Col = K_FLX_ID
    fc = frmMain.flxBrowse.CellForeColor
    bc = frmMain.flxBrowse.CellBackColor
    
    frmMain.flxBrowse.RemoveItem frmMain.flxBrowse.Row
    
    aInc = Split(IncStr, "|")
    
    For r = 1 To frmMain.flxBrowse.Rows - 1
        If frmMain.flxBrowse.TextMatrix(r, K_FLX_DateTime) > txtEditIncidentTime.Text Then
            Exit For
        End If
    Next r
    
    frmMain.flxBrowse.AddItem " ", r
    frmMain.flxBrowse.TextMatrix(r, K_FLX_DateTime) = txtEditIncidentTime.Text
    frmMain.flxBrowse.TextMatrix(r, K_FLX_ID) = aInc(0)
    
    For c = 3 To frmMain.flxBrowse.Cols - 1
        frmMain.flxBrowse.TextMatrix(r, c) = aInc(c - 1)
    Next c
'
'
'    frmMain.flxBrowse.TextMatrix(r, 3) = cmbSoundChannel.Text
'    frmMain.flxBrowse.Row = r
'    frmMain.flxBrowse.Col = 0
'    frmMain.flxBrowse.RowSel = frmMain.flxBrowse.Row
'    frmMain.flxBrowse.ColSel = frmMain.flxBrowse.Cols - 1
'    frmMain.flxBrowse.CellAlignment = flexAlignRightCenter

    frmMain.flxBrowse.Row = r
    For c = 1 To frmMain.flxBrowse.Cols - 1
        frmMain.flxBrowse.Col = c
        frmMain.flxBrowse.CellForeColor = fc
        frmMain.flxBrowse.CellBackColor = bc
    Next c
    
    frmEditIncident.Hide
End Sub
