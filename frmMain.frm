VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{C368D713-CC5F-40ED-9F53-F84FE197B96A}#4.7#0"; "MapWinGIS.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Simulation Manager"
   ClientHeight    =   9495
   ClientLeft      =   150
   ClientTop       =   780
   ClientWidth     =   16635
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   9495
   ScaleWidth      =   16635
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fmFilter 
      Caption         =   "Filters"
      Height          =   4395
      Left            =   9480
      TabIndex        =   197
      Top             =   540
      Width           =   3375
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clear List"
         Height          =   315
         Left            =   1980
         TabIndex        =   208
         Top             =   3840
         Width           =   915
      End
      Begin VB.TextBox txtTo 
         Height          =   315
         Left            =   1800
         TabIndex        =   200
         Top             =   660
         Width           =   615
      End
      Begin VB.TextBox txtFrom 
         Height          =   315
         Left            =   840
         TabIndex        =   199
         Top             =   660
         Width           =   615
      End
      Begin VB.CommandButton cmdIncMap 
         Caption         =   "Map"
         Height          =   315
         Left            =   1320
         TabIndex        =   207
         Top             =   3840
         Width           =   615
      End
      Begin VB.CommandButton cmdShow 
         Caption         =   "Show Calls"
         Height          =   315
         Left            =   60
         TabIndex        =   206
         Top             =   3840
         Width           =   1215
      End
      Begin VB.ListBox lstDivision 
         Height          =   645
         Left            =   60
         MultiSelect     =   2  'Extended
         TabIndex        =   205
         Top             =   3120
         Width           =   2835
      End
      Begin VB.ListBox lstJurisdiction 
         Height          =   840
         Left            =   60
         MultiSelect     =   2  'Extended
         TabIndex        =   203
         Top             =   1860
         Width           =   2835
      End
      Begin VB.CheckBox chkDivision 
         Caption         =   "Include All Divisions"
         Height          =   195
         Left            =   60
         TabIndex        =   204
         Top             =   2880
         Width           =   2835
      End
      Begin VB.CheckBox chkJurisdiction 
         Caption         =   "Include All Jurisdictions"
         Height          =   195
         Left            =   60
         TabIndex        =   202
         Top             =   1620
         Width           =   2835
      End
      Begin VB.ComboBox cmbAgency 
         Height          =   315
         Left            =   60
         TabIndex        =   201
         Text            =   "cmbAgency"
         Top             =   1260
         Width           =   2835
      End
      Begin MSComCtl2.DTPicker dtBrowse 
         Height          =   315
         Left            =   1080
         TabIndex        =   198
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   147587073
         CurrentDate     =   38020
      End
      Begin VB.Label Label35 
         Caption         =   "Select Date:"
         Height          =   195
         Left            =   60
         TabIndex        =   212
         Top             =   300
         Width           =   975
      End
      Begin VB.Label lblTo 
         Alignment       =   1  'Right Justify
         Caption         =   "To:"
         Height          =   255
         Left            =   1440
         TabIndex        =   211
         Top             =   720
         Width           =   255
      End
      Begin VB.Label lblFrom 
         Alignment       =   1  'Right Justify
         Caption         =   "From:"
         Height          =   255
         Left            =   300
         TabIndex        =   210
         Top             =   720
         Width           =   435
      End
      Begin VB.Label Label34 
         Caption         =   "Select Agency"
         Height          =   195
         Left            =   60
         TabIndex        =   209
         Top             =   1020
         Width           =   1395
      End
   End
   Begin VB.Frame fmCommand 
      Height          =   1515
      Left            =   12900
      TabIndex        =   190
      Top             =   3720
      Width           =   3135
      Begin VB.CommandButton cmdConnect 
         Caption         =   "Connect"
         Height          =   315
         Left            =   1560
         TabIndex        =   196
         Top             =   180
         Width           =   1395
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "E&xit"
         Height          =   315
         Left            =   60
         TabIndex        =   195
         Top             =   960
         Width           =   1395
      End
      Begin VB.CommandButton cmdSendInc 
         Caption         =   "Send Inc"
         Height          =   315
         Left            =   1500
         TabIndex        =   194
         Top             =   720
         Width           =   1395
      End
      Begin VB.CommandButton cmdScript 
         Caption         =   "Load Script"
         Height          =   315
         Left            =   1620
         TabIndex        =   193
         Top             =   1080
         Width           =   1395
      End
      Begin VB.CommandButton cmdPause 
         Caption         =   "&Pause"
         Height          =   315
         Left            =   60
         TabIndex        =   192
         Top             =   600
         Width           =   1395
      End
      Begin VB.CommandButton cmdAniAli 
         Caption         =   "ANI/ALI"
         Height          =   315
         Left            =   60
         TabIndex        =   191
         Top             =   240
         Width           =   1395
      End
   End
   Begin VB.Frame frmAniAli 
      Caption         =   "Send ANI/ALI"
      Height          =   1335
      Left            =   12900
      TabIndex        =   182
      Top             =   2280
      Width           =   3135
      Begin VB.ListBox lstAniAli 
         Height          =   255
         Left            =   120
         MultiSelect     =   2  'Extended
         Sorted          =   -1  'True
         TabIndex        =   187
         Top             =   300
         Width           =   2955
      End
      Begin VB.CommandButton cmdSendAniAli 
         Caption         =   "Send"
         Height          =   315
         Left            =   2160
         TabIndex        =   186
         Top             =   960
         Width           =   915
      End
      Begin VB.CommandButton cmdSelAll 
         Caption         =   "Select All"
         Height          =   315
         Left            =   900
         TabIndex        =   185
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton cmdSendCellular 
         Caption         =   "Cell"
         Height          =   315
         Left            =   60
         TabIndex        =   184
         Top             =   960
         Width           =   795
      End
      Begin VB.CheckBox chkANIALIForm 
         Caption         =   "Show Incident Details"
         Height          =   255
         Left            =   240
         TabIndex        =   183
         Top             =   600
         Width           =   1935
      End
   End
   Begin VB.Timer tmMapRefresh 
      Left            =   15540
      Top             =   7140
   End
   Begin VB.Timer tmClearHighlight 
      Left            =   15540
      Top             =   6660
   End
   Begin VB.Timer tmUpdateAVL 
      Left            =   16140
      Top             =   6480
   End
   Begin VB.Timer tmCADMsg 
      Left            =   16140
      Top             =   6060
   End
   Begin VB.Frame frmRadio 
      Caption         =   "Radio"
      Height          =   1695
      Left            =   12900
      TabIndex        =   175
      Top             =   540
      Width           =   3135
      Begin VB.TextBox txtTransmit 
         Height          =   375
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   179
         Text            =   "frmMain.frx":0442
         Top             =   600
         Width           =   2895
      End
      Begin VB.CommandButton cmdTransmit 
         Caption         =   "PTT"
         Height          =   315
         Left            =   2160
         TabIndex        =   178
         Top             =   1200
         Width           =   750
      End
      Begin VB.CommandButton cmdClearRadio 
         Caption         =   "Clear"
         Height          =   315
         Left            =   1320
         TabIndex        =   177
         Top             =   1200
         Width           =   750
      End
      Begin VB.CommandButton cmdWAV 
         Caption         =   "WAV"
         Height          =   315
         Left            =   480
         TabIndex        =   176
         Top             =   1200
         Width           =   750
      End
      Begin VB.Label Label24 
         Caption         =   "Selected Unit:"
         Height          =   255
         Left            =   120
         TabIndex        =   181
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label lblRadioUnit 
         Caption         =   "lblRadioUnit"
         Height          =   255
         Left            =   1320
         TabIndex        =   180
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame fmMain 
      Height          =   7755
      Left            =   8700
      TabIndex        =   0
      Top             =   540
      Width           =   3375
      Begin VB.ComboBox cmbDriveProfile 
         Height          =   315
         Left            =   1080
         TabIndex        =   6
         Text            =   "cmbDriveProfile"
         Top             =   2400
         Visible         =   0   'False
         Width           =   2175
      End
      Begin VB.ListBox lstRadio 
         Height          =   255
         Left            =   600
         TabIndex        =   4
         Top             =   2760
         Visible         =   0   'False
         Width           =   2655
      End
      Begin VB.CommandButton cmdMessage 
         Caption         =   "Clear Message"
         Height          =   315
         Left            =   1020
         TabIndex        =   2
         Top             =   1980
         Visible         =   0   'False
         Width           =   2115
      End
      Begin VB.TextBox txtRadio 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1755
         Left            =   600
         MultiLine       =   -1  'True
         TabIndex        =   1
         Text            =   "frmMain.frx":044E
         Top             =   180
         Visible         =   0   'False
         Width           =   2415
      End
      Begin VB.Label lblProfile 
         Caption         =   "Drive Profile:"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   2400
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label Label8 
         Caption         =   "Label8"
         Height          =   15
         Left            =   1560
         TabIndex        =   5
         Top             =   2880
         Width           =   135
      End
   End
   Begin MSComctlLib.StatusBar sBar 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   9120
      Width           =   16635
      _ExtentX        =   29342
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   5
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2117
            MinWidth        =   2117
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   2117
            MinWidth        =   2117
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1411
            MinWidth        =   1411
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1411
            MinWidth        =   1411
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   14111
            MinWidth        =   14111
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog dlgFile 
      Left            =   15600
      Top             =   6060
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSWinsockLib.Winsock tcpAVL 
      Left            =   16140
      Top             =   6900
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSWinsockLib.Winsock tcpSock 
      Left            =   16140
      Top             =   7320
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin TabDlg.SSTab SSTabMain 
      Height          =   8595
      Left            =   0
      TabIndex        =   8
      Top             =   60
      Width           =   9855
      _ExtentX        =   17383
      _ExtentY        =   15161
      _Version        =   393216
      Tabs            =   7
      Tab             =   4
      TabsPerRow      =   7
      TabHeight       =   520
      WordWrap        =   0   'False
      TabCaption(0)   =   "&Map"
      TabPicture(0)   =   "frmMain.frx":0452
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "fmMap"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "&Browser"
      TabPicture(1)   =   "frmMain.frx":046E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "frmBrowser"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "&Events"
      TabPicture(2)   =   "frmMain.frx":048A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "lvEvents"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "&Units"
      TabPicture(3)   =   "frmMain.frx":04A6
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "flxVehicle"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "&Setup"
      TabPicture(4)   =   "frmMain.frx":04C2
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "SetupTabs"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "&Logs"
      TabPicture(5)   =   "frmMain.frx":04DE
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "lvMain"
      Tab(5).Control(1)=   "lstDebug"
      Tab(5).Control(2)=   "lstTCP"
      Tab(5).ControlCount=   3
      TabCaption(6)   =   "Load Test"
      TabPicture(6)   =   "frmMain.frx":04FA
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "fmLoadTest"
      Tab(6).ControlCount=   1
      Begin VB.ListBox lstTCP 
         Height          =   450
         Left            =   -72720
         TabIndex        =   221
         Top             =   600
         Width           =   1335
      End
      Begin VB.Frame fmLoadTest 
         Caption         =   "Load Testing"
         Height          =   3435
         Left            =   -73920
         TabIndex        =   217
         Top             =   720
         Width           =   6675
         Begin VB.TextBox txtLoadRadius 
            Height          =   315
            Left            =   240
            TabIndex        =   223
            Text            =   "txtLoadRadius"
            Top             =   1680
            Width           =   615
         End
         Begin VB.CommandButton cmdUnitSelect 
            Caption         =   "Select All Units"
            Height          =   315
            Left            =   3240
            TabIndex        =   222
            Top             =   2520
            Width           =   1455
         End
         Begin VB.CheckBox chkLoadTest 
            Caption         =   "Simulator generates ALL LOAD (including all AVL and Status Updates)"
            Height          =   195
            Left            =   240
            TabIndex        =   219
            Top             =   2100
            Width           =   5355
         End
         Begin VB.CommandButton cmdLoadTest 
            Caption         =   "Run Load Test"
            Height          =   315
            Left            =   1500
            TabIndex        =   218
            Top             =   2580
            Width           =   1455
         End
         Begin MSComctlLib.ListView lvUnitList 
            Height          =   735
            Left            =   180
            TabIndex        =   220
            Top             =   360
            Width           =   2655
            _ExtentX        =   4683
            _ExtentY        =   1296
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin VB.Label lblLoadRadius 
            Caption         =   "Incident Selection Radius (kms)"
            Height          =   315
            Left            =   1200
            TabIndex        =   224
            Top             =   1680
            Width           =   2895
         End
      End
      Begin VB.Frame fmMap 
         Height          =   4335
         Left            =   -74400
         TabIndex        =   188
         Top             =   1500
         Width           =   6855
         Begin MapWinGIS.Map winMap1 
            Height          =   975
            Left            =   600
            TabIndex        =   215
            Top             =   1440
            Width           =   1095
            _Version        =   262151
            _ExtentX        =   1931
            _ExtentY        =   1720
            _StockProps     =   0
         End
         Begin MSComctlLib.Toolbar tbTools 
            Height          =   510
            Left            =   60
            TabIndex        =   189
            Top             =   240
            Width           =   7995
            _ExtentX        =   14102
            _ExtentY        =   900
            ButtonWidth     =   820
            ButtonHeight    =   794
            Appearance      =   1
            ImageList       =   "ImageList1"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   7
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ZoomIn"
                  Object.ToolTipText     =   "Zoom In"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ZoomOut"
                  Object.ToolTipText     =   "Zoom Out"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Pan"
                  Object.ToolTipText     =   "Pan"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ZoomFull"
                  Object.ToolTipText     =   "Zoom Full"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ZoomPrev"
                  Object.ToolTipText     =   "Previous View"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Identify"
                  Object.ToolTipText     =   "Identify"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ClearHighlight"
                  Object.ToolTipText     =   "Clear Highlight"
                  ImageIndex      =   7
               EndProperty
            EndProperty
            Begin MSComctlLib.ImageList ImageList1 
               Left            =   6300
               Top             =   0
               _ExtentX        =   1005
               _ExtentY        =   1005
               BackColor       =   -2147483643
               ImageWidth      =   24
               ImageHeight     =   24
               MaskColor       =   12632256
               _Version        =   393216
               BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
                  NumListImages   =   7
                  BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                     Picture         =   "frmMain.frx":0516
                     Key             =   "Zoom.In"
                     Object.Tag             =   "Zoom In"
                  EndProperty
                  BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                     Picture         =   "frmMain.frx":0C28
                     Key             =   "Zoom.Out"
                     Object.Tag             =   "Zoom Out"
                  EndProperty
                  BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                     Picture         =   "frmMain.frx":133A
                     Key             =   "Pan"
                     Object.Tag             =   "Pan"
                  EndProperty
                  BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                     Picture         =   "frmMain.frx":1A4C
                     Key             =   "Zoom.Full"
                     Object.Tag             =   "Zoom Full"
                  EndProperty
                  BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                     Picture         =   "frmMain.frx":215E
                     Key             =   "Zoom.Last"
                     Object.Tag             =   "Zoom Last"
                  EndProperty
                  BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                     Picture         =   "frmMain.frx":2870
                     Key             =   "Ident"
                     Object.Tag             =   "Identify"
                  EndProperty
                  BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
                     Picture         =   "frmMain.frx":2F82
                     Key             =   ""
                     Object.Tag             =   "Clear"
                  EndProperty
               EndProperty
            End
         End
      End
      Begin TabDlg.SSTab SetupTabs 
         Height          =   7395
         Left            =   0
         TabIndex        =   16
         Top             =   300
         Width           =   8595
         _ExtentX        =   15161
         _ExtentY        =   13044
         _Version        =   393216
         Tabs            =   5
         Tab             =   1
         TabsPerRow      =   5
         TabHeight       =   520
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "Configuration"
         TabPicture(0)   =   "frmMain.frx":3614
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "frmConfig"
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Unit Behavior"
         TabPicture(1)   =   "frmMain.frx":3630
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "frmSpdProfile"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "frmUnitBehavior"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).ControlCount=   2
         TabCaption(2)   =   "Workstations"
         TabPicture(2)   =   "frmMain.frx":364C
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "frmWorkstations"
         Tab(2).Control(1)=   "frmAniAliSetup"
         Tab(2).ControlCount=   2
         TabCaption(3)   =   "Radio Setup"
         TabPicture(3)   =   "frmMain.frx":3668
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "Label22"
         Tab(3).Control(1)=   "Label23"
         Tab(3).Control(2)=   "Label26"
         Tab(3).Control(3)=   "Label27"
         Tab(3).Control(4)=   "Label28"
         Tab(3).Control(5)=   "Label30"
         Tab(3).Control(6)=   "fmSector"
         Tab(3).Control(7)=   "cmbRadioAgency"
         Tab(3).Control(8)=   "lvRadioChannel"
         Tab(3).Control(9)=   "cmbChannelPick"
         Tab(3).Control(10)=   "cmdRadioSave"
         Tab(3).Control(11)=   "cmdRadioReset"
         Tab(3).Control(12)=   "txtInterMessage"
         Tab(3).Control(13)=   "udInterMessage"
         Tab(3).Control(14)=   "txtSpeechRate"
         Tab(3).Control(15)=   "udSpeechRate"
         Tab(3).Control(16)=   "txtChannelName"
         Tab(3).Control(17)=   "cmdSpeechConfig"
         Tab(3).Control(18)=   "txtControlName"
         Tab(3).Control(19)=   "chkSkipLeadingAlpha"
         Tab(3).ControlCount=   20
         TabCaption(4)   =   "GIS Setup"
         TabPicture(4)   =   "frmMain.frx":3684
         Tab(4).ControlEnabled=   0   'False
         Tab(4).Control(0)=   "frmESRI"
         Tab(4).ControlCount=   1
         Begin VB.CheckBox chkSkipLeadingAlpha 
            Alignment       =   1  'Right Justify
            Caption         =   "Do not include leading alpha characters in spoken radio call signs"
            Height          =   195
            Left            =   -72900
            TabIndex        =   214
            Top             =   5940
            Width           =   4995
         End
         Begin VB.TextBox txtControlName 
            Height          =   315
            Left            =   -69480
            TabIndex        =   158
            Text            =   "txtControlName"
            Top             =   4800
            Width           =   1575
         End
         Begin VB.Frame frmUnitBehavior 
            Caption         =   "Behavior When Clear"
            Height          =   1695
            Left            =   120
            TabIndex        =   154
            Top             =   3060
            Width           =   7875
            Begin VB.CheckBox chkUnitsSetAvailable 
               Caption         =   "Units can set themselves Available from an incident"
               Height          =   195
               Left            =   300
               TabIndex        =   225
               Top             =   1020
               Width           =   4875
            End
            Begin VB.CommandButton cmdSaveClear 
               Caption         =   "Save"
               Height          =   315
               Left            =   6600
               TabIndex        =   157
               Top             =   1320
               Width           =   1215
            End
            Begin VB.OptionButton opAwaitInstructions 
               Caption         =   "Wait for instructions from Dispatcher/Available for re-assignment"
               Height          =   195
               Left            =   300
               TabIndex        =   156
               Top             =   660
               Width           =   5835
            End
            Begin VB.OptionButton opReturnToBase 
               Caption         =   "Return to Station/Base"
               Height          =   255
               Left            =   300
               TabIndex        =   155
               Top             =   300
               Width           =   2535
            End
         End
         Begin VB.CommandButton cmdSpeechConfig 
            Caption         =   "Speech Configuration"
            Height          =   315
            Left            =   -73320
            TabIndex        =   147
            Top             =   6240
            Width           =   2055
         End
         Begin VB.TextBox txtChannelName 
            Height          =   285
            Left            =   -69480
            TabIndex        =   133
            Text            =   "txtChannelName"
            Top             =   4440
            Width           =   1575
         End
         Begin VB.Frame frmESRI 
            Height          =   6195
            Left            =   -74760
            TabIndex        =   145
            Top             =   480
            Width           =   8055
            Begin VB.DirListBox dirSharedFolder 
               Height          =   1440
               Left            =   1860
               TabIndex        =   229
               Top             =   2040
               Width           =   3315
            End
            Begin VB.DriveListBox drvSharedFolder 
               Height          =   315
               Left            =   1860
               TabIndex        =   228
               Top             =   1680
               Width           =   3315
            End
            Begin VB.CheckBox chkUseESRI 
               Caption         =   "chkUseESRI"
               Height          =   255
               Left            =   1860
               TabIndex        =   227
               Top             =   180
               Visible         =   0   'False
               Width           =   3555
            End
            Begin VB.TextBox txtRoutingSharedFolder 
               Height          =   315
               Left            =   1860
               TabIndex        =   226
               Text            =   "txtRoutingSharedFolder"
               Top             =   1260
               Width           =   5655
            End
            Begin VB.CheckBox chkEngineVisible 
               Caption         =   "Routing Engine Visible"
               Height          =   195
               Left            =   5400
               TabIndex        =   213
               Top             =   5760
               Visible         =   0   'False
               Width           =   2475
            End
            Begin VB.CheckBox chkUseOneWay 
               Caption         =   "Use OneWay Restrictions"
               Height          =   255
               Left            =   5400
               TabIndex        =   174
               Top             =   5460
               Visible         =   0   'False
               Width           =   2295
            End
            Begin VB.ComboBox cmbCostAttribute 
               Height          =   315
               Left            =   5400
               TabIndex        =   171
               Text            =   "cmbCostAttribute"
               Top             =   5040
               Visible         =   0   'False
               Width           =   1995
            End
            Begin VB.TextBox txtAVLUpdateLimit 
               Height          =   315
               Left            =   5400
               TabIndex        =   168
               Text            =   "txtAVLUpdateLimit"
               Top             =   4620
               Visible         =   0   'False
               Width           =   495
            End
            Begin VB.CheckBox chkUseSharedFolder 
               Caption         =   "Use Shared Folder for Calculated Route Transfer (non-IPC-based)"
               Height          =   315
               Left            =   1860
               TabIndex        =   167
               Top             =   900
               Width           =   5175
            End
            Begin VB.CommandButton cmdSaveESRI 
               Caption         =   "Save"
               Height          =   315
               Left            =   6780
               TabIndex        =   166
               Top             =   3240
               Width           =   1155
            End
            Begin VB.CommandButton cmdESRIBrowse 
               Caption         =   "..."
               Height          =   315
               Left            =   7560
               TabIndex        =   164
               Top             =   540
               Width           =   315
            End
            Begin VB.TextBox txtESRINetworkDataset 
               Height          =   315
               Left            =   1860
               TabIndex        =   163
               Text            =   "txtESRINetworkDataset"
               Top             =   540
               Width           =   5655
            End
            Begin VB.Label Label33 
               Caption         =   "Network Cost Attribute:"
               Height          =   195
               Left            =   3660
               TabIndex        =   172
               Top             =   5100
               Visible         =   0   'False
               Width           =   1695
            End
            Begin VB.Label Label32 
               Caption         =   "(meters)"
               Height          =   195
               Left            =   5940
               TabIndex        =   170
               Top             =   4680
               Visible         =   0   'False
               Width           =   615
            End
            Begin VB.Label Label31 
               Caption         =   "AVL Update Limit:"
               Height          =   195
               Left            =   3660
               TabIndex        =   169
               Top             =   4680
               Visible         =   0   'False
               Width           =   1275
            End
            Begin VB.Label Label3 
               Caption         =   "Shapefile Name:"
               Height          =   195
               Left            =   120
               TabIndex        =   165
               Top             =   600
               Width           =   1275
            End
         End
         Begin MSComCtl2.UpDown udSpeechRate 
            Height          =   285
            Left            =   -68160
            TabIndex        =   144
            Top             =   5580
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   503
            _Version        =   393216
            Value           =   2
            BuddyControl    =   "txtSpeechRate"
            BuddyDispid     =   196689
            OrigLeft        =   6600
            OrigTop         =   4800
            OrigRight       =   6855
            OrigBottom      =   5055
            Min             =   -10
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtSpeechRate 
            Height          =   285
            Left            =   -68520
            TabIndex        =   143
            Text            =   "txtSpeechRate"
            Top             =   5580
            Width           =   375
         End
         Begin MSComCtl2.UpDown udInterMessage 
            Height          =   285
            Left            =   -68160
            TabIndex        =   142
            Top             =   5220
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   503
            _Version        =   393216
            Value           =   2
            BuddyControl    =   "txtInterMessage"
            BuddyDispid     =   196690
            OrigLeft        =   6600
            OrigTop         =   4440
            OrigRight       =   6855
            OrigBottom      =   4695
            SyncBuddy       =   -1  'True
            BuddyProperty   =   65547
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtInterMessage 
            Height          =   285
            Left            =   -68520
            TabIndex        =   141
            Text            =   "txtInterMessage"
            Top             =   5220
            Width           =   375
         End
         Begin VB.CommandButton cmdRadioReset 
            Caption         =   "Reset"
            Height          =   315
            Left            =   -69060
            TabIndex        =   135
            Top             =   6240
            Width           =   1155
         End
         Begin VB.CommandButton cmdRadioSave 
            Caption         =   "&Save"
            Height          =   315
            Left            =   -70320
            TabIndex        =   134
            Top             =   6240
            Width           =   1155
         End
         Begin VB.ComboBox cmbChannelPick 
            Height          =   315
            ItemData        =   "frmMain.frx":36A0
            Left            =   -72000
            List            =   "frmMain.frx":36A2
            TabIndex        =   132
            Text            =   "cmbChannelPick"
            Top             =   4440
            Width           =   735
         End
         Begin MSComctlLib.ListView lvRadioChannel 
            Height          =   2655
            Left            =   -73320
            TabIndex        =   131
            Top             =   1680
            Width           =   5535
            _ExtentX        =   9763
            _ExtentY        =   4683
            LabelWrap       =   -1  'True
            HideSelection   =   -1  'True
            _Version        =   393217
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BorderStyle     =   1
            Appearance      =   1
            NumItems        =   0
         End
         Begin VB.ComboBox cmbRadioAgency 
            Height          =   315
            Left            =   -73320
            TabIndex        =   129
            Text            =   "cmbRadioAgency"
            Top             =   1320
            Width           =   2355
         End
         Begin VB.Frame fmSector 
            Caption         =   "Assign Channels by"
            Height          =   855
            Left            =   -74880
            TabIndex        =   126
            Top             =   360
            Width           =   1815
            Begin VB.OptionButton opSector 
               Caption         =   "Sector"
               Height          =   255
               Left            =   240
               TabIndex        =   128
               Top             =   480
               Width           =   1215
            End
            Begin VB.OptionButton opDivision 
               Caption         =   "Division"
               Height          =   255
               Left            =   240
               TabIndex        =   127
               Top             =   240
               Width           =   1215
            End
         End
         Begin VB.Frame frmAniAliSetup 
            Caption         =   "ANI/ALI Setup"
            Height          =   1935
            Left            =   -74880
            TabIndex        =   114
            Top             =   3060
            Width           =   7815
            Begin VB.TextBox txtANIALIWorkstation 
               Height          =   285
               Left            =   5520
               TabIndex        =   149
               Text            =   "txtANIALIWorkstation"
               Top             =   540
               Width           =   2115
            End
            Begin VB.CheckBox chkSendANIALI 
               Caption         =   "Send ANIALI before Scripted Incidents"
               Height          =   195
               Left            =   4500
               TabIndex        =   148
               Top             =   300
               Width           =   3135
            End
            Begin VB.CommandButton cmdANIALIReset 
               Caption         =   "Reset"
               Height          =   315
               Left            =   5280
               TabIndex        =   124
               Top             =   1500
               Width           =   1155
            End
            Begin VB.CommandButton cmdANIALISave 
               Caption         =   "Save"
               Height          =   315
               Left            =   6480
               TabIndex        =   123
               Top             =   1500
               Width           =   1155
            End
            Begin VB.TextBox txtCellNumber 
               Height          =   315
               Left            =   2100
               TabIndex        =   122
               Top             =   1380
               Width           =   1935
            End
            Begin VB.TextBox txtCellAddress 
               Height          =   315
               Left            =   2100
               TabIndex        =   121
               Top             =   960
               Width           =   2715
            End
            Begin VB.TextBox txtCellClass 
               Height          =   315
               Left            =   6660
               TabIndex        =   120
               Top             =   960
               Width           =   555
            End
            Begin VB.OptionButton opAniAli2Form 
               Caption         =   "Load ANI/ALI Info into ECT Form"
               Height          =   195
               Left            =   180
               TabIndex        =   116
               Top             =   600
               Width           =   2835
            End
            Begin VB.OptionButton opAniAliDialog 
               Caption         =   "Display ANI/ALI Accept-Decline Dialog"
               Height          =   195
               Left            =   180
               TabIndex        =   115
               Top             =   300
               Width           =   3555
            End
            Begin VB.Label lblANIALIWS 
               Caption         =   "Workstation:"
               Height          =   195
               Left            =   4500
               TabIndex        =   150
               Top             =   600
               Width           =   915
            End
            Begin VB.Label Label21 
               Caption         =   "Cellular Provider Number:"
               Height          =   195
               Left            =   240
               TabIndex        =   119
               Top             =   1440
               Width           =   1875
            End
            Begin VB.Label Label20 
               Caption         =   "Cellular Service Class:"
               Height          =   195
               Left            =   5040
               TabIndex        =   118
               Top             =   1020
               Width           =   1755
            End
            Begin VB.Label Label19 
               Caption         =   "Cellular ALI Address:"
               Height          =   195
               Left            =   240
               TabIndex        =   117
               Top             =   1020
               Width           =   1515
            End
         End
         Begin VB.Frame frmWorkstations 
            Caption         =   "Manage Workstations"
            Height          =   2355
            Left            =   -74880
            TabIndex        =   92
            Top             =   660
            Width           =   7815
            Begin VB.TextBox txtWSName 
               Height          =   315
               Left            =   1320
               TabIndex        =   96
               Text            =   "txtWSName"
               Top             =   1200
               Width           =   3075
            End
            Begin VB.CommandButton cmdWSSave 
               Caption         =   "Save"
               Height          =   315
               Left            =   3900
               TabIndex        =   98
               Top             =   1620
               Width           =   1155
            End
            Begin VB.CommandButton cmdWSDelete 
               Caption         =   "Delete"
               Height          =   315
               Left            =   2700
               TabIndex        =   95
               Top             =   1620
               Width           =   1155
            End
            Begin VB.CommandButton cmdWSCancel 
               Caption         =   "Cancel"
               Height          =   315
               Left            =   1500
               TabIndex        =   99
               Top             =   1620
               Width           =   1155
            End
            Begin VB.CommandButton cmdWSAdd 
               Caption         =   "Add WS"
               Height          =   315
               Left            =   300
               TabIndex        =   94
               Top             =   1620
               Width           =   1155
            End
            Begin VB.ListBox lstWorkstations 
               Height          =   1620
               Left            =   5280
               Sorted          =   -1  'True
               TabIndex        =   93
               Top             =   240
               Width           =   2415
            End
            Begin VB.Label ibiWSName 
               Caption         =   "Workstation:"
               Height          =   255
               Left            =   240
               TabIndex        =   97
               Top             =   1260
               Width           =   1095
            End
         End
         Begin VB.Frame frmSpdProfile 
            Caption         =   "Speed Profiles"
            Height          =   2655
            Left            =   120
            TabIndex        =   71
            Top             =   360
            Width           =   7875
            Begin VB.OptionButton opMiles 
               Caption         =   "Miles"
               Height          =   195
               Left            =   2880
               TabIndex        =   84
               Top             =   720
               Width           =   1095
            End
            Begin VB.OptionButton opKMs 
               Caption         =   "Kilometers"
               Height          =   195
               Left            =   2880
               TabIndex        =   83
               Top             =   420
               Width           =   1515
            End
            Begin VB.TextBox txtProfile 
               Height          =   285
               Left            =   1680
               TabIndex        =   82
               Text            =   "Profile Name"
               Top             =   2160
               Width           =   2175
            End
            Begin VB.TextBox txtStreet 
               Height          =   285
               Left            =   1680
               TabIndex        =   81
               Text            =   "Street"
               Top             =   1800
               Width           =   735
            End
            Begin VB.TextBox txtArterial 
               Height          =   285
               Left            =   1680
               TabIndex        =   80
               Text            =   "Arterial"
               Top             =   1440
               Width           =   735
            End
            Begin VB.TextBox txtHighway 
               Height          =   285
               Left            =   1680
               TabIndex        =   79
               Text            =   "Other"
               Top             =   1080
               Width           =   735
            End
            Begin VB.TextBox txtLimAccess 
               Height          =   285
               Left            =   1680
               TabIndex        =   78
               Text            =   "LimAccess"
               Top             =   720
               Width           =   735
            End
            Begin VB.TextBox txtInterstate 
               Height          =   285
               Left            =   1680
               TabIndex        =   77
               Text            =   "Interstate"
               Top             =   360
               Width           =   735
            End
            Begin VB.ListBox lstProfiles 
               Height          =   1620
               Left            =   4800
               TabIndex        =   76
               Top             =   480
               Width           =   3015
            End
            Begin VB.CommandButton cmdNewProfile 
               Caption         =   "Add New"
               Height          =   315
               Left            =   4440
               TabIndex        =   75
               Top             =   2280
               Width           =   1095
            End
            Begin VB.CommandButton cmdCancelNew 
               Caption         =   "Cancel"
               Height          =   315
               Left            =   3600
               TabIndex        =   74
               Top             =   1440
               Width           =   1095
            End
            Begin VB.CommandButton cmdDelProfile 
               Caption         =   "Delete"
               Height          =   315
               Left            =   5520
               TabIndex        =   73
               Top             =   2280
               Width           =   1095
            End
            Begin VB.CommandButton cmdSaveProfile 
               Caption         =   "Save Profile"
               Height          =   315
               Left            =   6600
               TabIndex        =   72
               Top             =   2280
               Width           =   1215
            End
            Begin VB.Label lblProfileList 
               Caption         =   "Profile List"
               Height          =   255
               Left            =   4800
               TabIndex        =   91
               Top             =   240
               Width           =   1335
            End
            Begin VB.Label Label7 
               Caption         =   "Profile Name:"
               Height          =   255
               Left            =   120
               TabIndex        =   90
               Top             =   2160
               Width           =   1935
            End
            Begin VB.Label Label6 
               Caption         =   "Streets:"
               Height          =   255
               Left            =   120
               TabIndex        =   89
               Top             =   1800
               Width           =   1935
            End
            Begin VB.Label Label5 
               Caption         =   "Arterial Roads:"
               Height          =   255
               Left            =   120
               TabIndex        =   88
               Top             =   1440
               Width           =   1935
            End
            Begin VB.Label Label4 
               Caption         =   "Other Highways:"
               Height          =   255
               Left            =   120
               TabIndex        =   87
               Top             =   1080
               Width           =   1935
            End
            Begin VB.Label Label2 
               Caption         =   "Limited Access Hwys:"
               Height          =   255
               Left            =   120
               TabIndex        =   86
               Top             =   720
               Width           =   1935
            End
            Begin VB.Label Label1 
               Caption         =   "Interstate Highways:"
               Height          =   255
               Left            =   120
               TabIndex        =   85
               Top             =   360
               Width           =   1935
            End
         End
         Begin VB.Frame frmConfig 
            Caption         =   "Configuration Settings"
            Height          =   6915
            Left            =   -74880
            TabIndex        =   17
            Top             =   480
            Width           =   8055
            Begin VB.CheckBox chkForceAVL 
               Caption         =   "Force AVL on All Units"
               Height          =   195
               Left            =   4920
               TabIndex        =   162
               Top             =   720
               Width           =   2715
            End
            Begin VB.CheckBox chkForceMST 
               Caption         =   "Force MST/MDT on All Units"
               Height          =   195
               Left            =   4920
               TabIndex        =   161
               Top             =   1080
               Width           =   2715
            End
            Begin VB.CheckBox chkUseXMLPort 
               Caption         =   "Use XML IPC Port"
               Height          =   195
               Left            =   4920
               TabIndex        =   160
               Top             =   360
               Width           =   2715
            End
            Begin VB.CheckBox chkMessageBell 
               Caption         =   "Audio Alert with Radio Messages"
               Height          =   195
               Left            =   1440
               TabIndex        =   125
               Top             =   360
               Width           =   2715
            End
            Begin VB.TextBox txtNoAVStatus 
               Height          =   315
               Left            =   6240
               TabIndex        =   110
               Text            =   "txtNoAVStatus"
               Top             =   2520
               Width           =   1455
            End
            Begin VB.CheckBox chkSavePos 
               Caption         =   "Save Screen Position at Exit"
               Height          =   195
               Left            =   1440
               TabIndex        =   60
               Top             =   690
               Width           =   2715
            End
            Begin VB.CommandButton cmdSave 
               Caption         =   "Save Setup"
               Height          =   315
               Left            =   6720
               TabIndex        =   59
               Top             =   6480
               Width           =   1215
            End
            Begin VB.TextBox txtAVLDriveFactor 
               Enabled         =   0   'False
               Height          =   315
               Left            =   1440
               TabIndex        =   57
               Text            =   "txtAVLDriveFactor"
               Top             =   2520
               Width           =   1455
            End
            Begin VB.TextBox txtAVLSegments 
               Height          =   315
               Left            =   1440
               TabIndex        =   56
               Text            =   "txtAVLSegments"
               Top             =   2160
               Width           =   1455
            End
            Begin VB.TextBox txtAVLPort 
               Height          =   315
               Left            =   1440
               TabIndex        =   55
               Text            =   "txtAVLPort"
               Top             =   1800
               Width           =   1455
            End
            Begin VB.TextBox txtAVLPath 
               Height          =   315
               Left            =   1440
               TabIndex        =   58
               Text            =   "txtAVLPath"
               Top             =   2880
               Width           =   6255
            End
            Begin VB.TextBox txtAVLIP 
               Height          =   315
               Left            =   1440
               TabIndex        =   54
               Text            =   "txtAVLIP"
               Top             =   1440
               Width           =   1455
            End
            Begin VB.TextBox txtIPCServer 
               Height          =   315
               Left            =   1440
               TabIndex        =   53
               Text            =   "txtIPCServer"
               Top             =   1080
               Width           =   1455
            End
            Begin VB.CommandButton cmdDiscard 
               Caption         =   "Reset"
               Height          =   315
               Left            =   5520
               TabIndex        =   52
               Top             =   6480
               Width           =   1215
            End
            Begin VB.Frame frmDestination 
               Caption         =   "Destination Choice"
               Height          =   1035
               Left            =   4020
               TabIndex        =   43
               Top             =   5400
               Width           =   3915
               Begin VB.TextBox txtDestLimit 
                  Height          =   285
                  Left            =   600
                  TabIndex        =   138
                  Text            =   "txtDestLimit"
                  Top             =   600
                  Width           =   495
               End
               Begin VB.OptionButton opReadDestination 
                  Caption         =   "Read from Source Incident"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   47
                  Top             =   240
                  Width           =   2295
               End
               Begin VB.OptionButton opCalcDestination 
                  Caption         =   "Calculate"
                  Height          =   255
                  Left            =   2640
                  TabIndex        =   46
                  Top             =   240
                  Width           =   1155
               End
               Begin VB.TextBox txtDestClose 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Left            =   3180
                  TabIndex        =   45
                  Top             =   600
                  Width           =   435
               End
               Begin VB.TextBox txtDestTrans 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Left            =   1800
                  TabIndex        =   44
                  Top             =   600
                  Width           =   435
               End
               Begin MSComCtl2.UpDown udTrans 
                  Height          =   285
                  Left            =   2220
                  TabIndex        =   48
                  Top             =   600
                  Width           =   240
                  _ExtentX        =   450
                  _ExtentY        =   503
                  _Version        =   393216
                  BuddyControl    =   "txtDestTrans"
                  BuddyDispid     =   196761
                  OrigLeft        =   2160
                  OrigTop         =   600
                  OrigRight       =   2400
                  OrigBottom      =   855
                  Max             =   100
                  SyncBuddy       =   -1  'True
                  BuddyProperty   =   65547
                  Enabled         =   -1  'True
               End
               Begin MSComCtl2.UpDown udClose 
                  Height          =   285
                  Left            =   3600
                  TabIndex        =   49
                  Top             =   600
                  Width           =   240
                  _ExtentX        =   450
                  _ExtentY        =   503
                  _Version        =   393216
                  BuddyControl    =   "txtDestClose"
                  BuddyDispid     =   196760
                  OrigLeft        =   3540
                  OrigTop         =   600
                  OrigRight       =   3780
                  OrigBottom      =   885
                  Max             =   100
                  SyncBuddy       =   -1  'True
                  BuddyProperty   =   65547
                  Enabled         =   -1  'True
               End
               Begin VB.Label Label25 
                  Caption         =   "Limit:"
                  Height          =   255
                  Left            =   120
                  TabIndex        =   137
                  Top             =   600
                  Width           =   375
               End
               Begin VB.Label lblDestClose 
                  Caption         =   "Closest%"
                  Height          =   255
                  Left            =   2520
                  TabIndex        =   51
                  Top             =   600
                  Width           =   615
               End
               Begin VB.Label lblDestTrans 
                  Caption         =   "Trans%"
                  Height          =   255
                  Left            =   1200
                  TabIndex        =   50
                  Top             =   600
                  Width           =   615
               End
            End
            Begin VB.TextBox txtTransPrio 
               Height          =   315
               Left            =   6240
               TabIndex        =   41
               Text            =   "txtTransPrio"
               Top             =   1440
               Width           =   1455
            End
            Begin VB.TextBox txtTransPro 
               Height          =   315
               Left            =   6240
               TabIndex        =   40
               Text            =   "txtTransPro"
               Top             =   1800
               Width           =   1455
            End
            Begin VB.TextBox txtClosePro 
               Height          =   315
               Left            =   6240
               TabIndex        =   39
               Text            =   "txtClosePro"
               Top             =   2160
               Width           =   1455
            End
            Begin VB.Frame frmDestTime 
               Caption         =   "Destination Time Calculations"
               Height          =   1035
               Left            =   4020
               TabIndex        =   32
               Top             =   4320
               Width           =   3915
               Begin VB.OptionButton opReadDestTime 
                  Caption         =   "Read from Source Incident"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   36
                  Top             =   300
                  Width           =   2295
               End
               Begin VB.OptionButton opCalcDestTime 
                  Caption         =   "Calculate"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   35
                  Top             =   600
                  Width           =   1155
               End
               Begin VB.TextBox txtDestTime 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Left            =   1800
                  TabIndex        =   34
                  Top             =   600
                  Width           =   435
               End
               Begin VB.TextBox txtDestRange 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Left            =   3180
                  TabIndex        =   33
                  Top             =   600
                  Width           =   435
               End
               Begin VB.Label lblDestMean 
                  Caption         =   "Mean:"
                  Height          =   255
                  Left            =   1320
                  TabIndex        =   38
                  Top             =   600
                  Width           =   495
               End
               Begin VB.Label lblDestRange 
                  Caption         =   "Range +-"
                  Height          =   255
                  Left            =   2400
                  TabIndex        =   37
                  Top             =   600
                  Width           =   735
               End
            End
            Begin VB.Frame frmScene 
               Caption         =   "On-Scene Time Calculations"
               Height          =   1035
               Left            =   60
               TabIndex        =   25
               Top             =   5400
               Width           =   3915
               Begin VB.OptionButton opReadScene 
                  Caption         =   "Read from Source Incident"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   29
                  Top             =   300
                  Width           =   3555
               End
               Begin VB.OptionButton opCalcScene 
                  Caption         =   "Calculate"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   28
                  Top             =   600
                  Width           =   1095
               End
               Begin VB.TextBox txtSceneTime 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Left            =   1800
                  TabIndex        =   27
                  Top             =   600
                  Width           =   435
               End
               Begin VB.TextBox txtSceneRange 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Left            =   3240
                  TabIndex        =   26
                  Top             =   600
                  Width           =   435
               End
               Begin VB.Label lblSceneRange 
                  Caption         =   "Range +-"
                  Height          =   195
                  Left            =   2520
                  TabIndex        =   31
                  Top             =   600
                  Width           =   975
               End
               Begin VB.Label lblSceneMean 
                  Caption         =   "Mean:"
                  Height          =   195
                  Left            =   1320
                  TabIndex        =   30
                  Top             =   600
                  Width           =   735
               End
            End
            Begin VB.Frame frmChute 
               Caption         =   "Chute Time Calculations"
               Height          =   1035
               Left            =   60
               TabIndex        =   18
               Top             =   4320
               Width           =   3915
               Begin VB.OptionButton opReadChute 
                  Caption         =   "Read from Source Incident"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   22
                  Top             =   300
                  Width           =   3675
               End
               Begin VB.OptionButton opCalcChute 
                  Caption         =   "Calculate"
                  Height          =   195
                  Left            =   120
                  TabIndex        =   21
                  Top             =   600
                  Width           =   1035
               End
               Begin VB.TextBox txtChuteTime 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Left            =   1800
                  TabIndex        =   20
                  Top             =   600
                  Width           =   435
               End
               Begin VB.TextBox txtChuteRange 
                  Alignment       =   1  'Right Justify
                  Height          =   285
                  Left            =   3240
                  TabIndex        =   19
                  Top             =   600
                  Width           =   435
               End
               Begin VB.Label lblChuteMean 
                  Caption         =   "Mean:"
                  Height          =   255
                  Left            =   1320
                  TabIndex        =   24
                  Top             =   600
                  Width           =   735
               End
               Begin VB.Label lblChuteRange 
                  Caption         =   "Range +-"
                  Height          =   255
                  Left            =   2520
                  TabIndex        =   23
                  Top             =   600
                  Width           =   735
               End
            End
            Begin VB.Frame frmScriptOptions 
               Caption         =   "Script Options"
               Height          =   1035
               Left            =   60
               TabIndex        =   104
               Top             =   3240
               Width           =   7875
               Begin MSComctlLib.Slider sldDriveWarp 
                  Height          =   495
                  Left            =   840
                  TabIndex        =   153
                  Top             =   420
                  Width           =   2775
                  _ExtentX        =   4895
                  _ExtentY        =   873
                  _Version        =   393216
                  Min             =   5
                  Max             =   20
                  SelStart        =   10
                  TickStyle       =   1
                  Value           =   10
               End
               Begin VB.TextBox txtDriveWarp 
                  Height          =   315
                  Left            =   180
                  TabIndex        =   152
                  Text            =   "txtDriveWarp"
                  Top             =   540
                  Width           =   555
               End
               Begin VB.TextBox txtTimeWarp 
                  Alignment       =   1  'Right Justify
                  Height          =   315
                  Left            =   4380
                  TabIndex        =   109
                  Text            =   "txtTimeWarp"
                  Top             =   540
                  Width           =   555
               End
               Begin MSComctlLib.Slider sldTimeWarp 
                  Height          =   495
                  Left            =   4980
                  TabIndex        =   107
                  Top             =   420
                  Width           =   2835
                  _ExtentX        =   5001
                  _ExtentY        =   873
                  _Version        =   393216
                  Min             =   5
                  Max             =   20
                  SelStart        =   10
                  TickStyle       =   1
                  Value           =   10
               End
               Begin VB.CheckBox chkSyncStartup 
                  Caption         =   "Synchronize Unit Context at Startup"
                  Height          =   195
                  Left            =   2580
                  TabIndex        =   105
                  Top             =   180
                  Visible         =   0   'False
                  Width           =   2835
               End
               Begin VB.CheckBox chkHistory 
                  Caption         =   "Copy Historic Activity for Script"
                  Height          =   195
                  Left            =   3060
                  TabIndex        =   106
                  Top             =   180
                  Visible         =   0   'False
                  Width           =   2475
               End
               Begin VB.Label Label16 
                  Caption         =   "Script Time Compression Factor"
                  Height          =   255
                  Left            =   4380
                  TabIndex        =   108
                  Top             =   180
                  Width           =   2355
               End
               Begin VB.Label Label29 
                  Caption         =   "Drive Time Compression Factor"
                  Height          =   195
                  Left            =   240
                  TabIndex        =   151
                  Top             =   180
                  Width           =   3675
               End
            End
            Begin VB.ComboBox cmbDB 
               Height          =   315
               Left            =   1920
               Style           =   2  'Dropdown List
               TabIndex        =   112
               Top             =   4560
               Width           =   1755
            End
            Begin VB.TextBox txtHospitalTypes 
               Height          =   315
               Left            =   6420
               TabIndex        =   42
               Text            =   "txtHospitalTypes"
               Top             =   3480
               Width           =   1455
            End
            Begin VB.Label Label17 
               Caption         =   "No AVL in Status:"
               Height          =   195
               Left            =   4920
               TabIndex        =   111
               Top             =   2520
               Width           =   1335
            End
            Begin VB.Label Label13 
               Caption         =   "Speed Multiplier:"
               Enabled         =   0   'False
               Height          =   255
               Left            =   120
               TabIndex        =   70
               Top             =   2520
               Width           =   1335
            End
            Begin VB.Label Label12 
               Caption         =   "Segments:"
               Height          =   255
               Left            =   120
               TabIndex        =   69
               Top             =   2160
               Width           =   1335
            End
            Begin VB.Label Label11 
               Caption         =   "AVL Port:"
               Height          =   255
               Left            =   120
               TabIndex        =   68
               Top             =   1800
               Width           =   1335
            End
            Begin VB.Label lblAVLPath 
               Caption         =   "AVL Path:"
               Height          =   255
               Left            =   120
               TabIndex        =   67
               Top             =   2880
               Width           =   1335
            End
            Begin VB.Label lblAVLIP 
               Caption         =   "AVL Interface IP:"
               Height          =   255
               Left            =   120
               TabIndex        =   66
               Top             =   1440
               Width           =   1335
            End
            Begin VB.Label lblIPC 
               Caption         =   "IPC Server:"
               Height          =   255
               Left            =   120
               TabIndex        =   65
               Top             =   1080
               Width           =   1335
            End
            Begin VB.Label Label9 
               Caption         =   "Hospital Type IDs:"
               Height          =   255
               Left            =   5100
               TabIndex        =   64
               Top             =   3480
               Width           =   1335
            End
            Begin VB.Label Label10 
               Caption         =   "Trans. Priority:"
               Height          =   255
               Left            =   4920
               TabIndex        =   63
               Top             =   1440
               Width           =   1335
            End
            Begin VB.Label Label14 
               Caption         =   "Trans. Protocol:"
               Height          =   255
               Left            =   4920
               TabIndex        =   62
               Top             =   1800
               Width           =   1335
            End
            Begin VB.Label Label15 
               Caption         =   "Emerg. Protocol:"
               Height          =   255
               Left            =   4920
               TabIndex        =   61
               Top             =   2160
               Width           =   1335
            End
            Begin VB.Label Label18 
               Caption         =   "Data Warehouse:"
               Height          =   195
               Left            =   600
               TabIndex        =   113
               Top             =   4560
               Width           =   1335
            End
         End
         Begin VB.Label Label30 
            Caption         =   "Comm Radio Name:"
            Height          =   195
            Left            =   -70920
            TabIndex        =   159
            Top             =   4860
            Width           =   1455
         End
         Begin VB.Label Label28 
            Caption         =   "Channel Name:"
            Height          =   255
            Left            =   -70620
            TabIndex        =   146
            Top             =   4440
            Width           =   1095
         End
         Begin VB.Label Label27 
            Alignment       =   1  'Right Justify
            Caption         =   "Speech Rate:"
            Height          =   255
            Left            =   -69720
            TabIndex        =   140
            Top             =   5580
            Width           =   1095
         End
         Begin VB.Label Label26 
            Alignment       =   1  'Right Justify
            Caption         =   "Wait between messages:"
            Height          =   255
            Left            =   -70440
            TabIndex        =   139
            Top             =   5220
            Width           =   1815
         End
         Begin VB.Label Label23 
            Alignment       =   1  'Right Justify
            Caption         =   "Select Channel:"
            Height          =   255
            Left            =   -73320
            TabIndex        =   136
            Top             =   4440
            Width           =   1215
         End
         Begin VB.Label Label22 
            Alignment       =   1  'Right Justify
            Caption         =   "Agency:"
            Height          =   315
            Left            =   -74280
            TabIndex        =   130
            Top             =   1320
            Width           =   855
         End
      End
      Begin VB.Frame frmBrowser 
         Height          =   3855
         Left            =   -74880
         TabIndex        =   13
         Top             =   1500
         Width           =   7875
         Begin MapWinGIS.Map incMap 
            Height          =   855
            Left            =   1920
            TabIndex        =   216
            Top             =   1800
            Width           =   1575
            _Version        =   262151
            _ExtentX        =   2778
            _ExtentY        =   1508
            _StockProps     =   0
         End
         Begin MSComctlLib.Toolbar tbBrowse 
            Height          =   510
            Left            =   2100
            TabIndex        =   173
            Top             =   180
            Width           =   4515
            _ExtentX        =   7964
            _ExtentY        =   900
            ButtonWidth     =   820
            ButtonHeight    =   794
            Appearance      =   1
            ImageList       =   "ImageList1"
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   7
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Zoom.In"
                  Object.ToolTipText     =   "Zoom In"
                  ImageIndex      =   1
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Zoom.Out"
                  Object.ToolTipText     =   "Zoom Out"
                  ImageIndex      =   2
               EndProperty
               BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Pan"
                  Object.ToolTipText     =   "Pan"
                  ImageIndex      =   3
               EndProperty
               BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Zoom.Full"
                  Object.ToolTipText     =   "Zoom Full"
                  ImageIndex      =   4
               EndProperty
               BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Zoom.Last"
                  Object.ToolTipText     =   "Previous View"
                  ImageIndex      =   5
               EndProperty
               BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Identify"
                  Object.ToolTipText     =   "Identify"
                  ImageIndex      =   6
               EndProperty
               BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "Clear"
                  Object.ToolTipText     =   "Clear Highlight"
                  ImageIndex      =   7
               EndProperty
            EndProperty
         End
         Begin VB.TextBox txtNote 
            Height          =   315
            Left            =   1080
            TabIndex        =   102
            Text            =   "txtNote"
            Top             =   3240
            Width           =   4515
         End
         Begin VB.CommandButton cmdOpenScript 
            Caption         =   "Open Script"
            Height          =   315
            Left            =   5220
            TabIndex        =   101
            Top             =   2400
            Width           =   1215
         End
         Begin VB.CommandButton cmdSaveScript 
            Caption         =   "Save Script"
            Height          =   315
            Left            =   6480
            TabIndex        =   100
            Top             =   2400
            Width           =   1215
         End
         Begin VB.ComboBox cmbDiv 
            Height          =   315
            ItemData        =   "frmMain.frx":36A4
            Left            =   4380
            List            =   "frmMain.frx":36A6
            Style           =   2  'Dropdown List
            TabIndex        =   15
            Top             =   1740
            Visible         =   0   'False
            Width           =   1815
         End
         Begin MSFlexGridLib.MSFlexGrid flxBrowse 
            Height          =   915
            Left            =   300
            TabIndex        =   14
            Top             =   420
            Width           =   2175
            _ExtentX        =   3836
            _ExtentY        =   1614
            _Version        =   393216
         End
         Begin VB.Label lblNote 
            Caption         =   "Note:"
            Height          =   255
            Left            =   600
            TabIndex        =   103
            Top             =   3300
            Width           =   495
         End
      End
      Begin VB.ListBox lstDebug 
         Height          =   450
         ItemData        =   "frmMain.frx":36A8
         Left            =   -74880
         List            =   "frmMain.frx":36AF
         TabIndex        =   9
         Top             =   600
         Width           =   915
      End
      Begin MSComctlLib.ListView lvMain 
         Height          =   495
         Left            =   -73680
         TabIndex        =   10
         Top             =   600
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   873
         View            =   3
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Col1"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Col2"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Col3"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Col4"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Col5"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lvEvents 
         Height          =   495
         Left            =   -74940
         TabIndex        =   11
         Top             =   60
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   873
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSFlexGridLib.MSFlexGrid flxVehicle 
         Height          =   795
         Left            =   -74940
         TabIndex        =   12
         Top             =   60
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   1402
         _Version        =   393216
         SelectionMode   =   1
      End
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuLogging 
         Caption         =   "&Logging"
      End
      Begin VB.Menu mnuDetailedDebugging 
         Caption         =   "&Detailed Debugging"
      End
      Begin VB.Menu mnuEnableLoadTesting 
         Caption         =   "&Enable Load Testing"
      End
      Begin VB.Menu mnuAbout 
         Caption         =   "&About"
      End
   End
   Begin VB.Menu mnuBrowserPopUp 
      Caption         =   "BrowserPopUpMenu"
      Begin VB.Menu mnuInsertSoundByte 
         Caption         =   "Insert SoundByte"
      End
      Begin VB.Menu mnuPlaySoundByte 
         Caption         =   "Play SoundByte"
      End
      Begin VB.Menu mnuDeleteSoundByte 
         Caption         =   "Delete SoundByte"
      End
      Begin VB.Menu mnuDeleteIncident 
         Caption         =   "Delete Incident"
      End
      Begin VB.Menu mnuEditProperties 
         Caption         =   "Edit Properties"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'=====================================================================================
'
'   Application Name:           Simulator for VisiCAD(tm)
'
'   Module Name:                frmMain
'
'   Date Written:               07/31/03
'
'   Developed by:               Brimac Systems Inc.
'                               123 Geoffrey Cres.,
'                               Stouffville, ON
'
'                               CAD North Inc.
'                               12-111 Fourth Av., Suite 354,
'                               St Catharines, ON  L2S 3P5
'
'   Description:                This module does all the work.
'
'   Copyright:                  Brimac Systems Inc., 2003-2004
'                               CAD North Inc., 2005-2009
'
'   Initially Developed for:    University of Toronto,
'                               Mechanical and Industrial Engineering Department,
'                               5 King's College Road,
'                               Toronto, ON
'
'                           with the cooperation of:
'
'                               Toronto EMS
'                               4330 Dufferin Street,
'                               Toronto, ON
'
'   New Version:                cnSimulator.International
'                               -   incorporates ESRI technology to permit use in countries
'                                   that are not covered by Microsoft MapPoint
'                               06/16/2009
'
'   v2.1                        Version incorporates additional updates for large international clients (ASNSW in particular)
'                               by adding:
'                                   Filtering Browser by Agency, Jurisdiction, Division
'                                   08/09/2010
'
'   v2.2                        Breaking routing calculations into its own EXE-DLL to unlink UI from background calculations
'
'   v2.3                        Internal modifications to accommodate cnSimOp operators' console for Singapore SCDF (ACES)
'                               Internal modifications to accommodate external interface requests for routing and to setting units off duty.
'
'   v3.0                        Strip out ALL ESRI references and move to a more agnostic ThinkGeo-based implementation
'                               All routing functions are resident in the cnRoutingTG module (VS2012+RAPTOR+TG+.NET 4.5)
'
'   v3.1                        Added back in the Load Testing functionality from v2.3b40 (oops!)
'
'   v3.2                        Modification to Load Testing functionality  -   2017.04.20
'                               -   select next incidents to assign from within a predefined radius of the available unit rather than at random
'                               -   save all units that were selected for a load test and reselect them for the next load test
'
'   v3.3                        Modifications for Central Square v22.1.5 and beyond - 2022.11.12
'                               -   Q: Share depricated and all system files are now on C: of the local machine
'
'   Pending                     Changing Unit Queue Handling (off duty units are optionally filtered out) - Implemented in cnSimOp (for Singapore) and will be incorporated into Simulator
'                               in a future version (soon).
'
'=====================================================================================
Option Explicit

#Const DEMO = False

Const ButtonMargin = 60

Const STAT_OffDuty = 0
Const STAT_Avail = 1
Const STAT_AtStation = 2
Const STAT_LocalArea = 3
Const STAT_AvailOS = 4
Const STAT_OOS = 5
Const STAT_Disp = 6
Const STAT_Resp = 7
Const STAT_Enr2Post = 8
Const STAT_Staged = 9
Const STAT_OnScene = 10
Const STAT_PtContact = 11
Const STAT_Transport = 12
Const STAT_AtDest = 13
Const STAT_DelayedAvail = 14
Const STAT_AssignEval = 15
Const STAT_ShiftPend = 16
Const STAT_ATP = 17
Const STAT_MultiAssign = 18
Const STAT_D2L = 19
Const STAT_R2L = 20
Const STAT_A2L = 21
Const STAT_FREEFORMTEXT = 98
Const STAT_RADIOCHANNELCHANGE = 99

Const DATE_FORMAT = "yyyy-mm-dd hh:nn:ss"

Const EV_AVL = 1                '   Event AVL Update
Const EV_STAT = 2               '   Event STATUS Update
Const EV_INC = 3                '   Event INCIDENT Creation
Const EV_TRANS = 4              '   Event Transporting (Destination) report
Const EV_RADIO = 5              '   Event Radio Message from Unit
Const EV_E911 = 6               '   Send ANIALI Message using the RMI.ID
Const EV_INIT_OFF = 7           '   Begin Intitialization - Units Off Duty
Const EV_INIT_ON = 8            '   Step 2 Intitialization - Units ON Duty
Const EV_INIT_INC = 9           '   Step 3 Intitialization - Link Incidents and Units
Const EV_INIT_DONE = 10         '   Step 4 Intitialization Complete
Const EV_ANIALI = 11            '   Event Send ANIALI message using the ANIALI table ID
Const EV_LOADTEST_ASSIGN = 12   '   Load Test Assign Unit to Incident
Const EV_LOADTEST_AVAIL = 13    '   Load Test Set Unit to Available
Const EV_LOADTEST_POST = 14     '   Load Test Assign Unit to Home Station Post
Const EV_KILL = 9999            '   Shut down

Const ROUTE_ToScene = 1         '   which segement to calculate
Const ROUTE_ToHosp = 2          '   which segement to calculate
Const ROUTE_ToStation = 3       '   which segement to calculate
Const ROUTE_ToDestination = 4   '   which segement to calculate

Const LVUnit = 1                '   Event Queue Subitems
Const LVType = 2
Const LVDur = 3
Const LVDist = 4
Const LVMessage = 5
Const LVSort = 6

Const NP_BRIMAC_MESSAGE_CLASS = 9750        '   Brimac Message Class - top level BRIMAC Message Type differentiated by message subtype
Const NP_BSI_OLDINC_NEWINC_XREF = 1         '   Brimac Submessage Type - cross-reference old and new incidents
Const NP_BSI_INITIALIZATION_COMPLETE = 2    '   Brimac Submessage Type - Simulator initialization complete
Const NP_BSI_RADIO_MESSAGE = 3              '   Brimac Submessage Type - Simulator Radio Message
Const NP_BSI_READ_RADIO_SETTINGS = 4        '   Brimac Submessage Type - Read Radio Settings Message
Const NP_BSI_RADIO_SOUNDBYTE = 5            '   Brimac Submessage Type - Play Sound File Message
Const NP_READ_VEHICLE = 20                  '   TriTech Read Vehicle Message

Const CN_CADNORTH_MSG = 9700
Const CN_ROUTE_FILEREQUEST = "0990"
Const CN_ROUTE_REQUEST = "0991"
Const CN_ROUTE_PAYLOAD = "0992"
Const CN_ROUTE_FILENAME = "0997"
Const CN_READSETTING = "0998"
Const CN_KILL_CNROUTE = "0999"

Dim gb110Version As Boolean
Dim gbLogging As Boolean
Dim gTCPLog As Integer
Dim gDebugLog As Integer
Dim gInterfaceLog As Integer
Dim gScript As Integer
Dim gbReadingScript As Boolean
Dim gServerName As String
Dim gConnectString As String
Dim gDWConnectString As String
Dim gAVLTaskID As Double
Dim gAVLIPAddress As String
Dim gAVLPath As String
Dim gAVLPort As String
Dim gAVLSegmentFactor As Integer
Dim gAVLDriveFactor As Integer
Dim gSendComplete As Boolean
Dim gStopProcessing As Boolean
Dim gScriptTime As Variant
Dim gInitializationInProgress As Boolean
Dim gPhase2Complete As Boolean
Dim gScriptFileName As String
Dim gSavePosition As String
Dim gStartPosition As Variant
Dim gDingWithMessage As Boolean
Dim gReturnToBase As Boolean

'   Time Parameters

Dim gChuteMean As Integer
Dim gChuteRange As Integer
Dim gUseSourceSceneTime As Boolean
Dim gSceneMean As Integer
Dim gSceneRange As Integer
Dim gUseSourceDestTime As Boolean
Dim gDestMean As Integer
Dim gDestRange As Integer
Dim gUseSourceDest As Boolean
Dim gTransportPercent As Double
Dim gUseClosestPercent As Double
Dim gTransportLimit As Double
Dim gTransportTypes As Variant
Dim gHospitalTypeIDs As String
Dim gTransportPrio As String
Dim gTransportPro As String
Dim gClosestPro As String
Dim gAVLMDTAppName As String
Dim gLoadContext As Boolean
Dim gLoadHistory As Boolean
Dim gCompression As Double
Dim gDriveCompression As Double
Dim gNoAVLinStatus As String

Dim gCellularAddress As String
Dim gCellularNumber As String
Dim gCellularService As String
Dim gUseANIALIDialog As Boolean
Dim gSendANIALIWithInc As Boolean
Dim gANIALIWorkstation As String

Dim gStatusList() As Status
Dim gUnitList() As Vehicle
Dim gEvents() As EventInfo
Dim gHospitals() As CADLocation
Dim gIncidents() As CADIncident
Dim gNumIncidents As Integer

Dim gDriveTime() As tDriveProfile
Dim gCurrentDriveTime As Integer

Dim gOldIncInfo() As tOldIncInfo
Dim gPostList() As tPost

Dim gRadioBuffer(0 To 1000) As String           '   buffer radio messages
Dim ptrReceive As Integer                       '   next to receive
Dim ptrDisplay As Integer                       '   currently displayed

Dim gSpeechLocations() As Variant               '   locations translation
Dim gSpeechCallSigns() As Variant               '   callsigns translation
Dim gRadioChannels() As Variant                 '   Sector/Division channel assignments
Dim gAssignChannelBy As String                  '   Division or Sector for Channel assignments ("S" or "D")
Dim gInterMessageWait As Integer                '   Setting for all radio consoles
Dim gSpeechRate As Integer                      '   Setting for all radio consoles
Dim gControlName As String                      '   String to speak when calling comm center
Dim gSkipLeadingAlphas As Boolean               '   If true (the default), leading alpha characters in a unit name will NOT be spoken - eg: Medic ALS2 will be spoken Medic 2

Dim gPartialMessage As String                   '   holding place for partial IP messages
Dim gReceivedWhileProcessing As String          '   buffer for messages that arrive while we are processing received messages

Dim gPriority() As tPriority

Dim CADQueue(0 To 10000) As CADMessage          '   revolving CAD Message FIFO Queue big enough to not loose any messages
Dim ptrCADFree As Integer                       '   next to store
Dim ptrCADNext As Integer                       '   next to process

'   Dim oMap As MapPointCtl.map
'   Dim oIncMap As MapPointCtl.map

Dim gMapProjection As String                    '   shapefile projection
Dim gDrawingLayerHandle As Long
Dim gUnitUpdateInterval As Integer
Dim gMapRefreshRequired As Boolean
Dim gIncidentLayerHandle As Long
Dim gHighLightLayerHandle As Long

Public gLicensedVersion As Boolean              '   key to licensed functionality
Public gClientInfo As String
Public gSiteInfo As String
Public gSerialNumber As String
Public gLicenseMode As String
Public gLicenseExpires As Variant
Public gClientLogo As Integer
Public gClientLat As Double
Public gClientLong As Double
Public gClientRadius As Integer
Public gClientGeoBase As String
Public gMeridianOffset As Double
Public gMaxVoices As Integer
Public gUseXMLPort As Boolean
Public gForceAVL As Boolean
Public gForceMST As Boolean

Public gLoadTestRunning As Boolean

Private gLoadTestUnitList() As Vehicle

Public gIPCMessageFragment As String
Public gIPCProcessing As Boolean

Public gcnRoute As Object

Public gUniqueRouteID As Long
Dim gRouteHashTable() As Long
Dim gLoadTestIncident() As tLoadIncident

Private Type XMLStatus
    UnitID As Long
    FromStat As Integer
    ToStat As Integer
    CallID As Long
    StatTime As Variant
    StationID As Long
End Type
    
Private Type XMLPosition
    UnitID As Long
    CurrLat As Double
    CurrLon As Double
    CurrLoc As String
    CurrCity As String
    LastAVLUpdate As Variant
    Speed As Single
    Heading As Single
    Altitude As Long
End Type

Private Type CADMessage
    msgTime As Variant
    msgType As String
    MsgBody As String
End Type

Private Type CADLocation
    Id As Long
    Code As String
    Name As String
    Address As String
    City As String
    Lat As Double
    Lon As Double
    Distance As Double      '   working field for determinine proximity to a
    TXApproved As Boolean   '   legitimate transport destination
    '   need to add reference to destination agency type here
End Type

Private Type tMapLocation
    IncId As Long
    Address As String
    Problem As String
    Lat As Double
    Lon As Double
End Type

Private Type EventItem
    Time As Variant
    Type As String
    Details As String
    Completed As Boolean
End Type

Private Type Status
    Id As Long
    Description As String
    ForeColor As Long
    BackColor As Long
End Type

Private Type CADIncident
    SourceID As Long
    TimeStart As Variant
    IncId As Long
    UnitList() As String
    Priority As String
    PU_Address As String
    PU_Location As String
    PU_City As String
    PU_Lat As Double
    PU_Lon As Double
    DE_Address As String
    DE_Location As String
    DE_City As String
    DE_Code As String
    DE_Lat As Double
    DE_Lon As Double
    NumPats As Integer
    ETimeInQueue As Double
    ETimeChute As Variant
    ETimeOnScene As Variant
    ETimeAtDest As Variant
    ForeColor As Long
    BackColor As Long
    PreScheduledIncident As Boolean         '   prescheduled incidents behave differently when selecting a destination
End Type

Private Type tOldIncInfo
    NewID As Long
    OldID As Long
    Transported As Integer
    TransportType As String
    Destination As String
    DestAddress As String
    Cancelled As Boolean
    ClearTime As Long
    OnSceneTime As Long                     '   onscene to depart scene/cancelled in seconds
    AtDestTime As Long                      '   At Destination to Available in seconds
    TimeCancelled As Long                   '   time assigned to time cancelled in seconds
    StatusCancelled As Integer              '   0, 3, 4 - status at cancellation (not cancelled, cancelled enroute, cancelled at scene)
    NumUnitsAssigned As Integer             '   number of units assigned
    FirstUnitOnScene As String              '   radio name of the first unit to arrive
    TimeFirstUnitAssigned As Variant        '   for use when more than one unit assigned
    TimeFirstUnitOnScene As Variant         '   for use when more than one unit assigned
    TimeFirstUnitDepartScene As Variant     '   for use when more than one unit assigned
    TimeFirstUnitAtDest As Variant          '   for use when more than one unit assigned
    PreScheduledIncident As Boolean         '   prescheduled incidents behave differently when selecting a destination
    DestLatitude As Double                  '   required for prescheduled incidents
    DestLongitude As Double                 '   required for prescheduled incidents
End Type

Private Type tLoadIncident
    RMIID As Long
    Latitude As Double
    Longitude As Double
    Proximity As Double
End Type

Private Type tLoadUnit
    UnitID As Long
    Latitude As Double
    Longitude As Double
    Incident As Long
    Proximity As Double
End Type

Private Type tPost
    Code As String
    Name As String
    IsPost As Boolean
End Type

Private Type tDriveProfile
    Name As String
    SpeedScale As String
    Interstate As Integer
    Highway As Integer
    OtherHwy As Integer
    Arterial As Integer
    Street As Integer
End Type

Private Type tPriority
    AgencyID As Long
    PNumber As Long
    ForeColor As Long
    BackColor As Long
End Type

Private Type tBoundingBox
    FromLat As Double
    FromLon As Double
    ToLat As Double
    ToLon As Double
End Type

Private Sub chkANIALIForm_Click()
    Dim Test As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkANIALIForm_Click")
    End If
    
End Sub

Private Sub chkDivision_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkDivision_Click")
    End If
    
    If chkDivision.Value = vbChecked Then
        lstDivision.Clear
        lstDivision.Enabled = False
    Else
        Call FillDivisionList
        lstDivision.Enabled = True
    End If

End Sub

Private Sub chkEngineVisible_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkEngineVisible_Click")
    End If
    cmdSaveESRI.Enabled = True
End Sub

Private Sub chkJurisdiction_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkJurisdiction_Click")
    End If
    
    If chkJurisdiction.Value = vbChecked Then
        chkDivision.Enabled = False
        chkDivision.Value = vbChecked
        lstJurisdiction.Clear
        lstJurisdiction.Enabled = False
    Else
        Call FillJurisdictionList
        chkDivision.Enabled = True
        chkDivision.Value = vbChecked
        lstJurisdiction.Enabled = True
    End If
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkJurisdiction_Click")
    End If
End Sub

Private Sub chkSendANIALI_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkSendANIALI_Click")
    End If
    
    If chkSendANIALI.Value Then
        txtANIALIWorkstation.Enabled = True
        lblANIALIWS.Enabled = True
    Else
        txtANIALIWorkstation.Enabled = False
        lblANIALIWS.Enabled = False
    End If
    
End Sub

Private Sub chkSkipLeadingAlpha_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkSkipLeadingAlpha_Click")
    End If
    cmdRadioSave.Enabled = True
    cmdRadioReset.Enabled = True
End Sub

Private Sub chkUnitsSetAvailable_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkUnitsSetAvailable_Click")
    End If
    cmdSaveClear.Enabled = True
End Sub

Private Sub chkUseESRI_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkUseESRI_Click")
    End If
    '    If chkUseESRI.Value = vbChecked Then
    '        chkUseESRI.Value = vbUnchecked
    '    Else
    '        chkUseESRI.Value = vbChecked
    '    End If
    cmdSaveESRI.Enabled = True
    
End Sub

Private Sub chkUseOneWay_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkUseOneWay_Click")
    End If
    
    cmdSaveESRI.Enabled = True
    
End Sub

Private Sub chkUseSharedFolder_Click()
    If chkUseSharedFolder.Value = vbChecked Then
        drvSharedFolder.Enabled = True
        dirSharedFolder.Enabled = True
        txtRoutingSharedFolder.Enabled = True
        txtRoutingSharedFolder.Text = gSharedRoutingFolder
    Else
        drvSharedFolder.Enabled = False
        dirSharedFolder.Enabled = False
        txtRoutingSharedFolder.Enabled = False
        txtRoutingSharedFolder.Text = "[Using IPC data exchange for Routing Calculations]"
    End If
    cmdSaveESRI.Enabled = True
End Sub

Private Sub chkUseXMLPort_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub chkUseXMLPort_Click")
    End If
    
    If chkUseXMLPort.Value = vbChecked Then
        txtIPCServer.Enabled = False
    Else
        txtIPCServer.Enabled = True
        txtIPCServer.SetFocus
        txtIPCServer.SelStart = 0
        txtIPCServer.SelLength = Len(txtIPCServer.Text)
    End If

End Sub

Private Sub cmbAgency_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmbAgency_Click")
    End If
    
    chkJurisdiction.Value = vbChecked
    lstJurisdiction.Clear
    lstDivision.Clear
    
    If cmbAgency.Text <> "<Show All Agencies>" Then
        chkJurisdiction.Enabled = True
    Else
        chkJurisdiction.Enabled = False
    End If

End Sub

Private Sub cmbChannelPick_Change()
    On Error GoTo cmbChannelPick_Change_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmbChannelPick_Change")
    End If
    
    If cmbChannelPick.Text <> "" Then
        If lvRadioChannel.SelectedItem.Text <> "" Then
            lvRadioChannel.SelectedItem.SubItems(1) = cmbChannelPick.Text
        End If
    End If
    Exit Sub
cmbChannelPick_Change_ERH:
    Call AddToDebugList("[ERROR] Error in procedure cmbChannelPick_Change of Form frmMain - " & Err.Description & "(" & Err.Number & ")", lstDebug)
End Sub

Private Sub cmbChannelPick_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmbChannelPick_Click")
    End If
    
    If lvRadioChannel.SelectedItem.Text <> "" Then
        lvRadioChannel.SelectedItem.SubItems(1) = cmbChannelPick.Text
    End If
End Sub

Private Sub cmbCostAttribute_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmbCostAttribute_Change")
    End If
    
    If cmbCostAttribute.ListIndex >= 0 Then
        cmbCostAttribute.Text = cmbCostAttribute.List(cmbCostAttribute.ListIndex)
        cmdSaveESRI.Enabled = True
    End If
End Sub

Private Sub cmbDB_Click()
    Dim n As Integer
    Dim aDSN As Variant
    Dim aPart As Variant
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmbDB_Click")
    End If
    
    If Right(gDWConnectString, 1) = ";" Then
        gDWConnectString = Left(gDWConnectString, Len(gDWConnectString) - 1)
    End If
    
    aDSN = Split(gDWConnectString, ";")
    
    gDWConnectString = ""
    
    For n = 0 To UBound(aDSN)
        aPart = Split(aDSN(n), "=")
        If aPart(0) = "DSN" Then
            gDWConnectString = gDWConnectString & "DSN=" & cmbDB.Text & ";"
        Else
            gDWConnectString = gDWConnectString & aDSN(n) & ";"
        End If
    Next n

End Sub

Private Sub cmbRadioAgency_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmbRadioAgency_Click")
    End If
    
    Call LoadRadioListView
    
End Sub

Private Sub cmdAniAli_Click()
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdAniAli_Click")
    End If
    
    '   chkANIALIForm.Value = bsiGetSettings("DEFAULT", "SHOWINCDETAILS", vbChecked, INIPATH & "\Simulator.INI")
    chkANIALIForm.Value = bsiGetSettings("DEFAULT", "SHOWINCDETAILS", vbChecked, gSystemDirectory & "\Simulator.INI")
    
    If cmdAniAli.Caption = "ANI/ALI" Then
        frmAniAli.Visible = True
        cmdAniAli.Caption = "Cancel Send"
        cmdSendAniAli.Enabled = False
        cmdSendCellular.Enabled = False
        fmFilter.Visible = False
    Else
        frmAniAli.Visible = False
        cmdAniAli.Caption = "ANI/ALI"
        If lstAniAli.SelCount > 0 Then
            For n = 0 To lstAniAli.ListCount - 1
                lstAniAli.Selected(n) = False
            Next n
        End If
        fmFilter.Visible = True
    End If
    
End Sub

Private Sub cmdANIALIReset_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdANIALIReset_Click")
    End If
    
    txtCellAddress.Text = gCellularAddress
    txtCellNumber.Text = gCellularNumber
    txtCellClass.Text = gCellularService
    opAniAliDialog.Value = gUseANIALIDialog
    opAniAli2Form.Value = Not gUseANIALIDialog
    chkSendANIALI.Value = IIf(gSendANIALIWithInc, vbChecked, vbUnchecked)
    txtANIALIWorkstation.Text = gANIALIWorkstation

End Sub

Private Sub cmdANIALISave_Click()
    Dim Test As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdANIALISave_Click")
    End If
    
    gCellularAddress = txtCellAddress.Text
    gCellularNumber = txtCellNumber.Text
    gCellularService = txtCellClass.Text
    gUseANIALIDialog = opAniAliDialog.Value
    gSendANIALIWithInc = IIf(chkSendANIALI.Value = vbChecked, True, False)
    gANIALIWorkstation = txtANIALIWorkstation.Text
    
    '    Test = bsiPutSettings("SystemDefaults", "CellularAddress", gCellularAddress, INIPATH & "\Simulator.INI")
    '    Test = bsiPutSettings("SystemDefaults", "CellularNumber", gCellularNumber, INIPATH & "\Simulator.INI")
    '    Test = bsiPutSettings("SystemDefaults", "CellularService", gCellularService, INIPATH & "\Simulator.INI")
    '    Test = bsiPutSettings("SystemDefaults", "UseANIALIDialog", IIf(gUseANIALIDialog, "TRUE", "FALSE"), INIPATH & "\Simulator.INI")
    '    Test = bsiPutSettings("SystemDefaults", "SendANIALIWithInc", IIf(gSendANIALIWithInc, "TRUE", "FALSE"), INIPATH & "\Simulator.INI")
    '    Test = bsiPutSettings("SystemDefaults", "ANIALIWorkstation", gANIALIWorkstation, INIPATH & "\Simulator.INI")
    
    Test = bsiPutSettings("SystemDefaults", "CellularAddress", gCellularAddress, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "CellularNumber", gCellularNumber, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "CellularService", gCellularService, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "UseANIALIDialog", IIf(gUseANIALIDialog, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "SendANIALIWithInc", IIf(gSendANIALIWithInc, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "ANIALIWorkstation", gANIALIWorkstation, gSystemDirectory & "\Simulator.INI")
    
End Sub

Private Sub cmdCancelNew_Click()
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdCancelNew_Click")
    End If
    
    For n = 0 To lstProfiles.ListCount - 1
        lstProfiles.Selected(n) = False
    Next n
    
    txtInterstate.Text = ""
    txtLimAccess.Text = ""
    txtHighway.Text = ""
    txtArterial.Text = ""
    txtStreet.Text = ""
    txtProfile.Text = ""
    
    cmdDelProfile.Enabled = False
    cmdNewProfile.Enabled = True
    cmdSaveProfile.Enabled = False
    cmdCancelNew.Enabled = False

End Sub

Private Sub cmdClear_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdClear_Click")
    End If

    Call SetupIncidentBrowser
    Call SetUpFilterPane
    
    dtBrowse.Value = CDate(Format(Now, "MMM DD YYYY 00:00:00"))
    txtFrom.Text = "00:00"
    txtTo.Text = "23:59"
    txtNote.Text = ""
    '    cmbDiv.ListIndex = 0
    
    cmdIncMap.Enabled = False
    cmdSaveScript.Enabled = False
    
    cmdIncMap.Caption = "Map"
    incMap.Visible = False
    tbBrowse.Visible = False
    flxBrowse.Visible = True
    
End Sub

Private Sub cmdClearRadio_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdClearRadio_Click")
    End If
    
    txtTransmit.Text = ""
    cmdTransmit.Enabled = False

End Sub

Private Sub cmdConnect_Click()
    '   Dim cnRoute As Object
    Dim cMsg As String
    Dim cMsgNum As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdConnect_Click")
    End If
    '   Read all IP Settings each time we try to connect
    
    '    txtIPCServer.Text = bsiGetSettings("SystemPreferences", "TCPServer", "**ERROR**", INIPATH & "\System.INI")
    '    gAVLIPAddress = bsiGetSettings("AVL Settings", "AVLIPAddress", "127.0.0.1", INIPATH & "\Simulator.INI")
    '    gAVLPath = bsiGetSettings("AVL Settings", "AVLPath", App.Path, INIPATH & "\Simulator.INI")
    '    gAVLPort = bsiGetSettings("AVL Settings", "AVLPort", "19191", INIPATH & "\Simulator.INI")
    
    '    txtIPCServer.Text = bsiGetSettings("SystemPreferences", "TCPServer", "**ERROR**", gSystemDirectory & "\System.INI")
    txtIPCServer.Text = bsiGetSettings("MAIN", "TCPServer", "**ERROR**", App.Path & "\Share.INI")
    gAVLIPAddress = bsiGetSettings("AVL Settings", "AVLIPAddress", "127.0.0.1", gSystemDirectory & "\Simulator.INI")
    gAVLPath = bsiGetSettings("AVL Settings", "AVLPath", App.Path, gSystemDirectory & "\Simulator.INI")
    gAVLPort = bsiGetSettings("AVL Settings", "AVLPort", "19191", gSystemDirectory & "\Simulator.INI")
    
    If txtIPCServer.Text <> "" Then
        If cmdConnect.Caption = "&Connect" Then
            sBar.Panels(2).Text = "Connecting ..."
            If ConnectToServer Then                             '   connecting to IPC Server
                sBar.Panels(2).Text = "Connecting ..."
                If ConnectToAVLInterface(5) Then                '   Connecting to AVL interface - retry five times - allow for loading time
                    cmdConnect.Caption = "Dis&connect"
                    If gLicensedVersion Then
                        sBar.Panels(2).Text = "Connected"
                        
                        '    Set gcnRoute = CreateObject("cnRoutingESRI.cnRouting")   '   launch routing module
                        '
                        '    DoEvents
                        '
                        '    Call gcnRoute.cnReadSettings(INIPATH & "\Simulator.INI")
                        '    Call gcnRoute.cnResetIPCConnection
                        
                        cMsg = Format(CN_MSG_RUNNING_SIMULATION, "0000") & tcpSock.LocalHostName
                        
                        Call SendToCADNoWait(Format(CN_MSG_CLASS, "0000"), cMsg)
                
                    Else
                        sBar.Panels(2).Text = "Unlicensed"
                    End If
                    Call LoadUnitList
                    Call RefreshUnitQueue
                    '   Call CenterMap
                    tmUpdateAVL.Enabled = True
                    '   tmCADMsg.Enabled = True
                    cmdScript.Enabled = True
                    '    cmdSendInc.Enabled = True
                    '    cmdAniAli.Enabled = True
                    cmdPause.Enabled = True
                    '    SSTabMain.TabEnabled(1) = True
                    '    SSTabMain.TabEnabled(2) = True
                    '    SSTabMain.TabEnabled(3) = True
                    
                    tmMapRefresh.Interval = gUnitUpdateInterval * 1000
                    tmMapRefresh.Enabled = True
                    
                Else
                    sBar.Panels(2).Text = "Error on AVL Connect"
                    Call DisconnectServer                       '   on fail with AVL interface, disconnect from IPC server
                    
                    tmMapRefresh.Enabled = False
                
                End If
            Else
                sBar.Panels(2).Text = "Error on IPC Connect"
            End If
        Else
            
            cMsgNum = Format(CN_MSG_CLASS, "0000")
            
            cMsg = Format(CN_MSG_SIM_KILL_cnROUTE, "0000") & "- Kill cnRoutingEngine"
            Call SendToCADNoWait(cMsgNum, cMsg)
            
            '   Set gcnRoute = Nothing
            
            DoEvents
            
            cMsg = Format(CN_MSG_SIM_MASTER_SHUTDOWN, "0000") & "- Master Console has shut down."
            Call SendToCADNoWait(cMsgNum, cMsg)
            
            DoEvents
            
            If tcpAVL.RemoteHostIP = "127.0.0.1" Then      '    only send AVL KILL if the AVL interface is running on the localhost
                If Not SendToAVL(EV_KILL) Then
                    AddToDebugList "[TIMEOUT] Sending KILL to AVL Interface"
                End If
            
                DoEvents
            End If
            
            cmdScript.Enabled = False
            cmdSendInc.Enabled = False
            cmdAniAli.Enabled = False
            cmdPause.Enabled = False
            sBar.Panels(2).Text = "Not Connected"
            '   tmCADMsg.Enabled = False
            tmUpdateAVL.Enabled = False
            cmdConnect.Caption = "&Connect"
            '    SSTabMain.TabEnabled(1) = False
            '    SSTabMain.TabEnabled(2) = False
            '    SSTabMain.TabEnabled(3) = False
            Call DisconnectServer
            Call DisconnectAVL
            
            tmMapRefresh.Enabled = False
        
        End If
    End If

End Sub

Private Sub cmdDelProfile_Click()
    Dim n As Integer
    Dim i As Integer
    Dim j As Integer
    Dim tempDrive() As tDriveProfile
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdDelProfile_Click")
    End If
    
    j = 0
    
    i = lstProfiles.ListIndex
    
    ReDim tempDrive(0 To UBound(gDriveTime))
    
    For n = 0 To UBound(gDriveTime)
        tempDrive(n) = gDriveTime(n)
    Next n
    
    ReDim gDriveTime(0 To UBound(gDriveTime) - 1)
    
    For n = 0 To UBound(tempDrive)
        If tempDrive(n).Name <> lstProfiles.List(i) Then
            gDriveTime(j) = tempDrive(n)
            j = j + 1
        End If
    Next n
    
    txtInterstate.Text = ""
    txtLimAccess.Text = ""
    txtHighway.Text = ""
    txtArterial.Text = ""
    txtStreet.Text = ""
    txtProfile.Text = ""
    
    Call cmdSaveProfile_Click
    
End Sub

Private Sub cmdDiscard_Click()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cErrorMessage As String
    Dim nFile As Integer
    Dim OffLat As Long
    Dim OffLon As Long
    Dim dSomeDate As Variant
    Dim Test As Boolean
    
    Dim cError As String
    
    On Error GoTo ErrorHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdDiscard_Click")
    End If
    
    cError = "Start of cmdDiscard"
    
    gAVLIPAddress = bsiGetSettings("AVL Settings", "AVLIPAddress", "127.0.0.1", gSystemDirectory & "\Simulator.INI")
    gAVLPath = bsiGetSettings("AVL Settings", "AVLPath", "", gSystemDirectory & "\Simulator.INI")
    gAVLPort = bsiGetSettings("AVL Settings", "AVLPort", "19191", gSystemDirectory & "\Simulator.INI")
    gAVLSegmentFactor = Val(bsiGetSettings("AVL Settings", "AVLSegmentDivisor", "8", gSystemDirectory & "\Simulator.INI"))
    gAVLDriveFactor = Val(bsiGetSettings("AVL Settings", "AVLTimeDivisor", "95", gSystemDirectory & "\Simulator.INI"))
    gNoAVLinStatus = bsiGetSettings("AVL Settings", "NoAVLStatus", "02,10,13", gSystemDirectory & "\Simulator.INI")
    gUnitUpdateInterval = Val(bsiGetSettings("AVL Settings", "AVLMapUpdateInterval", "2", gSystemDirectory & "\Simulator.INI"))
    
    gChuteMean = Val(bsiGetSettings("AVL Settings", "ChuteTimeMean", 60, gSystemDirectory & "\Simulator.INI"))
    gChuteRange = Val(bsiGetSettings("AVL Settings", "ChuteTimeRange", 18, gSystemDirectory & "\Simulator.INI"))
    
    gUseSourceSceneTime = IIf(UCase(bsiGetSettings("AVL Settings", "UseSourceSceneTime", "false", gSystemDirectory & "\Simulator.INI")) = "TRUE", True, False)
    gSceneMean = Val(bsiGetSettings("AVL Settings", "SceneTimeMean", 600, gSystemDirectory & "\Simulator.INI"))
    gSceneRange = Val(bsiGetSettings("AVL Settings", "SceneTimeRange", 600, gSystemDirectory & "\Simulator.INI"))
    
    gUseSourceDestTime = IIf(UCase(bsiGetSettings("AVL Settings", "UseSourceDestTime", "false", gSystemDirectory & "\Simulator.INI")) = "TRUE", True, False)
    gDestMean = Val(bsiGetSettings("AVL Settings", "DestTimeMean", 600, gSystemDirectory & "\Simulator.INI"))
    gDestRange = Val(bsiGetSettings("AVL Settings", "DestTimeRange", 600, gSystemDirectory & "\Simulator.INI"))
    
    gUseSourceDest = IIf(UCase(bsiGetSettings("AVL Settings", "UseSourceDest", "false", gSystemDirectory & "\Simulator.INI")) = "TRUE", True, False)
    gTransportPercent = Val(bsiGetSettings("AVL Settings", "TransportPercent", "70", gSystemDirectory & "\Simulator.INI"))
    gUseClosestPercent = Val(bsiGetSettings("AVL Settings", "UseClosestPercent", "70", gSystemDirectory & "\Simulator.INI"))
    gTransportLimit = Val(bsiGetSettings("AVL Settings", "DestinationTransportLimit", "20", gSystemDirectory & "\Simulator.INI"))
    
    gTransportPrio = bsiGetSettings("SystemDefaults", "DefaultTransportPriority", "01", gSystemDirectory & "\Simulator.INI")
    gTransportPro = bsiGetSettings("SystemDefaults", "DefaultTransportProtocol", "02", gSystemDirectory & "\Simulator.INI")
    gClosestPro = bsiGetSettings("SystemDefaults", "ClosestTransportProtocol", "03", gSystemDirectory & "\Simulator.INI")
    
    gDingWithMessage = IIf(UCase(bsiGetSettings("SystemDefaults", "DingWithMessage", "false", gSystemDirectory & "\Simulator.INI")) = "TRUE", True, False)
    If gDingWithMessage Then
        chkMessageBell.Value = vbChecked
    Else
        chkMessageBell.Value = vbUnchecked
    End If
    
    cError = "XMLPort in cmdDiscard"
    
    gUseXMLPort = IIf(UCase(bsiGetSettings("SystemDefaults", "UseXMLPort", "1", gSystemDirectory & "\Simulator.INI")) = "1", True, False)
    If gUseXMLPort Then
        chkUseXMLPort.Value = vbChecked
    Else
        chkUseXMLPort.Value = vbUnchecked
    End If
    
    gForceAVL = IIf(UCase(bsiGetSettings("SystemDefaults", "ForceAVL", "1", gSystemDirectory & "\Simulator.INI")) = "1", True, False)
    If gForceAVL Then
        chkForceAVL.Value = vbChecked
    Else
        chkForceAVL.Value = vbUnchecked
    End If
    
    gForceMST = IIf(UCase(bsiGetSettings("SystemDefaults", "ForceMST", "1", gSystemDirectory & "\Simulator.INI")) = "1", True, False)
    If gForceMST Then
        chkForceMST.Value = vbChecked
    Else
        chkForceMST.Value = vbUnchecked
    End If
    
    Call AddToDebugList("[XML Port] " & IIf(gUseXMLPort, "TRUE", "FALSE"), lstDebug)
    
    gCellularAddress = bsiGetSettings("SystemDefaults", "CellularAddress", "47 CELLULAR ST", gSystemDirectory & "\Simulator.INI")
    gCellularNumber = bsiGetSettings("SystemDefaults", "CellularNumber", "416-511-0000", gSystemDirectory & "\Simulator.INI")
    gCellularService = bsiGetSettings("SystemDefaults", "CellularService", "CEL", gSystemDirectory & "\Simulator.INI")
    gUseANIALIDialog = IIf(UCase(bsiGetSettings("SystemDefaults", "UseANIALIDialog", "TRUE", gSystemDirectory & "\Simulator.INI")) = "TRUE", True, False)
    gANIALIWorkstation = bsiGetSettings("SystemDefaults", "ANIALIWorkstation", "GHOSTWORKSTATION", gSystemDirectory & "\Simulator.INI")
    gSendANIALIWithInc = IIf(bsiGetSettings("SystemDefaults", "SendANIALIWithInc", "TRUE", gSystemDirectory & "\Simulator.INI") = "TRUE", True, False)
    txtANIALIWorkstation.Text = gANIALIWorkstation
    If gSendANIALIWithInc Then
        chkSendANIALI.Value = vbChecked
        txtANIALIWorkstation.Enabled = True
        lblANIALIWS.Enabled = True
    Else
        chkSendANIALI.Value = vbUnchecked
        txtANIALIWorkstation.Enabled = False
        lblANIALIWS.Enabled = False
    End If
    
    cError = "opReturnToBase in cmdDiscard"
    
    opReturnToBase.Value = IIf(bsiGetSettings("SystemDefaults", "ReturnToBase", "TRUE", gSystemDirectory & "\Simulator.INI") = "TRUE", True, False)
    chkUnitsSetAvailable.Value = IIf(bsiGetSettings("SystemDefaults", "UnitsCanClear", "FALSE", gSystemDirectory & "\Simulator.INI") = "TRUE", vbChecked, vbUnchecked)

    opAwaitInstructions.Value = Not opReturnToBase.Value
    
    gInterMessageWait = Val(bsiGetSettings("SystemDefaults", "RadioWaitToReceive", "3", gSystemDirectory & "\Simulator.INI"))
    gSpeechRate = Val(bsiGetSettings("SystemDefaults", "RadioSpeechRate", "2", gSystemDirectory & "\Simulator.INI"))
    gControlName = bsiGetSettings("SystemDefaults", "RadioControlName", "Control", gSystemDirectory & "\Simulator.INI")
    gSkipLeadingAlphas = IIf(bsiGetSettings("SystemDefaults", "SkipLeadingAlpha", "TRUE", gSystemDirectory & "\Simulator.INI") = "TRUE", vbChecked, vbUnchecked)
    chkSkipLeadingAlpha.Value = IIf(gSkipLeadingAlphas, vbChecked, vbUnchecked)
    
    gLoadContext = IIf(UCase(bsiGetSettings("SystemDefaults", "LoadUnitContext", "FALSE", gSystemDirectory & "\Simulator.INI")) = "TRUE", True, False)
    If gLoadContext Then
        chkSyncStartup.Value = vbChecked
    Else
        chkSyncStartup.Value = vbUnchecked
    End If
    
    gLoadHistory = IIf(UCase(bsiGetSettings("SystemDefaults", "LoadUnitHistory", "FALSE", gSystemDirectory & "\Simulator.INI")) = "TRUE", True, False)
    If gLoadHistory Then
        chkHistory.Value = vbChecked
    Else
        chkHistory.Value = vbUnchecked
    End If
    
    gCompression = Val(bsiGetSettings("SystemDefaults", "TimeCompression", "1.0", gSystemDirectory & "\Simulator.INI"))
    txtTimeWarp.Text = Format(gCompression, "0.0")
    sldTimeWarp.Value = gCompression * 10
    
    gDriveCompression = Val(bsiGetSettings("SystemDefaults", "DriveCompression", "1.0", gSystemDirectory & "\Simulator.INI"))
    txtDriveWarp.Text = Format(gDriveCompression, "0.0")
    sldDriveWarp.Value = gDriveCompression * 10
    
    gSavePosition = bsiGetSettings("SystemDefaults", "SaveStartupPosition", "N", gSystemDirectory & "\Simulator.INI")
    If gSavePosition = "Y" Then
        chkSavePos.Value = vbChecked
        gStartPosition = Split(bsiGetSettings("SystemDefaults", "StartupPosition", "0|0", gSystemDirectory & "\Simulator.INI"), "|")
        frmMain.Top = Val(gStartPosition(0))
        frmMain.Left = Val(gStartPosition(1))
    Else
        chkSavePos.Value = vbUnchecked
        frmMain.Top = Screen.Height / 2 - frmMain.Height / 2
        frmMain.Left = Screen.Width / 2 - frmMain.Width / 2
    End If
    
    '   Get the GeoCorrection required to match VisiCAD database with MapPoint database
    '   Use OFFSET.FIL from the default location if it's there ...
    '   Let the user specify (in Simulator.INI) which version of the file to ise if it's not there.
    
    cError = "gOffsetFile in cmdDiscard"
    
    Dim cShareDrive As String
    cShareDrive = Left(gSystemDirectory, 2)
    
    gOffsetFile = cShareDrive & "\TRITECH\VISICAD\DATA\MAPS\OFFSET.FIL"
    If Dir(gOffsetFile) = "" Then                               '   if Offset.fil is not in the default location, use the user-specified location
        gOffsetFile = bsiGetSettings("GEOCORRECTION", "OFFSETFILE", cShareDrive & "\TRITECH\VISICAD\DATA\MAPS\OFFSET.FIL", gSystemDirectory & "\Simulator.INI")
    End If

    Call GetGeographicOffsets(gOffsetFile, gLatitudeAdjust, gLongitudeAdjust)

    Test = bsiPutSettings("GEOCORRECTION", "OFFSETFILE", gOffsetFile, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("GEOCORRECTION", "LATITUDE", Format(gLatitudeAdjust, "###0.000000"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("GEOCORRECTION", "LONGITUDE", Format(gLongitudeAdjust, "###0.000000"), gSystemDirectory & "\Simulator.INI")
    Call AddToDebugList("[GEOFIX] Using Lat/Lon Offset: " & Format(gLatitudeAdjust, "###0.000000") & " / " & Format(gLongitudeAdjust, "###0.000000"), lstDebug)
    
    gAVLMDTAppName = bsiGetSettings("AVL Settings", "AVLAPPName", "SimulatorAVL-MDT.exe", gSystemDirectory & "\Simulator.INI")
    
    gHospitalTypeIDs = bsiGetSettings("AVL Settings", "HospitalTypeID", "", gSystemDirectory & "\Simulator.INI")
    
    gTransportTypes = bsiGetSettings("AVL Settings", "AVLIPAddress", "127.0.0.1", gSystemDirectory & "\Simulator.INI")
    
    '    txtIPCServer.Text = bsiGetSettings("SystemPreferences", "TCPServer", "**ERROR**", gSystemDirectory & "\System.INI")
    txtIPCServer.Text = bsiGetSettings("MAIN", "TCPServer", "**ERROR**", App.Path & "\Share.INI")
    
    cError = "gConnectString in cmdDiscard"
    
    gConnectString = FixConnectString()
    
    Call AddToDebugList("[CONFIG] Using Connect String: " & gConnectString, lstDebug)
    
    gDWConnectString = bsiGetSettings("SystemDefaults", "DataWareHouse", gConnectString, gSystemDirectory & "\Simulator.INI")
    
    gReturnToBase = IIf(bsiGetSettings("SystemDefaults", "RETURNTOBASE", "FALSE", gSystemDirectory & "\Simulator.INI") = "TRUE", True, False)
    
    '   gUseESRIResources = IIf(bsiGetSettings("ESRI", "UseESRIResources", "0", INIPATH & "\Simulator.INI") = "1", True, False)
    gUseESRIResources = True
    gESRINetworkDataset = bsiGetSettings("ESRI", "ESRINetworkDataset", App.Path & "\NotYetSet.shp", gSystemDirectory & "\Simulator.INI")
    gESRINetworkCost = bsiGetSettings("ESRI", "CostAttribute", "", gSystemDirectory & "\Simulator.INI")
    gAVLUpdateLimit = Val(bsiGetSettings("ESRI", "AVLUpdateLimit", 150, gSystemDirectory & "\Simulator.INI"))
    gUseOneWayRestrictions = IIf(bsiGetSettings("ESRI", "UseOneWayRestrictions", "0", gSystemDirectory & "\Simulator.INI") = "1", True, False)
    chkEngineVisible.Value = bsiGetSettings("ESRI", "RoutingEngineVisible", "0", gSystemDirectory & "\Simulator.INI")
    
    chkUseSharedFolder.Value = bsiGetSettings("ESRI", "UseShareRoutingFolder", "0", gSystemDirectory & "\Simulator.INI")
    gUseShareRoutingFolder = IIf(chkUseSharedFolder.Value = vbChecked, True, False)
    
    If gUseShareRoutingFolder Then
        txtRoutingSharedFolder.Text = bsiGetSettings("ESRI", "SharedRoutingFolder", "<Using IPC Route Communications>", gSystemDirectory & "\Simulator.INI")
        drvSharedFolder.Drive = Left(txtRoutingSharedFolder.Text, 1) & ":"
        txtRoutingSharedFolder.Text = bsiGetSettings("ESRI", "SharedRoutingFolder", "<Using IPC Route Communications>", gSystemDirectory & "\Simulator.INI")
        dirSharedFolder.Path = txtRoutingSharedFolder.Text
    Else
        drvSharedFolder.Drive = Left(gSystemDirectory, 2)
        txtRoutingSharedFolder.Text = bsiGetSettings("ESRI", "SharedRoutingFolder", "<Using IPC Route Communications>", gSystemDirectory & "\Simulator.INI")
        txtRoutingSharedFolder.Enabled = False
        drvSharedFolder.Enabled = False
        dirSharedFolder.Enabled = False
    End If
    
    cError = "SyncCombo in cmdDiscard"
    
    Call SyncCombo(gDWConnectString, cmbDB)
    
    cSQL = "Select count(*) from Response_Master_Incident"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
'        MsgBox Str(nRecs) & "," & Str(aSQL(0, 0)), vbInformation + vbOKOnly, "SQL Connection Verified"
    Else
        Call MsgBox("[ERROR] SQL Connection Verification failed: " & cError)
    End If
    
    cError = "CheckVersion in cmdDiscard"
    
    gb110Version = Check110Version()

    opReadChute.Value = False
    opCalcChute.Value = True

    opReadScene.Value = gUseSourceSceneTime
    opCalcScene.Value = Not gUseSourceSceneTime

    opReadDestTime.Value = gUseSourceDestTime
    opCalcDestTime.Value = Not gUseSourceDestTime

    opReadDestination.Value = gUseSourceDest
    opCalcDestination.Value = Not gUseSourceDest
    
    opAniAliDialog = gUseANIALIDialog
    opAniAli2Form = Not gUseANIALIDialog
    
    opReturnToBase.Value = gReturnToBase
    opAwaitInstructions.Value = Not gReturnToBase
    
    txtHospitalTypes.Text = gHospitalTypeIDs
    
    txtChuteTime.Text = gChuteMean
    txtChuteRange.Text = gChuteRange

    txtSceneTime.Text = gSceneMean
    txtSceneRange.Text = gSceneRange

    txtDestTime.Text = gDestMean
    txtDestRange.Text = gDestRange
    txtDestLimit.Text = gTransportLimit

    txtDestTrans.Text = gTransportPercent
    txtDestClose.Text = gUseClosestPercent
    
    txtAVLIP.Text = gAVLIPAddress
    txtAVLPath.Text = gAVLPath
    txtAVLPort.Text = gAVLPort
    txtAVLSegments.Text = gAVLSegmentFactor
    txtAVLDriveFactor.Text = gAVLDriveFactor
    txtNoAVStatus.Text = gNoAVLinStatus
    
    txtTransPrio.Text = gTransportPrio
    txtTransPro.Text = gTransportPro
    txtClosePro.Text = gClosestPro
    
    txtCellAddress.Text = gCellularAddress
    txtCellNumber.Text = gCellularNumber
    txtCellClass.Text = gCellularService
    
    chkUseESRI.Value = IIf(gUseESRIResources, vbChecked, vbUnchecked)
    txtESRINetworkDataset.Text = gESRINetworkDataset
    cmbCostAttribute.Text = gESRINetworkCost
    txtAVLUpdateLimit.Text = gAVLUpdateLimit
    chkUseOneWay.Value = IIf(gUseOneWayRestrictions, vbChecked, vbUnchecked)

    chkUseSharedFolder.Value = IIf(gUseShareRoutingFolder, vbChecked, vbUnchecked)
    gSharedRoutingFolder = txtRoutingSharedFolder.Text
    
    gSimScriptDir = bsiGetSettings("SystemDefaults", "ScriptDirectory", "Q:\TriTech\CADNorth\SimScripts", gSystemDirectory & "\Simulator.INI")
    gSoundByteDir = bsiGetSettings("SystemDefaults", "SoundDirectory", "Q:\TriTech\CADNorth\SimMedia", gSystemDirectory & "\Simulator.INI")
    
'    gMeridianOffset = Val(bsiGetSettings("GeoBaseConstants", "MeridianOffset", "0", INIPATH & "\Simulator.INI"))
    
'    cmbDB.Text = gDWConnectString
    
'    txtInterstate.Text = ""
'    txtLimAccess.Text = ""
'    txtHighway.Text = ""
'    txtArterial.Text = ""
'    txtStreet.Text = ""
'    txtProfile.Text = ""

    cmdSaveClear.Enabled = False

    Exit Sub

ErrorHandler:
    cErrorMessage = "Error occurred in cmdDiscard_Click:" & Str(Err.Number) & " " & Err.Description & "   (" & cError & ")"
    Call AddToDebugList(cErrorMessage, lstDebug)
    MsgBox cErrorMessage, vbOKOnly
    Resume Next
End Sub

'---------------------------------------------------------------------------------------
' Procedure : cmdESRIBrowse_Click
' DateTime  : 6/16/2009 17:12
' Author    : brianmcg
' Purpose   :
'---------------------------------------------------------------------------------------
'
Private Sub cmdESRIBrowse_Click()
    Dim cWSPath As String
    Dim cWSName As String
    Dim cError As String
    Dim cPath As String

    On Error GoTo cmdESRIBrowse_Click_Error
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdESRIBrowse_Click")
    End If
    
    cError = "Start of cmdESRIBrowse_Click"
    
    cPath = ParsePath(gESRINetworkDataset)

    With dlgFile
        .CancelError = True
        .InitDir = IIf(cPath <> "", cPath, App.Path)
        .Filter = "ESRI Shapefiles (*.shp)|*.shp|All Files (*.*)|*.*"
        .DialogTitle = "Select ESRI Shapefile"
        .ShowOpen
    End With

    txtESRINetworkDataset.Text = dlgFile.FileName
    
    Call cnParsePath(txtESRINetworkDataset.Text, cWSPath, cWSName)
    cWSName = Left(cWSName, InStr(1, cWSName, ".") - 1)

'    Dim pFWorkspace As IFeatureWorkspace
'    Dim pNetworkDataset As INetworkDataset
'    Dim pContext As INAContext
    
    Dim aRouteMP As tRoute
    Dim aRouteSH As tRoute
    
    cError = "Loading Shapefile ... Configuring Workspace"
    
'    Set pFWorkspace = OpenWorkspace(cWSPath)
'    Set pNetworkDataset = OpenNetworkDataset(pFWorkspace, cWSName)
  
    ' Get Cost Attributes
    
'    Dim pNetworkAttribute As INetworkAttribute
    Dim i As Long
    
    cmbCostAttribute.Clear
    
'    If pNetworkDataset.AttributeCount > 0 Then
'        For i = 0 To pNetworkDataset.AttributeCount - 1
'            Set pNetworkAttribute = pNetworkDataset.Attribute(i)
'            If pNetworkAttribute.UsageType = esriNAUTCost Then
'                cmbCostAttribute.AddItem pNetworkAttribute.Name
'            End If
'        Next i
'
'        cmbCostAttribute.Text = "<Select Cost Attribute>"
'
'    Else
'        cmbCostAttribute.Text = "** ERROR **"
'    End If
    
     
    cmdSaveESRI.Enabled = True
    
    On Error GoTo 0
    Exit Sub

cmdESRIBrowse_Click_Error:
    
    If Err.Number <> cdlCancel Then
        Call AddToDebugList("[ERROR] Error in procedure cmdESRIBrowse_Click of Form frmMain - " & Err.Description & "(" & Err.Number & ")", lstDebug)
    End If
    
End Sub

Private Sub cmdExit_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdExit_Click")
    End If
        
    Unload frmMain

End Sub

Private Sub cmdIncMap_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdIncMap_Click")
    End If
    
    If cmdIncMap.Caption = "Map" Then
        cmdIncMap.Caption = "List"
        flxBrowse.Visible = False
        tbBrowse.Visible = True
        incMap.Visible = True
    Else
        cmdIncMap.Caption = "Map"
        flxBrowse.Visible = True
        tbBrowse.Visible = False
        incMap.Visible = False
    End If

End Sub

Private Sub cmdLoadTest_Click()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim aUnitList() As String
    Dim n As Integer
    Dim xUnit As Vehicle
    Dim Test As Boolean
    Dim cUnitList As String

    On Error GoTo cmdLoadTest_Click_ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdLoadTest_Click")
    End If
    
    If cmdLoadTest.Caption = "Run Load Test" Then
        cmdLoadTest.Caption = "End Load Test"
        cmdPause.Caption = "&Pause Load Test"
        gLoadTestRunning = True
        
        aUnitList = SaveCheckedUnits(cUnitList)
        Test = bsiPutSettings("LOADTEST", "CREATEALLLOAD", IIf(chkLoadTest.Value = vbChecked, "1", "0"), gSystemDirectory & "\Simulator.INI")
        Test = bsiPutSettings("LOADTEST", "LOADRADIUS", txtLoadRadius.Text, gSystemDirectory & "\Simulator.INI")
        
        Call GetLoadTestIncidents

'        No need for this now, since we are doing it when we save the checked units above
'        ReDim aUnitList(0 To 0)
'
'        For n = 1 To lvUnitList.ListItems.count                                                 '   get the list of selected units
'            If lvUnitList.ListItems.Item(n).Checked Then
'                If aUnitList(UBound(aUnitList, 1)) = "" Then
'                    aUnitList(UBound(aUnitList, 1)) = lvUnitList.ListItems.Item(n).Text
'                Else
'                    ReDim Preserve aUnitList(0 To UBound(aUnitList, 1) + 1)
'                    aUnitList(UBound(aUnitList, 1)) = lvUnitList.ListItems.Item(n).Text
'                End If
'            End If
'        Next n
'
'        ReDim gLoadTestUnitList(0 To UBound(aUnitList, 1))
        
        For n = 0 To UBound(aUnitList, 1)                                                       '   confirm that all units are set to in-station status or this will fail
            If Not IsUnitAssigned(GetUnitRecordbyName(aUnitList(n)).VehID) Then                 '   only assign an incident to an unassigned unit
                Call SetUnitAssignToIncident(GetUnitRecordbyName(aUnitList(n)).VehID, GetUnitRecordbyName(aUnitList(n)).CurrentLat, GetUnitRecordbyName(aUnitList(n)).CurrentLon)
            Else
                Call AddToDebugList("[DEBUG] in cmdLoadTest_Click of frmMain: Unit " & aUnitList(n) & " is assigned to an incident. Skipping.")
            End If
        Next n
        
    Else
        If MsgBox("END LOAD TEST and purge all remaining events?", vbQuestion + vbYesNo, "CAUTION!!") = vbYes Then
            cmdLoadTest.Caption = "Run Load Test"
            cmdPause.Caption = "Pause"
            gLoadTestRunning = False
            lvEvents.ListItems.Clear
        End If
    End If
    
    Exit Sub

cmdLoadTest_Click_ERH:
    Call AddToDebugList("[ERROR] in cmdLoadTest_Click of frmMain: (" & Err.Number & ") " & Err.Description)
End Sub

Private Function SaveCheckedUnits(ByRef cBuffer As String) As String()
    Dim n As Long
    Dim Test As Boolean
    
    On Error GoTo SaveCheckedUnits_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function SaveCheckedUnits")
    End If
    
    For n = 1 To lvUnitList.ListItems.count
        If lvUnitList.ListItems.Item(n).Checked Then
            If cBuffer <> "" Then
                cBuffer = cBuffer & ";" & lvUnitList.ListItems.Item(n).Text
            Else
                cBuffer = lvUnitList.ListItems.Item(n).Text
            End If
        End If
    Next n
    
    Test = bsiPutSettings("LOADTEST", "SELECTEDUNITS", cBuffer, gSystemDirectory & "\Simulator.INI")
    
    SaveCheckedUnits = Split(cBuffer, ";")
    
    Exit Function
    
SaveCheckedUnits_ERH:
    Call AddToDebugList("[ERROR] in Function SaveCheckedUnits of frmMain: (" & Err.Number & ") " & Err.Description & ", Count = " & CStr(lvUnitList.ListItems.count))
End Function

Private Sub GetLoadTestIncidents()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    
    On Error GoTo GetLoadTestIncidents_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub GetLoadTestIncidents")
    End If
    
    cSQL = "SELECT TOP 1000 ID, Latitude, Longitude FROM Response_Master_Incident WHERE WhichQueue=' ' AND Latitude IS NOT NULL AND Latitude <> 0 ORDER BY Response_Date DESC"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        ReDim gLoadTestIncident(0 To nRows - 1)
        For n = 0 To nRows - 1
            gLoadTestIncident(n).RMIID = CLng(aSQL(0, n))                                           '   must be a long, so CLng() - not CInt
            gLoadTestIncident(n).Latitude = CDbl(IIf(IsNull(aSQL(1, n)), 0, aSQL(1, n))) / 1000000 - gLatitudeAdjust
            gLoadTestIncident(n).Longitude = CDbl(IIf(IsNull(aSQL(2, n)), 0, aSQL(2, n))) / -1000000 - gLongitudeAdjust
            Debug.Assert gLoadTestIncident(n).Longitude <> 0
            gLoadTestIncident(n).Proximity = 0
        Next n
    Else
        Call AddToDebugList("[ERROR] No Incidents Found in GetLoadTestIncidents of frmMain: " & cError, lstDebug)
        Call MsgBox("No incidents found for Load Test!" & vbCrLf & cError, vbCritical + vbOKOnly, "Load Test Error")
    End If
    
    Exit Sub
    
GetLoadTestIncidents_ERH:
    Call AddToDebugList("[ERROR] in GetLoadTestIncidents of frmMain @ n = " & CStr(n) & ": (" & Err.Number & ") " & Err.Description & cError, lstDebug)
End Sub

Private Function GetLoadTestIncidentsForUnit(nLatitude As Double, nLongitude As Double, nRadius As Double) As Long
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim BoundingBox As tBoundingBox
    Dim nLat1 As Long
    Dim nLon1 As Long
    Dim nLat2 As Long
    Dim nLon2 As Long
    
    On Error GoTo GetLoadTestIncidentsForUnit_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub GetLoadTestIncidentsForUnit")
    End If
    
    If UBound(gLoadTestIncident) > 30000 Then
        Call GetLoadTestIncidents               '   we're getting too big, so truncate and start over.
    End If
    
    BoundingBox = GetBoundingBox(nLatitude, nLongitude, nRadius)
    
    nLat1 = CLng((BoundingBox.FromLat + gLatitudeAdjust) * 1000000)
    nLon1 = CLng((BoundingBox.FromLon + gLongitudeAdjust) * -1000000)
    nLat2 = CLng((BoundingBox.ToLat + gLatitudeAdjust) * 1000000)
    nLon2 = CLng((BoundingBox.ToLon + gLongitudeAdjust) * -1000000)
    
    cSQL = "SELECT TOP 200 ID, Latitude, Longitude FROM Response_Master_Incident WHERE WhichQueue=' ' AND Latitude IS NOT NULL "
    If nLat1 < nLat2 Then
        cSQL = cSQL & " AND Latitude BETWEEN " & CStr(nLat1) & " AND " & CStr(nLat2)
    Else
        cSQL = cSQL & " AND Latitude BETWEEN " & CStr(nLat2) & " AND " & CStr(nLat1)
    End If
    If nLon1 < nLon2 Then
        cSQL = cSQL & " AND Longitude BETWEEN " & CStr(nLon1) & " AND " & CStr(nLon2)
    Else
        cSQL = cSQL & " AND Longitude BETWEEN " & CStr(nLon2) & " AND " & CStr(nLon1)
    End If
    cSQL = cSQL & " ORDER BY Response_Date DESC"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        Dim nStartHere As Long
        nStartHere = UBound(gLoadTestIncident, 1) + 1
        ReDim Preserve gLoadTestIncident(0 To UBound(gLoadTestIncident, 1) + nRows)
        For n = 0 To nRows - 1
            gLoadTestIncident(nStartHere + n).RMIID = CLng(aSQL(0, n))                                           '   must be a long, so CLng() - not CInt
            gLoadTestIncident(nStartHere + n).Latitude = CDbl(IIf(IsNull(aSQL(1, n)), 0, aSQL(1, n))) / 1000000 - gLatitudeAdjust
            gLoadTestIncident(nStartHere + n).Longitude = CDbl(IIf(IsNull(aSQL(2, n)), 0, aSQL(2, n))) / -1000000 - gLongitudeAdjust
            gLoadTestIncident(nStartHere + n).Proximity = 0
        Next n
    Else
        Call AddToDebugList("[ERROR] No Incidents Found in GetLoadTestIncidentsForUnit of frmMain: " & cError & vbCrLf & "cSQL = " & cSQL, lstDebug)
        '   Call MsgBox("No incidents found for Load Test Unit " & vbCrLf & cError, vbCritical + vbOKOnly, "Load Test Error")
    End If
    
    GetLoadTestIncidentsForUnit = nRows
    
    Exit Function
    
GetLoadTestIncidentsForUnit_ERH:
    Call AddToDebugList("[ERROR] in GetLoadTestIncidentsForUnit of frmMain @ n = " & CStr(n) & ": (" & Err.Number & ") " & Err.Description & cError, lstDebug)
End Function

Private Sub cmdNewProfile_Click()
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdNewProfile_Click")
    End If
    
    For n = 0 To lstProfiles.ListCount - 1
        lstProfiles.Selected(n) = False
    Next n
    
    txtInterstate.Enabled = True
    txtLimAccess.Enabled = True
    txtHighway.Enabled = True
    txtArterial.Enabled = True
    txtStreet.Enabled = True
    txtProfile.Enabled = True
    
    txtInterstate.Text = ""
    txtLimAccess.Text = ""
    txtHighway.Text = ""
    txtArterial.Text = ""
    txtStreet.Text = ""
    txtProfile.Text = ""
    
    cmdDelProfile.Enabled = False
    cmdSaveProfile.Enabled = False
    cmdNewProfile.Enabled = False
    cmdCancelNew.Enabled = True
    
    opMiles.Enabled = True
    opKMs.Enabled = True
    
    txtInterstate.SetFocus
    
End Sub

Private Sub cmdOpenScript_Click()
    Dim nFile As Integer
    Dim sFile As String
    Dim Buffer As String
    Dim StringAdder As String
    Dim FileMatrix As Variant
    Dim nRows As Integer
    Dim StringMatrix As Variant
    Dim FinalMatrix As Variant
    Dim TempMatrix As Variant
    Dim n As Integer
    Dim M As Integer
    Dim UserInput As Variant
    '    Dim IncLoc As MapPointCtl.Location
    '    Dim IncPin As MapPointCtl.Pushpin
    '    Dim maxLoc As MapPointCtl.Location
    '    Dim minLoc As MapPointCtl.Location
    Dim MaxLat As Double
    Dim MaxLon As Double
    Dim MinLat As Double
    Dim MinLon As Double
    Dim Lat As Double
    Dim Lon As Double
    Dim aPath As Variant
    Dim Test As Boolean
    
    Dim aMapList() As tMapLocation
    Dim cError As String
        
    On Error GoTo ErrorHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdOpenScript_Click")
    End If
    
    cError = "SetupIncidentBrowser Call"
    
    Call SetupIncidentBrowser
    
    nRows = 0

    cError = "FileOpen Dialog"
    
    dlgFile.InitDir = gSimScriptDir
    dlgFile.FileName = ""
    dlgFile.CancelError = True
    dlgFile.Filter = "Simulator Files (*.sim)|*.sim|All Files|*.*"
    dlgFile.ShowOpen
    
    aPath = Split(dlgFile.FileName, "\")
    
    gSimScriptDir = aPath(0)
    For n = 1 To UBound(aPath) - 1
        gSimScriptDir = gSimScriptDir & "\" & aPath(n)
    Next n
    
    Test = bsiPutSettings("SystemDefaults", "ScriptDirectory", gSimScriptDir, gSystemDirectory & "\Simulator.INI")
        
    nFile = FreeFile()
    sFile = dlgFile.FileName
    
    cError = "Reading " & sFile
    
    Open sFile For Input As #nFile
    
    Do While Not EOF(nFile)
        Line Input #nFile, StringAdder
        If nRows > 0 Then
            Buffer = Buffer & "+" & StringAdder
        Else
            Buffer = StringAdder
        End If
        nRows = nRows + 1
    Loop
    
    Close #nFile
        
    FileMatrix = Split(Buffer, "+")
    
    ReDim FinalMatrix(nRows) As Variant
    ReDim aMapList(0 To UBound(FileMatrix, 1) - 1)
    
    flxBrowse.Rows = nRows + 5

    MinLat = 999
    MinLon = 999
    MaxLat = -999
    MaxLon = -999

    For n = 0 To nRows - 1
        
        TempMatrix = Split(FileMatrix(n), "|")
    
        If n = 0 Then
            
            cError = "Restoring Filter Settings"
            
            '   dtBrowse.Value = CDate(Left(TempMatrix(1), Len(TempMatrix(1)) - 6))
            dtBrowse.Value = CDate(TempMatrix(1))
            txtFrom.Text = Right(TempMatrix(1), 5)
            txtTo.Text = Right(TempMatrix(2), 5)
            
            Call RestoreFilterSettings(TempMatrix(3))
            
            '    For M = 0 To cmbDiv.ListCount - 1
            '        If cmbDiv.List(M) = TempMatrix(3) Then
            '            cmbDiv.ListIndex = M
            '            Exit For
            '        End If
            '    Next M
            
            txtNote.Text = TempMatrix(4)
            
        Else
            
            cError = "Filling Browser, n = " & n
            
            For M = 1 To UBound(TempMatrix) - 1
                flxBrowse.Row = n
                flxBrowse.Col = M
                flxBrowse.CellForeColor = Val(TempMatrix(UBound(TempMatrix) - 1))
                flxBrowse.CellBackColor = Val(TempMatrix(UBound(TempMatrix)))
                flxBrowse.TextMatrix(n, M) = TempMatrix(M - 1)
                
                Select Case M
                    Case K_FLX_Address
                        flxBrowse.CellAlignment = flexAlignLeftCenter
                    Case K_FLX_Problem
                        flxBrowse.CellAlignment = flexAlignLeftCenter
                    Case K_FLX_Unit
                        flxBrowse.CellAlignment = flexAlignLeftCenter
                End Select
            
            Next M
            
            If TempMatrix(0) <> "SOUND" Then
                Lat = Val(TempMatrix(UBound(TempMatrix) - 3))
                Lon = Val(TempMatrix(UBound(TempMatrix) - 2))
                
                aMapList(n - 1).IncId = TempMatrix(0)
                aMapList(n - 1).Address = TempMatrix(K_FLX_Address - 1)
                aMapList(n - 1).Problem = TempMatrix(K_FLX_Problem - 1)
                aMapList(n - 1).Lat = Lat
                aMapList(n - 1).Lon = Lon
    
    '                Set IncLoc = mapInc.ActiveMap.GetLocation(Lat, Lon)
    '                Set IncPin = mapInc.ActiveMap.AddPushpin(IncLoc, TempMatrix(0))
    '                IncPin.Symbol = 41
    '                IncPin.Note = TempMatrix(2) & Chr(13) & TempMatrix(5)
                If Lat > MaxLat Then MaxLat = Lat           '   get the box coords
                If Lon > MaxLon Then MaxLon = Lon
                If Lat < MinLat Then MinLat = Lat
                If Lon < MinLon Then MinLon = Lon
            End If

        End If
        
    Next n
    
    cError = "Calling FillBrowserMap"
    
    Call FillBrowserMap(aMapList, cError)
    
'    Call cmdShow_Click
            
    '    Set maxLoc = mapInc.ActiveMap.GetLocation(MaxLat + 0.003, MaxLon + 0.003)
    '    Set minLoc = mapInc.ActiveMap.GetLocation(MinLat - 0.003, MinLon - 0.003)
    '
    '    Call mapInc.ActiveMap.Union(Array(maxLoc, minLoc)).GoTo
    
    cmdIncMap.Enabled = True
    cmdSaveScript.Enabled = True
    
    Exit Sub
    
ErrorHandler:
    If Err.Number = cdlCancel Then
        Exit Sub
    Else
        Call AddToDebugList("[ERROR] Error occurred in cmdOpenScript of frmMain: " & Err.Number & ", " & Err.Description & " >> " & cError, lstDebug)
        '   MsgBox "Another error occurred in the file routine:" & Str(Err.Number) & " " & Err.Description, vbOKOnly
    End If
End Sub

Private Sub cmdPause_Click()
    Static StopTime As Variant
    Dim TimeStopped As Integer
    Dim WorkTime As Variant
    Dim n As Integer
    
    Static OldCaption As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdPause_Click")
    End If
    
    If Left(cmdPause.Caption, 6) = "&Pause" Then
        Call AddToDebugList("[SCRIPT] Pausing Script: " & gScriptFileName, lstDebug)
        gStopProcessing = True
        OldCaption = cmdPause.Caption
        cmdPause.Caption = "&Resume"
        StopTime = Now
'        tmUpdateAVL.Enabled = False
'        tmCADMsg.Enabled = False
    Else
        gStopProcessing = False
        TimeStopped = DateDiff("S", StopTime, Now)
        For n = lvEvents.ListItems.count To 1 Step -1
            WorkTime = CDate(lvEvents.ListItems.Item(n).SubItems(LVSort))
            lvEvents.ListItems.Item(n).SubItems(LVSort) = Format(DateAdd("S", TimeStopped, WorkTime), DATE_FORMAT)
            lvEvents.ListItems.Item(n).Text = Format(DateAdd("S", TimeStopped, WorkTime), "hh:nn:ss")
        Next n
        cmdPause.Caption = OldCaption
        tmUpdateAVL.Enabled = True
        '   tmCADMsg.Enabled = True
        Call AddToDebugList("[SCRIPT] Resuming Script: " & gScriptFileName, lstDebug)
        
        If txtRadio.Text = "Scenario Startup Paused: Initialization Complete. Press 'Resume' to continue" Then
            lstRadio.Clear                                                  '   when the script is resumed, clear the list and
            txtRadio.Text = ""                                              '   the radio box
        End If
    End If
    
    Call ProcessSimStatusRequest("LOCALHOST")

End Sub

Private Sub cmdRadioReset_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdRadioReset_Click")
    End If
    
    Call LoadRadioListView
    
    '   MsgBox "Reset the Radio Stuff back to saved values here", vbOKOnly + vbExclamation, "Stuff to test here"
End Sub

Private Sub cmdRadioSave_Click()
    Dim PropertyString As String
    Dim Test As Boolean
    Dim n As Integer
    Dim SettingSection As String
    Dim SettingItem As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdRadioSave_Click")
    End If
    
    SettingSection = "RADIO.CHANNELS"
    SettingItem = "RADIOSETUP"
    
    If opSector.Value Then
        PropertyString = "S"        '   channel assignments by Sector
    Else
        PropertyString = "D"        '   channel assignments by Division
    End If
    
    For n = 1 To lvRadioChannel.ListItems.count                     '   save all the configured channels
        If lvRadioChannel.ListItems.Item(n).SubItems(1) <> "" Then
            PropertyString = PropertyString & "|" & lvRadioChannel.ListItems.Item(n).Text & ";" _
                                & lvRadioChannel.ListItems.Item(n).SubItems(1) & ";" _
                                & lvRadioChannel.ListItems.Item(n).SubItems(2)
        End If
    Next
    
    Test = bsiPutSettings(SettingSection, SettingItem, PropertyString, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "RadioWaitToReceive", txtInterMessage.Text, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "RadioSpeechRate", txtSpeechRate.Text, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "RadioControlName", txtControlName.Text, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "SkipLeadingAlpha", IIf(chkSkipLeadingAlpha.Value = vbChecked, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    
    Call SetupRadioChannels     '   reload the new radio channel assignments
    
    Call SendToCADNoWait(Format(NP_BRIMAC_MESSAGE_CLASS, "0000"), Format(NP_BSI_READ_RADIO_SETTINGS, "0000"))
    
    '   MsgBox "Save the Radio Stuff to file here", vbOKOnly + vbExclamation, "Stuff to test here"

End Sub

Private Sub cmdSave_Click()
    Dim Test As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSave_Click")
    End If
    
    gAVLIPAddress = txtAVLIP.Text
    gAVLPath = txtAVLPath.Text
    gAVLPort = txtAVLPort.Text
    gAVLSegmentFactor = txtAVLSegments.Text
    gAVLDriveFactor = txtAVLDriveFactor.Text
    gNoAVLinStatus = txtNoAVStatus.Text
    
    gTransportPrio = txtTransPrio.Text
    gTransportPro = txtTransPro.Text
    gClosestPro = txtClosePro.Text
    
    gChuteMean = Val(txtChuteTime.Text)
    gChuteRange = Val(txtChuteRange.Text)
    
    gUseSourceSceneTime = opCalcScene.Value
    gSceneMean = Val(txtSceneTime.Text)
    gSceneRange = Val(txtSceneRange.Text)
    
    gUseSourceDestTime = opReadDestTime.Value
    gDestMean = Val(txtDestTime.Text)
    gDestRange = Val(txtDestRange.Text)
    
    gCellularAddress = UCase(txtCellAddress.Text)
    gCellularNumber = txtCellNumber.Text
    gCellularService = UCase(txtCellClass.Text)
    gUseANIALIDialog = opAniAliDialog.Value
    
    gUseSourceDest = opCalcDestination.Value
    gTransportPercent = Val(txtDestTrans.Text)
    gUseClosestPercent = Val(txtDestClose.Text)
    gTransportLimit = Val(txtDestLimit.Text)
    gSavePosition = IIf(chkSavePos.Value = vbChecked, "Y", "N")
    
    gUseXMLPort = IIf(chkUseXMLPort.Value = vbChecked, True, False)
    gForceAVL = IIf(chkForceAVL.Value = vbChecked, True, False)
    gForceMST = IIf(chkForceMST.Value = vbChecked, True, False)
    
    gDingWithMessage = IIf(chkMessageBell.Value = vbChecked, True, False)
    
    gReturnToBase = opReturnToBase.Value
    
    gLoadContext = IIf(chkSyncStartup.Value = vbChecked, True, False)
    gLoadHistory = IIf(chkHistory.Value = vbChecked, True, False)
    gCompression = Val(txtTimeWarp.Text)
    gDriveCompression = Val(txtDriveWarp.Text)
    
    gHospitalTypeIDs = txtHospitalTypes.Text
    
'    gTransportTypes = txt***insertsomethinghere***Types.Text
    
    opReadChute.Value = False
    opCalcChute.Value = True

    gUseSourceSceneTime = opReadScene.Value

    gUseSourceDestTime = opReadDestTime.Value

    gUseSourceDest = opReadDestination.Value
    
    Test = bsiPutSettings("AVL Settings", "AVLIPAddress", gAVLIPAddress, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "AVLPath", gAVLPath, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "AVLPort", gAVLPort, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "AVLSegmentDivisor", Str(gAVLSegmentFactor), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "AVLTimeDivisor", Str(gAVLDriveFactor), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "NoAVLStatus", gNoAVLinStatus, gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("AVL Settings", "ChuteTimeMean", Str(gChuteMean), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "ChuteTimeRange", Str(gChuteRange), gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("AVL Settings", "UseSourceSceneTime", IIf(gUseSourceSceneTime, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "SceneTimeMean", Str(gSceneMean), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "SceneTimeRange", Str(gSceneRange), gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("AVL Settings", "UseSourceDestTime", IIf(gUseSourceDestTime, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "DestTimeMean", Str(gDestMean), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "DestTimeRange", Str(gDestRange), gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("AVL Settings", "AVLAPPName", gAVLMDTAppName, gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("AVL Settings", "UseSourceDest", IIf(gUseSourceDest, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "TransportPercent", Str(gTransportPercent), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "UseClosestPercent", Str(gUseClosestPercent), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("AVL Settings", "DestinationTransportLimit", Str(gTransportLimit), gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("AVL Settings", "HospitalTypeID", txtHospitalTypes.Text, gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("AVL Settings", "AVLMapUpdateInterval", Str(gUnitUpdateInterval), gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("SystemDefaults", "DefaultTransportPriority", gTransportPrio, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "DefaultTransportProtocol", gTransportPro, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "ClosestTransportProtocol", gClosestPro, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "SaveStartupPosition", gSavePosition, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "UseXMLPort", IIf(gUseXMLPort, "1", "0"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "ForceAVL", IIf(gForceAVL, "1", "0"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "ForceMST", IIf(gForceMST, "1", "0"), gSystemDirectory & "\Simulator.INI")
    
    '    Test = bsiPutSettings("SystemPreferences", "TCPServer", txtIPCServer.Text, gSystemDirectory & "\System.INI")
    Test = bsiPutSettings("MAIN", "TCPServer", txtIPCServer.Text, App.Path & "\Share.INI")

    Test = bsiPutSettings("SystemDefaults", "DingWithMessage", IIf(gDingWithMessage, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("SystemDefaults", "LoadUnitContext", IIf(gLoadContext, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "LoadUnitHistory", IIf(gLoadHistory, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "TimeCompression", Format(gCompression, "0.0"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "DriveCompression", Format(gDriveCompression, "0.0"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "DataWareHouse", gDWConnectString, gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("SystemDefaults", "CellularAddress", gCellularAddress, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "CellularNumber", gCellularNumber, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "CellularService", gCellularService, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "UseANIALIDialog", IIf(gUseANIALIDialog, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "ReturnToBase", IIf(gReturnToBase, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    
End Sub

Private Sub cmdSaveClear_Click()
    Dim Test As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSaveClear_Click")
    End If
    
    gReturnToBase = opReturnToBase.Value
    Test = bsiPutSettings("SystemDefaults", "ReturnToBase", IIf(gReturnToBase, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("SystemDefaults", "UnitsCanClear", IIf(chkUnitsSetAvailable.Value = vbChecked, "TRUE", "FALSE"), gSystemDirectory & "\Simulator.INI")
    cmdSaveClear.Enabled = False

End Sub

Private Sub cmdSaveESRI_Click()
    Dim Test As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSaveESRI_Click")
    End If
    
    Test = bsiPutSettings("ESRI", "ESRINetworkDataset", txtESRINetworkDataset.Text, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("ESRI", "UseESRIResources", chkUseESRI.Value, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("ESRI", "AVLUpdateLimit", txtAVLUpdateLimit.Text, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("ESRI", "CostAttribute", cmbCostAttribute.Text, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("ESRI", "UseOneWayRestrictions", chkUseOneWay.Value, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("ESRI", "RoutingEngineVisible", chkEngineVisible.Value, gSystemDirectory & "\Simulator.INI")
    
    Test = bsiPutSettings("ESRI", "UseShareRoutingFolder", chkUseSharedFolder.Value, gSystemDirectory & "\Simulator.INI")
    Test = bsiPutSettings("ESRI", "SharedRoutingFolder", txtRoutingSharedFolder.Text, gSystemDirectory & "\Simulator.INI")
    
    cmdSaveESRI.Enabled = False
    
    gUseShareRoutingFolder = IIf(chkUseSharedFolder.Value = vbChecked, True, False)
    gSharedRoutingFolder = txtRoutingSharedFolder.Text
    
    gESRINetworkDataset = txtESRINetworkDataset.Text
    gESRINetworkCost = cmbCostAttribute.Text
    '   gUseESRIResources = IIf(chkUseESRI.Value = vbChecked, True, False)
    gUseESRIResources = True
    gUseOneWayRestrictions = IIf(chkUseOneWay.Value = vbChecked, True, False)
    
    Call SendToCADNoWait(Format(CN_CADNORTH_MSG, "0000"), CN_READSETTING & gSystemDirectory & "\Simulator.INI")
    
    Call InitMapWindow
    Call InitBrowseMapWindow
    
End Sub

Private Sub cmdSaveProfile_Click()
    Dim NameString As String
    Dim DriveScale As String
    Dim TempProfile As String
    Dim ProfileString As String
    Dim Test As Boolean
    Dim n As Integer
    Dim i As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSaveProfile_Click")
    End If
    
    Test = False
    
    For n = 0 To UBound(gDriveTime)
        If txtProfile.Text = gDriveTime(n).Name Then        '   check for duplicate names
            Test = True
        End If
    Next n
    
    If Not Test Then            '   no duplicate names found
    
        DriveScale = IIf(opMiles.Value, "M", "K")
    
        NameString = gDriveTime(0).Name & ";" & gDriveTime(0).SpeedScale
        ProfileString = Trim(Str(gDriveTime(0).Interstate)) & ";" _
                        & Trim(Str(gDriveTime(0).Highway)) & ";" _
                        & Trim(Str(gDriveTime(0).OtherHwy)) & ";" _
                        & Trim(Str(gDriveTime(0).Arterial)) & ";" _
                        & Trim(Str(gDriveTime(0).Street))
        
        For n = 1 To UBound(gDriveTime)
            NameString = NameString & "|" & gDriveTime(n).Name & ";" & gDriveTime(n).SpeedScale
            ProfileString = ProfileString & "|" & Trim(Str(gDriveTime(n).Interstate)) & ";" _
                            & Trim(Str(gDriveTime(n).Highway)) & ";" _
                            & Trim(Str(gDriveTime(n).OtherHwy)) & ";" _
                            & Trim(Str(gDriveTime(n).Arterial)) & ";" _
                            & Trim(Str(gDriveTime(n).Street))
        Next n
        
        If txtProfile.Text <> "" Then
            NameString = NameString & "|" & txtProfile.Text & ";" & DriveScale
            ProfileString = ProfileString & "|" & Trim(Str(ValidateSpeeds(Val(txtInterstate.Text)))) & ";" _
                            & Trim(Str(ValidateSpeeds(Val(txtLimAccess.Text)))) & ";" _
                            & Trim(Str(ValidateSpeeds(Val(txtHighway.Text)))) & ";" _
                            & Trim(Str(ValidateSpeeds(Val(txtArterial.Text)))) & ";" _
                            & Trim(Str(ValidateSpeeds(Val(txtStreet.Text))))
        End If

        Test = bsiPutSettings("DriveTimeProfiles", "ProfileNames", NameString, gSystemDirectory & "\Simulator.INI")
        Test = bsiPutSettings("DriveTimeProfiles", "ProfileSpeeds", ProfileString, gSystemDirectory & "\Simulator.INI")
    
        Call SetupDriveProfiles
        
    Else
        MsgBox "Duplicate Profile Name found", vbCritical + vbOKOnly, "Error!"
        txtProfile.SetFocus
    End If
    
End Sub

Private Sub cmdSaveScript_Click()
    Dim FileName As Variant
    Dim LogItem As String
    Dim Name As String
    Dim dFrom As Variant
    Dim dTo As Variant
    Dim n As Integer
    Dim M As Integer
    Dim PrevID As String
    Dim aPath As Variant
    Dim Test As Boolean
    Dim cFilterSettings As String
    
    Dim cError As String
    
    On Error GoTo ErrorHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSaveScript_Click")
    End If
    
    cError = "Save Dialog"
    
    dlgFile.CancelError = True
    dlgFile.InitDir = gSimScriptDir
    dlgFile.Filter = "Simulator Files (*.sim)|*.sim|All Files|*"
    dlgFile.ShowSave
    
    aPath = Split(dlgFile.FileName, "\")
    
    gSimScriptDir = aPath(0)
    For n = 1 To UBound(aPath) - 1
        gSimScriptDir = gSimScriptDir & "\" & aPath(n)
    Next n
    
    Test = bsiPutSettings("SystemDefaults", "ScriptDirectory", gSimScriptDir, gSystemDirectory & "\Simulator.INI")
        
    dFrom = CDate(Format(dtBrowse.Value, "MMM DD, YYYY ") & txtFrom.Text)
    dTo = CDate(Format(dtBrowse.Value, "MMM DD, YYYY ") & txtTo.Text)
    If dFrom > dTo Then
        dTo = DateAdd("D", 1, dTo)                  '   change to the next day (same time)
    End If
       
    Name = dlgFile.FileTitle
    
    FileName = FreeFile()
    
    cError = "Opening " & Name
    
    Open dlgFile.FileName For Output As #FileName
        
    cError = "Getting Filter Settings"
        
    cFilterSettings = GetFilterSettings()
    
    LogItem = Name
    LogItem = LogItem & "|" & Format(dFrom, "MMM DD, YYYY HH:NN")
    LogItem = LogItem & "|" & Format(dTo, "MMM DD, YYYY HH:NN")
    LogItem = LogItem & "|" & cFilterSettings                                                   '   & cmbDiv.Text
    LogItem = LogItem & "|" & txtNote.Text
    '        LogItem = LogItem & "|" & IIf(chkSync.Value = vbChecked, "Y", "N")
    '        LogItem = LogItem & "|" & IIf(chkHistory.Value = vbChecked, "Y", "N")
    
    Print #FileName, LogItem
    
    PrevID = ""
    
    For n = 1 To flxBrowse.Rows - 1
    
        If flxBrowse.TextMatrix(n, K_FLX_ID) <> " " _
                And CDate(flxBrowse.TextMatrix(n, K_FLX_DateTime)) >= dFrom _
                And CDate(flxBrowse.TextMatrix(n, K_FLX_DateTime)) <= dTo Then
            
            cError = "Writing Script, n = " & n
        
            PrevID = flxBrowse.TextMatrix(n, K_FLX_ID)
            LogItem = PrevID
            For M = K_FLX_DateTime To flxBrowse.Cols - 1
                LogItem = LogItem & "|" & flxBrowse.TextMatrix(n, M)
            Next M
            flxBrowse.Row = n
            flxBrowse.Col = K_FLX_ID
            LogItem = LogItem & "|" & Str(flxBrowse.CellForeColor)
            LogItem = LogItem & "|" & Str(flxBrowse.CellBackColor)
        
            Print #FileName, LogItem
        
        End If
    
    Next n
    
    Close #FileName
    
    Exit Sub

ErrorHandler:
    If Err.Number = cdlCancel Then
        Exit Sub
    Else
        Call AddToDebugList("[ERROR] Error occurred in cmdSaveScript of frmMain: " & Err.Number & ", " & Err.Description & " >> " & cError, lstDebug)
        '   MsgBox "Another error occurred in the file routine:" & Str(Err.Number) & " " & Err.Description, vbOKOnly
    End If
    Close #FileName
End Sub

Private Function GetFilterSettings() As String
    Dim cSettings As String
    Dim cJurisdictions As String
    Dim cDivisions As String
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetFilterSettings")
    End If
    
    cSettings = "a=" & cmbAgency.Text & ";"
    cSettings = cSettings & "cj=" & chkJurisdiction.Value & ";"
    
    For n = 0 To lstJurisdiction.ListCount - 1
        If lstJurisdiction.Selected(n) Then
            cJurisdictions = cJurisdictions & "'" & lstJurisdiction.List(n) & "',"
        End If
    Next n
    
    If Len(cJurisdictions) > 0 Then cJurisdictions = Left(cJurisdictions, Len(cJurisdictions) - 1)
    
    cSettings = cSettings & "j=" & cJurisdictions & ";"
    cSettings = cSettings & "cd=" & chkDivision.Value & ";"
    
    For n = 0 To lstDivision.ListCount - 1
        If lstDivision.Selected(n) Then
            cDivisions = cDivisions & "'" & lstDivision.List(n) & "',"
        End If
    Next n
    
    If Len(cDivisions) > 0 Then cDivisions = Left(cDivisions, Len(cDivisions) - 1)
    
    cSettings = cSettings & "d=" & cDivisions
    
    GetFilterSettings = cSettings
    
End Function

Private Sub RestoreFilterSettings(ByVal cFilterString As String)
    Dim aSettings() As String
    Dim cSettings As String
    Dim aItem() As String
    Dim cValue As String
    Dim n As Integer
    Dim i As Integer
    
    Dim cError As String

    On Error GoTo RestoreFilterSettings_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub RestoreFilterSettings")
    End If
    
    cError = "Start of RestoreFilterSettings"

    aSettings = Split(cFilterString, ";")
    
    If UBound(aSettings, 1) = 0 Then            '   old sim file, so we need to get the hierarchy from just the division
        cSettings = GetHierarchyFromDivision(aSettings(0))
        cError = "Recursive call of RestoreFilterSettings with '" & cSettings & "'"
        Call RestoreFilterSettings(cSettings)
    Else
        For n = 0 To UBound(aSettings, 1)
            aItem = Split(aSettings(n), "=")
            Select Case aItem(0)
                Case "a"
                    cmbAgency.Text = aItem(1)
                    chkJurisdiction.Enabled = True
                Case "cj"
                    chkJurisdiction.Value = Val(aItem(1))
                Case "j"
                    If aItem(1) <> "" Then
                        lstJurisdiction.Enabled = True
                        For i = 0 To lstJurisdiction.ListCount - 1
                            If InStr(1, aItem(1), lstJurisdiction.List(i)) > 0 Then
                                lstJurisdiction.Selected(i) = True
                            End If
                        Next i
                    End If
                Case "cd"
                    chkDivision.Value = Val(aItem(1))
                Case "d"
                    If aItem(1) <> "" Then
                        For i = 0 To lstDivision.ListCount - 1
                            If InStr(1, aItem(1), lstDivision.List(i)) > 0 Then
                                lstDivision.Selected(i) = True
                            End If
                        Next i
                    End If
            End Select
        Next n
        
    End If

    On Error GoTo 0
    Exit Sub

RestoreFilterSettings_ERH:

    Call AddToDebugList("[ERROR] Error in procedure RestoreFilterSettings of Form frmMain - " & Err.Description & " (" & Err.Number & ")", lstDebug)

End Sub

Private Function GetHierarchyFromDivision(ByVal cDivision As String)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim n As Integer
    Dim cError As String
    Dim cSettings As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetHierarchyFromDivision")
    End If
    
    If UCase(cDivision) = "ALL" Then
        cSettings = "a=<Show All Agencies>;cj=1;j=;cd=1;d="
    Else
        cSQL = "SELECT a.Agency_Type, j.Name FROM Division d JOIN Jurisdiction j on d.JurisdictionID = j.ID JOIN AgencyTypes a on j.AgencyID = a.ID "
        cSQL = cSQL & " WHERE d.DivName = '" & cDivision & "'"
        
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
        
        If nRows > 0 Then
            cSettings = "a=" & aSQL(0, 0) & ";"
            cSettings = cSettings & "cj=0;"
            cSettings = cSettings & "j='" & aSQL(1, 0) & "';"
            cSettings = cSettings & "cd=0;"
            cSettings = cSettings & "d=" & cDivision
        Else
            Call MsgBox("No Hierarchy found for Division='" & cDivision & "'. Fitler settings not restored." & vbCrLf & "SQL Error=" & cError, vbCritical + vbOKOnly, "Fitler Error")
            cSettings = "a=<Show All Agencies>;cj=1;j=;cd=1;d="
        End If
    End If
    
    GetHierarchyFromDivision = cSettings

End Function

Private Sub cmdScript_Click()
    Dim Cancel As Integer
    
    On Error GoTo UserCancel
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdScript_Click")
    End If
    
    If Not gbReadingScript Then
    
        dlgFile.CancelError = True
        
        dlgFile.InitDir = gSimScriptDir
        dlgFile.FileName = ""
        dlgFile.Filter = "Simulator Scripts (*.sim)|*.sim|All Files (*.*)|*.*"
        dlgFile.FilterIndex = 1
        dlgFile.flags = cdlOFNFileMustExist
        
        dlgFile.ShowOpen
        
        gScriptFileName = dlgFile.FileName
        
        If gScriptFileName <> "" Then
            tmUpdateAVL.Enabled = False         '   pause message processing until after all script events are created
            '   tmCADMsg.Enabled = False
            gInitializationInProgress = True
            gPhase2Complete = False
            Call AddToDebugList("[SCRIPT] Loading: " & gScriptFileName, lstDebug)
            gbReadingScript = True
            sBar.Panels(5).Text = "Running " & gScriptFileName
            cmdScript.Caption = "Cancel Script"
            gScript = FreeFile()
            Open gScriptFileName For Input As #gScript
            Call LoadScript
            Close #gScript
            
            cmdPause.Enabled = True
            
            Call cmdPause_Click                         '   pause script processing pending completion of the initialization phase
            
            tmUpdateAVL.Enabled = True          '   resume message processing now that the script events have been created
            '   tmCADMsg.Enabled = True
        End If
    Else
        gInitializationInProgress = False
        Cancel = MsgBox("Close script and purge ALL EVENTS?", vbExclamation + vbYesNo, "Cancel Current Script")
        If Cancel = vbYes Then
            gbReadingScript = False
            sBar.Panels(5).Text = ""
            cmdScript.Caption = "Load Script"
            Call PurgeAllEvents
            
            If cmdPause.Caption = "&Resume" Then
                Call cmdPause_Click                         '   click pause button to resume processing
                '    cmdPause.Caption = "&Pause"
                '    tmUpdateAVL.Enabled = True
                '    tmCADMsg.Enabled = True
            End If
        
        End If
    End If
    
    Exit Sub
    
UserCancel:

'    MsgBox "User pressed 'Cancel'"
    
End Sub

Private Sub cmdSelAll_Click()
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSelAll_Click")
    End If
    
    For n = 0 To lstAniAli.ListCount - 1
        lstAniAli.Selected(n) = True
    Next n
    
End Sub

Private Sub cmdSendAniAli_Click()
    '   Dim opin As MapPointCtl.Pushpin
    Dim ANIALIMsg As String
    Dim n As Long
    Dim Test As Boolean
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSendAniAli_Click")
    End If
    
    Test = bsiPutSettings("DEFAULT", "SHOWINCDETAILS", chkANIALIForm.Value, gSystemDirectory & "\Simulator.INI")
    
    '   If flxBrowse.Visible Then                  '   selected from the List (and actually selected)
        If flxBrowse.RowSel <> 0 Then
            If lstAniAli.SelCount > 0 Then
                For n = 0 To lstAniAli.ListCount - 1        '   send to all selected workstations
                    If lstAniAli.Selected(n) Then
                        ANIALIMsg = flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID) & "," _
                                    & lstAniAli.List(n) & "," _
                                    & IIf(gUseANIALIDialog, "0", "1")
                        Call SendToAVLNoWait(EV_E911, ANIALIMsg)
                        lstAniAli.Selected(n) = False
                    End If
                Next n
                If chkANIALIForm.Value = vbChecked Then
                    frmIncident.Caption = "VisiCAD ID " & flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID) & " incident details"
                    frmIncident.Show
                    Call WriteIncidentDetails(flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID))
                End If
'                Else
'                    MsgBox "No Incident was Selected in List", vbCritical + vbOKOnly, "Send Incident Error"
'                End If
            Else
                MsgBox "No Workstations were Selected in List", vbCritical + vbOKOnly, "Send Incident Error"
            End If
        Else
            MsgBox "No Incident was Selected in List", vbCritical + vbOKOnly, "Send Incident Error"
        End If
    '   Else                                        '   selected from the Map
        '    If TypeOf mapInc.ActiveMap.Selection Is Pushpin Then
        '        Set opin = mapInc.ActiveMap.Selection
        '        If lstAniAli.SelCount > 0 Then
        '            For n = 0 To lstAniAli.ListCount - 1
        '                If lstAniAli.Selected(n) Then
        '                    ANIALIMsg = opin.Name & "," _
        '                                & lstAniAli.List(n) & "," _
        '                                & IIf(gUseANIALIDialog, "0", "1")
        '                    Call SendToAVLNoWait(EV_E911, ANIALIMsg)
        '                    lstAniAli.Selected(n) = False
        '                End If
        '            Next n
        '            If chkANIALIForm.Value = vbChecked Then
        '                frmIncident.Caption = "VisiCAD ID " & opin.Name & " incident details"
        '                frmIncident.Show
        '                Call WriteIncidentDetails(opin.Name)
        '            End If
        '        Else
        '            MsgBox "No Workstations were Selected in List", vbCritical + vbOKOnly, "Send Incident Error"
        '        End If
        '    Else
        '        MsgBox "No Incident Selected on Map", vbCritical + vbOKOnly, "Send Incident Error"
        '    End If
    '   End If
    
CleanUP:
    
    Call cmdAniAli_Click        '   simulate the mouse click to close the Workstation window and stuff.
    
    Exit Sub
ERH:
    MsgBox "No Incident Selected on Map", vbCritical + vbOKOnly, "Send Incident Error"
    Resume CleanUP
End Sub

Private Sub cmdSendCellular_Click()
    '   Dim opin As MapPointCtl.Pushpin
    Dim ANIALIMsg As String
    Dim n As Long
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSendCellular_Click")
    End If
    
    '   If flxBrowse.Visible Then                  '   selected from the List (and actually selected)
        If flxBrowse.RowSel <> 0 Then
            If lstAniAli.SelCount > 0 Then
                For n = 0 To lstAniAli.ListCount - 1
                    If lstAniAli.Selected(n) Then
                        ANIALIMsg = flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID) & "," _
                                    & lstAniAli.List(n) & "," _
                                    & IIf(gUseANIALIDialog, "0", "1") & "," _
                                    & gCellularAddress & "," _
                                    & gCellularService & "," _
                                    & gCellularNumber
                        Call SendToAVLNoWait(EV_E911, ANIALIMsg)
                        lstAniAli.Selected(n) = False
                    End If
                Next n
                If chkANIALIForm.Value = vbChecked Then
                    frmIncident.Caption = "VisiCAD ID " & flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID) & " incident details"
                    frmIncident.Show
                    Call WriteIncidentDetails(flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID))
                End If
            Else
                MsgBox "No Workstations were Selected in List", vbCritical + vbOKOnly, "Send Incident Error"
            End If
        Else
            MsgBox "No Incident was Selected in List", vbCritical + vbOKOnly, "Send Incident Error"
        End If
    '    Else                                        '   selected from the Map
    '        If TypeOf mapInc.ActiveMap.Selection Is Pushpin Then
    '            Set opin = mapInc.ActiveMap.Selection
    '            If lstAniAli.SelCount > 0 Then
    '                For n = 0 To lstAniAli.ListCount - 1
    '                    If lstAniAli.Selected(n) Then
    '                        ANIALIMsg = opin.Name & "," _
    '                                    & lstAniAli.List(n) & "," _
    '                                    & IIf(gUseANIALIDialog, "0", "1") & "," _
    '                                    & gCellularAddress & "," _
    '                                    & gCellularService & "," _
    '                                    & gCellularNumber
    '                        Call SendToAVLNoWait(EV_E911, ANIALIMsg)
    '                        lstAniAli.Selected(n) = False
    '                    End If
    '                Next n
    '            Else
    '                MsgBox "No Workstations were Selected in List", vbCritical + vbOKOnly, "Send Incident Error"
    '            End If
    '        Else
    '            MsgBox "No Incident Selected on Map", vbCritical + vbOKOnly, "Send Incident Error"
    '        End If
    '    End If
    
CleanUP:
    
    Call cmdAniAli_Click        '   simulate the mouse click to close the Workstation window and stuff.
    
    Exit Sub
ERH:
    MsgBox "No Incident Selected on Map", vbCritical + vbOKOnly, "Send Incident Error"
    Resume CleanUP
End Sub

Private Sub WriteIncidentDetails(IncNum As String)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim n As Integer, i As Integer, x As Integer
    Dim pRow As Long
    Dim cString As String
    Dim PrevRow As Long
    Dim aWork As Variant
    Dim atemp As Variant
    Dim cError As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub WriteIncidentDetails")
    End If
    
    frmIncident.Cls
    
    Call frmIncident.FormPrint(1, 0, "Incident:", True)
    Call frmIncident.FormPrint(1, 1, "Address:", True)
    Call frmIncident.FormPrint(55, 1, "Apt:", True)
    Call frmIncident.FormPrint(1, 2, "Location:", True)
    Call frmIncident.FormPrint(55, 2, "City:", True)
    Call frmIncident.FormPrint(1, 3, "X-Streets:", True)
    Call frmIncident.FormPrint(1, 5, "Originator:", True)
    Call frmIncident.FormPrint(55, 5, "Phone:", True)
    Call frmIncident.FormPrint(1, 7, "Nature/Problem:", True)
    Call frmIncident.FormPrint(1, 9, "Comments:", True)
    
    cSQL = "Select address, location_name, city, apartment, cross_street, caller_name, call_back_phone, problem, caller_location_name " _
            & " From response_master_incident " _
            & " Where ID = " & IncNum
            
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    If nRows > 0 Then
        Call frmIncident.FormPrint(20, 0, IncNum)           '   incident number
        Call frmIncident.FormPrint(20, 1, aSQL(0, 0))       '   address
        Call frmIncident.FormPrint(65, 1, aSQL(3, 0))       '   apartment
        Call frmIncident.FormPrint(20, 2, aSQL(1, 0))       '   location
        Call frmIncident.FormPrint(65, 2, aSQL(2, 0))       '   city
        Call frmIncident.FormPrint(20, 3, aSQL(4, 0))       '   Minor x-streets
        Call frmIncident.FormPrint(20, 4, aSQL(8, 0))       '   Major x-streets (caller_location_name)
        Call frmIncident.FormPrint(20, 5, aSQL(5, 0))       '   originator
        Call frmIncident.FormPrint(65, 5, aSQL(6, 0))       '   phone number
        Call frmIncident.FormPrint(20, 7, aSQL(7, 0))       '   problem nature
    End If

    cSQL = "Select Additional_Comment_Group_ID, date_time, CONVERT(CHAR(1000),comment) " _
            & " From response_comments " _
            & " Where Master_Incident_ID = " & IncNum _
            & " Order by date_time "
            
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    pRow = 10
    PrevRow = 0
    
    If nRows > 0 Then
    
        ReDim aWork(0 To 1, 0 To nRows - 1)
        
        PrevRow = aSQL(0, n)
        i = 0
        For n = 0 To nRows - 1
            If aSQL(0, n) = PrevRow Then
                aWork(0, i) = aSQL(1, n)
                aWork(1, i) = aWork(1, i) & Trim(aSQL(2, n))
            Else
                i = i + 1
                PrevRow = aSQL(0, n)
                aWork(0, i) = aSQL(1, n)
                aWork(1, i) = aWork(1, i) & Trim(aSQL(2, n))
            End If
        Next n
        
        For n = 0 To i
            
            Call frmIncident.FormPrint(3, pRow, aWork(0, n))        '   comment date/time
            
            cString = aWork(1, n)
            
            atemp = Split(cString, vbCrLf)
            
            For x = 0 To UBound(atemp)
                cString = atemp(x)
                Do
                    Call frmIncident.FormPrint(20, pRow, Left(cString, 80))        '   comment
                    
                    pRow = pRow + 1
                    If Len(cString) > 80 Then
                        cString = Right(cString, Len(cString) - 80)
                    Else
                        cString = ""
                    End If
                Loop While Len(cString) > 0
            Next x
                    
        Next n
    End If

End Sub

Private Sub cmdSendInc_Click()
    '    Dim opin As MapPointCtl.Pushpin
    Dim n As Long
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSendInc_Click")
    End If
    
    '   If flxBrowse.Visible Then                  '   selected from the List (and actually selected)
        If flxBrowse.Row <> 0 Then
            Call InsertIncEvents(Val(flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID)))
        Else
            MsgBox "No Incident Selected in List", vbCritical + vbOKOnly, "Send Incident Error"
        End If
    '   Else                                        '   selected from the Map, Browser is synced
    '        If flxBrowse.Row <> 0 Then
    '            Call InsertIncEvents(Val(flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID)))
    '        Else
    '            MsgBox "No Incident Selected in List", vbCritical + vbOKOnly, "Send Incident Error"
    '        End If
    '    End If
    
    Exit Sub
ERH:
    MsgBox "No Incident Selected on Map", vbCritical + vbOKOnly, "Send Incident Error"
End Sub

Private Sub cmdShow_Click()
    Dim dFrom As Variant
    Dim dTo As Variant
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdShow_Click")
    End If
    
    '   test for browser contents before overwriting with new content
    
    Dim n As Integer
    For n = 1 To flxBrowse.Rows - 1
        If flxBrowse.TextMatrix(n, K_FLX_DateTime) <> "" Then
            If MsgBox("About to overwrite data!!", vbOKCancel + vbCritical + vbDefaultButton2, "WARNING!!") = vbCancel Then
                Exit Sub        '   oops! getting out!
            Else
                Exit For        '   ok! carrying on!
            End If
        End If
    Next
    
    If bsiValidateTime(txtFrom.Text) Then
        If bsiValidateTime(txtTo.Text) Then
            dFrom = CDate(Format(dtBrowse.Value, "mmm dd yyyy ") & txtFrom.Text)
            dTo = CDate(Format(dtBrowse.Value, "mmm dd yyyy ") & txtTo.Text)
            If dFrom < dTo Then
                '   valid information has been entered. Proceed with the process
                Call SetupIncidentBrowser
                '   Call FillIncidentBrowser(dFrom, dTo, cmbDiv.List(cmbDiv.ListIndex))
                Call FillIncidentBrowser(dFrom, dTo, "")
                cmdIncMap.Enabled = True
                cmdSaveScript.Enabled = True
            Else
                '   from later than to - to os on the next day
                dTo = DateAdd("D", 1, dTo)                  '   change to the next day (same time)
                Call SetupIncidentBrowser
                '   Call FillIncidentBrowser(dFrom, dTo, cmbDiv.List(cmbDiv.ListIndex))
                Call FillIncidentBrowser(dFrom, dTo, "")
                cmdIncMap.Enabled = True
                cmdSaveScript.Enabled = True
            End If
        Else
            '   they didn't fix the to time field, so put them back there
            txtTo.SetFocus
            txtTo.SelStart = 0
            txtTo.SelLength = Len(txtTo.Text)
            cmdIncMap.Enabled = False
            cmdSaveScript.Enabled = False
        End If
    Else
        '   they didn't fix the from time field, so put them back there
        txtFrom.SetFocus
        txtFrom.SelStart = 0
        txtFrom.SelLength = Len(txtFrom.Text)
        cmdIncMap.Enabled = False
        cmdSaveScript.Enabled = False
    End If
    
End Sub

Private Sub cmdSpeechConfig_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSpeechConfig_Click")
    End If
    
    frmSpeech.Top = (frmMain.Height + frmMain.Top) / 2 - frmSpeech.Height / 2
    frmSpeech.Left = (frmMain.Width + frmMain.Width) / 2 - frmSpeech.Width / 2
    Call SetTopMostWindow(frmSpeech.hWnd, True)
    frmSpeech.Show
    Call SetTopMostWindow(frmSpeech.hWnd, False)

End Sub

Private Sub cmdTransmit_Click()
    Dim UnitRecord As Vehicle
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdTransmit_Click")
    End If
    
    UnitRecord = GetUnitRecordbyName(lblRadioUnit.Caption)
    
    Call InsertRadioMessage(UnitRecord, STAT_FREEFORMTEXT, Now, , , txtTransmit.Text)
    
End Sub

Private Sub cmdUnitSelect_Click()
    If cmdUnitSelect.Caption = "Select All Units" Then
        cmdUnitSelect.Caption = "Clear Selection"
        Call SetAllUnitsChecked(True)
    Else
        cmdUnitSelect.Caption = "Select All Units"
        Call SetAllUnitsChecked(False)
    End If
End Sub

Private Sub SetAllUnitsChecked(bChecked As Boolean)
    Dim n As Long
    For n = 1 To lvUnitList.ListItems.count
        lvUnitList.ListItems.Item(n).Checked = bChecked
    Next n
End Sub

Private Sub cmdWAV_Click()
    Dim UnitRecord As Vehicle
    Dim cError As String
    
    On Error GoTo cmdWAV_Click_ErrorHandler
   
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdWAV_Click")
    End If
    
    cError = "Getting Unit Record for " & lblRadioUnit.Caption
    
    UnitRecord = GetUnitRecordbyName(lblRadioUnit.Caption)
        
    cError = "Open SoundByte Dialog"
    
    dlgFile.InitDir = gSoundByteDir
    dlgFile.CancelError = True
    dlgFile.Filter = "Sound Files (*.wav)|*.wav"
    dlgFile.ShowOpen
    
    cError = "Inserting SoundByte Event"
    
    Call InsertSoundEvent(UnitRecord, dlgFile.FileName, Now)
            
    Exit Sub
    
cmdWAV_Click_ErrorHandler:
    If Err.Number = cdlCancel Then
        Exit Sub
    Else
        Call AddToDebugList("[ERROR] Error occurred in cmdWAV_Click of frmMain: " & Err.Number & ", " & Err.Description & " >> " & cError, lstDebug)
        '   MsgBox "Another error occurred in the file routine:" & Str(Err.Number) & " " & Err.Description, vbOKOnly
    End If
End Sub

Private Sub cmdWSAdd_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdWSAdd_Click")
    End If
    
    txtWSName.Text = ""
    txtWSName.SetFocus
    cmdWSAdd.Enabled = False
    cmdWSCancel.Enabled = True
    cmdWSDelete.Enabled = False
    cmdWSSave.Enabled = False

End Sub

Private Sub cmdWSCancel_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdWSCancel_Click")
    End If
    
    If lstWorkstations.SelCount > 0 Then
        lstWorkstations.Selected(lstWorkstations.ListIndex) = False
    End If
    cmdWSAdd.Enabled = True
    cmdWSAdd.SetFocus
    cmdWSCancel.Enabled = False
    cmdWSDelete.Enabled = False
    cmdWSSave.Enabled = False

End Sub

Private Sub cmdWSDelete_Click()
    Dim WSString As String
    Dim n As Integer
    Dim Test As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdWSDelete_Click")
    End If
    
    If lstWorkstations.SelCount > 0 Then
        lstWorkstations.RemoveItem (lstWorkstations.ListIndex)
    End If
    
    If lstWorkstations.ListCount > 0 Then
        WSString = lstWorkstations.List(0)
        For n = 1 To lstWorkstations.ListCount - 1
            WSString = WSString & "|" & lstWorkstations.List(n)
        Next n
        Test = bsiPutSettings("Workstations", "WSList", WSString, gSystemDirectory & "\Simulator.INI")
    Else
        Test = bsiPutSettings("Workstations", "WSList", WSString, gSystemDirectory & "\Simulator.INI")
    End If
    
    Call SetupWorkstations
    cmdWSAdd.SetFocus

End Sub

Private Sub cmdWSSave_Click()
    Dim WSString As String
    Dim n As Integer
    Dim Test As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdWSSave_Click")
    End If
    
    If lstWorkstations.ListCount > 0 Then
        WSString = lstWorkstations.List(0)
        For n = 1 To lstWorkstations.ListCount - 1
            WSString = WSString & "|" & lstWorkstations.List(n)
        Next n
        WSString = WSString & "|" & UCase(txtWSName.Text)
        Test = bsiPutSettings("Workstations", "WSList", WSString, gSystemDirectory & "\Simulator.INI")
    Else
        WSString = UCase(txtWSName.Text)
        Test = bsiPutSettings("Workstations", "WSList", WSString, gSystemDirectory & "\Simulator.INI")
    End If
    
    If Not Test Then
        MsgBox "Error writing changes to Simulator.INI" & Chr(13) & "Please contact your System Administrator", vbCritical + vbOKOnly, "Error!"
    End If
    
    Call SetupWorkstations
    cmdWSAdd.SetFocus
    
End Sub

Private Sub dirSharedFolder_Change()
    txtRoutingSharedFolder.Text = dirSharedFolder.Path
    cmdSaveESRI.Enabled = True
End Sub

Private Sub drvSharedFolder_Change()
    dirSharedFolder.Path = drvSharedFolder.Drive
End Sub

Private Sub flxBrowse_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim n As Integer
    Dim SelectedInc As String
    '   Dim opin As MapPointCtl.Pushpin
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub flxBrowse_MouseUp")
    End If
    
    Do While flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_ID) = " "
        flxBrowse.Row = flxBrowse.Row - 1
        flxBrowse.RowSel = flxBrowse.Row
        flxBrowse.Col = 0
        flxBrowse.ColSel = flxBrowse.Cols - 1
    Loop
    
    Select Case Button
        Case vbLeftButton
            SelectedInc = flxBrowse.TextMatrix(flxBrowse.Row, K_FLX_ID)
            
            Call SyncMapWithBrowser(Val(SelectedInc))
    
            '   Set opin = mapInc.ActiveMap.Selection
            
            '   opin.BalloonState = geoDisplayNone
            '   opin.Highlight = False
            
            '   Set opin = mapInc.ActiveMap.FindPushpin(SelectedInc)
            
            '   opin.Select
            '   opin.BalloonState = geoDisplayBalloon
            
        Case vbRightButton
            flxBrowse.Row = flxBrowse.MouseRow
            flxBrowse.Col = 0
            flxBrowse.RowSel = flxBrowse.Row
            flxBrowse.ColSel = flxBrowse.Cols - 1
            
            SelectedInc = flxBrowse.TextMatrix(flxBrowse.Row, K_FLX_ID)
    
            '   If SelectedInc <> "" And SelectedInc <> "ID" Then
                mnuInsertSoundByte.Enabled = True
                mnuDeleteIncident.Enabled = False
                mnuEditProperties.Enabled = False
                If SelectedInc = "" Then
                    mnuInsertSoundByte.Caption = "Insert Sound Byte"
                    mnuPlaySoundByte.Enabled = False
                    mnuDeleteSoundByte.Enabled = False
                    mnuEditProperties.Enabled = False
                    mnuDeleteIncident.Enabled = False
                ElseIf SelectedInc <> "SOUND" And SelectedInc <> "ID" Then
                    mnuInsertSoundByte.Caption = "Insert Sound Byte"
                    mnuPlaySoundByte.Enabled = False
                    mnuDeleteSoundByte.Enabled = False
                    mnuEditProperties.Enabled = True
                    mnuDeleteIncident.Enabled = True
                ElseIf SelectedInc = "SOUND" Then
                    mnuInsertSoundByte.Caption = "Edit Sound Byte"
                    mnuPlaySoundByte.Enabled = True
                    mnuDeleteSoundByte.Enabled = True
                    mnuEditProperties.Enabled = False
                    mnuDeleteIncident.Enabled = False
                Else
                End If
                PopupMenu mnuBrowserPopUp
            '   Else
            '   End If
    End Select
    
    Exit Sub
    
ERH:
    Resume Next
End Sub

Private Sub flxVehicle_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub flxVehicle_Click")
    End If
    
    '    MsgBox "Unit " & flxVehicle.TextMatrix(flxVehicle.RowSel, 0) & " selected in unit list.", vbOKOnly + vbInformation, "Testing in progress"
    lblRadioUnit.Caption = flxVehicle.TextMatrix(flxVehicle.RowSel, 0)
    If txtTransmit.Text <> "" Then
        cmdTransmit.Enabled = True
    End If
    txtTransmit.SetFocus

End Sub

Private Sub Form_Load()
    Dim ParamString As String
    Dim nBytes As Integer
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim RetryCancel As Integer
    Dim WaitTime As Variant
    Dim LicenseBuffer As Variant
    '   Dim cnRoute As Object
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub Form_Load")
    End If
    
    '   Me.Show
    
    If bsiGetSettings("SystemDefaults", "DetailedDebugging", "X", gSystemDirectory & "\Simulator.INI") = "X" Then
        Call bsiPutSettings("SystemDefaults", "DetailedDebugging", "N", gSystemDirectory & "\Simulator.INI")
    End If
    
    gDetailedDebugging = (bsiGetSettings("SystemDefaults", "DetailedDebugging", "N", gSystemDirectory & "\Simulator.INI") = "Y")
    
    mnuDetailedDebugging.Checked = gDetailedDebugging
    
    If App.PrevInstance Then
        MsgBox "Another instance of cnSimulator is already open (probably minimized).", vbInformation + vbOKOnly, "Oops!"
        Call ExitApplication(False)
    End If
    
    Call mnuLogging_Click           '   turn on logging by default

    gLicensedVersion = ValidateLicense()
        
    gLoadTestEnabled = (gClientGeoBase = "LoadTest")    '   if this is true, Load Test mode is licensed and the simulator can be used for both training and load testing (for EACC NZ).  2014.07.11 BM
    gLoadTestRunning = False                            '   if this is true, we are actually running the Load Test and the simulator is not being used for training (for EACC NZ).  2014.07.11 BM
    
    mnuEnableLoadTesting.Visible = gLoadTestEnabled
    SSTabMain.TabCaption(6) = ""

    gSavePosition = bsiGetSettings("SystemDefaults", "SaveStartupPosition", "N", gSystemDirectory & "\Simulator.INI")
    If gSavePosition = "Y" Then
        gStartPosition = Split(bsiGetSettings("SystemDefaults", "StartupPosition", "0|0", gSystemDirectory & "\Simulator.INI"), "|")
        frmMain.Top = Val(gStartPosition(0))
        frmMain.Left = Val(gStartPosition(1))
    Else
        frmMain.Top = Screen.Height / 2 - frmMain.Height / 2
        frmMain.Left = Screen.Width / 2 - frmMain.Width / 2
    End If
    
    frmSplash.Left = frmMain.Left + (frmMain.Width / 2 - frmSplash.Width / 2)
    frmSplash.Top = frmMain.Top + (frmMain.Height / 2 - frmSplash.Height / 2)
    
    frmSplash.Show
    
    frmSplash.LoadStatus = "Reading Configuration"
'    frmSplash.LoadStatus.Refresh
    
    '    If Dir(INIPATH & "\System.INI") = "" Then
    '        RetryCancel = MsgBox("ERROR: Could not find the VisiCAD Q: Share! Please correct the problem and restart.", _
    '                                vbCritical + vbRetryCancel, "VisiCAD Directory Check")
    '        If RetryCancel = vbCancel Then
    '            End
    '        Else
    '            '   carry on with defaults
    '        End If
    '    End If
        
    If Dir(gSystemDirectory & "\System.INI") = "" Then
        RetryCancel = MsgBox("ERROR: Could not find the VisiCAD Folder Structure! Please correct the problem and restart.", _
                                vbCritical + vbRetryCancel, "VisiCAD Directory Check")
        If RetryCancel = vbCancel Then
            End
        Else
            '   carry on with defaults
        End If
    End If
    
    gAVLTaskID = -1
    gStopProcessing = False
    
    tmUpdateAVL.Enabled = False
    tmUpdateAVL.Interval = 500         '   update once per second
    
    tmCADMsg.Enabled = False
    tmCADMsg.Interval = 100             '   check often
    
    tmMapRefresh.Enabled = False
    
    ptrCADNext = 0                      '   pointers into the CAD Message Queue
    ptrCADFree = 0                      '   pointers into the CAD Message Queue
    
    ptrDisplay = 0                      '   pointers into Radio Buffer Queue
    ptrReceive = 0                      '   pointers into Radio Buffer Queue
    
    sBar.Style = sbrNormal
    sBar.Panels(3).Alignment = sbrRight
    sBar.Panels(4).Alignment = sbrRight
    
    frmMain.Width = frmRadio.Left + (frmMain.Width - frmMain.ScaleWidth)
    
    fmMain.Top = frmMain.ScaleTop
    fmMain.Width = (cmdExit.Width * 2) + (ButtonMargin * 3)
    fmMain.Left = frmMain.ScaleWidth - fmMain.Width
    
    fmCommand.Height = cmdExit.Height * 3 + 5.5 * ButtonMargin
    fmCommand.Left = fmMain.Left
    fmCommand.Top = frmMain.ScaleHeight - fmCommand.Height - sBar.Height
    fmCommand.Width = fmMain.Width
    
    fmMain.Height = fmCommand.Top
    
    SSTabMain.Top = frmMain.ScaleTop
    SSTabMain.Left = frmMain.ScaleLeft
    SSTabMain.Height = frmMain.ScaleHeight - sBar.Height
    SSTabMain.Width = frmMain.ScaleWidth - fmMain.Width - ButtonMargin
    
    SetupTabs.Top = SSTabMain.TabHeight + 2 * ButtonMargin
    SetupTabs.Left = ButtonMargin
    SetupTabs.Height = SSTabMain.Height - SetupTabs.Top - ButtonMargin
    SetupTabs.Width = SSTabMain.Width - 2 * ButtonMargin
    
    frmSplash.LoadStatus = "Loading Mapping for " & gClientGeoBase
'    frmSplash.LoadStatus.Refresh

    frmSplash.ZOrder 1
    
'    If gClientGeoBase = "North America" Then
'        map.NewMap geoMapNorthAmerica
'    Else
'        map.NewMap geoMapEurope
'    End If
    
    frmSplash.ZOrder 0
    
    frmSplash.LoadStatus = "Setting up forms"
'    frmSplash.LoadStatus.Refresh
    
'    map.Top = SSTabMain.TabHeight + ButtonMargin
'    map.Left = ButtonMargin
'    map.Height = SSTabMain.Height - SSTabMain.TabHeight - 2 * ButtonMargin
'    map.Width = SSTabMain.Width - 2 * ButtonMargin
    
'    Me.Show

    fmFilter.Top = fmMain.Top
    fmFilter.Left = fmMain.Left
    fmFilter.Height = fmMain.Height
    fmFilter.Width = fmMain.Width
    
    fmMap.Top = SSTabMain.TabHeight + ButtonMargin
    fmMap.Left = ButtonMargin
    fmMap.Height = SSTabMain.Height - SSTabMain.TabHeight - 2 * ButtonMargin
    fmMap.Width = SSTabMain.Width - 2 * ButtonMargin
    
    tbTools.Left = ButtonMargin
    tbTools.Top = 2 * ButtonMargin
    tbTools.Width = SSTabMain.Width - 3.5 * ButtonMargin
    
    winMap1.Top = tbTools.Height + 2 * ButtonMargin
    winMap1.Left = ButtonMargin
    winMap1.Height = fmMap.Height - tbTools.Height - 3 * ButtonMargin
    winMap1.Width = fmMap.Width - 2 * ButtonMargin
    
'    fmAbout.Top = SSTabMain.TabHeight + ButtonMargin
'    fmAbout.Left = ButtonMargin
'    fmAbout.Height = SSTabMain.Height - SSTabMain.TabHeight - 2 * ButtonMargin
'    fmAbout.Width = SSTabMain.Width - 2 * ButtonMargin

    cmdExit.Top = fmCommand.Height - cmdExit.Height - ButtonMargin
    cmdExit.Left = fmCommand.Width - cmdExit.Width - ButtonMargin
    
    cmdConnect.Top = cmdExit.Top
    cmdConnect.Left = cmdExit.Left - cmdConnect.Width - ButtonMargin
    
    cmdAniAli.Left = cmdExit.Left
    cmdAniAli.Top = cmdExit.Top - cmdExit.Height - ButtonMargin
    
    cmdSendInc.Left = cmdConnect.Left
    cmdSendInc.Top = cmdExit.Top - cmdAniAli.Height - ButtonMargin
    
    cmdScript.Left = cmdAniAli.Left
    cmdScript.Top = cmdAniAli.Top - cmdScript.Height - ButtonMargin
    
    cmdScript.Enabled = False           '   disabled at startup
    cmdSendInc.Enabled = False
    cmdAniAli.Enabled = False
    cmdPause.Enabled = False
    
    cmdPause.Top = cmdScript.Top
    cmdPause.Left = cmdSendInc.Left
    '    cmdPause.Enabled = False
    
    txtRadio.Top = 3 * ButtonMargin
    txtRadio.Left = ButtonMargin
    txtRadio.Width = fmMain.Width - (2 * ButtonMargin)
    txtRadio.Text = ""
    
    cmdMessage.Left = txtRadio.Left
    cmdMessage.Width = txtRadio.Width
    cmdMessage.Top = txtRadio.Top + txtRadio.Height + ButtonMargin
    
    ReDim gOldIncInfo(0 To 0)               '   initialize the structure for use later
    
    SSTabMain.Tab = 0
    
    frmSplash.LoadStatus = "Loading Simulator Settings"
    Call cmdDiscard_Click                   '   this is where all the globals get initialized
    Call SetupPriorities
    Call SetupDivisions
    Call LoadRadioAgencyCombo
    
    frmSplash.LoadStatus = "Configuring Browser"
    Call SetupBrowser
    
    frmSplash.LoadStatus = "Configuring Unit Queue"
    Call SetupUnitQ
    
    frmSplash.LoadStatus = "Configuring Logging"
    Call SetupLogView
    
    frmSplash.LoadStatus = "Configuring Events"
    Call SetupEventsView
    
    frmSplash.LoadStatus = "Configuring Setup Forms"
    Call SetupSetup
    Call SetupDriveProfiles
    Call SetupWorkstations
    
    frmSplash.LoadStatus = "Configuring Incident Browser"
    Call SetupIncidentBrowser
    Call SetUpFilterPane
    Call SetUpFilterPaneComponents

    frmSplash.LoadStatus = "Loading Speech Settings"
    Call SetupSpeechLists
    Call SetupRadioChannels
    
    frmSplash.LoadStatus = "Loading ESRI Mapping"
    Call InitMapWindow

    lblProfile.Top = cmdMessage.Top + cmdMessage.Height + ButtonMargin
    lblProfile.Left = cmdMessage.Left
    lblProfile.AutoSize = True
    cmbDriveProfile.Top = lblProfile.Top
    cmbDriveProfile.Left = lblProfile.Left + lblProfile.Width + ButtonMargin
    cmbDriveProfile.Width = fmMain.Width - lblProfile.Width - 3 * ButtonMargin
    
    lstRadio.Top = cmbDriveProfile.Top + cmbDriveProfile.Height + ButtonMargin
    lstRadio.Height = fmMain.Height - lstRadio.Top - ButtonMargin
    lstRadio.Left = cmdMessage.Left
    lstRadio.Width = cmdMessage.Width
    
    '    frmAniAli.Top = lstRadio.Top
    '    frmAniAli.Height = cmdPause.Top - frmAniAli.Top - ButtonMargin
    '    frmAniAli.Left = lstRadio.Left
    '    frmAniAli.Width = lstRadio.Width
    frmAniAli.Top = fmMain.Top
    frmAniAli.Height = fmMain.Height
    frmAniAli.Left = fmMain.Left
    frmAniAli.Width = fmMain.Width
    cmdSendAniAli.Width = (frmAniAli.Width - (4 * ButtonMargin)) / 4
    cmdSendAniAli.Top = frmAniAli.Height - cmdSendAniAli.Height - ButtonMargin
    cmdSendAniAli.Left = frmAniAli.Width - cmdSendAniAli.Width - ButtonMargin
    cmdSelAll.Width = (frmAniAli.Width - (4 * ButtonMargin)) / 2
    cmdSelAll.Top = cmdSendAniAli.Top
    cmdSelAll.Left = cmdSendAniAli.Left - cmdSelAll.Width - ButtonMargin
    cmdSendCellular.Width = cmdSendAniAli.Width
    cmdSendCellular.Top = cmdSendAniAli.Top
    cmdSendCellular.Left = cmdSelAll.Left - cmdSendCellular.Width - ButtonMargin
    chkANIALIForm.Top = cmdSendAniAli.Top - chkANIALIForm.Height - (2 * ButtonMargin)
    chkANIALIForm.Left = (frmAniAli.Width / 2) - (chkANIALIForm.Width / 2)
    lstAniAli.Top = 4 * ButtonMargin
    lstAniAli.Left = ButtonMargin
    lstAniAli.Height = chkANIALIForm.Top - lstAniAli.Top - ButtonMargin
    lstAniAli.Width = frmAniAli.Width - 2 * ButtonMargin
    
    frmAniAli.Visible = False
    
    '    frmRadio.Top = lstRadio.Top
    '    frmRadio.Height = cmdPause.Top - frmRadio.Top - ButtonMargin
    '    frmRadio.Left = lstRadio.Left
    '    frmRadio.Width = lstRadio.Width
    frmRadio.Top = fmMain.Top
    frmRadio.Height = fmMain.Height
    frmRadio.Left = fmMain.Left
    frmRadio.Width = fmMain.Width
    cmdTransmit.Width = (cmdExit.Width * 2 / 3) - ButtonMargin / 3
    cmdClearRadio.Width = cmdTransmit.Width
    cmdWAV.Width = cmdTransmit.Width
    cmdTransmit.Top = frmRadio.Height - cmdTransmit.Height - ButtonMargin
    cmdTransmit.Left = frmRadio.Width - cmdTransmit.Width - ButtonMargin
    cmdClearRadio.Top = cmdTransmit.Top
    cmdClearRadio.Left = cmdTransmit.Left - cmdTransmit.Width - ButtonMargin
    cmdWAV.Top = cmdTransmit.Top
    cmdWAV.Left = cmdClearRadio.Left - cmdWAV.Width - ButtonMargin
    txtTransmit.Top = 4 * ButtonMargin + lblRadioUnit.Height
    txtTransmit.Left = ButtonMargin
    txtTransmit.Height = cmdClearRadio.Top - txtTransmit.Top - ButtonMargin
    txtTransmit.Width = frmRadio.Width - 2 * ButtonMargin
    lblRadioUnit.Caption = ""
    txtTransmit.Text = ""

    frmRadio.Visible = False
    
    Call InitStatus
    
    frmSplash.LoadStatus = "Loading Locations"
    Call InitHospitals
    
    frmSplash.LoadStatus = "Loading Posts"
    Call InitPosts
    
    Call InitRoutingHashTable
    
    cmdConnect.Caption = "&Connect"
    
'    txtIPCServer.Enabled = False
    
    Load frmSpeech
    Call AddToDebugList("[Voice Fonts] Found" & Str(gMaxVoices), lstDebug)
    
    mnuBrowserPopUp.Visible = False
    
    If Not gLoadTestMode Then
        Call SetStartupVisibility
    Else
        Call SetLoadTestMode
    End If
    
    SSTabMain.TabEnabled(6) = gLoadTestMode
    
    frmMain.Caption = "cnSimulator v" & App.Major & "." & App.Minor & " (build " & App.Revision & ")"
    
    frmMain.Show
    
    DoEvents
    
    Unload frmSplash
    
    Exit Sub
    
ERH:
    Call MsgBox("Error occurred in Form.Load: " & Err.Number & ", " & Err.Description, vbOKOnly + vbCritical, "Critical Error")
    AddToDebugList "[ERROR] Error occurred in Form.Load: " & Err.Number & ", " & Err.Description
    Unload Me
End Sub

Private Sub SetLoadTestMode()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetLoadTestMode")
    End If

    With fmLoadTest
        .Top = frmMain.SSTabMain.TabHeight + ButtonMargin
        .Left = ButtonMargin
        .Height = (frmMain.SSTabMain.Height - frmMain.SSTabMain.TabHeight) - 150
        .Width = frmMain.SSTabMain.Width - 2 * ButtonMargin
    End With
    
    cmdLoadTest.Top = fmLoadTest.Height - cmdLoadTest.Height - 150
    cmdLoadTest.Left = fmLoadTest.Width - cmdLoadTest.Width - 150
    cmdUnitSelect.Top = cmdLoadTest.Top - cmdUnitSelect.Height - ButtonMargin
    cmdUnitSelect.Left = cmdLoadTest.Left
    
    SSTabMain.Visible = True
    fmLoadTest.Visible = True
    winMap1.Visible = False
    tbTools.Visible = False
    flxVehicle.Visible = False
    SetupTabs.Visible = False
    frmBrowser.Visible = False
    fmFilter.Visible = False

    lvEvents.Visible = False
    lvMain.Visible = False
    lstTCP.Visible = False
    lstDebug.Visible = False
    frmESRI.Visible = False
    lstRadio.Visible = False
    cmdSendInc.Enabled = False
    cmdAniAli.Enabled = False
    
    With lvUnitList
        .View = lvwReport
        .Top = 300
        .Left = 150
        .Height = cmdUnitSelect.Top - 450
        .Width = fmLoadTest.Width - 300
        .Font = "Arial"
        .FullRowSelect = True
        .MultiSelect = True
        .HideSelection = False
        .ColumnHeaders.Add 1, , "Unit Name", 1100
        .ColumnHeaders.Add 2, , "Status", 900
        .ColumnHeaders.Add 3, , "Current Location", 2500
        .ColumnHeaders.Add 4, , "Current City", 1300
        .ColumnHeaders.Add 5, , "Home Station", 2500
        .Checkboxes = True
        .GridLines = True
        .Sorted = True
        .SortKey = 1                '   Sort Key column
        .SortOrder = lvwAscending
    End With
    
    chkLoadTest.Top = cmdLoadTest.Top + 20
    chkLoadTest.Left = lvUnitList.Left
    txtLoadRadius.Top = cmdUnitSelect.Top
    txtLoadRadius.Left = chkLoadTest.Left
    lblLoadRadius.Top = txtLoadRadius.Top + 20
    lblLoadRadius.Left = txtLoadRadius.Left + txtLoadRadius.Width + ButtonMargin
    
    txtLoadRadius.Text = bsiGetSettings("LOADTEST", "LOADRADIUS", "10", gSystemDirectory & "\Simulator.INI")
    chkLoadTest.Value = IIf(bsiGetSettings("LOADTEST", "CREATEALLLOAD", "0", gSystemDirectory & "\Simulator.INI") = "1", vbChecked, vbUnchecked)
    
    SSTabMain.Tab = SSTabMain.Tabs - 1
    
    cmdLoadTest.Enabled = False
    
    Call FillLoadTestUnitList
    
    If cmdConnect.Caption = "&Connect" Then
        Call cmdConnect_Click
    End If
    
    cmdScript.Enabled = False
    
End Sub

Private Sub SetStartupVisibility()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetStartupVisibility")
    End If
    
    winMap1.Visible = True
    tbTools.Visible = True
    flxVehicle.Visible = False
    SetupTabs.Visible = False
    frmBrowser.Visible = False
    fmFilter.Visible = False
    fmLoadTest.Visible = False

'            map.Visible = True
    lvEvents.Visible = False
    lvMain.Visible = False
    lstTCP.Visible = False
    lstDebug.Visible = False
    frmESRI.Visible = False
    lstRadio.Visible = False
    cmdSendInc.Enabled = False
    cmdAniAli.Enabled = False

End Sub

Private Function FixConnectString() As String
    Dim Temp As Variant
    Dim sTemp As String
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function FixConnectString")
    End If
    
    sTemp = bsiGetSettings("SystemPreferences", "ODBCPassword", "UID=tritech;PWD=europa;DSN=system;", gSystemDirectory & "\System.INI")
    Temp = Split(sTemp, ";")
    sTemp = ""
    
    For n = 0 To UBound(Temp)
        If Temp(n) <> "" And Temp(n) <> "ODBC" Then
            sTemp = sTemp & Temp(n) & ";"
        End If
    Next n
        
    FixConnectString = sTemp
    
End Function

Private Sub SetupLogView()
'
'       Replaces SetupTCPLogView() and SetupDebugLog()                  BM 012204
'
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupLogView")
    End If
    
    With lvMain
        .View = lvwReport
        '        .Top = frmMain.TabStrip1.ClientTop
        '        .Left = frmMain.TabStrip1.ClientLeft
        '        .Height = frmMain.TabStrip1.ClientHeight / 2 + 250
        '        .Width = frmMain.TabStrip1.ClientWidth
        .Top = frmMain.SSTabMain.TabHeight + ButtonMargin
        .Left = ButtonMargin
        .Height = (frmMain.SSTabMain.Height - frmMain.SSTabMain.TabHeight) / 2 + 250
        .Width = frmMain.SSTabMain.Width - 2 * ButtonMargin
        .Font = "Courier New"
        .FullRowSelect = True
        .ColumnHeaders.Add 1, , "Time", 1000
        .ColumnHeaders.Add 2, , "Type", 800
        .ColumnHeaders.Add 3, , "Size", 800
        .ColumnHeaders.Add 4, , "IPC Message Type", 4000
        .ColumnHeaders.Add 5, , "Message Body", 30000
        .GridLines = True
    End With
    
    With lstTCP
        .Top = frmMain.SSTabMain.TabHeight + ButtonMargin
        .Left = ButtonMargin
        .Height = (frmMain.SSTabMain.Height - frmMain.SSTabMain.TabHeight) / 2 + 250
        .Width = frmMain.SSTabMain.Width - 2 * ButtonMargin
        .Font = "Courier New"
        .Clear
    End With

    With lstDebug
        '        .Top = frmMain.TabStrip1.ClientTop + frmMain.lvMain.Height + ButtonMargin
        '        .Left = frmMain.TabStrip1.ClientLeft
        '        .Height = frmMain.TabStrip1.ClientHeight - .Top + 600
        '        .Width = frmMain.TabStrip1.ClientWidth
        .Top = frmMain.SSTabMain.TabHeight + frmMain.lstTCP.Height + 2 * ButtonMargin
        .Left = ButtonMargin
        .Height = (frmMain.SSTabMain.Height - frmMain.SSTabMain.TabHeight) - .Top + 400
        .Width = frmMain.SSTabMain.Width - 2 * ButtonMargin
        .Font = "Courier New"
        .Clear
    End With

End Sub

Private Sub SetupUnitQ()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupUnitQ")
    End If
    
    With flxVehicle
        .Top = SSTabMain.TabHeight + ButtonMargin
        .Left = ButtonMargin
        .Height = SSTabMain.Height - SSTabMain.TabHeight - 2 * ButtonMargin
        .Width = SSTabMain.Width - 2 * ButtonMargin
        .FixedCols = 0
        .FixedRows = 1
        .Cols = 15
        .Rows = 40
    End With

End Sub

Private Sub SetupBrowser()
    On Error GoTo ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupBrowser")
    End If
    
    With frmBrowser
        .Top = SSTabMain.TabHeight + ButtonMargin
        .Left = ButtonMargin
        .Height = SSTabMain.Height - SSTabMain.TabHeight - 2 * ButtonMargin
        .Width = SSTabMain.Width - 2 * ButtonMargin
    End With

    cmdSaveScript.Top = frmBrowser.Height - cmdSaveScript.Height - ButtonMargin
    cmdSaveScript.Left = frmBrowser.Width - cmdSaveScript.Width - ButtonMargin
'    cmdClear.Top = cmdSaveScript.Top
'    cmdClear.Left = cmdSaveScript.Left - cmdOpenScript.Width - ButtonMargin
    cmdOpenScript.Top = cmdSaveScript.Top
    cmdOpenScript.Left = cmdSaveScript.Left - cmdOpenScript.Width - ButtonMargin
    
    lblNote.Left = ButtonMargin
    lblNote.Top = cmdSaveScript.Top + cmdSaveScript.Height - lblNote.Height
    txtNote.Top = cmdSaveScript.Top
    txtNote.Left = lblNote.Left + lblNote.Width
    txtNote.Width = cmdOpenScript.Left - txtNote.Left - ButtonMargin
    
    '    dtBrowse.Top = cmdSaveScript.Top - dtBrowse.Height - ButtonMargin
    '    dtBrowse.Left = 2 * ButtonMargin
    '    lblFrom.Left = dtBrowse.Left + dtBrowse.Width + ButtonMargin
    '    lblFrom.Top = cmdSaveScript.Top - lblFrom.Height - ButtonMargin
    '    txtFrom.Left = lblFrom.Left + lblFrom.Width + ButtonMargin
    '    txtFrom.Top = cmdSaveScript.Top - txtFrom.Height - ButtonMargin
    '    lblTo.Left = txtFrom.Left + txtFrom.Width + ButtonMargin
    '    lblTo.Top = cmdSaveScript.Top - lblTo.Height - ButtonMargin
    '    txtTo.Left = lblTo.Left + lblTo.Width + ButtonMargin
    '    txtTo.Top = cmdSaveScript.Top - txtTo.Height - ButtonMargin
    '    lblDiv.Left = txtTo.Left + txtTo.Width + ButtonMargin
    '    lblDiv.Top = cmdSaveScript.Top - lblDiv.Height - ButtonMargin
    '   cmbDiv.Left = lblDiv.Left + lblDiv.Width + ButtonMargin
    '   cmbDiv.Top = cmdSaveScript.Top - cmbDiv.Height - ButtonMargin
    '   cmbDiv.Width = cmdOpenScript.Left + cmdOpenScript.Width - cmbDiv.Left
    '    cmdShow.Left = cmdClear.Left
    '    cmdShow.Top = cmdClear.Top - cmdSave.Height - ButtonMargin
    '    cmdIncMap.Top = cmdShow.Top
    '    cmdIncMap.Left = cmdSaveScript.Left
    
    txtNote.Text = ""
    txtFrom.Text = "00:00"
    txtTo.Text = "23:59"
    
    dtBrowse.Value = CDate(Format(Now, "MMM DD YYYY 00:00:00"))
    
    cmdIncMap.Enabled = False
    cmdSaveScript.Enabled = False
    
    With flxBrowse
        .Top = 3 * ButtonMargin
        .Left = ButtonMargin
        .Height = txtNote.Top - 4 * ButtonMargin
        .Width = frmBrowser.Width - 2 * ButtonMargin
        .FixedCols = 0
        .FixedRows = 1
        .SelectionMode = flexSelectionByRow
        .FocusRect = flexFocusNone
        .Cols = 15
        .Rows = 40
        .Row = 0
        .ColAlignment(K_FLX_ID) = flexAlignRightCenter
        .ColAlignment(K_FLX_Address) = flexAlignLeftCenter
        .ColAlignment(K_FLX_City) = flexAlignLeftCenter
    End With
    
    frmSplash.ZOrder 1
    
    If Not gUseESRIResources Then
    '        If gClientGeoBase = "North America" Then
    '            mapInc.NewMap geoMapNorthAmerica
    '        Else
    '            mapInc.NewMap geoMapEurope
    '        End If
        
        frmSplash.ZOrder 0
    '
    '        With mapInc
    '            .Top = flxBrowse.Top
    '            .Left = flxBrowse.Left
    '            .Height = flxBrowse.Height
    '            .Width = flxBrowse.Width
    '            .Visible = False
    '        End With
    Else
        '   mapInc.Visible = False
        With tbBrowse
            .Top = flxBrowse.Top
            .Left = flxBrowse.Left
            .Width = flxBrowse.Width
            .Visible = False
        End With
        With incMap
            .Top = flxBrowse.Top + tbBrowse.Height - ButtonMargin
            .Left = flxBrowse.Left
            .Width = flxBrowse.Width
            .Height = flxBrowse.Height - tbBrowse.Height + ButtonMargin
            .Visible = False
        End With
        Call InitBrowseMapWindow
    End If
    
    frmBrowser.Visible = False
    
    Exit Sub
    
ERH:
    Call MsgBox("Error occurred in SetupBrowser:" & Err.Number & ", " & Err.Description, vbOKOnly + vbCritical, "Critical Error")
    AddToDebugList "[ERROR] Error occurred in SetupBrowser: " & Err.Number & ", " & Err.Description
    Unload Me
End Sub

Private Sub SetupIncidentBrowser()

    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupIncidentBrowser")
    End If
    
    flxBrowse.Clear
    flxBrowse.SelectionMode = flexSelectionByRow

    flxBrowse.FixedCols = 0
    flxBrowse.Cols = 20
    
    flxBrowse.TextMatrix(0, K_FLX_ROLL) = "[+]"
    flxBrowse.TextMatrix(0, K_FLX_ID) = "ID"
    flxBrowse.TextMatrix(0, K_FLX_DateTime) = "Date/Time"
    flxBrowse.TextMatrix(0, K_FLX_Address) = "Address"
    flxBrowse.TextMatrix(0, K_FLX_City) = "City"
    flxBrowse.TextMatrix(0, K_FLX_Quad) = "Quad"
    flxBrowse.TextMatrix(0, K_FLX_Problem) = "Problem"
    flxBrowse.TextMatrix(0, K_FLX_Unit) = "Unit"
    flxBrowse.TextMatrix(0, K_FLX_UnitType) = "Unit Type"
    flxBrowse.TextMatrix(0, K_FLX_CallTaker) = "Call Taker"
    flxBrowse.TextMatrix(0, K_FLX_Destination) = "Destination"
    flxBrowse.TextMatrix(0, K_FLX_Assigned) = "Assigned"
    flxBrowse.TextMatrix(0, K_FLX_Enroute) = "Enroute"
    flxBrowse.TextMatrix(0, K_FLX_AtScene) = "AtScene"
    flxBrowse.TextMatrix(0, K_FLX_Transport) = "Transport"
    flxBrowse.TextMatrix(0, K_FLX_AtDest) = "AtDest"
    flxBrowse.TextMatrix(0, K_FLX_AvailAtDest) = "AvailAtDest"
    flxBrowse.TextMatrix(0, K_FLX_Available) = "Available"
    flxBrowse.TextMatrix(0, K_FLX_Latitude) = "Latitude"
    flxBrowse.TextMatrix(0, K_FLX_Longitude) = "Longitude"
    
    flxBrowse.ColAlignment(K_FLX_ROLL) = flexAlignCenterCenter

    flxBrowse.ColWidth(K_FLX_ROLL) = 350                             '   [+] header
    flxBrowse.ColWidth(K_FLX_DateTime) = 2 * flxBrowse.ColWidth(19)      '   Date/Time
    flxBrowse.ColWidth(K_FLX_Address) = 2 * flxBrowse.ColWidth(19)      '   Address
    flxBrowse.ColWidth(K_FLX_Quad) = 0.5 * flxBrowse.ColWidth(19)    '   Quadrant
    flxBrowse.ColWidth(K_FLX_Problem) = 2 * flxBrowse.ColWidth(19)      '   Problem
    flxBrowse.ColWidth(K_FLX_CallTaker) = 1.5 * flxBrowse.ColWidth(19)    '   Call Taker
    flxBrowse.ColWidth(K_FLX_Destination) = 2 * flxBrowse.ColWidth(19)     '   Destination
    
    flxBrowse.Row = 0
    
    '    If mapInc.ActiveMap.DataSets.Item(1).Name = "My Pushpins" Then
    '        mapInc.ActiveMap.DataSets.Item(1).Delete
    '    End If
    
    Exit Sub

ERH:

    '   MsgBox "Err:" & Str(Err.Number) & "  Description: " & Err.Description, vbInformation + vbOKOnly, "SetupIncidentBrowser"

End Sub

Private Sub SetUpFilterPane()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim LastAgency As String
    
    On Error GoTo SetUpFilterPane_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetUpFilterPane")
    End If
    
    cSQL = "SELECT Agency_Type FROM AgencyTypes"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        cmbAgency.Clear
        cmbAgency.AddItem "<Show All Agencies>"
        For n = 0 To nRows - 1
            cmbAgency.AddItem aSQL(0, n)
        Next n
        LastAgency = bsiGetSettings("SystemDefaults", "LastAgency", "***NOTDEFINED***", gSystemDirectory & "\Simulator.INI")
        cmbAgency.Text = IIf(LastAgency = "***NOTDEFINED***", cmbAgency.List(0), LastAgency)
    Else
        Call MsgBox("No Agencies returned. Check ODBC Settings.", vbCritical + vbOKOnly, "Loading Agencies")
        cmbAgency.Clear
        cmbAgency.AddItem "<LOAD ERROR>"
    End If
    
    chkJurisdiction.Value = vbChecked
    chkJurisdiction.Enabled = False
    
    Exit Sub
    
SetUpFilterPane_ERH:
    AddToDebugList ("[ERROR] in SetUpFilterPane Loading Agency Filter (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub SetUpFilterPaneComponents()
    Dim PaneHeight As Long
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetUpFilterPaneComponents")
    End If
    
    cmdClear.Top = fmFilter.Height - cmdClear.Height - ButtonMargin
    cmdClear.Left = fmFilter.Width - cmdClear.Width - ButtonMargin
    
    cmdIncMap.Top = cmdClear.Top
    cmdIncMap.Left = cmdClear.Left - cmdIncMap.Width - ButtonMargin

    cmdShow.Top = cmdClear.Top
    cmdShow.Left = cmdIncMap.Left - cmdShow.Width - ButtonMargin
    
    PaneHeight = cmdClear.Top - (cmbAgency.Top + cmbAgency.Height)
    cmbAgency.Width = lstJurisdiction.Width
    
    chkJurisdiction.Top = cmbAgency.Top + cmbAgency.Height + ButtonMargin
    lstJurisdiction.Top = chkJurisdiction.Top + chkJurisdiction.Height + ButtonMargin
    lstJurisdiction.Height = PaneHeight / 2 - 2 * chkJurisdiction.Height - 2 * ButtonMargin
    
    chkDivision.Top = lstJurisdiction.Top + lstJurisdiction.Height + ButtonMargin
    lstDivision.Top = chkDivision.Top + chkDivision.Height + ButtonMargin
    lstDivision.Height = cmdClear.Top - lstDivision.Top - 2 * ButtonMargin
    
End Sub

Private Sub FillJurisdictionList()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim LastAgency As String
    
    On Error GoTo FillJurisdictionList_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub FillJurisdictionList")
    End If
    
    cSQL = "SELECT j.Name FROM Jurisdiction j JOIN AgencyTypes a ON j.AgencyID = a.ID WHERE a.Agency_Type = '" & cmbAgency.Text & "'"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        lstJurisdiction.Clear
        For n = 0 To nRows - 1
            lstJurisdiction.AddItem aSQL(0, n)
        Next n
    Else
        Call MsgBox("No Jurisdictions returned for Agency '" & cmbAgency.Text & "'. Check ODBC Settings." & vbCrLf & "Error: " & cError, vbCritical + vbOKOnly, "Loading Jurisdictions")
        lstJurisdiction.Clear
        lstJurisdiction.AddItem "<LOAD ERROR>"
    End If
    
    Exit Sub

FillJurisdictionList_ERH:
    AddToDebugList ("[ERROR] in FillJurisdictionList Loading Jurisdiction List (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub FillDivisionList()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim cJurisdictions As String
    
    On Error GoTo FillDivisionList_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub FillDivisionList")
    End If
    
    For n = 0 To lstJurisdiction.ListCount - 1
        If lstJurisdiction.Selected(n) Then
            cJurisdictions = cJurisdictions & "'" & lstJurisdiction.List(n) & "',"
        End If
    Next n
    
    cJurisdictions = Left(cJurisdictions, Len(cJurisdictions) - 1)
    
    cSQL = "SELECT d.DivName FROM Division d WHERE d.JurisdictionID IN (Select ID FROM Jurisdiction WHERE Name IN (" & cJurisdictions & "))"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        lstDivision.Clear
        For n = 0 To nRows - 1
            lstDivision.AddItem aSQL(0, n)
        Next n
    Else
        Call MsgBox("No Divisions returned. Check ODBC Settings." & vbCrLf & "Error: " & cError, vbCritical + vbOKOnly, "Loading Divisions")
        lstDivision.Clear
        lstDivision.AddItem "<LOAD ERROR>"
    End If
    
    Exit Sub
    
FillDivisionList_ERH:
    AddToDebugList ("[ERROR] in FillDivisionList Loading Division List (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Sub FillLoadTestUnitList()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    Dim iItem As ListItem
    Dim cBuffer As String
    Dim aCheckedUnits() As String
    
    On Error GoTo FillLoadTestUnitList_ERH
    
    cBuffer = bsiGetSettings("LOADTEST", "SELECTEDUNITS", "***", gSystemDirectory & "\Simulator.INI")
    
    If cBuffer <> "***" Then cmdLoadTest.Enabled = True         '   if we have saved checked units, then we can enable the run load test command button
    If cBuffer <> "***" Then cmdUnitSelect.Caption = "Clear Selection"     '   if we have saved checked units, then we can enable the clear selection command button
    
    aCheckedUnits = Split(cBuffer, ";")

    cSQL = "SELECT u.Code, s.Description, v.Current_Location, v.Current_City, st.Name" _
            & " FROM Vehicle v RIGHT JOIN Stations st on v.Post_Station_ID = st.ID" _
            & " JOIN Unit_Names u on v.UnitName_ID = u.ID" _
            & " JOIN Status s on v.Status_ID = s.ID" _
            & " Where v.Status_ID <> " & Str(STAT_OffDuty) _
            & " ORDER BY u.Code"
        
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        lvUnitList.ListItems.Clear
        For n = 0 To nRows - 1
            Set iItem = lvUnitList.ListItems.Add(, "K" & aSQL(0, n), aSQL(0, n))
            iItem.SubItems(1) = IIf(IsNull(aSQL(1, n)), "", aSQL(1, n))
            iItem.SubItems(2) = IIf(IsNull(aSQL(2, n)), "", aSQL(2, n))
            iItem.SubItems(3) = IIf(IsNull(aSQL(3, n)), "", aSQL(3, n))
            iItem.SubItems(4) = IIf(IsNull(aSQL(4, n)), "", aSQL(4, n))
            If CheckForCheckedUnit(aCheckedUnits, CStr(aSQL(0, n))) Then iItem.Checked = True
        Next n
    End If
    
    Exit Sub
    
FillLoadTestUnitList_ERH:
    AddToDebugList ("[ERROR] in FillLoadTestUnitList (" & Err.Number & ") - " & Err.Description & " - " & cError)
End Sub

Private Function CheckForCheckedUnit(aUnits() As String, cUnit As String) As Boolean
    Dim n As Long
    
    On Error GoTo CheckForCheckedUnit_ERH
    
    CheckForCheckedUnit = False
    
    For n = 0 To UBound(aUnits, 1)
        If aUnits(n) = cUnit Then
            CheckForCheckedUnit = True
            Exit For
        End If
    Next n
    
    Exit Function

CheckForCheckedUnit_ERH:
    AddToDebugList ("[ERROR] in CheckForCheckedUnit (" & Err.Number & ") - " & Err.Description)
End Function

Private Sub lvUnitList_BeforeLabelEdit(Cancel As Integer)
    Cancel = 1
End Sub

Private Sub lvUnitList_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Dim PrevKey As Integer
    
    PrevKey = lvUnitList.SortKey
    
    lvUnitList.SortKey = ColumnHeader.Index - 1
    
    If lvUnitList.SortKey <> PrevKey Then
        If lvUnitList.SortKey = 0 Then
            lvUnitList.SortOrder = lvwDescending
        Else
            lvUnitList.SortOrder = lvwAscending
        End If
    Else
        If lvUnitList.SortOrder <> lvwAscending Then
            lvUnitList.SortOrder = lvwAscending
        Else
            lvUnitList.SortOrder = lvwDescending
        End If
    End If

End Sub

Private Sub lvUnitList_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Static CheckedCount As Integer
    Dim cUnits As String
    Dim aUnits() As String
    
    If Item.Checked Then
        CheckedCount = CheckedCount + 1
        cmdLoadTest.Enabled = True
    Else
        CheckedCount = CheckedCount - 1
        If CheckedCount <= 0 Then
            cmdLoadTest.Enabled = False
        End If
    End If
    
    If CheckedCount > 0 Then                        '   save the checked units each time a change is made
        aUnits = SaveCheckedUnits(cUnits)
    End If
    
End Sub

Private Sub SetupTCPLogView()
'
'       Replaced by SetupLogView() - incorporated on a single tab       BM 012204
'
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupTCPLogView")
    End If
    
    With lvMain
        .View = lvwReport
        '        .Top = frmMain.TabStrip1.ClientTop
        '        .Left = frmMain.TabStrip1.ClientLeft
        '        .Height = frmMain.TabStrip1.ClientHeight
        '        .Width = frmMain.TabStrip1.ClientWidth
        .Top = frmMain.SSTabMain.TabHeight + ButtonMargin
        .Left = ButtonMargin
        .Height = frmMain.SSTabMain.Height - frmMain.SSTabMain.TabHeight - 2 * ButtonMargin
        .Width = frmMain.SSTabMain.Width - 2 * ButtonMargin
        .Font = "Courier New"
        .FullRowSelect = True
        .ColumnHeaders.Add 1, , "Time", 1000
        .ColumnHeaders.Add 2, , "Type", 800
        .ColumnHeaders.Add 3, , "Size", 800
        .ColumnHeaders.Add 4, , "Message Type", 4000
        .ColumnHeaders.Add 5, , "Message Body", 30000
        .GridLines = True
    End With

    With lstTCP
        .Top = frmMain.SSTabMain.TabHeight + ButtonMargin
        .Left = ButtonMargin
        .Height = frmMain.SSTabMain.Height - frmMain.SSTabMain.TabHeight - 2 * ButtonMargin
        .Width = frmMain.SSTabMain.Width - 2 * ButtonMargin
        .Font = "Courier New"
    End With

End Sub

Private Sub SetupDebugLog()
'
'       Replaced by SetupLogView() - incorporated on a single tab       BM 012204
'
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupDebugLog")
    End If
    
    With lstDebug
        '        .Top = frmMain.TabStrip1.ClientTop
        '        .Left = frmMain.TabStrip1.ClientLeft
        '        .Height = frmMain.TabStrip1.ClientHeight
        '        .Width = frmMain.TabStrip1.ClientWidth
        .Top = frmMain.SSTabMain.TabHeight + ButtonMargin
        .Left = ButtonMargin
        .Height = frmMain.SSTabMain.Height - frmMain.SSTabMain.TabHeight - 2 * ButtonMargin
        .Width = frmMain.SSTabMain.Width - 2 * ButtonMargin
        .Font = "Courier New"
    End With

End Sub

Private Sub SetupEventsView()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupEventsView")
    End If
    
    With lvEvents
        .View = lvwReport
        '        .Top = frmMain.TabStrip1.ClientTop
        '        .Left = frmMain.TabStrip1.ClientLeft
        '        .Height = frmMain.TabStrip1.ClientHeight
        '        .Width = frmMain.TabStrip1.ClientWidth
        .Top = frmMain.SSTabMain.TabHeight + ButtonMargin
        .Left = ButtonMargin
        .Height = frmMain.SSTabMain.Height - frmMain.SSTabMain.TabHeight - 2 * ButtonMargin
        .Width = frmMain.SSTabMain.Width - 2 * ButtonMargin
        .Font = "Courier New"
        .FullRowSelect = True
        .ColumnHeaders.Add 1, , "Time", 1000
        .ColumnHeaders.Add 2, , "Unit", 700
        .ColumnHeaders.Add 3, , "Type", 700
        .ColumnHeaders.Add 4, , "SegTime", 1000
        .ColumnHeaders.Add 5, , "Dist", 1000
        .ColumnHeaders.Add 6, , "Message Body", 30000
        .ColumnHeaders.Add 7, , "Sort Key", 2000
        .GridLines = True
        .Sorted = True
        .SortKey = 6                '   Sort Key column
        .SortOrder = lvwAscending
    End With

End Sub

Private Sub SetupDivisions()
    Dim cSQL As String
    Dim aArray As Variant
    Dim nRows As Integer
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupDivisions")
    End If
    '    cmbDiv.Clear
    '
    '    cmbDiv.AddItem "All"
    '
    '    cmbDiv.ListIndex = 0
    '
    '    cSQL = "Select div.DivName From Division div Order by div.DivName"
    '
    '    nRows = bsiSQLGet(cSQL, aArray, gConnectString)
    '
    '    For n = 0 To nRows - 1
    '        cmbDiv.AddItem aArray(0, n)
    '    Next n
    
End Sub

'Private Sub SetupAbout()
'    fmInside.Top = fmAbout.Height / 4
'    fmInside.Left = fmAbout.Width / 4
'    fmInside.Height = 2 * fmAbout.Height / 4
'    fmInside.Width = 2 * fmAbout.Width / 4
'
'    lblApp.Left = fmInside.Width / 2 - (lblApp.Width / 2)
'    lblApp.Top = fmInside.Height / 5
'    lblVersion.Left = fmInside.Width / 2 - (lblApp.Width / 2)
'    lblVersion.Top = 2 * fmInside.Height / 5
'    lblLicense.Left = fmInside.Width / 2 - (lblApp.Width / 2)
'    lblLicense.Top = 3 * fmInside.Height / 5
'    lblLink.Top = lblLicense.Top + (lblLicense.Height * 2)
'
'    lblApp.Caption = App.Title
'    lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
'    lblLicense.Caption = "Copyright (c) 2002-" & Trim(Str(year(Now))) & " CAD North Inc."
'End Sub
'
Private Sub SetupSetup()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupSetup")
    End If
    
    '    frmESRI.Top = TabStrip1.ClientHeight / 2 - frmESRI.Height / 2
    '    frmESRI.Left = TabStrip1.ClientWidth / 2 - frmESRI.Width / 2
    
    frmESRI.Top = SSTabMain.Height / 2 - frmESRI.Height / 2
    frmESRI.Left = SSTabMain.Width / 2 - frmESRI.Width / 2
    
    frmSpdProfile.Top = SetupTabs.TabHeight + 2 * ButtonMargin
    frmSpdProfile.Left = ButtonMargin
    frmSpdProfile.Width = SetupTabs.Width - 2 * ButtonMargin
    lstProfiles.Left = frmSpdProfile.Width - lstProfiles.Width - 2 * ButtonMargin
    lblProfileList.Left = lstProfiles.Left
    cmdSaveProfile.Top = frmSpdProfile.Height - cmdSaveProfile.Height - ButtonMargin
    cmdSaveProfile.Left = frmSpdProfile.Width - cmdSaveProfile.Width - ButtonMargin
    cmdDelProfile.Left = cmdSaveProfile.Left - cmdDelProfile.Width - ButtonMargin
    cmdDelProfile.Top = cmdSaveProfile.Top
    cmdCancelNew.Top = cmdSaveProfile.Top
    cmdCancelNew.Left = cmdDelProfile.Left - cmdCancelNew.Width - ButtonMargin
    cmdNewProfile.Top = cmdSaveProfile.Top
    cmdNewProfile.Left = cmdCancelNew.Left - cmdNewProfile.Width - ButtonMargin
    
    frmUnitBehavior.Top = frmSpdProfile.Top + frmSpdProfile.Height + 2 * ButtonMargin
    frmUnitBehavior.Width = frmSpdProfile.Width
    frmUnitBehavior.Left = frmSpdProfile.Left
    cmdSaveClear.Top = frmUnitBehavior.Height - cmdSaveClear.Height - ButtonMargin
    cmdSaveClear.Left = frmUnitBehavior.Width - cmdSaveClear.Width - ButtonMargin
    
    frmWorkstations.Top = frmSpdProfile.Top
    frmWorkstations.Left = frmSpdProfile.Left
    frmWorkstations.Width = frmSpdProfile.Width
    frmWorkstations.Height = frmSpdProfile.Height
    cmdWSSave.Top = frmWorkstations.Height - cmdWSSave.Height - ButtonMargin
    cmdWSSave.Left = frmWorkstations.Width - cmdWSSave.Width - ButtonMargin
    lstWorkstations.Top = 4 * ButtonMargin
    lstWorkstations.Left = frmWorkstations.Width - lstWorkstations.Width - 2 * ButtonMargin
    lstWorkstations.Height = cmdWSSave.Top - lstWorkstations.Top - ButtonMargin
    cmdWSDelete.Top = cmdWSSave.Top
    cmdWSDelete.Left = cmdWSSave.Left - cmdWSDelete.Width - ButtonMargin
    cmdWSCancel.Top = cmdWSSave.Top
    cmdWSCancel.Left = cmdWSDelete.Left - cmdWSCancel.Width - ButtonMargin
    cmdWSAdd.Top = cmdWSSave.Top
    cmdWSAdd.Left = cmdWSCancel.Left - cmdWSAdd.Width - ButtonMargin
    
    frmAniAliSetup.Top = frmWorkstations.Top + frmWorkstations.Height + ButtonMargin
    frmAniAliSetup.Left = frmWorkstations.Left
    frmAniAliSetup.Width = frmWorkstations.Width
    
    cmdANIALISave.Top = frmAniAliSetup.Height - cmdANIALISave.Height - ButtonMargin
    cmdANIALISave.Left = frmAniAliSetup.Width - cmdANIALISave.Width - ButtonMargin
    cmdANIALIReset.Top = cmdANIALISave.Top
    cmdANIALIReset.Left = cmdANIALISave.Left - cmdANIALIReset.Width - ButtonMargin
    
    lstProfiles.Height = cmdSaveProfile.Top - lstProfiles.Top - ButtonMargin
    
    frmConfig.Top = SetupTabs.TabHeight + 2 * ButtonMargin
    frmConfig.Height = SetupTabs.Height - frmConfig.Top - ButtonMargin
    frmConfig.Left = frmSpdProfile.Left
    frmConfig.Width = frmSpdProfile.Width
            
    frmScene.Height = frmChute.Height
    frmDestTime.Height = frmChute.Height
    frmDestination.Height = frmChute.Height
    
    frmScene.Width = frmChute.Width
    frmDestTime.Width = frmChute.Width
    frmDestination.Width = frmChute.Width
    
    cmdSave.Top = frmConfig.Height - cmdSave.Height - ButtonMargin
    cmdSave.Left = frmConfig.Width - cmdSave.Width - ButtonMargin
    
    cmdDiscard.Top = cmdSave.Top
    cmdDiscard.Left = cmdSave.Left - cmdDiscard.Width - ButtonMargin
    
'    chkSavePos.Left = txtAVLPath.Left
'    chkSavePos.Top = txtAVLPath.Top + txtAVLPath.Height + 2 * ButtonMargin
'
'    chkMessageBell.Left = Label17.Left
'    chkMessageBell.Top = txtAVLPath.Top + txtAVLPath.Height + 2 * ButtonMargin
    
    Call bsiGetDSNInfo(cmbDB)
    
    frmScene.Top = cmdSave.Top - frmScene.Height - ButtonMargin
    frmDestTime.Top = cmdSave.Top - frmDestTime.Height - ButtonMargin
    frmChute.Top = frmScene.Top - frmChute.Height - ButtonMargin
    frmDestination.Top = frmDestTime.Top - frmDestination.Height - ButtonMargin
    frmScene.Left = ButtonMargin
    frmChute.Left = ButtonMargin
    frmDestTime.Left = frmChute.Left + frmChute.Width + ButtonMargin
    frmDestination.Left = frmChute.Left + frmChute.Width + ButtonMargin
    frmScriptOptions.Left = ButtonMargin
    frmScriptOptions.Top = frmChute.Top - frmScriptOptions.Height - ButtonMargin
    
    txtTimeWarp.Text = Format(sldTimeWarp.Value / 10, "0.0")
    
    With lvRadioChannel
        .GridLines = True
        .FullRowSelect = True
        .View = lvwReport
        .ColumnHeaders.Add , , "Sector Name", 2650
        .ColumnHeaders.Add , , "Channel", 800
        .ColumnHeaders.Add , , "Channel Name", 2000
    End With
    
'    opSector.Value = True
'    opDivision.Value = False
'
    opReadChute.Enabled = False
    opCalcChute.Value = True
    
'    Call SetupDestChoice
    
    cmdDelProfile.Enabled = False
    cmdSaveProfile.Enabled = False
    '   cmdNewProfile.Enabled = False
    cmdCancelNew.Enabled = False
    cmdSaveClear.Enabled = False
    
    cmdSaveESRI.Enabled = False
    
    frmESRI.Visible = True
    frmConfig.Visible = False
    frmSpdProfile.Visible = False
    frmUnitBehavior.Visible = False
    frmWorkstations.Visible = False
    frmAniAliSetup.Visible = False
    SetupTabs.Visible = False

End Sub

Private Sub SetupDriveProfiles()
    Dim Buffer As String
    Dim tempSpeeds As Variant
    Dim tempNames As Variant
    Dim count As Integer
    Dim n As Integer
    Dim ProfileNames As Variant
    Dim ProfileSpeeds As Variant
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupDriveProfiles")
    End If
    
    lstProfiles.Clear
    cmbDriveProfile.Clear
    
    Buffer = bsiGetSettings("DriveTimeProfiles", "ProfileNames", "<Default>|Test", gSystemDirectory & "\Simulator.INI")
    
    ProfileNames = Split(Buffer, "|")
    
    Buffer = bsiGetSettings("DriveTimeProfiles", "ProfileSpeeds", "65;60;50;35;20|5;4;3;2;1", gSystemDirectory & "\Simulator.INI")
    
    ProfileSpeeds = Split(Buffer, "|")
    count = UBound(ProfileNames)
    
    ReDim gDriveTime(0 To count)
    
    For n = 0 To count
    
        If InStr(1, ProfileNames(n), ";") Then              '   new profile format
            tempNames = Split(ProfileNames(n), ";")
        Else
            tempNames = Array(ProfileNames(n), "M")         '   old (miles only) profile format
        End If
        
        gDriveTime(n).Name = tempNames(0)
        gDriveTime(n).SpeedScale = tempNames(1)
        
        tempSpeeds = Split(ProfileSpeeds(n), ";")
        
        gDriveTime(n).Interstate = tempSpeeds(0)
        gDriveTime(n).Highway = tempSpeeds(1)
        gDriveTime(n).OtherHwy = tempSpeeds(2)
        gDriveTime(n).Arterial = tempSpeeds(3)
        gDriveTime(n).Street = tempSpeeds(4)
        
        frmMain.lstProfiles.AddItem gDriveTime(n).Name
        
        cmbDriveProfile.AddItem gDriveTime(n).Name
    
    Next n
    
    txtInterstate.Text = ""
    txtLimAccess.Text = ""
    txtHighway.Text = ""
    txtArterial.Text = ""
    txtStreet.Text = ""
    txtProfile.Text = ""
    
    cmbDriveProfile.ListIndex = 0
    cmbDriveProfile.SelLength = 0
    
    cmdDelProfile.Enabled = False
    cmdNewProfile.Enabled = True
    cmdSaveProfile.Enabled = False
    cmdCancelNew.Enabled = False

End Sub

Private Sub SetupWorkstations()
    Dim WSList As Variant
    Dim WSString As String
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupWorkstations")
    End If
    
    lstWorkstations.Clear
    lstAniAli.Clear
    
    WSString = bsiGetSettings("Workstations", "WSList", "", gSystemDirectory & "\Simulator.INI")
    
    WSList = Split(WSString, "|")
    
    For n = 0 To UBound(WSList)
        lstWorkstations.AddItem WSList(n)
        lstAniAli.AddItem WSList(n)
    Next n
    
    txtWSName.Text = ""
    
    cmdWSAdd.Enabled = True
    cmdWSCancel.Enabled = False
    cmdWSDelete.Enabled = False
    cmdWSSave.Enabled = False
    
End Sub

Private Sub SetupDestChoice()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupDestChoice")
    End If
    
    opReadDestination.Value = True
    opCalcDestination.Value = False
    udTrans.Value = 70
    udClose.Value = 70
    udTrans.Enabled = False
    udClose.Enabled = False
    txtDestRange.Enabled = False
    txtDestClose.Enabled = False
    ' lblRandom.Enabled = False

End Sub

Private Sub SetupPriorities()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim n As Integer
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupPriorities")
    End If
    
    cSQL = "select AgencyTypeID, Priority, Forecolor, Backcolor from Priority"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    If nRows > 0 Then
        ReDim gPriority(0 To nRows - 1)
        
        For n = 0 To nRows - 1
            gPriority(n).AgencyID = aSQL(0, n)
            gPriority(n).PNumber = aSQL(1, n)
            If IsNull(aSQL(2, n)) Then
                gPriority(n).ForeColor = 1
            Else
                gPriority(n).ForeColor = aSQL(2, n)
            End If
            If IsNull(aSQL(3, n)) Then
                gPriority(n).BackColor = 1
                If gPriority(n).ForeColor = gPriority(n).BackColor Then
                    gPriority(n).BackColor = RGB(255, 255, 255)
                End If
            ElseIf gPriority(n).ForeColor = aSQL(3, n) Then
                gPriority(n).BackColor = RGB(255, 255, 255) - aSQL(3, n)
            Else
                gPriority(n).BackColor = IIf(aSQL(3, n) = 0, 1, aSQL(3, n))
            End If
        Next n
        
    Else
        AddToDebugList "Error: Retrieved " & Str(nRows) & " rows from Priority"
    End If
    
    Exit Sub
    
ERH:
    AddToDebugList "Error(LoadPriorities): " & Str(Err.Number) & "  " & Err.Description
End Sub

Private Sub SetupRadioChannels()
    Dim aProperties As Variant
    Dim PropertyString As String
    Dim n As Integer
    Dim atemp As Variant
    
    On Error GoTo SetupRadioChannels_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupRadioChannels")
    End If
    
    PropertyString = bsiGetSettings("RADIO.CHANNELS", "RADIOSETUP", "D", gSystemDirectory & "\Simulator.INI")
    
    aProperties = Split(PropertyString, "|")
    
    gAssignChannelBy = aProperties(0)
    
    gInterMessageWait = Val(bsiGetSettings("SystemDefaults", "RadioWaitToReceive", "3", gSystemDirectory & "\Simulator.INI"))
    gSpeechRate = Val(bsiGetSettings("SystemDefaults", "RadioSpeechRate", "2", gSystemDirectory & "\Simulator.INI"))
    gControlName = bsiGetSettings("SystemDefaults", "RadioControlName", "Control", gSystemDirectory & "\Simulator.INI")
    gSkipLeadingAlphas = IIf(bsiGetSettings("SystemDefaults", "SkipLeadingAlpha", "TRUE", gSystemDirectory & "\Simulator.INI") = "TRUE", vbChecked, vbUnchecked)
    
    If lvRadioChannel.ListItems.count > 0 Then
        lvRadioChannel.SelectedItem.Selected = False
    End If
    
    txtInterMessage.Text = gInterMessageWait
    txtSpeechRate.Text = gSpeechRate
    txtChannelName.Text = ""
    txtControlName.Text = gControlName

    If UBound(aProperties) > 0 Then
        ReDim gRadioChannels(0 To UBound(aProperties) - 1, 0 To 2)
        
        For n = 0 To UBound(gRadioChannels)
            If UBound(Split(aProperties(n + 1), ";")) = 1 Then
                aProperties(n + 1) = aProperties(n + 1) & ";"
            End If
            atemp = Split(aProperties(n + 1), ";")
            gRadioChannels(n, 0) = atemp(0)
            gRadioChannels(n, 1) = atemp(1)
            gRadioChannels(n, 2) = atemp(2)
        Next n
    Else
        ReDim gRadioChannels(0 To 0, 0 To 2)                        '   nothing defined, to create an empty list
    End If
    
    If gAssignChannelBy = "S" Then
        opSector.Value = True
        opDivision.Value = False
    Else
        opSector.Value = False
        opDivision.Value = True
    End If
    
    txtSpeechRate.Enabled = False
    udSpeechRate.Enabled = False
    txtInterMessage.Enabled = False
    udInterMessage.Enabled = False
    cmbChannelPick.Enabled = False
    txtChannelName.Enabled = False
    cmdRadioSave.Enabled = False
    cmdRadioReset.Enabled = False

    Exit Sub
SetupRadioChannels_ERH:
    AddToDebugList "Error(SetupRadioChannels): " & Str(Err.Number) & "  " & Err.Description, lstDebug
End Sub

Public Sub SetupSpeechLists()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim atemp As Variant
    Dim nRows As Integer
    Dim n As Integer
    Dim SpeechBuffer As String
    Dim TempBuffer As String

    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupSpeechLists")
    End If
    
    '   Get Location Names
    
    cSQL = "Select Description from LocationTypes"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    If nRows > 0 Then
        For n = 0 To nRows - 1
            If SpeechBuffer = "" Then
                SpeechBuffer = bsiGetSettings("SPEECH.LOCATIONS", Replace(aSQL(0, n), " ", "_"), "", gSystemDirectory & "\Simulator.INI")
            Else
                TempBuffer = bsiGetSettings("SPEECH.LOCATIONS", Replace(aSQL(0, n), " ", "_"), "", gSystemDirectory & "\Simulator.INI")
                If TempBuffer <> "" Then
                    SpeechBuffer = SpeechBuffer & "|" & TempBuffer
                End If
            End If
        Next n
    Else
    End If
    
    '   Get Station Names by AgencyType
    
'    SpeechBuffer = ""
'
    cSQL = "Select Agency_Type from AgencyTypes"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    If nRows > 0 Then
        For n = 0 To nRows - 1
            If SpeechBuffer = "" Then
                SpeechBuffer = bsiGetSettings("SPEECH.STATIONS", Replace(aSQL(0, n), " ", "_"), "", gSystemDirectory & "\Simulator.INI")
            Else
                TempBuffer = bsiGetSettings("SPEECH.STATIONS", Replace(aSQL(0, n), " ", "_"), "", gSystemDirectory & "\Simulator.INI")
                If TempBuffer <> "" Then
                    SpeechBuffer = SpeechBuffer & "|" & TempBuffer
                End If
            End If
        Next n
    Else
    End If
    
    If Len(SpeechBuffer) > 0 Then
        atemp = Split(SpeechBuffer, "|")
        ReDim gSpeechLocations(0 To UBound(atemp), 0 To 2)
        For n = 0 To UBound(atemp)
            If UBound(Split(atemp(n), ";")) = 1 Then atemp(n) = atemp(n) & ";0"
            gSpeechLocations(n, 0) = Split(atemp(n), ";")(0)
            gSpeechLocations(n, 1) = Split(atemp(n), ";")(1)
            gSpeechLocations(n, 2) = IIf(Split(atemp(n), ";")(2) = "1", True, False)
        Next n
    Else
        ReDim gSpeechLocations(0 To 0, 0 To 2)
    End If
    
    '   Get Unit Call Signs by AgencyType
    
    SpeechBuffer = ""
    
    If nRows > 0 Then
        For n = 0 To nRows - 1
            If SpeechBuffer = "" Then
                SpeechBuffer = bsiGetSettings("SPEECH.UNITS", Replace(aSQL(0, n), " ", "_"), "0;0;0", gSystemDirectory & "\Simulator.INI")
            Else
                TempBuffer = bsiGetSettings("SPEECH.UNITS", Replace(aSQL(0, n), " ", "_"), "0;0;0", gSystemDirectory & "\Simulator.INI")
                If TempBuffer <> "" Then
                    SpeechBuffer = SpeechBuffer & "|" & TempBuffer
                End If
            End If
        Next n
    Else
    End If
    
    If Len(SpeechBuffer) > 0 Then
        atemp = Split(SpeechBuffer, "|")
        ReDim gSpeechCallSigns(0 To UBound(atemp), 0 To 2)
        For n = 0 To UBound(atemp)
            If UBound(Split(atemp(n), ";")) = 1 Then atemp(n) = atemp(n) & ";0"
            gSpeechCallSigns(n, 0) = Split(atemp(n), ";")(0)
            gSpeechCallSigns(n, 1) = Split(atemp(n), ";")(1)
            gSpeechCallSigns(n, 2) = IIf(Split(atemp(n), ";")(2) = "1", True, False)
        Next n
    Else
        ReDim gSpeechCallSigns(0 To 0, 0 To 2)
    End If
    
    Exit Sub
    
ERH:
    AddToDebugList "Error(LoadSpeechLists): " & Str(Err.Number) & "  " & Err.Description, lstDebug
End Sub

Private Sub SyncCombo(ConnectString As String, CBBox As ComboBox)
    Dim n As Integer
    Dim DSN As String
    Dim aDSN As Variant
    Dim aPart As Variant
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SyncCombo")
    End If
    
    aDSN = Split(ConnectString, ";")
    For n = 0 To UBound(aDSN)
        aPart = Split(aDSN(n), "=")
        If UCase(aPart(0)) = "DSN" Then
            DSN = aPart(1)
            Exit For
        End If
    Next n
    
    For n = 0 To CBBox.ListCount - 1
        If CBBox.List(n) = DSN Then
            CBBox.ListIndex = n
            Exit For
        End If
    Next n

End Sub

Private Sub FillIncidentBrowser(dFrom As Variant, dTo As Variant, sDivision As String)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long, M As Long
    Dim PrioColor As tPriority
    Dim PrevID As Long
    '    Dim IncLoc As MapPointCtl.Location
    '    Dim IncPin As MapPointCtl.Pushpin
    '    Dim maxLoc As MapPointCtl.Location
    '    Dim minLoc As MapPointCtl.Location
    Dim MaxLat As Double
    Dim MaxLon As Double
    Dim MinLat As Double
    Dim MinLon As Double
    Dim Lat As Double
    Dim Lon As Double
    Dim Progress As String
    Dim aMapLocs() As tMapLocation
    
    Dim cJurList As String
    Dim cDivList As String
    
    Dim cError As String
    
    On Error GoTo FillIncidentBrowser_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub FillIncidentBrowser")
    End If
    
    Progress = "Start of FillIncidentBrowser"

    cSQL = "select rm.ID, " _
            & " rm.Time_CallEnteredQueue, " _
            & " rm.address, " _
            & " rm.city, " _
            & " rm.division, " _
            & " rm.problem, " _
            & " rv.radio_name, " _
            & " re.description, " _
            & " rm.CallTaking_Performed_By, " _
            & " rt.Location_name, " _
            & " rv.time_assigned, " _
            & " rv.time_enroute, " _
            & " rv.time_ArrivedAtScene, " _
            & " rt.time_Depart_Scene, " _
            & " rt.time_arrive_destination, " _
            & " rv.time_delayed_availability, " _
            & " rv.time_call_cleared, " _
            & " rm.latitude, " _
            & " rm.longitude, "
    cSQL = cSQL & " rv.ID, " _
            & " rt.ID, " _
            & " rm.Priority_Number, " _
            & " a.ID, " _
            & " rm.master_incident_number "
    cSQL = cSQL & "from Response_master_incident rm left outer join " _
            & "Response_Vehicles_assigned rv on rv.master_incident_id = rm.id left outer join " _
            & "Response_Transports rt on rt.vehicle_assigned_id = rv.id left outer join " _
            & "Vehicle ve on ve.ID = rv.Vehicle_ID left outer join " _
            & "Resource re on ve.PrimaryResource_ID = re.ID join " _
            & "AgencyTypes a on rm.Agency_Type = a.Agency_Type " _
            & "Where rm.Time_CallEnteredQueue between '" & Format(dFrom, "mmm dd yyyy hh:nn:ss") & "' and '" & Format(dTo, "mmm dd yyyy hh:nn:ss") & "' " _
            & " and rm.Priority_Number <> 0 " _
            & " and rm.ID is not NULL "

    If cmbAgency.Text <> "<Show All Agencies>" Then
        cSQL = cSQL & " and rm.Agency_Type ='" & Trim(cmbAgency.Text) & "' "
    End If
    
    If chkJurisdiction.Value = vbUnchecked Then
        cJurList = GetJurisdictionList()
        If cJurList <> "" Then
            cSQL = cSQL & " and rm.Jurisdiction in (" & cJurList & ") "
        End If
    End If
    
    If chkDivision.Value = vbUnchecked Then
        cDivList = GetDivisionList()
        If cDivList <> "" Then
            cSQL = cSQL & " and rm.Division in (" & cDivList & ") "
        End If
    End If

'    If sDivision <> "All" Then
'        cSQL = cSQL & " and rm.Division ='" & Trim(sDivision) & "' "
'    End If
    
    cSQL = cSQL & "order by rm.Time_CallEnteredQueue "
    
    ' txtEnterField.Text = cSQL
    
    Progress = "Executing Query"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
   
    ' MsgBox "Query executed: " & Str(NumRows) & " rows returned"
    
    If nRows > 0 Then
    
        ReDim aMapLocs(0 To UBound(aSQL, 2))
    
        Progress = "Evaluating Min/Max"

        flxBrowse.Rows = nRows + 1
        flxBrowse.ScrollBars = flexScrollBarBoth
        
        PrevID = -1
        
        If IsNumeric(aSQL(17, n)) And IsNumeric(aSQL(18, n)) Then
        
            MaxLat = (Val(aSQL(17, 0)) / 1000000) - gLatitudeAdjust
            MaxLon = (Val(aSQL(18, 0)) / -1000000) - gLongitudeAdjust
            MinLat = (Val(aSQL(17, 0)) / 1000000) - gLatitudeAdjust
            MinLon = (Val(aSQL(18, 0)) / -1000000) - gLongitudeAdjust
        
        End If
        
        For n = 0 To nRows - 1
        
            Progress = " >> Fill Browser Row Loop for n =" & Str(n) & ", Master Incident Number = " & IIf(IsNull(aSQL(23, n)), "NULL", aSQL(23, n))
        
            If IsNumeric(aSQL(17, n)) And IsNumeric(aSQL(18, n)) Then
        
                Lat = (Val(IIf(IsNull(aSQL(17, n)), 0#, aSQL(17, n)) / 1000000)) - gLatitudeAdjust
                Lon = (Val(IIf(IsNull(aSQL(18, n)), 0#, aSQL(18, n)) / -1000000)) - gLongitudeAdjust
            
            Else
            
                Lat = 0 - gLatitudeAdjust
                Lon = 0 - gLongitudeAdjust
            
            End If
            
            PrioColor = bsiGetPriority(IIf(IsNull(aSQL(21, n)), 0, aSQL(21, n)), IIf(IsNull(aSQL(22, n)), 0, aSQL(22, n)))
            
            aMapLocs(n).IncId = aSQL(0, n)
            aMapLocs(n).Address = IIf(IsNull(aSQL(2, n)), "NULL", aSQL(2, n))
            aMapLocs(n).Problem = IIf(IsNull(aSQL(5, n)), "NULL", aSQL(5, n))
            aMapLocs(n).Lat = Lat
            aMapLocs(n).Lon = Lon

            If PrevID <> aSQL(0, n) Then
                PrevID = aSQL(0, n)
                flxBrowse.Row = n + 1
                flxBrowse.Col = K_FLX_ID
                flxBrowse.CellForeColor = PrioColor.ForeColor
                flxBrowse.CellBackColor = PrioColor.BackColor
                flxBrowse.Text = aSQL(0, n)
                
                '    If Not (Lat = 0 Or Lon = 0) Then
                '        Set IncLoc = mapInc.ActiveMap.GetLocation(Lat, Lon)
                '        Set IncPin = mapInc.ActiveMap.AddPushpin(IncLoc, aSQL(0, n))
                '        IncPin.Symbol = 41
                '        IncPin.Note = aSQL(2, n) & Chr(13) & aSQL(5, n)
                '        If Lat > MaxLat Then MaxLat = Lat           '   get the box coords
                '        If Lon > MaxLon Then MaxLon = Lon
                '        If Lat < MinLat Then MinLat = Lat
                '        If Lon < MinLon Then MinLon = Lon
                '    End If
                
            Else
            '    flxBrowse.Row = n
            '    flxBrowse.Col = K_FLX_ROLL
            '    flxBrowse.Text = "[+]"
                flxBrowse.Row = n + 1
                flxBrowse.Col = K_FLX_ID
                flxBrowse.Text = " "
            End If
            
            For M = K_FLX_DateTime To flxBrowse.Cols - 1
            
                Progress = " >> FillIncidentBrowser - Filling Time Fields - m =" & Str(M)

                flxBrowse.Row = n + 1
                flxBrowse.Col = M
                flxBrowse.CellForeColor = PrioColor.ForeColor
                flxBrowse.CellBackColor = PrioColor.BackColor
                Select Case M
                    Case K_FLX_Address
                        flxBrowse.CellAlignment = flexAlignLeftCenter
                    Case K_FLX_Problem
                        flxBrowse.CellAlignment = flexAlignLeftCenter
                    Case K_FLX_Unit
                        flxBrowse.CellAlignment = flexAlignLeftCenter
                End Select
                If M > K_FLX_UnitType And M < K_FLX_Latitude Then
                    flxBrowse.Text = Format(aSQL(M - 1, n), "HH:NN:SS")     '   time fields
                ElseIf M = K_FLX_Latitude Then
                    Progress = Progress & ", Lat=" & Lat
                    flxBrowse.Text = Format(Lat, "###0.000000")             '   latitude ... using the applied correction
                ElseIf M = K_FLX_Longitude Then
                    Progress = Progress & ", Lon=" & Lon
                    flxBrowse.Text = Format(Lon, "###0.000000")             '   longitude ... using the applied correction
                ElseIf M = K_FLX_DateTime Then
                    flxBrowse.Text = Format(aSQL(M - 1, n), "MMM DD YYYY HH:NN:SS")   '   Incident Time
                Else
                    flxBrowse.Text = IIf(IsNull(aSQL(M - 1, n)), " ", aSQL(M - 1, n))   '   text fields
                End If
                
            Next M
            
        Next n
        
        Call FillBrowserMap(aMapLocs, cError)
        If cError <> "" Then
            Call AddToDebugList("[ERROR] in procedure FillBrowserMap of Form frmMain: " & cError, lstDebug)
        End If

        flxBrowse.Row = 0
        
    Else
    
        Debug.Print cError
        
    End If

    On Error GoTo 0
    Exit Sub

FillIncidentBrowser_ERH:

    On Error Resume Next
    
    Call AddToDebugList("[ERROR] in procedure FillIncidentBrowser of Form frmMain:" & Err.Number & ", " & Err.Description & Progress, lstDebug)
    Call AddToDebugList("        nRows     = " & Str(nRows), lstDebug)
    Call AddToDebugList("        n         = " & Str(n), lstDebug)
    Call AddToDebugList("        rm.ID     = " & IIf(IsNull(aSQL(0, n)), "NULL", aSQL(0, n)), lstDebug)
    Call AddToDebugList("        Address   = " & IIf(IsNull(aSQL(2, n)), "NULL", aSQL(2, n)), lstDebug)
    Call AddToDebugList("        Problem   = " & IIf(IsNull(aSQL(5, n)), "NULL", aSQL(5, n)), lstDebug)
    Call AddToDebugList("        Priority  = " & IIf(IsNull(aSQL(21, n)), "NULL", aSQL(21, n)), lstDebug)
    Call AddToDebugList("        AgencyID  = " & IIf(IsNull(aSQL(22, n)), "NULL", aSQL(22, n)), lstDebug)
    Call AddToDebugList("        Latitude  = " & IIf(IsNull(aSQL(17, n)), "NULL", aSQL(17, n)), lstDebug)
    Call AddToDebugList("        Longitude = " & IIf(IsNull(aSQL(18, n)), "NULL", aSQL(18, n)), lstDebug)
    Call AddToDebugList("    Raw Latitude  = >" & aSQL(17, n) & "<", lstDebug)
    Call AddToDebugList("    Raw Longitude = >" & aSQL(18, n) & "<", lstDebug)
    Call AddToDebugList("        Latitude  = " & IIf(IsEmpty(aSQL(17, n)), "EMPTY", aSQL(17, n)), lstDebug)
    Call AddToDebugList("        Longitude = " & IIf(IsEmpty(aSQL(18, n)), "EMPTY", aSQL(18, n)), lstDebug)
    Call AddToDebugList("        Latitude  = " & IIf(IsMissing(aSQL(17, n)), "MISSING", aSQL(17, n)), lstDebug)
    Call AddToDebugList("        Longitude = " & IIf(IsMissing(aSQL(18, n)), "MISSING", aSQL(18, n)), lstDebug)
    Call AddToDebugList("        Latitude  = " & IIf(Not IsNumeric(aSQL(17, n)), "NOT NUMERIC", aSQL(17, n)), lstDebug)
    Call AddToDebugList("        Longitude = " & IIf(Not IsNumeric(aSQL(18, n)), "NOT NUMERIC", aSQL(18, n)), lstDebug)
    Call AddToDebugList("        rm.M.I.N. = " & IIf(IsNull(aSQL(23, n)), "NULL", aSQL(23, n)), lstDebug)
    Call AddToDebugList("        ForeColor = " & Str(PrioColor.ForeColor), lstDebug)
    Call AddToDebugList("        BackColor = " & Str(PrioColor.BackColor), lstDebug)

    On Error GoTo 0
    
End Sub

Private Function GetJurisdictionList() As String
    Dim cList As String
    Dim n As Long
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetJurisdictionList")
    End If
    
    For n = 0 To lstJurisdiction.ListCount - 1
        If lstJurisdiction.Selected(n) Then
            cList = cList & "'" & lstJurisdiction.List(n) & "',"
        End If
    Next n
    
    If cList <> "" Then
        cList = Left(cList, Len(cList) - 1)
    End If
    
    GetJurisdictionList = cList
    
End Function

Private Function GetDivisionList() As String
    Dim cList As String
    Dim n As Long
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetDivisionList")
    End If
    
    For n = 0 To lstDivision.ListCount - 1
        If lstDivision.Selected(n) Then
            cList = cList & "'" & lstDivision.List(n) & "',"
        End If
    Next n
    
    If cList <> "" Then
        cList = Left(cList, Len(cList) - 1)
    End If
    
    GetDivisionList = cList
    
End Function

Private Sub FillBrowserMap(aIncList() As tMapLocation, Optional ByRef cError As String = "")
    Dim n As Long
    Dim sf As MapWinGIS.Shapefile
    Dim pShape As MapWinGIS.Shape
    Dim pField As MapWinGIS.Field
    Dim pPoint As MapWinGIS.Point
    Dim success As Boolean
    Dim nShapeIndex As Long
    Dim nFieldIndex As Long
    Dim nPointIndex As Long
    Dim nHnd As Long
    Dim Lat As Double
    Dim Lon As Double
    Dim nPrevInc As Long
    
    On Error GoTo FillBrowserMapERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub FillBrowserMap")
    End If
    
    Set sf = New MapWinGIS.Shapefile
    
    If gIncidentLayerHandle <> 0 Then
        Call incMap.RemoveLayer(gIncidentLayerHandle)
    End If
    
    If Dir(App.Path & "\IncidentFile.*") <> "" Then Kill App.Path & "\IncidentFile.*"
    success = sf.CreateNew(App.Path & "\IncidentFile.shp", SHP_POINT)
    
    If Not success Then cError = "Error on CreateNew: " & sf.ErrorMsg(sf.LastErrorCode)
    
    If success Then
    
        If gMapProjection <> "" Then
            sf.Projection = gMapProjection
        End If
        
        success = cnSFAddField(sf, "rmiID", INTEGER_FIELD, 10, nFieldIndex)
    
        success = cnSFAddField(sf, "rmiADDRESS", STRING_FIELD, 80, nFieldIndex)

        success = cnSFAddField(sf, "rmiPROBLEM", STRING_FIELD, 30, nFieldIndex)

        nShapeIndex = 0
        nPointIndex = 0
        
        For n = 0 To UBound(aIncList, 1)
            
            If aIncList(n).IncId <> nPrevInc And aIncList(n).Lat <> 0 And aIncList(n).Lon <> 0 Then
            
                Set pShape = New MapWinGIS.Shape
                Set pPoint = New MapWinGIS.Point
                
                success = pShape.Create(SHP_POINT)
                If Not success Then cError = cError & vbCrLf & "Error on Create:" & pShape.ErrorMsg(pShape.LastErrorCode)
                
                pPoint.y = aIncList(n).Lat
                pPoint.x = aIncList(n).Lon
                
                success = pShape.InsertPoint(pPoint, pShape.numPoints)
                If Not success Then cError = cError & vbCrLf & "Error on insert point:" & pShape.ErrorMsg(pShape.LastErrorCode)
                
                success = sf.EditInsertShape(pShape, nShapeIndex)
                If Not success Then cError = cError & vbCrLf & "Error on Edit Point:" & sf.ErrorMsg(sf.LastErrorCode)
                
                '   rm.ID
                success = sf.EditCellValue(0, nShapeIndex, Val(aIncList(n).IncId))
                If Not success Then cError = cError & vbCrLf & "Error on Edit Cell Value 0:" & sf.ErrorMsg(sf.LastErrorCode)
                
                '   rm.Address
                success = sf.EditCellValue(1, nShapeIndex, aIncList(n).Address)
                If Not success Then cError = cError & vbCrLf & "Error on Edit Cell Value 1:" & sf.ErrorMsg(sf.LastErrorCode)
                
                '   rm.Problem
                success = sf.EditCellValue(2, nShapeIndex, aIncList(n).Problem)
                If Not success Then cError = cError & vbCrLf & "Error on Edit Cell Value 2:" & sf.ErrorMsg(sf.LastErrorCode)
                
                nShapeIndex = nShapeIndex + 1
                nPointIndex = nPointIndex + 1
                
            End If
            
            nPrevInc = aIncList(n).IncId
            
        Next n
        
        success = sf.StopEditingShapes(True, True)
        
        gIncidentLayerHandle = incMap.AddLayer(sf, True)
        incMap.ShapeLayerPointSize(gIncidentLayerHandle) = 8
        incMap.ShapeLayerPointColor(gIncidentLayerHandle) = RGB(255, 0, 0)
        
        success = incMap.MoveLayerTop(incMap.LayerPosition(gIncidentLayerHandle))
        If Not success Then cError = cError & vbCrLf & "Error Adding Layer to Map:" & incMap.ErrorMsg(incMap.LastErrorCode)

        Set pPoint = Nothing
        Set pField = Nothing
        Set pShape = Nothing
        
        '   incMap.SendMouseDown = True
        incMap.SendMouseUp = True
        
    End If
        
    Exit Sub
FillBrowserMapERH:
    cError = cError & vbCrLf & "Error creating shapefile: " & Err.Description & " (" & Str(Err.Number) & ") "
End Sub

Private Sub incMap_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal x As Long, ByVal y As Long)
    Dim nLatT As Double
    Dim nLonL As Double
    Dim nLatB As Double
    Dim nLonR As Double
    Dim cMsg As String
    Dim cFieldMsg As String
    Dim n As Long
    Dim nIncID As Long
    
    Dim aResults As Variant
    Dim shp As MapWinGIS.Shape
    Dim ext As MapWinGIS.Extents
    
    Dim pExtents As MapWinGIS.Extents
    Dim sf As MapWinGIS.Shapefile
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub incMap_MouseUp")
    End If
    If incMap.CursorMode = cmSelection Then
    
        Set pExtents = New MapWinGIS.Extents
        
        Set sf = incMap.GetObject(gIncidentLayerHandle)
        
        Debug.Print sf.FileName
        
        incMap.PixelToProj CDbl(x - 5), CDbl(y - 5), nLonL, nLatT
        incMap.PixelToProj CDbl(x + 5), CDbl(y + 5), nLonR, nLatB
        
        Call pExtents.SetBounds(nLonL, nLatT, 0, nLonR, nLatB, 0)
        
        If sf.SelectShapes(pExtents, 0, INTERSECTION, aResults) Then
        
            Set shp = sf.Shape(aResults(0))
            Set ext = shp.Extents
            
            If UBound(aResults) = 0 Then
                cMsg = cMsg & aResults(n)
                cFieldMsg = cFieldMsg & Str(sf.CellValue(0, aResults(n))) & ", " & sf.CellValue(1, aResults(n)) & ", " & sf.CellValue(2, aResults(n))
                nIncID = sf.CellValue(0, aResults(n))
            Else
                For n = 0 To UBound(aResults)
                    cMsg = cMsg & aResults(n) & " "
                    If n = 0 Then
                        cFieldMsg = Str(sf.CellValue(0, aResults(n)))
                        nIncID = sf.CellValue(0, aResults(n))
                    Else
                        cFieldMsg = cFieldMsg & ", " & Str(sf.CellValue(0, aResults(n)))
                    End If
                Next n
            End If
            
            Debug.Print "Found these Shape ID's: " & cMsg
            
            Call incMap.ShowToolTip(IIf(Len(cFieldMsg) > 20, Left(cFieldMsg, 20) & " ...", cFieldMsg), 5000)
            Call incMap.Redraw
            
            If gHighLightLayerHandle >= 0 Then incMap.ClearDrawing (gHighLightLayerHandle)
            gHighLightLayerHandle = incMap.NewDrawing(dlSpatiallyReferencedList)
            
            Call incMap.DrawPointEx(gHighLightLayerHandle, ext.xMax, ext.yMax, 15, RGB(255, 125, 0))
            
            tmClearHighlight.Enabled = False    '   reset timer to 0 to make sure we get the full interval
            tmClearHighlight.Enabled = True     '   re-start the timer
            
            Call SyncBrowserWithMap(nIncID)
            
        End If
        
    End If
    
End Sub

Private Sub lstJurisdiction_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lstJurisdiction_Click")
    End If
    chkDivision.Value = vbChecked
End Sub

Private Sub mnuDetailedDebugging_Click()
    Dim Test As Boolean
    
    mnuDetailedDebugging.Checked = Not mnuDetailedDebugging.Checked
    
    gDetailedDebugging = mnuDetailedDebugging.Checked
    
    Test = bsiPutSettings("SystemDefaults", "DetailedDebugging", IIf(gDetailedDebugging, "Y", "N"), gSystemDirectory & "\Simulator.INI")
    
End Sub

Private Sub mnuEnableLoadTesting_Click()
    mnuEnableLoadTesting.Checked = Not mnuEnableLoadTesting.Checked
    gLoadTestMode = mnuEnableLoadTesting.Checked
    If gLoadTestMode Then
        SSTabMain.TabCaption(6) = "Load Test"
        SSTabMain.TabEnabled(6) = True
        Call SetLoadTestMode
    Else
        SSTabMain.TabCaption(6) = ""
        SSTabMain.TabEnabled(6) = False
        Call SetStartupVisibility
        SSTabMain.Tab = 0
    End If
End Sub

Private Sub mnuLogging_Click()
    Dim nErrors As Integer
    
    On Error GoTo MakeDir
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuLogging_Click")
    End If
    
    If Not gbLogging Then
        mnuLogging.Checked = True
        gbLogging = True
'        gTCPLog = FreeFile()
'        Open App.Path & "\Logs\TCPlog - " & Format(Now, "YYYYMMDD") & ".log" For Append As #gTCPLog
'        gDebugLog = FreeFile()
'        Open App.Path & "\Logs\Debuglog - " & Format(Now, "YYYYMMDD") & ".log" For Append As #gDebugLog
'        gInterfaceLog = FreeFile()
'        Open App.Path & "\Logs\InterfaceLog - " & Format(Now, "YYYYMMDD") & ".log" For Append As #gInterfaceLog
        Call AddToTCPList("9000CAD North Logging Start: " & Format(Now, "MMM DD YYYY HH:NN:SS"), lstTCP, , True)
        Call AddToDebugList("[STARTUP] CAD North Logging Start: " & Format(Now, "MMM DD YYYY HH:NN:SS"), lstDebug)
        Call AddToDebugList("[APPLICATION] Title: " & App.Title, lstDebug)
        Call AddToDebugList("[APPLICATION] Version: " & App.Major & "." & App.Minor & " Build " & App.Revision, lstDebug)
        Call AddToDebugList("[APPLICATION] Path: " & App.Path, lstDebug)
        Call SendToInterface("CAD North Logging Start: " & Format(Now, "MMM DD YYYY HH:NN:SS"))
    Else
        mnuLogging.Checked = False
        gbLogging = False
'        Close #gTCPLog
'        Close #gDebugLog
'        Close #gInterfaceLog
    End If
    
    Exit Sub

MakeDir:
    nErrors = nErrors + 1
    Debug.Assert (nErrors > 1)
    MkDir App.Path & "\Logs"
    Resume
End Sub

Private Sub tbBrowse_ButtonClick(ByVal Button As MSComctlLib.Button)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tbBrowse_ButtonClick")
    End If
    Select Case Button.Key
        Case "Zoom.In"     '   zoom in
            incMap.CursorMode = cmZoomIn
        Case "Zoom.Out"    '   zoom out
            incMap.CursorMode = cmZoomOut
        Case "Pan"      '   pan
            incMap.CursorMode = cmPan
        Case "Zoom.Last"   '   zoom last
            incMap.ZoomToPrev
        Case "Zoom.Full"   '   zoom full
            incMap.ZoomToMaxExtents
        Case "Identify"      '   identify
            incMap.CursorMode = cmSelection
        Case "Clear"
            Call incMap.ClearDrawing(gHighLightLayerHandle)
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim cMsgNum As String
    Dim cMsg As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub Form_Unload")
    End If
    AddToDebugList "[KILL AVL] Sending KILL to AVL Interface", lstDebug
    
    cMsgNum = Format(CN_MSG_CLASS, "0000")
    
    '   If Not gcnRoute Is Nothing Then
        '   Call gcnRoute.cnKillRoutingEngine(INIPATH & "\Simulator.INI")
    '   End If
    
    cMsg = Format(CN_MSG_SIM_KILL_cnROUTE, "0000") & "Kill cnRouteESRI"
    Call SendToCADNoWait(cMsgNum, cMsg)
    
    DoEvents
    
    '   Set gcnRoute = Nothing
    
    cMsg = Format(CN_MSG_SIM_MASTER_SHUTDOWN, "0000") & "Master Console has shut down."
    Call SendToCADNoWait(cMsgNum, cMsg)
    
    DoEvents
    
    If tcpAVL.RemoteHostIP = "127.0.0.1" Then
        If Not SendToAVL(EV_KILL) Then
            AddToDebugList "[TIMEOUT] Sending KILL to AVL Interface", lstDebug
        End If
    End If
    
    '    If WritePrivateProfileString("COMM", "IPCServer", UCase(txtIPCServer.Text), App.Path & "\Simulator.INI") = 0 Then
    '        MsgBox "Error writing to INI file", vbExclamation + vbOKOnly, "INI File Error"
    '    End If
    
    Call ExitApplication

End Sub

Private Sub lblLink_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lblLink_Click")
    End If
    ' Shell
End Sub

Private Sub lstAniAli_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lstAniAli_Click")
    End If
    If lstAniAli.SelCount > 0 Then
        cmdSendAniAli.Enabled = True
        cmdSendCellular.Enabled = True
    Else
        cmdSendAniAli.Enabled = False
        cmdSendCellular.Enabled = False
    End If
End Sub

'   Private Sub lstDebug_Click()
'       txtRadio.Text = lstDebug.List(lstDebug.ListIndex)
'   End Sub

Private Sub lstProfiles_Click()
    Dim n As Integer
    Dim i As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lstProfiles_Click")
    End If
    txtInterstate.Text = ""
    txtLimAccess.Text = ""
    txtHighway.Text = ""
    txtArterial.Text = ""
    txtStreet.Text = ""
    txtProfile.Text = ""

    i = lstProfiles.ListIndex
    
    If lstProfiles.List(i) = "<Default>" Then
        txtInterstate.Text = gDriveTime(0).Interstate
        txtLimAccess.Text = gDriveTime(0).Highway
        txtHighway.Text = gDriveTime(0).OtherHwy
        txtArterial.Text = gDriveTime(0).Arterial
        txtStreet.Text = gDriveTime(0).Street
        txtProfile.Text = "<Default>"
        opMiles.Value = True
        opKMs.Value = False
    
        txtInterstate.Enabled = False
        txtLimAccess.Enabled = False
        txtHighway.Enabled = False
        txtArterial.Enabled = False
        txtStreet.Enabled = False
        txtProfile.Enabled = False
        cmdDelProfile.Enabled = False
        cmdSaveProfile.Enabled = False
    
        opMiles.Enabled = False
        opKMs.Enabled = False
    
    ElseIf i > 0 Then
    
        txtInterstate.Text = gDriveTime(i).Interstate
        txtLimAccess.Text = gDriveTime(i).Highway
        txtHighway.Text = gDriveTime(i).OtherHwy
        txtArterial.Text = gDriveTime(i).Arterial
        txtStreet.Text = gDriveTime(i).Street
        txtProfile.Text = gDriveTime(i).Name
            
        If gDriveTime(i).SpeedScale = "M" Then
            opMiles.Value = True
            opKMs.Value = False
        Else
            opMiles.Value = False
            opKMs.Value = True
        End If
        
        txtInterstate.Enabled = True
        txtLimAccess.Enabled = True
        txtHighway.Enabled = True
        txtArterial.Enabled = True
        txtStreet.Enabled = True
        txtProfile.Enabled = True
        cmdDelProfile.Enabled = True
        cmdSaveProfile.Enabled = True
    
        opMiles.Enabled = True
        opKMs.Enabled = True
    
    End If

End Sub

Private Sub lstWorkstations_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lstWorkstations_Click")
    End If
    cmdWSAdd.Enabled = False
    cmdWSCancel.Enabled = True
    cmdWSSave.Enabled = False
    cmdWSDelete.Enabled = True
End Sub

Private Sub lvEvents_BeforeLabelEdit(Cancel As Integer)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lvEvents_BeforeLabelEdit")
    End If
    Cancel = True
End Sub

Private Sub lvMain_BeforeLabelEdit(Cancel As Integer)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lvMain_BeforeLabelEdit")
    End If
    Cancel = True
End Sub

Private Sub lvRadioChannel_BeforeLabelEdit(Cancel As Integer)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lvRadioChannel_BeforeLabelEdit")
    End If
    Cancel = True
End Sub

Private Sub lvRadioChannel_ItemClick(ByVal Item As MSComctlLib.ListItem)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lvRadioChannel_ItemClick")
    End If
    cmbChannelPick.Text = Item.SubItems(1)
    txtChannelName.Text = Item.SubItems(2)
End Sub

'Private Sub mapInc_SelectionChange(ByVal pNewSelection As Object, ByVal pOldSelection As Object)
'    Dim n As Integer
'    '   Dim opin As MapPointCtl.Pushpin
'    Dim ExitThisTime As Boolean
'
'    On Error GoTo ERH
'
'    If Not flxBrowse.Visible Then
'
'        If TypeOf pOldSelection Is Pushpin Then
'            pOldSelection.BalloonState = geoDisplayNone
'        End If
'
'ResumeFromHere:
'
'        If TypeOf pNewSelection Is Pushpin Then
'            pNewSelection.BalloonState = geoDisplayBalloon
'            For n = 1 To flxBrowse.Rows - 1
'                If pNewSelection.Name = flxBrowse.TextMatrix(n, K_FLX_ID) Then
'                    With flxBrowse
'                        .Row = n
'                        .Col = 0
'                        .RowSel = n
'                        .ColSel = .Cols - 1
'                        If Not .RowIsVisible(n) Then
'                            .TopRow = n
'                        End If
'                    End With
'                    Exit For
'                End If
'            Next n
'        End If
'
'    End If
'
'    Exit Sub
'
'ERH:
'    '   MsgBox "Unknown Selection Type", vbCritical, "mapInc_SelectionChange"
'    If Not ExitThisTime Then
'        ExitThisTime = True
'        Resume ResumeFromHere
'    End If
'End Sub

Private Sub SyncBrowserWithMap(nIncID As Long)
    Dim n As Integer
    Dim cFindInc As String
    Dim ExitThisTime As Boolean
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SyncBrowserWithMap")
    End If
    
    cFindInc = Trim(Str(nIncID))
    
    If Not flxBrowse.Visible Then
    
ResumeFromHere:
    
        For n = 1 To flxBrowse.Rows - 1
            If cFindInc = flxBrowse.TextMatrix(n, K_FLX_ID) Then
                With flxBrowse
                    .Row = n
                    .Col = 0
                    .RowSel = n
                    .ColSel = .Cols - 1
                    If Not .RowIsVisible(n) Then
                        .TopRow = n
                    End If
                End With
                Exit For
            End If
        Next n
    
    End If
    
    Exit Sub
    
ERH:
    '   MsgBox "Unknown Selection Type", vbCritical, "mapInc_SelectionChange"
    If Not ExitThisTime Then
        ExitThisTime = True
        Resume ResumeFromHere
    End If
End Sub

Private Sub SyncMapWithBrowser(nIncID As Long)
    Dim n As Long
    Dim shp As MapWinGIS.Shape
    Dim ext As MapWinGIS.Extents
    Dim pExt As MapWinGIS.Extents
    Dim sf As MapWinGIS.Shapefile
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SyncMapWithBrowser")
    End If
    
    Set sf = incMap.GetObject(gIncidentLayerHandle)
    
    Set pExt = incMap.Extents
    
    Debug.Print sf.FileName
    
    For n = 0 To sf.NumShapes
        If sf.CellValue(0, n) = nIncID Then
            Exit For
        End If
    Next n
    
    Set shp = sf.Shape(n)
    Set ext = shp.Extents
        
    Debug.Print "Found these Shape ID's: " & sf.CellValue(0, n) & ", " & sf.CellValue(1, n) & ", " & sf.CellValue(2, n)
        
    '   Call incMap.ShowToolTip(cFieldMsg, 5000)
        
    If gHighLightLayerHandle >= 0 Then incMap.ClearDrawing (gHighLightLayerHandle)
    gHighLightLayerHandle = incMap.NewDrawing(dlSpatiallyReferencedList)
    
    Call incMap.DrawPointEx(gHighLightLayerHandle, ext.xMax, ext.yMax, 15, RGB(255, 125, 0))
    
    Call incMap.ZoomToShape(gIncidentLayerHandle, n)
    Call incMap.ZoomOut(1.5)
    
End Sub

Private Sub mnuAbout_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuAbout_Click")
    End If
    frmAbout.Show
End Sub

Private Sub mnuDeleteIncident_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuDeleteIncident_Click")
    End If
    If MsgBox("Delete this Incident record?", vbYesNo + vbQuestion, "Confirm, Please") = vbYes Then
        flxBrowse.RemoveItem flxBrowse.Row
    End If
End Sub

Private Sub mnuDeleteSoundByte_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuDeleteSoundByte_Click")
    End If
    If MsgBox("Delete this SoundByte record?", vbYesNo + vbQuestion, "Confirm, Please") = vbYes Then
        flxBrowse.RemoveItem flxBrowse.Row
    End If
End Sub

Private Sub mnuEditProperties_Click()
    Dim r As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuEditProperties_Click")
    End If
    
    r = flxBrowse.Row
    
    frmEditIncident.Caption = "Edit Incident Properties"
    
'    Call MsgBox("Edit Incident Properties has not been fully implemented yet", vbOKOnly + vbInformation, "Thanks for your interest!")
    
    Do While flxBrowse.TextMatrix(r, K_FLX_ID) = " "
        r = r - 1
        flxBrowse.Row = r
        flxBrowse.Col = 0
        flxBrowse.ColSel = flxBrowse.Cols - 1
        If r = 0 Then
            Call MsgBox("Please select an incident to edit", vbOKOnly + vbInformation, "Nothing Selected!")
            Exit Sub
        End If
    Loop
    
    frmEditIncident.lblIncID.Caption = flxBrowse.TextMatrix(r, K_FLX_ID)
    frmEditIncident.lblIncAddress.Caption = flxBrowse.TextMatrix(r, K_FLX_Address)
    frmEditIncident.lblIncCity.Caption = flxBrowse.TextMatrix(r, K_FLX_City)
    frmEditIncident.lblIncDivision.Caption = flxBrowse.TextMatrix(r, K_FLX_Quad)
    frmEditIncident.lblIncProblem.Caption = flxBrowse.TextMatrix(r, K_FLX_Problem)
    
'    frmEditIncident.lblIncID.Enabled = False
'    frmEditIncident.lblIncAddress.Enabled = False
'    frmEditIncident.lblIncCity.Enabled = False
'    frmEditIncident.lblIncDivision.Enabled = False
'    frmEditIncident.lblIncProblem.Enabled = False
    
    flxBrowse.Col = K_FLX_ID
    
    frmEditIncident.lblIncID.ForeColor = flxBrowse.CellForeColor
    frmEditIncident.lblIncAddress.ForeColor = flxBrowse.CellForeColor
    frmEditIncident.lblIncCity.ForeColor = flxBrowse.CellForeColor
    frmEditIncident.lblIncDivision.ForeColor = flxBrowse.CellForeColor
    frmEditIncident.lblIncProblem.ForeColor = flxBrowse.CellForeColor
    
    frmEditIncident.lblIncID.BackColor = flxBrowse.CellBackColor
    frmEditIncident.lblIncAddress.BackColor = flxBrowse.CellBackColor
    frmEditIncident.lblIncCity.BackColor = flxBrowse.CellBackColor
    frmEditIncident.lblIncDivision.BackColor = flxBrowse.CellBackColor
    frmEditIncident.lblIncProblem.BackColor = flxBrowse.CellBackColor
    
    frmEditIncident.txtEditIncidentTime.Text = flxBrowse.TextMatrix(r, K_FLX_DateTime)
    
    flxBrowse.Col = 0
    flxBrowse.ColSel = flxBrowse.Cols - 1
    
    frmEditIncident.Show

End Sub

Private Sub mnuExit_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuExit_Click")
    End If
    Call cmdExit_Click
End Sub

Private Sub mnuInsertSoundByte_Click()
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuInsertSoundByte_Click")
    End If
    
    frmSoundByte.Top = frmMain.Top + (frmMain.Height / 2 - frmSoundByte.Height / 2)
    frmSoundByte.Left = frmMain.Left + (frmMain.Width / 2 - frmSoundByte.Width / 2)
    
    frmSoundByte.txtSoundTime.Text = ""
    frmSoundByte.txtSoundFile.Text = ""
    frmSoundByte.cmbSoundChannel.ListIndex = 0
    
    If Left(mnuInsertSoundByte.Caption, 1) = "I" Then               '   insert
        frmSoundByte.Caption = "Insert SoundByte"
        frmSoundByte.txtSoundTime.Text = flxBrowse.TextMatrix(flxBrowse.RowSel - n, K_FLX_DateTime)
        If frmSoundByte.txtSoundTime.Text = "" Then
            For n = 0 To flxBrowse.RowSel - 1
                If flxBrowse.TextMatrix(flxBrowse.RowSel - n, K_FLX_DateTime) <> "" Then
                    frmSoundByte.txtSoundTime.Text = flxBrowse.TextMatrix(flxBrowse.RowSel - n, K_FLX_DateTime)
                    Exit For
                End If
            Next n
            If frmSoundByte.txtSoundTime.Text = "" Then frmSoundByte.txtSoundTime.Text = Format(dtBrowse.Value, "MMM DD YYYY HH:NN:SS")
        End If
    Else
        frmSoundByte.Caption = "Edit SoundByte"
        frmSoundByte.txtSoundTime.Text = flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_DateTime)
        frmSoundByte.txtSoundFile.Text = flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_Address)
        frmSoundByte.cmbSoundChannel.Text = flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_City)
    End If
    
    frmSoundByte.Show
    Call SetTopMostWindow(frmSoundByte.hWnd, True)
    Call SetTopMostWindow(frmSoundByte.hWnd, False)
    
End Sub

Private Sub mnuPlaySoundByte_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuPlaySoundByte_Click")
    End If
    frmSoundByte.mmSoundByte.DeviceType = "WaveAudio"
    frmSoundByte.mmSoundByte.Notify = False
    frmSoundByte.mmSoundByte.FileName = flxBrowse.TextMatrix(flxBrowse.RowSel, K_FLX_Address)
    frmSoundByte.mmSoundByte.Command = "Open"
    frmSoundByte.mmSoundByte.Notify = True
    frmSoundByte.mmSoundByte.Wait = False
    frmSoundByte.mmSoundByte.Command = "Play"
End Sub

Private Sub opAwaitInstructions_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opAwaitInstructions_Click")
    End If
    opReturnToBase.Value = False
    opAwaitInstructions.Value = True
    cmdSaveClear.Enabled = True
End Sub

Private Sub opDivision_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opDivision_Click")
    End If
    opSector.Value = False
    opDivision.Value = True
    lvRadioChannel.ListItems.Clear
    lvRadioChannel.ColumnHeaders.Item(1).Text = "Division Name"
    cmbRadioAgency.Text = "<Select Agency>"
    cmbChannelPick.Text = ""
End Sub

'Private Sub opDestRead_Click()
'    If opDestRead.Value Then
'        udDest.Enabled = False
'        txtDestChoice.Enabled = False
'        lblRandom.Enabled = False
'    Else
'        udDest.Enabled = True
'        lblRandom.Enabled = True
'    End If
'End Sub
'
'Private Sub opDestClose_Click()
'    If opDestClose.Value Then
'        udDest.Enabled = True
'        lblRandom.Enabled = True
'    Else
'        udDest.Enabled = False
'        txtDestChoice.Enabled = False
'        lblRandom.Enabled = False
'    End If
'End Sub
'
Private Sub opReadDestination_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opReadDestination_Click")
    End If
    If opReadDestination.Value Then
        txtDestTrans.Enabled = False
        txtDestClose.Enabled = False
        lblDestTrans.Enabled = False
        lblDestClose.Enabled = False
        udTrans.Enabled = False
        udClose.Enabled = False
    End If
End Sub

Private Sub opReadDestTime_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opReadDestTime_Click")
    End If
    If opReadDestTime.Value Then
        txtDestTime.Enabled = False
        txtDestRange.Enabled = False
        lblDestMean.Enabled = False
        lblDestRange.Enabled = False
    End If
End Sub

Private Sub opReadScene_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opReadScene_Click")
    End If
    If opReadScene.Value Then
        txtSceneTime.Enabled = False
        txtSceneRange.Enabled = False
        lblSceneMean.Enabled = False
        lblSceneRange.Enabled = False
    End If
End Sub

Private Sub opCalcDestination_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opCalcDestination_Click")
    End If
    If opCalcDestination.Value Then
        txtDestTrans.Enabled = True
        txtDestClose.Enabled = True
        lblDestTrans.Enabled = True
        lblDestClose.Enabled = True
        udTrans.Enabled = True
        udClose.Enabled = True
    End If
End Sub

Private Sub opCalcDestTime_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opCalcDestTime_Click")
    End If
    If opCalcDestTime.Value Then
        txtDestTime.Enabled = True
        txtDestRange.Enabled = True
        lblDestMean.Enabled = True
        lblDestRange.Enabled = True
    End If
End Sub

Private Sub opCalcScene_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opCalcScene_Click")
    End If
    If opCalcScene.Value Then
        txtSceneTime.Enabled = True
        txtSceneRange.Enabled = True
        lblSceneMean.Enabled = True
        lblSceneRange.Enabled = True
    End If
End Sub

Private Sub opReturnToBase_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opReturnToBase_Click")
    End If
    opReturnToBase.Value = True
    opAwaitInstructions.Value = False
    cmdSaveClear.Enabled = True
End Sub

Private Sub opSector_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub opSector_Click")
    End If
    opDivision.Value = False
    opSector.Value = True
    lvRadioChannel.ListItems.Clear
    lvRadioChannel.ColumnHeaders.Item(1).Text = "Sector Name"
    cmbRadioAgency.Text = "<Select Agency>"
    cmbChannelPick.Text = " "
End Sub

Private Sub LoadRadioAgencyCombo()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub LoadRadioAgencyCombo")
    End If
    
    If gConnectString <> "" Then
    
        cSQL = "Select Agency_Type from AgencyTypes order by agency_type"
        
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
        
        If nRows > 0 Then
            Do
                cmbRadioAgency.AddItem aSQL(0, n)
                n = n + 1
            Loop While n < nRows
        Else
            MsgBox "That's odd. No rows returned. Check ODBC settings.", vbOKOnly + vbCritical, "Error Loading List"
        End If
        
    End If
    
End Sub

Private Sub LoadRadioListView()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Integer
    Dim xItem As ListItem
    Dim RadioChannel As String
    Dim ChannelName As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub LoadRadioListView")
    End If
    
    If gConnectString <> "" Then
    
        If opSector.Value Then
            cSQL = "Select description from sector " _
                    & " where agencyid in " _
                    & "(Select ID from AgencyTypes where Agency_Type = '" _
                    & cmbRadioAgency.Text & "') " _
                    & " order by Description"
        Else
            cSQL = "Select DivName from Division " _
                    & " where JurisdictionID in " _
                    & "(Select ID from Jurisdiction " _
                    & " where agencyid in " _
                    & "(Select ID from AgencyTypes where Agency_Type = '" _
                    & cmbRadioAgency.Text & "')) " _
                    & " order by DivName"
        End If
        
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
        
        If nRows > 0 Then
            lvRadioChannel.ListItems.Clear
            
            Do
                Call GetRadioChannel(ByVal aSQL(0, n), RadioChannel, ChannelName)
                Set xItem = lvRadioChannel.ListItems.Add(, , aSQL(0, n))
                xItem.SubItems(1) = RadioChannel
                xItem.SubItems(2) = ChannelName
                n = n + 1
                '   sync the list with saved radio setup
            Loop While n < nRows
            
            cmbChannelPick.Clear                    '   reload the channel pick list. In a later version,
            cmbChannelPick.AddItem " "              '   we'll check to see how many channels are needed
            cmbChannelPick.AddItem "1"
            cmbChannelPick.AddItem "2"
            cmbChannelPick.AddItem "3"
            cmbChannelPick.AddItem "4"
            cmbChannelPick.AddItem "5"
            cmbChannelPick.AddItem "6"
            cmbChannelPick.AddItem "7"
            cmbChannelPick.AddItem "8"
            
        Else
            MsgBox "That's odd. No rows returned. Check ODBC settings.", vbOKOnly + vbCritical, "Error Loading List"
        End If
        
        txtChannelName.Text = ""
        
        txtSpeechRate.Enabled = True
        udSpeechRate.Enabled = True
        txtInterMessage.Enabled = True
        udInterMessage.Enabled = True
        txtChannelName.Enabled = True
        cmbChannelPick.Enabled = True
        cmdRadioSave.Enabled = True
        cmdRadioReset.Enabled = True
    
    End If
    
End Sub

Private Sub sldDriveWarp_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub sldDriveWarp_Click")
    End If
    txtDriveWarp.Text = Format(sldDriveWarp.Value / 10, "0.0")
End Sub

Private Sub sldTimeWarp_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub sldTimeWarp_Change")
    End If
    txtTimeWarp.Text = Format(sldTimeWarp.Value / 10, "0.0")
End Sub

Private Sub SSTabMain_Click(PreviousTab As Integer)

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SSTabMain_Click")
    End If
    
    If PreviousTab = 1 And frmAniAli.Visible Then
        Call cmdAniAli_Click
    End If
    
    frmRadio.Visible = False
    cmdTransmit.Default = False
    
    Select Case SSTabMain.Tab
        Case 0          '   map page
            flxVehicle.Visible = False
            SetupTabs.Visible = False
            frmBrowser.Visible = False
            '   map.Visible = True
            lvEvents.Visible = False
            lvMain.Visible = False
            lstTCP.Visible = False
            lstDebug.Visible = False
            frmESRI.Visible = False
            lstRadio.Visible = False
            cmdSendInc.Enabled = False
            cmdAniAli.Enabled = False
            fmMap.Visible = True
            winMap1.Visible = True
            tbTools.Visible = True
            fmFilter.Visible = False
            fmLoadTest.Visible = False
        Case 1          '   Browser
            frmBrowser.Visible = True
            SetupTabs.Visible = False
            flxVehicle.Visible = False
            '   map.Visible = False
            lvEvents.Visible = True
            lvMain.Visible = False
            lstTCP.Visible = False
            lstDebug.Visible = False
            frmESRI.Visible = False
            lstRadio.Visible = False
            cmdSendInc.Enabled = True
            If Not gLoadTestMode Then
                cmdAniAli.Enabled = True
            End If
            fmMap.Visible = False
            winMap1.Visible = False
            tbTools.Visible = False
            fmFilter.Visible = True
            dtBrowse.SetFocus
            fmLoadTest.Visible = False
        Case 2          '   Events
            lvEvents.Visible = True
            SetupTabs.Visible = False
            frmBrowser.Visible = False
            lvMain.Visible = False
            lstTCP.Visible = False
            '   map.Visible = False
            lstDebug.Visible = False
            flxVehicle.Visible = False
            frmSpdProfile.Visible = False
            frmESRI.Visible = True
            lstRadio.Visible = False
            cmdSendInc.Enabled = False
            cmdAniAli.Enabled = False
            fmMap.Visible = False
            winMap1.Visible = False
            tbTools.Visible = False
            fmFilter.Visible = False
            fmLoadTest.Visible = False
        Case 3          '   unit queue
            flxVehicle.Visible = True
            SetupTabs.Visible = False
            '   map.Visible = False
            frmBrowser.Visible = False
            lvEvents.Visible = False
            lvMain.Visible = False
            lstTCP.Visible = False
            lstDebug.Visible = False
'            frmSpdProfile.Visible = False
            frmESRI.Visible = False
            lstRadio.Visible = False
            cmdSendInc.Enabled = False
            cmdAniAli.Enabled = False
            frmRadio.Visible = True
            cmdTransmit.Default = True
            fmMap.Visible = False
            winMap1.Visible = False
            tbTools.Visible = False
            fmFilter.Visible = False
            fmLoadTest.Visible = False
        Case 4          '   Setup Page
            SetupTabs.Tab = 0
            SetupTabs.Visible = True
            frmConfig.Visible = True
            frmBrowser.Visible = False
            lvEvents.Visible = False
            lvMain.Visible = False
            lstTCP.Visible = False
            '   map.Visible = False
            frmConfig.Visible = True
'            frmESRI.Visible = True
            frmESRI.Visible = False
            lstDebug.Visible = False
            flxVehicle.Visible = False
            lstRadio.Visible = False
            cmdSendInc.Enabled = False
            cmdAniAli.Enabled = False
            fmMap.Visible = False
            winMap1.Visible = False
            tbTools.Visible = False
            fmFilter.Visible = False
            fmLoadTest.Visible = False
        Case 5          '   Logs
            flxVehicle.Visible = False
            SetupTabs.Visible = False
            '   map.Visible = False
            frmBrowser.Visible = False
            lvEvents.Visible = False
            lvMain.Visible = False
            lstTCP.Visible = True
            lstDebug.Visible = True
            frmESRI.Visible = False
            lstRadio.Visible = True
            cmdSendInc.Enabled = False
            cmdAniAli.Enabled = False
            fmMap.Visible = False
            winMap1.Visible = False
            tbTools.Visible = False
            fmFilter.Visible = False
            fmLoadTest.Visible = False
        Case 6          '   LoadTest Tab
            flxVehicle.Visible = False
            SetupTabs.Visible = False
            '   map.Visible = False
            frmBrowser.Visible = False
            lvEvents.Visible = False
            lvMain.Visible = False
            lstTCP.Visible = False
            lstDebug.Visible = False
            frmESRI.Visible = False
            lstRadio.Visible = False
            cmdSendInc.Enabled = False
            cmdAniAli.Enabled = False
            fmMap.Visible = False
            winMap1.Visible = False
            tbTools.Visible = False
            fmFilter.Visible = False
            fmLoadTest.Visible = True
    End Select
End Sub

Private Sub SetupTabs_Click(PreviousTab As Integer)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetupTabs_Click")
    End If
    Select Case SetupTabs.Tab
        Case 0          '   Configuration page
            frmConfig.Visible = True
            frmSpdProfile.Visible = False
            frmUnitBehavior.Visible = False
            frmESRI.Visible = False
            frmWorkstations.Visible = False
            frmAniAliSetup.Visible = False
            frmESRI.Visible = False
        Case 1          '   Unit Behavior / Speed Profile Page
            frmConfig.Visible = False
            frmSpdProfile.Visible = True
            frmUnitBehavior.Visible = True
            frmESRI.Visible = False
            frmWorkstations.Visible = False
            frmAniAliSetup.Visible = False
            frmESRI.Visible = False
        Case 2          '   Workstations Page
            frmConfig.Visible = False
            frmSpdProfile.Visible = False
            frmUnitBehavior.Visible = False
            frmESRI.Visible = False
            frmWorkstations.Visible = True
            frmAniAliSetup.Visible = True
            If cmdWSAdd.Enabled Then                        '   added to trap error if workstation is selected
                cmdWSAdd.SetFocus                           '   when form gets focus - 20061010BM
            Else
                cmdWSCancel.SetFocus
            End If
            frmESRI.Visible = False
        Case 3          '   Radio Channels Page
            frmConfig.Visible = False
            frmSpdProfile.Visible = False
            frmUnitBehavior.Visible = False
            frmESRI.Visible = False
            frmWorkstations.Visible = False
            frmAniAliSetup.Visible = False
            frmESRI.Visible = False
        '    Case 4          '   Speech Setup Page
        '        frmConfig.Visible = False
        '        frmSpdProfile.Visible = False
        '        frmUnitBehavior.Visible = False
        '        frmESRI.Visible = True
        '        frmWorkstations.Visible = False
        '        frmAniAliSetup.Visible = False
        '        frmESRI.Visible = False
        Case 4          '   ESRI Setup
            frmConfig.Visible = False
            frmSpdProfile.Visible = False
            frmUnitBehavior.Visible = False
            frmESRI.Visible = True
            frmWorkstations.Visible = False
            frmAniAliSetup.Visible = False
            frmESRI.Visible = True
    End Select
End Sub

Private Sub AddToCADMsgQueue(ByVal cMessage)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub AddToCADMsgQueue")
    End If

'    AddToDebugList "[Adding to CADQueue] Next=" & Trim(Str(ptrCADNext)) & "  Free=" & Trim(Str(ptrCADFree))
    
    CADQueue(ptrCADFree).msgTime = Now
    CADQueue(ptrCADFree).msgType = Left(cMessage, 4)
    CADQueue(ptrCADFree).MsgBody = Right(cMessage, Len(cMessage) - 4)
    
    ptrCADFree = ptrCADFree + 1
    If ptrCADFree > UBound(CADQueue) Then
        ptrCADFree = 0
    End If
    
    If ptrCADFree < ptrCADNext Then
        sBar.Panels(3).Text = UBound(CADQueue) - ptrCADNext + ptrCADFree
    Else
        sBar.Panels(3).Text = ptrCADFree - ptrCADNext
    End If
    
End Sub

Private Sub PutCADMessage(ByVal cMessage As String)
    
    On Error GoTo PutCADMessageERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub PutCADMessage")
    End If
    
    CADQueue(ptrCADFree).msgTime = Now
    CADQueue(ptrCADFree).msgType = Left(cMessage, 4)
    CADQueue(ptrCADFree).MsgBody = Right(cMessage, Len(cMessage) - 4)
    
    ptrCADFree = ptrCADFree + 1
    
    sBar.Panels(3).Text = ptrCADFree - 1
    
    Exit Sub
    
PutCADMessageERH:
    
    Call AddToDebugList("[ERROR] in PutCADMessage: (" & Err.Number & ") " & Err.Description & " (ptrCADFree = " & ptrCADFree & ") Message = >" & cMessage & "<", lstDebug)
    
End Sub

Private Function GetNextCADMessage() As CADMessage
    Dim n As Integer
    
    On Error GoTo GetNextCADMessage_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetNextCADMessage")
    End If
    
    GetNextCADMessage = CADQueue(0)     '   get the first item in the queue
    
    For n = 0 To ptrCADFree             '   shift the queue items one step up
        CADQueue(n) = CADQueue(n + 1)
    Next
    
    If ptrCADFree > 0 Then              '   reset pointer to the first available free item in the queue
        ptrCADFree = ptrCADFree - 1
    End If
    
    Exit Function
    
GetNextCADMessage_ERH:
    
    Call AddToDebugList("[ERROR] in GetNextCADMessage: (" & Err.Number & ") " & Err.Description & " (ptrCADFree = " & ptrCADFree & ")", lstDebug)
    
End Function

Private Sub NewProcessCADMessages()
    Dim NextMessage As CADMessage
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub NewProcessCADMessages")
    End If
    
    NextMessage = GetNextCADMessage
    
    '   tmCADMsg.Enabled = False        '   disable timer to prevent calls to ProcessCADMsg before current operation is complete
    Call ProcessCADMsg(NextMessage.msgType, NextMessage.MsgBody)
    '   tmCADMsg.Enabled = True
    
End Sub

Private Sub ProcessCADMessages()
    Static ZeroTimer As Variant
    Static TimerON As Boolean

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessCADMessages")
    End If

    '   tmCADMsg.Enabled = False                        '   prevent interruptions during processing
    
    While ptrCADNext <> ptrCADFree                  '   as long as unprocessed messages exist
        
'        AddToDebugList "[Process CADMsg] Next=" & Trim(Str(ptrCADNext)) & "  Free=" & Trim(Str(ptrCADFree))
    
        Call ProcessCADMsg(CADQueue(ptrCADNext).msgType, CADQueue(ptrCADNext).MsgBody)
        
        If ptrCADFree < ptrCADNext Then
            sBar.Panels(3).Text = UBound(CADQueue) - ptrCADNext + ptrCADFree
        Else
            sBar.Panels(3).Text = ptrCADFree - ptrCADNext
        End If
        
        sBar.Panels(4).Text = Str(DateDiff("S", CADQueue(ptrCADNext).msgTime, Now))
        
        CADQueue(ptrCADNext).msgType = ""           '   after processing, set to blank
        CADQueue(ptrCADNext).MsgBody = ""           '   after processing, set to blank
        
        ptrCADNext = ptrCADNext + 1                 '   point to the next message spot in the queue
        If ptrCADNext > UBound(CADQueue) Then       '   deal with queue wrapping
            ptrCADNext = 0
        End If
        
        '   DoEvents                                    '   allow for other events to be handled
        
    Wend
    
    If Val(sBar.Panels(3).Text) <= 2 And gInitializationInProgress And gPhase2Complete Then         '   initializing and almost no messages
        If Not TimerON Then                                                                         '   and timer isn't running
            ZeroTimer = Now                                                                         '   start timer
            TimerON = True
        ElseIf DateDiff("S", ZeroTimer, Now) > 30 Then                                              '   otherwise if timer > 30
            gInitializationInProgress = False
            lstRadio.Clear
            txtRadio.Text = "Scenario Startup Complete: Press 'Resume' to continue"
            cmdMessage.Caption = "Clear Message"
            Beep
        Else
            
        End If
    Else
        If Val(sBar.Panels(3).Text) > 2 Then
            TimerON = False
            ZeroTimer = Now
        End If
    End If

    '   tmCADMsg.Enabled = True                         '   re-enable timer once we're done this batch of messages

End Sub

Private Sub ProcessCADMsg(msgType As String, MsgBody As String)
    Dim StatusXML As XMLStatus
    Dim PositionXML As XMLPosition
    Dim cSQL As String
    Dim aArray As Variant
    Dim nCalls As Long
    Dim n As Integer
    
    On Error GoTo ProcessCADMsg_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessCADMsg")
    End If
    
    Select Case msgType
        Case "0002"         '   RESPONSE_CHANGEDADDRESS_DEST
'            MsgBox "0002 - RESPONSE_CHANGEDADDRESS_DEST Received", vbOKOnly
        Case "0020"         '   NP_READ_VEHICLE
            aArray = Split(MsgBody, ",")            '   sometimes the NP_READ_VEHICLE message has a string of comma-separated vehicle IDs
            For n = 0 To UBound(aArray)             '   and we need to process all of them
                If Val(aArray(n)) > 0 Then          '   trap empty vehicle messages - 2014.07.29 BM
                    Call RefreshUnitInfo(Val(aArray(n)))
                End If
            Next n
        Case "0030"         '   NP_READ_MACHINESETTINGS
        Case "0035"         '   NP_NEW_RESPONSE
'            MsgBox "0035 - NP_NEW_RESPONSE Received", vbOKOnly
            If gLoadTestRunning Then
                Call SetUnitAssignToIncident(0, 0, 0)       '   no specific unit so assign the next available (we just created this incident so we could, so no surprise there)
            End If
        Case "0036"         '   NP_REMOVE_RESPONSE
'            MsgBox "0036 - NP_REMOVE_RESPONSE Received", vbOKOnly
'            Call IncidentCancelled(msgBody)
        Case "0046"
'            MsgBox "0046 - NP_READ_RESPONSE_TRANSPORT Received", vbOKOnly
        Case "0134"         '   NP_READ_SSMPLANS (Change to View Controller settings)
        Case "0152"         '   NP_READ_RESPONSE_DESTINATION
'            MsgBox "0152 - NP_READ_RESPONSE_DESTINATION Received", vbOKOnly
        Case "0165"         '   NP_VEHICLEOUTOFSERVICE
            Call NewStatus(Val(MsgBody))
            Call UnitOutOfService(Val(MsgBody))
        Case "0234"         '   NP_READ_MULTI_ASSIGN_VEHICLE
            Call UnitCancelled(MsgBody)
        Case "0235"         '   NP_CHANGED_RESPONSE_DESTINATION
'            MsgBox "0235 - NP_CHANGED_RESPONSE_DESTINATION Received", vbOKOnly
            Call ChangeDestination(MsgBody)
        Case "0244"         '   NP_READ_VEHICLE_STATUS
            StatusXML = ParseXMLStatus(MsgBody)
            Select Case StatusXML.ToStat
                Case STAT_OffDuty
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_Avail
                    If StatusXML.FromStat <> STAT_OffDuty Then
                        '   Call PurgeEvents(StatusXML.UnitID)
                        If gLoadTestRunning Then
                            If gReturnToBase Then                                   '   obey the settings for regular simulations here
                                Call SetAssignedToPostStatus(StatusXML.UnitID)
                            Else
                                Call SetUnitAssignToIncident(StatusXML.UnitID, GetUnitRecord(StatusXML.UnitID).CurrentLat, GetUnitRecord(StatusXML.UnitID).CurrentLon)
                            End If
                        Else
                            If gReturnToBase Then
                                Call ReturnToStation(StatusXML.UnitID, CDate(StatusXML.StatTime))
                            Else
                                Call AwaitInstructionsFromDispatch(StatusXML.UnitID, CDate(StatusXML.StatTime))
                            End If
                        End If
                    Else                        '   Hey! A unit is coming on-duty
                        Call LoadUnitList
                        Call RefreshUnitQueue
                    End If
                Case STAT_AtStation
                    Call AtStationStatus(StatusXML.UnitID)
                    If gLoadTestRunning Then
                        Call SetUnitAssignToIncident(StatusXML.UnitID, GetUnitRecord(StatusXML.UnitID).CurrentLat, GetUnitRecord(StatusXML.UnitID).CurrentLon)
                    End If
                Case STAT_LocalArea
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_AvailOS
                    If gLoadTestRunning Or chkUnitsSetAvailable.Value = vbChecked Then
                        Call SetUnitToAvailableStatus(StatusXML.UnitID)
                    Else
                        Call NewStatus(StatusXML.UnitID)
                    End If
                Case STAT_OOS
                    Call NewStatus(StatusXML.UnitID)
                    Call UnitOutOfService(StatusXML.UnitID)
                Case STAT_Disp
                    If gLoadTestRunning Then
                        If chkLoadTest.Value = vbChecked Then       '   we want Simulator to create ALL LOAD, so create status changes and routes
                            Call DispatchToAtScene(StatusXML.UnitID, STAT_Disp, StatusXML.StatTime)
                        End If
                    Else
                        Call DispatchToAtScene(StatusXML.UnitID, STAT_Disp, StatusXML.StatTime)
                    End If
                Case STAT_Resp
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_Enr2Post
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_Staged
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_OnScene
                    Call ArrivedAtScene(StatusXML.UnitID)
                Case STAT_PtContact
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_Transport
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_AtDest
                    If gLoadTestRunning Then
                        '   Call SetUnitToAvailableStatus(StatusXML.UnitID)         '   I once thought like this,
                        Call ArrivedAtDest(StatusXML.UnitID)                        '   but the customer asked me to think like this. 2019.08.19 BM
                    Else
                        Call ArrivedAtDest(StatusXML.UnitID)
                    End If
                Case STAT_DelayedAvail
                    If gLoadTestRunning Or chkUnitsSetAvailable.Value = vbChecked Then
                        Call SetUnitToAvailableStatus(StatusXML.UnitID)
                    Else
                        Call NewStatus(StatusXML.UnitID)
                    End If
                Case STAT_AssignEval
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_ShiftPend
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_ATP
                    If gLoadTestRunning Then
                        If chkLoadTest.Value = vbChecked Then       '   we want Simulator to create ALL LOAD, so create status changes and routes
                            Call NewPost(StatusXML)
                        End If
                    Else
                        Call NewPost(StatusXML)
                    End If
                Case STAT_MultiAssign
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_D2L
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_R2L
                    Call NewStatus(StatusXML.UnitID)
                Case STAT_A2L
                    Call NewStatus(StatusXML.UnitID)
            End Select
        Case "0245"         '   NP_READ_VEHICLE_POSITION
            PositionXML = ParseXMLPosition(MsgBody)
            Call RefreshUnitInfo(PositionXML.UnitID)
        Case "9700"         '   NP_CADNORTH_MESSAGE_CLASS
            Call cnProcessCADNorthMessage(MsgBody)
        Case "9750"         '   NP_BRIMAC_MESSAGE_CLASS
            Call bsiProcessBrimacMessage(MsgBody)
    End Select
        
    Exit Sub
    
ProcessCADMsg_ERH:

    Call AddToDebugList("[ERROR] in ProcessCADMsg: (" & Err.Number & ") " & Err.Description & " - Message = >" & msgType & MsgBody & "<", lstDebug)
        
End Sub

Private Sub cnProcessCADNorthMessage(MsgBody As String)
    Dim SubType As String
    Dim Payload As String
    
    On Error GoTo cnProcessCADNorthMessage_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cnProcessCADNorthMessage")
    End If
    
    SubType = Left(MsgBody, 4)
    
    If Len(MsgBody) > 4 Then Payload = Right(MsgBody, Len(MsgBody) - 4)
    
    Select Case SubType
        Case Format(CN_ROUTE_PAYLOAD, "0000")           '   Receive calculated Route from cnRoutingESRI
            Call ProcessReceivedRoute(Payload)
        Case Format(CN_ROUTE_FILENAME, "0000")          '   Receive filename of calculated Route file transfer
            Call ProcessRouteFile(Payload)
        Case Format(CN_MSG_SIM_STATUS, "0000")          '   Status request from SimOp
            Call ProcessSimStatusRequest(Payload)
        Case Format(CN_MSG_SIM_RADIOMSG, "0000")        '   Create Radio Message request from SimOp
            Call ProcessSimRadioMsgRequest(Payload)
        Case Format(CN_MSG_SIM_SOUNDBYTE, "0000")       '   Create Radio Message request from SimOp
            Call ProcessSimSoundByteRequest(Payload)
        Case Format(CN_MSG_SIM_ANIALIMSG, "0000")       '   Create ANIALI drop request from SimOp console
            '   Call ProcessSimStatusRequest(Payload)
        Case Format(CN_MSG_SIM_INCIDENT, "0000")        '   Create Incident request from SimOp console
            '   Call ProcessSimStatusRequest(Payload)
        Case Format(CN_MSG_PASSTHROUGH, "0000")         '   Process Passthrough request from SimOp console
            Call ProcessPassthroughRequest(Payload)
        Case Format(CN_MSG_CALC_SPECIALROUTE, "0000")         '   Process Passthrough request from SimOp console
            Call ProcessSpecialRoute(Payload)
        Case Format(CN_MSG_SET_RESOURCE_OFFDUTY, "0000")         '   Process Passthrough request from SimOp console
            Call ProcessUnitsOffDuty(Payload)
        Case Else
    End Select
    
    Exit Sub
    
cnProcessCADNorthMessage_ERH:
    
    Call AddToDebugList("[ERROR] in cnProcessCADNorthMessage: (" & Err.Number & ") " & Err.Description & " - Message = >" & MsgBody & "<", lstDebug)
    
End Sub

Private Sub PurgeEvents(nUnitID As Long)
    Dim lItem As ListItem
    Dim nCount As Long
    Dim Finished As Boolean
    Dim Found As Boolean
    Dim n As Integer

    '   Before inserting new events for a unit, we must be sure that the event queue is
    '   purged of all pending events for the unit.
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub PurgeEvents")
    End If
    
    Found = False
    n = 0
    
    '   Find UnitName in list
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).VehID = nUnitID Then
            Found = True
            Exit For
        End If
    Next n
    
    If Found Then
        
        '   Purge all events for the Unit in the event queue
    
        AddToDebugList "[Purging Event List] " & gUnitList(n).UnitName, lstDebug
        
        Finished = False
        nCount = 0
        
        Do While Not Finished
            If lvEvents.ListItems.count > 0 Then
                Set lItem = lvEvents.FindItem(gUnitList(n).UnitName, lvwSubItem, LVUnit)
                If lItem Is Nothing Then
                    Finished = True
                Else
                    lvEvents.ListItems.Remove lItem.Index
                    nCount = nCount + 1
                End If
            Else
                Finished = True
            End If
        Loop
        
        AddToDebugList "[Purging Event List] " & Str(nCount) & " events deleted.", lstDebug
    Else
        AddToDebugList "[ERROR Purging Event List] Unit ID:" & Str(nUnitID) & " NOT FOUND.", lstDebug
    End If
    
End Sub

Private Sub PurgeAllEvents()
    Dim nCount As Long
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub PurgeAllEvents")
    End If
    
    AddToDebugList "[Purging Event List] Purging all events.", lstDebug
    
    nCount = lvEvents.ListItems.count
    
    lvEvents.ListItems.Clear
    
    AddToDebugList "[Purging Event List] " & Str(nCount) & " events purged.", lstDebug
    
End Sub

Private Sub NewPost(StatusXML As XMLStatus)
    Dim n As Long
    Dim nCurrentStatus As Integer
    Dim NextStatus As Integer
    Dim cChute As Double
    Dim StatusChangeTime As Variant
    Dim EndingStatus As Integer
    Dim StatusAtPost As Integer
    Dim Found As Boolean
    Dim OldPost As String
    Dim NewPost As String
    Dim IsPost As Boolean
    Dim UnitRecord As Vehicle
    
    On Error GoTo BugReport
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub NewPost")
    End If
    
    OldPost = GetUnitRecord(StatusXML.UnitID).CurrentLoc
    
    n = RefreshUnitInfo(StatusXML.UnitID)
            
    NewPost = gUnitList(n).DestinationLoc
    
    If IsPostOrStation(NewPost, IsPost) Then        '   is it in the stations table
        If IsPost Then                              '   is it a post
            StatusAtPost = STAT_LocalArea
        Else
            StatusAtPost = STAT_AtStation           '   is it an address
        End If
    Else
        If IsNumeric(Left(NewPost, 1)) Then         '   identify intersections vs. addresses (test is for street address)
            StatusAtPost = STAT_AtStation           '   street address
        Else
            StatusAtPost = STAT_LocalArea           '   non-street-address location
        End If
    End If
    
    
    If StatusXML.FromStat = StatusXML.ToStat And OldPost = NewPost Then         '   trap redundant post assignments
        
        AddToDebugList "[NewPost] FromStat = ToStat and Destination Unchanged:" & gUnitList(n).UnitName & ". Ignoring.", lstDebug
        Exit Sub
        
    Else
    
        Call PurgeEvents(StatusXML.UnitID)                           '   purge any pending events for this unit
        
        nCurrentStatus = GetCurrentStatus(StatusXML.UnitID)
        
        If nCurrentStatus >= 0 Then
        
'            StatusChangeTime = CalculateChuteTime(nCurrentStatus, Now)
            StatusChangeTime = CalculateChuteTime(UnitRecord.LastStatus, StatusXML.StatTime)
        
            AddToDebugList "[NewPost] ChuteTime calculated:" & StatusXML.UnitID, lstDebug
            
            Select Case nCurrentStatus
                Case STAT_AtStation
                    NextStatus = STAT_Enr2Post
                    EndingStatus = StatusAtPost
                Case STAT_ATP
                    NextStatus = STAT_Enr2Post
                    EndingStatus = StatusAtPost
                Case STAT_Enr2Post
                    NextStatus = STAT_Enr2Post
                    EndingStatus = StatusAtPost
                Case STAT_Avail
                    NextStatus = STAT_Enr2Post
                    EndingStatus = StatusAtPost
                Case Else
                    NextStatus = STAT_Enr2Post
                    EndingStatus = StatusAtPost
            End Select
            
            Call InsertStatusEvent(gUnitList(n), nCurrentStatus, NextStatus, StatusChangeTime)      '   insert the ACK status change
            Call InsertRadioMessage(gUnitList(n), NextStatus, StatusChangeTime, , gUnitList(n).LastStatus)
            
            gUnitList(n).StatusChangeTime = StatusChangeTime
            gUnitList(n).NextStatus = NextStatus
            gUnitList(n).EndStatus = EndingStatus
            
            If gUseESRIResources Then
                Call CalculateESRIRoute(gUnitList(n), ROUTE_ToDestination, gUseOneWayRestrictions)
            Else
                Call CalculateRoute(gUnitList(n), ROUTE_ToDestination)
            End If
            
            '   if the post is a station, go InQuarters, otherwise, leave the status unchanged
            '   *** We need to figure out how to do this ***
            
            '    StatusChangeTime = InsertAVLEvents(gUnitList(n), StatusChangeTime)
            '
            '    Call InsertStatusEvent(gUnitList(n), NextStatus, EndingStatus, StatusChangeTime)    '   insert the termination status change
            '    Call InsertRadioMessage(gUnitList(n), EndingStatus, StatusChangeTime, , NextStatus)
        
        Else
            AddToDebugList "[NewPost] No current status for:" & StatusXML.UnitID, lstDebug
        End If
        
    End If
    
    Exit Sub
    
BugReport:
    On Error Resume Next
    
    Call AddToDebugList("ERROR Occurred in NewPost: " & Str(Err.Number) & ", " & Err.Description, lstDebug)
    Call AddToDebugList("   varname: n              = " & Str(n), lstDebug)
    Call AddToDebugList("   varname: nCurrentStatus = " & Str(nCurrentStatus), lstDebug)
    Call AddToDebugList("   varname: NextStatus     = " & Str(NextStatus), lstDebug)
    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT), lstDebug)
    Call AddToDebugList("   varname: Found          = " & IIf(Found, "True", "False"), lstDebug)
    Call AddToDebugList("   varname: OldPost        = " & OldPost, lstDebug)
    Call AddToDebugList("   varname: NewPost        = " & NewPost, lstDebug)

End Sub

'Private Sub NewDispatch(nUnitID As Long, CurrentStatus As Integer)
'    Dim nUnit As Long, nInc As Integer
'    Dim nOldIDInfo As Long
'    Dim OldStatus As Integer
'    Dim NextStatus As Integer
'    Dim ChuteTime As Double
'    Dim LongChute As Double, ShortChute As Double
'    Dim StatusChangeTime As Variant
'    Dim Found As Boolean
'    Dim tHosp As CADLocation
'    Dim UsedClosest As Boolean
'    Dim UseSourceSceneTime As Boolean
'    Dim UseSourceDestTime As Boolean
'    Dim UseSourceDest As Boolean
'    Dim SceneMean As Integer
'    Dim SceneRange As Integer
'    Dim DestMean As Integer
'    Dim DestRange As Integer
'
'    On Error GoTo BugReport
'
'    '   get Status Timer settings from INI file
'
'    SceneMean = Val(bsiGetSettings("AVL", "SceneTimeMean", 600, App.Path & "\Simulator.INI"))
'    SceneRange = Val(bsiGetSettings("AVL", "SceneTimeRange", 600, App.Path & "\Simulator.INI"))
'    DestMean = Val(bsiGetSettings("AVL", "DestTimeMean", 600, App.Path & "\Simulator.INI"))
'    DestRange = Val(bsiGetSettings("AVL", "DestTimeRange", 600, App.Path & "\Simulator.INI"))
'    UseSourceSceneTime = (bsiGetSettings("AVL", "UseSourceSceneTime", "false", App.Path & "\Simulator.INI") = "True")
'    UseSourceDestTime = (bsiGetSettings("AVL", "UseSourceDestTime", "false", App.Path & "\Simulator.INI") = "True")
'    UseSourceDest = (bsiGetSettings("AVL", "UseSourceDest", "false", App.Path & "\Simulator.INI") = "True")
'
'    '   Before inserting new events for a unit, we must be sure that the event queue is
'    '   purged of all pending events for the unit.
'
'    Call PurgeEvents(nUnitID)
'
'    OldStatus = GetCurrentStatus(nUnitID)
'
'    '   Calculate Chute Time for the new assignment
'
'    ChuteTime = CalculateChuteTime(OldStatus)
'
'    nUnit = RefreshUnitInfo(nUnitID)                    '   update the unit's info
'
'    nInc = RefreshIncidentInfo(gUnitList(nUnit))        '   get all info on the unit's current assignment
'
'    nOldIDInfo = bsiFindOldIncID(gUnitList(nUnit).RMIID)
'
'    NextStatus = STAT_Resp
'    Call InsertStatusEvent(gUnitList(nUnit), CurrentStatus, NextStatus, ChuteTime)      '   insert the ACK status change
'    Call InsertRadioMessage(gUnitList(nUnit), NextStatus, ChuteTime)
'
'    '   calculate first segment of route
'
'    Call CalculateRoute(gUnitList(nUnit), ROUTE_ToScene)
'
'    StatusChangeTime = InsertAVLEvents(gUnitList(nUnit), ChuteTime)
'
'    CurrentStatus = NextStatus      '   increment to the next status (OnScene)
'    NextStatus = STAT_OnScene
'    Call InsertStatusEvent(gUnitList(nUnit), CurrentStatus, NextStatus, StatusChangeTime)    '   insert the call segment termination status change
'    Call InsertRadioMessage(gUnitList(nUnit), NextStatus, StatusChangeTime)
'
'    If Not UseSourceDest Or nOldIDInfo < 0 Then
'        tHosp = FindClosestHosp(gIncidents(nInc), UsedClosest)       '   select destination
'    Else
'
'    End If
'
'    gUnitList(nUnit).DestinationCode = tHosp.Code
'    gUnitList(nUnit).DestinationLat = tHosp.Lat
'    gUnitList(nUnit).DestinationLon = tHosp.Lon
'
'    gIncidents(nInc).DE_Code = tHosp.Code
'    gIncidents(nInc).DE_Location = tHosp.Name
'    gIncidents(nInc).DE_Address = tHosp.Address
'    gIncidents(nInc).DE_City = tHosp.City
'    gIncidents(nInc).DE_Lat = tHosp.Lat
'    gIncidents(nInc).DE_Lon = tHosp.Lon
'
'    CurrentStatus = NextStatus      '   increment to the next status
'    NextStatus = STAT_Transport
'
'    If Not UseSourceSceneTime Or nOldIDInfo < 0 Then
'        StatusChangeTime = DateAdd("S", (SceneMean - SceneRange / 2) + (Rnd * SceneRange), StatusChangeTime)   '   calculate onscene time
'    Else
'        StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).OnSceneTime, StatusChangeTime)    '   calculate onscene time
'    End If
'
'    Call InsertTransportEvent(gUnitList(nUnit), CurrentStatus, NextStatus, StatusChangeTime, UsedClosest)    '   insert the termination status change
'    Call InsertRadioMessage(gUnitList(nUnit), NextStatus, StatusChangeTime, tHosp.Name)
'
'    Call CalculateRoute(gUnitList(nUnit), ROUTE_ToHosp)
'
'    StatusChangeTime = InsertAVLEvents(gUnitList(nUnit), StatusChangeTime)
'
'    CurrentStatus = NextStatus      '   increment to the next status
'    NextStatus = STAT_AtDest
'
'    Call InsertStatusEvent(gUnitList(nUnit), CurrentStatus, NextStatus, StatusChangeTime)    '   insert the termination status change
'    Call InsertRadioMessage(gUnitList(nUnit), NextStatus, StatusChangeTime, tHosp.Name)
'
'    CurrentStatus = NextStatus      '   increment to the next status
'    NextStatus = STAT_DelayedAvail
'
'    If Not UseSourceDestTime Or nOldIDInfo < 0 Then
'        StatusChangeTime = DateAdd("S", (DestMean - DestRange / 2) + (Rnd * DestRange), StatusChangeTime)   '   calculate AtHosp
'    Else
'        StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).AtDestTime, StatusChangeTime)     '   calculate onscene time
'    End If
'
'    Call InsertStatusEvent(gUnitList(nUnit), CurrentStatus, NextStatus, StatusChangeTime)   '   insert the termination status change
'    Call InsertRadioMessage(gUnitList(nUnit), NextStatus, StatusChangeTime, tHosp.Name)
'
'    Exit Sub
'
'BugReport:
'    On Error Resume Next
'
'    Call AddToDebugList("ERROR Occurred in NewDispatch: " & Str(Err.Number) & ", " & Err.Description)
'    Call AddToDebugList("   varname: nUnitID       = " & Str(nUnitID))
'    Call AddToDebugList("   varname: CurrentStatus = " & Str(CurrentStatus))
'    Call AddToDebugList("   varname: nUnit         = " & Str(nUnit))
'    Call AddToDebugList("   varname: nInc          = " & Str(nInc))
'    Call AddToDebugList("   varname: nOldIDInfo    = " & Str(nOldIDInfo))
'    Call AddToDebugList("   varname: OldStatus     = " & Str(OldStatus))
'    Call AddToDebugList("   varname: NextStatus    = " & Str(NextStatus))
'    Call AddToDebugList("   varname: ChuteTime     = " & Str(ChuteTime))
'    Call AddToDebugList("   varname: LongChute     = " & Str(LongChute))
'    Call AddToDebugList("   varname: ShortChute    = " & Str(ShortChute))
'    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT))
'    Call AddToDebugList("   varname: Found         = " & IIf(Found, "True", "False"))
'    Call AddToDebugList("   varname: UsedClosest   = " & IIf(UsedClosest, "True", "False"))
'
'End Sub

Private Sub DispatchToAtScene(nUnitID As Long, CurrentStatus As Integer, StatusTime As Variant)
    Dim nUnit As Long, nInc As Integer
    Dim nOldIDInfo As Long
    Dim OldStatus As Integer
    Dim NextStatus As Integer
    Dim ChuteTime As Double
    Dim LongChute As Double, ShortChute As Double
    Dim TempTime As Variant
    Dim StatusChangeTime As Variant
    Dim Found As Boolean
    Dim cMessage As String
    Dim UnitRecord As Vehicle
    Dim n As Long
    
    On Error GoTo BugReport
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub DispatchToAtScene")
    End If
    
    cMessage = "Start of DispatchToAtScene"
    
    '   Before inserting new events for a unit, we must be sure that the event queue is
    '   purged of all pending events for the unit.
    
    Call AddToDebugList("[DEBUG] DispatchToAtScene: params nUnitID =" & Str(nUnitID), lstDebug)
    Call AddToDebugList("[DEBUG] DispatchToAtScene: params CurrentStatus =" & Str(CurrentStatus), lstDebug)
    Call AddToDebugList("[DEBUG] DispatchToAtScene: params StatusTime =" & Format(StatusTime, DATE_FORMAT), lstDebug)
    
    Call PurgeEvents(nUnitID)
    
    Call AddToDebugList("[DEBUG] DispatchToAtScene: Events Purged", lstDebug)
    
    OldStatus = GetCurrentStatus(nUnitID)
    
    Call AddToDebugList("[DEBUG] DispatchToAtScene: CurrentStatus =" & Str(OldStatus), lstDebug)
   
    '   Calculate Chute Time for the new assignment
    
'    ChuteTime = CalculateChuteTime(OldStatus, StatusTime)

'    Call AddToDebugList("[DEBUG] DispatchToAtScene: ChuteTime =" & Str(ChuteTime))
    
    n = RefreshUnitInfo(nUnitID)                   '   update the unit's info

    cMessage = "RefreshUnitInfo"
    
    If gUnitList(n).Status <> STAT_OOS Then
    
        ChuteTime = CalculateChuteTime(gUnitList(n).LastStatus, StatusTime)
        cMessage = "CalculateChuteTime"
        
        nInc = RefreshIncidentInfo(gUnitList(n))        '   get all info on the unit's current assignment
        cMessage = "RefreshIncidentInfo"
        
        gUnitList(n).UpdatePending = True
    
        nOldIDInfo = bsiFindOldIncID(gUnitList(n).RMIID)
        cMessage = "bsiFindOldIncID"
        
    '    Call AddToDebugList("[DEBUG] DispatchToAtScene: nOldIDInfo =" & Str(nOldIDInfo))
        
        NextStatus = STAT_Resp
        Call InsertStatusEvent(gUnitList(n), CurrentStatus, NextStatus, ChuteTime)      '   insert the ACK status change
        cMessage = "InsertStatusEvent"
        Call InsertRadioMessage(gUnitList(n), NextStatus, ChuteTime, , gUnitList(n).LastStatus)
        cMessage = "InsertRadioMessage"
        
        gUnitList(n).StatusChangeTime = ChuteTime
        gUnitList(n).NextStatus = STAT_Resp
        gUnitList(n).EndStatus = STAT_OnScene
        
        '   calculate route to scene
        
        If gUseESRIResources Then
            Call CalculateESRIRoute(gUnitList(n), ROUTE_ToScene, gUseOneWayRestrictions)
            cMessage = "CalculateESRIRoute"
        Else
            Call CalculateRoute(gUnitList(n), ROUTE_ToScene)
        End If
        
        '   StatusChangeTime = InsertAVLEvents(UnitRecord, ChuteTime)
        
    '    Call AddToDebugList("[DEBUG] DispatchToAtScene: StatusChangeTime =" & Format(StatusChangeTime, DATE_FORMAT))
        
        '   CurrentStatus = NextStatus      '   increment to the next status (OnScene)
        '   NextStatus = STAT_OnScene
        
    '    Call AddToDebugList("[DEBUG] DispatchToAtScene: Beginning Cancellation Checks")
        
        If nOldIDInfo >= 0 Then
        
            cMessage = "If nOldIDInfo >= 0"
            If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitAssigned) Then                              '   keep track of the first assign time
                gOldIncInfo(nOldIDInfo).TimeFirstUnitAssigned = Now                                     '   so we can deal with cancellations later
            End If
            
            If gOldIncInfo(nOldIDInfo).Cancelled And gOldIncInfo(nOldIDInfo).OnSceneTime = 0 Then       '   cancelled prior to at scene
                
                If gOldIncInfo(nOldIDInfo).ClearTime > 0 Then                                           '   we should have this time, but you never know
                    
                    TempTime = DateAdd("S", gOldIncInfo(nOldIDInfo).ClearTime, Now)
                    If TempTime < StatusChangeTime Then
                        cMessage = "CancelledEnroute"
                        Call CancelledEnroute(nUnitID, TempTime)
                    Else
    '                    Call AddToDebugList("  cMessage: Call NOT Cancelled Prior to At Scene")
                    End If
                End If
            End If
        
    '        Call AddToDebugList("  cMessage: Check for FirstUnitOnScene Time")
            
            If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene) Then
                gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene = StatusChangeTime
    '            Call AddToDebugList("  cMessage: FirstUnitOnScene Time Set")
            End If
        End If
        
        '   Call InsertStatusEvent(UnitRecord, CurrentStatus, NextStatus, StatusChangeTime)    '   insert the call segment termination status change
        '   Call InsertRadioMessage(UnitRecord, NextStatus, StatusChangeTime)
    
        gUnitList(n).UpdatePending = False
    
    Else
    
        Call AddToDebugList("[INFO] DispatchToAtScene: Unit " & gUnitList(n).UnitName & "(ID=" & nUnitID & ") Out of Service when STATUS DISPATCHED Received.", lstDebug)

    End If
    
    Exit Sub

BugReport:
    
    Call AddToDebugList("[ERROR] Occurred in DispatchToAtScene: " & Str(Err.Number) & ", " & Err.Description, lstDebug)
    On Error Resume Next
    Call AddToDebugList("  cMessage: >" & cMessage & "<", lstDebug)
    Call AddToDebugList("   varname: nUnitID       = " & Str(nUnitID), lstDebug)
    Call AddToDebugList("   varname: CurrentStatus = " & Str(CurrentStatus), lstDebug)
    Call AddToDebugList("   varname: nUnit         = " & Str(nUnit), lstDebug)
    Call AddToDebugList("   varname: nInc          = " & Str(nInc), lstDebug)
    Call AddToDebugList("   varname: nOldIDInfo    = " & Str(nOldIDInfo), lstDebug)
    Call AddToDebugList("   varname: OldStatus     = " & Str(OldStatus), lstDebug)
    Call AddToDebugList("   varname: NextStatus    = " & Str(NextStatus), lstDebug)
    Call AddToDebugList("   varname: ChuteTime     = " & Format(ChuteTime, DATE_FORMAT), lstDebug)
    Call AddToDebugList("   varname: LongChute     = " & Str(LongChute), lstDebug)
    Call AddToDebugList("   varname: ShortChute    = " & Str(ShortChute), lstDebug)
    Call AddToDebugList("   varname: CancelTime = " & Format(TempTime, DATE_FORMAT), lstDebug)
    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT), lstDebug)
    Call AddToDebugList("   varname: Found         = " & IIf(Found, "True", "False"), lstDebug)

End Sub

Private Sub ArrivedAtScene(nUnitID As Long)
    Dim nUnit As Long, nInc As Integer
    Dim nOldIDInfo As Long
    Dim OldStatus As Integer
    Dim NextStatus As Integer
    Dim ChuteTime As Double
    Dim LongChute As Double, ShortChute As Double
    Dim StatusChangeTime As Variant
    Dim tHosp As CADLocation
    Dim UsedClosest As Boolean
    Dim cMsg As String
    Dim UnitRecord As Vehicle
    Dim n As Long
    Dim bUseSourceInfo As Boolean
    
    On Error GoTo BugReport
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ArrivedAtScene")
    End If
    
    '    gSceneMean = Val(bsiGetSettings("AVL", "SceneTimeMean", 600, INIPath & "\Simulator.INI"))
    '    gSceneRange = Val(bsiGetSettings("AVL", "SceneTimeRange", 600, INIPath & "\Simulator.INI"))
    '    gDestMean = Val(bsiGetSettings("AVL", "DestTimeMean", 600, INIPath & "\Simulator.INI"))
    '    gDestRange = Val(bsiGetSettings("AVL", "DestTimeRange", 600, INIPath & "\Simulator.INI"))
    '    gUseSourceSceneTime = IIf(UCase(bsiGetSettings("AVL", "UseSourceSceneTime", "false", INIPath & "\Simulator.INI")) = "TRUE", True, False)
    '    gUseSourceDestTime = IIf(UCase(bsiGetSettings("AVL", "UseSourceDestTime", "false", INIPath & "\Simulator.INI")) = "TRUE", True, False)
    '    gUseSourceDest = IIf(UCase(bsiGetSettings("AVL", "UseSourceDest", "false", INIPath & "\Simulator.INI")) = "TRUE", True, False)
    
    '   Just in case, before inserting new events for a unit, we must be sure that the event queue is
    '   purged of all pending events for the unit.
    
    cMsg = "Purging Events"
    
    Call PurgeEvents(nUnitID)
    
    cMsg = "Unit Info"
    
    n = RefreshUnitInfo(nUnitID)                     '   update the unit's info

    cMsg = "Incident Info"
    
    bUseSourceInfo = gUseSourceDest
    
    nInc = RefreshIncidentInfo(gUnitList(n))        '   get all info on the unit's current assignment
    
    gUnitList(n).UpdatePending = True

    cMsg = "Find Old"
    
    If (nOldIDInfo = bsiFindOldIncID(gUnitList(n).RMIID)) >= 0 Then         '   if there is a source incident
        If gOldIncInfo(nOldIDInfo).PreScheduledIncident Then                '   and it is a prescheduled call,
            bUseSourceInfo = True                                           '   override destination choice settings and use source information
        End If
    End If
    
    If gIncidents(nInc).PreScheduledIncident Then
        bUseSourceInfo = True                               '   this is a prescheduled call, so override destination choice settings and use source information
    End If
    
    If bUseSourceInfo Then                                 '   This is a script incident, so we have history to deal with.
    
        cMsg = "Scripted Incident"
    
        If gOldIncInfo(nOldIDInfo).Transported > 0 And gUnitList(n).TXApproved Then      '   decrement the number of patients left to transport transport
            gOldIncInfo(nOldIDInfo).Transported = gOldIncInfo(nOldIDInfo).Transported - 1   '   and once it gets to 0, everyone else will be cancelled on scene
            If Not gUseSourceSceneTime Or nOldIDInfo < 0 Then
                StatusChangeTime = DateAdd("S", (gSceneMean - gSceneRange) + (Rnd * gSceneRange * 2), Now)  '   calculate onscene time
            Else
                If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene) Then                       '   if we don't have an onscene time already
                    gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene = Now                              '   record it, then
                    StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).OnSceneTime, Now)       '   calculate onscene time
                Else                                                                                '   otherwise, calculate the onscene time based on the
                    StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).OnSceneTime, gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene)       '   recorded onscene value
                End If
            End If
            
            If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitDepartScene) Then                   '   save the first unit depart scene time
                gOldIncInfo(nOldIDInfo).TimeFirstUnitDepartScene = StatusChangeTime             '   for use in later cancellations
            End If
            
            cMsg = "Getting Hospital"
        
            If Not gUseSourceDest Or nOldIDInfo < 0 Then
                tHosp = FindClosestHosp(gIncidents(nInc), UsedClosest)              '   select closest destination
            Else
                tHosp = FindSourceHosp(gOldIncInfo(nOldIDInfo), UsedClosest, gIncidents(nInc))        '   select source destination
            End If
            
            gUnitList(n).DestinationCode = tHosp.Code
            gUnitList(n).DestinationLoc = tHosp.Name
            gUnitList(n).DestinationLat = tHosp.Lat
            gUnitList(n).DestinationLon = tHosp.Lon
            
            gUnitList(n).StatusChangeTime = StatusChangeTime
            gUnitList(n).NextStatus = STAT_Transport
            gUnitList(n).EndStatus = STAT_AtDest
            
            gIncidents(nInc).DE_Code = tHosp.Code
            gIncidents(nInc).DE_Location = tHosp.Name
            gIncidents(nInc).DE_Address = tHosp.Address
            gIncidents(nInc).DE_City = tHosp.City
            gIncidents(nInc).DE_Lat = tHosp.Lat
            gIncidents(nInc).DE_Lon = tHosp.Lon
        
            Call InsertTransportEvent(gUnitList(n), STAT_OnScene, STAT_Transport, StatusChangeTime, UsedClosest)    '   insert the termination status change
            Call InsertRadioMessage(gUnitList(n), STAT_Transport, StatusChangeTime, tHosp.Name)
            
            If gUseESRIResources Then
                Call CalculateESRIRoute(gUnitList(n), ROUTE_ToHosp, gUseOneWayRestrictions)
            Else
                Call CalculateRoute(gUnitList(n), ROUTE_ToHosp)
            End If
            
            '   StatusChangeTime = InsertAVLEvents(UnitRecord, StatusChangeTime)
            
            '   Call InsertStatusEvent(UnitRecord, STAT_Transport, STAT_AtDest, StatusChangeTime)    '   insert the termination status change
            '   Call InsertRadioMessage(UnitRecord, STAT_AtDest, StatusChangeTime, tHosp.Name)
            
            '    If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitAtDest) Then                                '   record time at destination for future calculations
            '        gOldIncInfo(nOldIDInfo).TimeFirstUnitAtDest = StatusChangeTime
            '    End If
        
        Else
            '   this unit will get cancelled
            If Not gUseSourceSceneTime Or nOldIDInfo < 0 Then
                StatusChangeTime = DateAdd("S", (gSceneMean - gSceneRange) + (Rnd * gSceneRange * 2), Now)  '   calculate onscene time
                Call ClearedAtScene(nUnitID, StatusChangeTime)
            ElseIf gOldIncInfo(nOldIDInfo).Cancelled Then                                               '   no one transported from this call
                If gOldIncInfo(nOldIDInfo).OnSceneTime = 0 Then                                         '   no onscene time was calculated, so clear is from time assigned
                    StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).ClearTime, gOldIncInfo(nOldIDInfo).TimeFirstUnitAssigned)  '   calculate time call cancelled
                Else
                    If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene) Then                       '   if we don't have an onscene time already
                        gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene = Now                              '   record it, then
                        StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).ClearTime, Now)         '   calculate time call cancelled
                    Else                                                                                '   otherwise, calculate the onscene time based on the
                        StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).ClearTime, gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene)       '   recorded onscene value
                    End If
                End If
                
                If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitDepartScene) Then                       '   save the first unit cancelled time
                    gOldIncInfo(nOldIDInfo).TimeFirstUnitDepartScene = StatusChangeTime
                Else
                    StatusChangeTime = DateAdd("S", 30, gOldIncInfo(nOldIDInfo).TimeFirstUnitDepartScene)   '   calculate time call cancelled - 30 seconds after other unit transport
                End If
                Call ClearedAtScene(nUnitID, StatusChangeTime)
            Else
                If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitDepartScene) Then                                '   no time for first unit, so this is the first unit
                    If gOldIncInfo(nOldIDInfo).OnSceneTime > 0 Then
                        If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene) Then                       '   if we don't have an onscene time already
                            gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene = Now                              '   record it, then
                            StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).OnSceneTime + 30, Now)       '   calculate time call cancelled
                        Else                                                                                '   otherwise, calculate the onscene time based on the
                            StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).OnSceneTime + 30, gOldIncInfo(nOldIDInfo).TimeFirstUnitOnScene)       '   recorded onscene value
                        End If
                    ElseIf gOldIncInfo(nOldIDInfo).ClearTime > 0 Then
                        StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).ClearTime + 30, gOldIncInfo(nOldIDInfo).TimeFirstUnitAssigned)     '   calculate onscene time
                    End If
                Else
                    StatusChangeTime = DateAdd("S", 30, gOldIncInfo(nOldIDInfo).TimeFirstUnitDepartScene)   '   second unit clears 30 seconds after the first
                End If
                Call ClearedAtScene(nUnitID, StatusChangeTime)
            End If
        
        End If
    Else
        
        '   we are not depending on incident history for destination decisions, so we pick based on our configuration
        
        StatusChangeTime = DateAdd("S", (gSceneMean - gSceneRange) + (Rnd * gSceneRange * 2), Now)  '   calculate onscene time
            
        If (Rnd * 100) <= gTransportPercent And gUnitList(n).TXApproved Or gLoadTestRunning Then        '   randomly determine if we will transport. In a load test, all units transport regardless of capabilities
            
            tHosp = FindClosestHosp(gIncidents(nInc), UsedClosest)              '   select closest destination
            
            gUnitList(n).DestinationCode = tHosp.Code
            gUnitList(n).DestinationLoc = tHosp.Name
            gUnitList(n).DestinationCity = tHosp.City
            gUnitList(n).DestinationLat = tHosp.Lat
            gUnitList(n).DestinationLon = tHosp.Lon
            
            gUnitList(n).StatusChangeTime = StatusChangeTime
            gUnitList(n).NextStatus = STAT_Transport
            gUnitList(n).EndStatus = STAT_AtDest
            
            gIncidents(nInc).DE_Code = tHosp.Code
            gIncidents(nInc).DE_Location = tHosp.Name
            gIncidents(nInc).DE_Address = tHosp.Address
            gIncidents(nInc).DE_City = tHosp.City
            gIncidents(nInc).DE_Lat = tHosp.Lat
            gIncidents(nInc).DE_Lon = tHosp.Lon
        
            Call InsertTransportEvent(gUnitList(n), STAT_OnScene, STAT_Transport, StatusChangeTime, UsedClosest)    '   insert the termination status change
            Call InsertRadioMessage(gUnitList(n), STAT_Transport, StatusChangeTime, tHosp.Name)
            
            If gUseESRIResources Then
                Call CalculateESRIRoute(gUnitList(n), ROUTE_ToHosp, gUseOneWayRestrictions)
            Else
                Call CalculateRoute(gUnitList(n), ROUTE_ToHosp)
            End If
            
            '    StatusChangeTime = InsertAVLEvents(UnitRecord, StatusChangeTime)
            '
            '    Call InsertStatusEvent(UnitRecord, STAT_Transport, STAT_AtDest, StatusChangeTime)    '   insert the termination status change
            '    Call InsertRadioMessage(UnitRecord, STAT_AtDest, StatusChangeTime, tHosp.Name)
        Else
            '   this unit will get cancelled
            Call ClearedAtScene(nUnitID, StatusChangeTime)
        End If
    End If
    
    gUnitList(n).UpdatePending = False

    Exit Sub

BugReport:
    Call AddToDebugList("ERROR Occurred in ArrivedAtScene: " & Str(Err.Number) & ", " & Err.Description, lstDebug)
    On Error Resume Next
    Call AddToDebugList("   varname: nUnitID       = " & Str(nUnitID), lstDebug)
    Call AddToDebugList("   varname: nUnit         = " & Str(nUnit), lstDebug)
    Call AddToDebugList("   varname: nInc          = " & Str(nInc), lstDebug)
    Call AddToDebugList("   varname: nOldIDInfo    = " & Str(nOldIDInfo), lstDebug)
    Call AddToDebugList("   varname: OldStatus     = " & Str(OldStatus), lstDebug)
    Call AddToDebugList("   varname: NextStatus    = " & Str(NextStatus), lstDebug)
    Call AddToDebugList("   varname: ChuteTime     = " & Str(ChuteTime), lstDebug)
    Call AddToDebugList("   varname: LongChute     = " & Str(LongChute), lstDebug)
    Call AddToDebugList("   varname: ShortChute    = " & Str(ShortChute), lstDebug)
    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT), lstDebug)
    Call AddToDebugList("   varname: UsedClosest   = " & IIf(UsedClosest, "True", "False"), lstDebug)
End Sub

Private Sub ArrivedAtDest(nUnitID As Long)
    Dim nUnit As Long, nInc As Integer
    Dim nOldIDInfo As Long
    Dim OldStatus As Integer
    Dim NextStatus As Integer
    Dim StatusChangeTime As Variant
    Dim UnitRecord As Vehicle
    
    On Error GoTo BugReport
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ArrivedAtDest")
    End If
    
    '    gDestMean = Val(bsiGetSettings("AVL", "DestTimeMean", 600, INIPath & "\Simulator.INI"))
    '    gDestRange = Val(bsiGetSettings("AVL", "DestTimeRange", 600, INIPath & "\Simulator.INI"))
    '    gUseSourceDestTime = IIf(UCase(bsiGetSettings("AVL", "UseSourceDestTime", "false", INIPath & "\Simulator.INI")) = "TRUE", True, False)
    
    nUnit = RefreshUnitInfo(nUnitID)                    '   update the unit's info

    gUnitList(nUnit).UpdatePending = True

    nInc = RefreshIncidentInfo(gUnitList(nUnit))        '   get all info on the unit's current assignment
    
    nOldIDInfo = bsiFindOldIncID(gUnitList(nUnit).RMIID)
    
    If Not gUseSourceDestTime Or nOldIDInfo < 0 Then
        StatusChangeTime = DateAdd("S", (gDestMean - gDestRange) + (Rnd * gDestRange * 2), Now)  '   calculate onscene time
    Else
        '   If IsEmpty(gOldIncInfo(nOldIDInfo).TimeFirstUnitAtDest) Then
            StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).AtDestTime, Now)     '   calculate at destination time
        '    Else
        '        StatusChangeTime = DateAdd("S", gOldIncInfo(nOldIDInfo).AtDestTime, gOldIncInfo(nOldIDInfo).TimeFirstUnitAtDest)     '   calculate at destination time
        '    End If
    End If

    '    If gReturnToBase Then
    '        Call InsertStatusEvent(gUnitList(nUnit), STAT_AtDest, STAT_DelayedAvail, StatusChangeTime)                 '   insert the termination status change
    '        Call InsertRadioMessage(gUnitList(nUnit), STAT_Avail, StatusChangeTime)
        '   Call ReturnToStation(nUnitID, StatusChangeTime)
    '    Else
        Call InsertStatusEvent(gUnitList(nUnit), STAT_AtDest, STAT_DelayedAvail, StatusChangeTime)          '   insert the termination status change
        Call InsertRadioMessage(gUnitList(nUnit), STAT_DelayedAvail, StatusChangeTime)
    '    End If
    
    Call InsertRadioMessage(gUnitList(nUnit), STAT_FREEFORMTEXT, DateAdd("S", 90, StatusChangeTime), , , "Did you get our last message? We are ready to clear at " & gUnitList(nUnit).CurrentLoc)
    
    gUnitList(nUnit).UpdatePending = False

    Exit Sub
    
BugReport:
    On Error Resume Next
    
    Call AddToDebugList("ERROR Occurred in ArrivedAtDest: " & Str(Err.Number) & ", " & Err.Description, lstDebug)
    Call AddToDebugList("   varname: nUnitID       = " & Str(nUnitID), lstDebug)
    Call AddToDebugList("   varname: nUnit         = " & Str(nUnit), lstDebug)
    Call AddToDebugList("   varname: nInc          = " & Str(nInc), lstDebug)
    Call AddToDebugList("   varname: nOldIDInfo    = " & Str(nOldIDInfo), lstDebug)
    Call AddToDebugList("   varname: OldStatus     = " & Str(OldStatus), lstDebug)
    Call AddToDebugList("   varname: NextStatus    = " & Str(NextStatus), lstDebug)
    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT), lstDebug)

End Sub

Private Sub ClearedAtScene(nUnitID As Long, StatusChangeTime As Variant)
    Dim nUnit As Long, nInc As Integer
    Dim nOldIDInfo As Long
    Dim OldStatus As Integer
    Dim NextStatus As Integer
    Dim UnitRecord As Vehicle
    
    On Error GoTo BugReport
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ClearedAtScene")
    End If
    
    nUnit = RefreshUnitInfo(nUnitID)                     '   update the unit's info

    gUnitList(nUnit).UpdatePending = True

    nInc = RefreshIncidentInfo(gUnitList(nUnit))        '   get all info on the unit's current assignment
    
    nOldIDInfo = bsiFindOldIncID(gUnitList(nUnit).RMIID)
    
    Call InsertStatusEvent(gUnitList(nUnit), STAT_OnScene, STAT_DelayedAvail, StatusChangeTime)         '   insert the termination status change
    Call InsertRadioMessage(gUnitList(nUnit), STAT_AvailOS, StatusChangeTime)
    Call InsertRadioMessage(gUnitList(nUnit), STAT_FREEFORMTEXT, DateAdd("S", 45, StatusChangeTime), , , "Did you get our last message? We are cancelled at " & gUnitList(nUnit).CurrentLoc)
    
    If gReturnToBase Then
        '   Call ReturnToStation(nUnitID, StatusChangeTime, True)
    Else
        Call AwaitInstructionsFromDispatch(nUnitID, StatusChangeTime, True)
    End If

    gUnitList(nUnit).UpdatePending = False

    Exit Sub
    
BugReport:
    On Error Resume Next
    
    Call AddToDebugList("ERROR Occurred in ClearedAtScene: " & Str(Err.Number) & ", " & Err.Description, lstDebug)
    Call AddToDebugList("   varname: nUnitID       = " & Str(nUnitID), lstDebug)
    Call AddToDebugList("   varname: nUnit         = " & Str(nUnit), lstDebug)
    Call AddToDebugList("   varname: nInc          = " & Str(nInc), lstDebug)
    Call AddToDebugList("   varname: nOldIDInfo    = " & Str(nOldIDInfo), lstDebug)
    Call AddToDebugList("   varname: OldStatus     = " & Str(OldStatus), lstDebug)
    Call AddToDebugList("   varname: NextStatus    = " & Str(NextStatus), lstDebug)
    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT), lstDebug)

End Sub

Private Sub CancelledEnroute(nUnitID As Long, StatusChangeTime As Variant)
    Dim nUnit As Long, nInc As Integer
    Dim nOldIDInfo As Long
    Dim OldStatus As Integer
    Dim NextStatus As Integer
    Dim xItem As ListItem
    Dim UnitRecord As Vehicle
    
    On Error GoTo BugReport
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub CancelledEnroute")
    End If
    
    nUnit = RefreshUnitInfo(nUnitID)                    '   update the unit's info

    nInc = RefreshIncidentInfo(gUnitList(nUnit))        '   get all info on the unit's current assignment
    
    nOldIDInfo = bsiFindOldIncID(gUnitList(nUnit).RMIID)
    
    Set xItem = lvEvents.ListItems.Add(, , Format(StatusChangeTime, "hh:nn:ss"))    '   insert the cancellation message
    xItem.SubItems(LVUnit) = ""
    xItem.SubItems(LVType) = Format(EV_RADIO, "0000")
    xItem.SubItems(LVDur) = "TALK"
    xItem.SubItems(LVDist) = "TALK"
    xItem.SubItems(LVMessage) = "Cancellation for (" & gUnitList(nUnit).CurrentDivision & "): Call at " & gUnitList(nUnit).SceneLoc & " cancelled"
    xItem.SubItems(LVSort) = Format(StatusChangeTime, DATE_FORMAT)
    
    Exit Sub
    
BugReport:
    On Error Resume Next
    
    Call AddToDebugList("ERROR Occurred in CancelledEnroute: " & Str(Err.Number) & ", " & Err.Description, lstDebug)
    Call AddToDebugList("   varname: nUnitID       = " & Str(nUnitID), lstDebug)
    Call AddToDebugList("   varname: nUnit         = " & Str(nUnit), lstDebug)
    Call AddToDebugList("   varname: nInc          = " & Str(nInc), lstDebug)
    Call AddToDebugList("   varname: nOldIDInfo    = " & Str(nOldIDInfo), lstDebug)
    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT), lstDebug)

End Sub

Private Sub ReturnToStation(nUnitID As Long, Optional StatusChangeTime As Variant, Optional CancelledOnScene As Boolean = False)
    Dim CurrentStatus As Integer
    Dim nUnit As Long
    Dim UnitRecord As Vehicle
    
    On Error GoTo BugReport
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ReturnToStation")
    End If
    
    If IsMissing(StatusChangeTime) Then
        StatusChangeTime = Now
    End If
    
    nUnit = RefreshUnitInfo(nUnitID)                    '   update the unit's info
    
    gUnitList(nUnit).UpdatePending = True
    
    If Not CancelledOnScene Then
        Call PurgeEvents(nUnitID)                           '   delete events, just in case there are some left over
    
        gUnitList(nUnit).NextStatus = STAT_Avail
        gUnitList(nUnit).EndStatus = STAT_AtStation
        gUnitList(nUnit).DestinationLoc = gUnitList(nUnit).Station
    
        CurrentStatus = gUnitList(nUnit).Status
        If gUnitList(nUnit).LastStatus = STAT_OOS Or gUnitList(nUnit).LastStatus = STAT_OnScene Or gUnitList(nUnit).LastStatus = STAT_AtDest Then
            Call InsertRadioMessage(gUnitList(nUnit), CurrentStatus, Now)
        End If
        gUnitList(nUnit).StatusChangeTime = StatusChangeTime
    Else
        gUnitList(nUnit).NextStatus = STAT_Avail
        gUnitList(nUnit).EndStatus = STAT_AtStation
        gUnitList(nUnit).DestinationLoc = gUnitList(nUnit).Station
    
        CurrentStatus = gUnitList(nUnit).NextStatus
        '   Call InsertRadioMessage(gUnitList(nUnit), CurrentStatus, StatusChangeTime)
        gUnitList(nUnit).StatusChangeTime = StatusChangeTime
    End If
    
    If gUseESRIResources Then
        Call CalculateESRIRoute(gUnitList(nUnit), ROUTE_ToStation, gUseOneWayRestrictions)
    Else
        Call CalculateRoute(gUnitList(nUnit), ROUTE_ToStation)
    End If
    
    '    StatusChangeTime = InsertAVLEvents(UnitRecord, StatusChangeTime)
    '
    '    Call InsertStatusEvent(UnitRecord, CurrentStatus, STAT_AtStation, StatusChangeTime)    '   insert the termination status change
    '    Call InsertRadioMessage(UnitRecord, STAT_AtStation, StatusChangeTime)
    '
    gUnitList(nUnit).UpdatePending = False

    Exit Sub
    
BugReport:
    On Error Resume Next
    
    Call AddToDebugList("ERROR Occurred in ReturnToStation: " & Str(Err.Number) & ", " & Err.Description, lstDebug)
    Call AddToDebugList("   varname: nUnitID       = " & Str(nUnitID), lstDebug)
    Call AddToDebugList("   varname: CurrentStatus = " & Str(CurrentStatus), lstDebug)
    Call AddToDebugList("   varname: nUnit         = " & Str(nUnit), lstDebug)
    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT), lstDebug)

End Sub

Private Sub AwaitInstructionsFromDispatch(nUnitID As Long, Optional StatusChangeTime As Variant, Optional CancelledOnScene As Boolean = False)
    Dim CurrentStatus As Integer
    Dim nUnit As Long
    Dim UnitRecord As Vehicle
    
    On Error GoTo BugReport
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub AwaitInstructionsFromDispatch")
    End If
    
    If IsMissing(StatusChangeTime) Then
        StatusChangeTime = Now
    End If
    
    nUnit = RefreshUnitInfo(nUnitID)                    '   update the unit's info
    
    If Not CancelledOnScene Then
        Call PurgeEvents(nUnitID)                           '   delete events, just in case there are some left over
        CurrentStatus = gUnitList(nUnit).Status
        Call InsertRadioMessage(gUnitList(nUnit), CurrentStatus, Now)
    End If
    
    Exit Sub
    
BugReport:
    On Error Resume Next
    
    Call AddToDebugList("ERROR Occurred in AwaitInstructionsFromDispatch: " & Str(Err.Number) & ", " & Err.Description, lstDebug)
    Call AddToDebugList("   varname: nUnitID       = " & Str(nUnitID), lstDebug)
    Call AddToDebugList("   varname: CurrentStatus = " & Str(CurrentStatus), lstDebug)
    Call AddToDebugList("   varname: nUnit         = " & Str(nUnit), lstDebug)
    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT), lstDebug)

End Sub

Private Sub ChangeDestination(IPCMessage As String)
    Dim aIDs As Variant
    Dim nUnit As Long
    Dim CurrentStatus As Integer
    Dim StatusChangeTime As Variant
    Dim UnitRecord As Vehicle
    
    On Error GoTo BugReport
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ChangeDestination")
    End If
    
    aIDs = Split(IPCMessage, ";")
    
    Call PurgeEvents(Val(aIDs(1)))                      '   delete events, just in case there are some left over

    nUnit = RefreshUnitInfo(Val(aIDs(1)))               '   update the unit's info

    CurrentStatus = gUnitList(nUnit).Status
    
    Call InsertRadioMessage(gUnitList(nUnit), CurrentStatus, Now)

    If gUseESRIResources Then
        Call CalculateESRIRoute(gUnitList(nUnit), ROUTE_ToDestination, gUseOneWayRestrictions)
    Else
        Call CalculateRoute(gUnitList(nUnit), ROUTE_ToDestination)
    End If

    '    StatusChangeTime = InsertAVLEvents(UnitRecord, Now)
    '
    '    Call InsertStatusEvent(UnitRecord, CurrentStatus, STAT_AtDest, StatusChangeTime)    '   insert the termination status change
    '    Call InsertRadioMessage(UnitRecord, STAT_AtDest, StatusChangeTime)
    
    Exit Sub

BugReport:
    On Error Resume Next
    
    Call AddToDebugList("ERROR Occurred in ChangeDestination: " & Str(Err.Number) & ", " & Err.Description, lstDebug)
    Call AddToDebugList("   varname: IPCMessage    = " & IPCMessage, lstDebug)
    Call AddToDebugList("   varname: nUnit         = " & Str(nUnit), lstDebug)
    Call AddToDebugList("   varname: CurrentStatus = " & Str(CurrentStatus), lstDebug)
    Call AddToDebugList("   varname: StatusChangeTime = " & Format(StatusChangeTime, DATE_FORMAT), lstDebug)

End Sub

Private Function GetUnitRecord(nUnitID As Long) As Vehicle
    Dim Found As Boolean
    Dim n As Long
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetUnitRecord")
    End If
    
    Found = False
    
    '   Find UnitName in list
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).VehID = nUnitID Then
            Found = True
            Exit For
        End If
    Next n
    
    GetUnitRecord = gUnitList(n)

End Function

Private Function GetUnitRecordbyName(cName As String) As Vehicle
    Dim Found As Boolean
    Dim n As Long
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetUnitRecordbyName")
    End If
    
    Found = False
    
    '   Find UnitName in list
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).UnitName = cName Then
            Found = True
            Exit For
        End If
    Next n
    
    GetUnitRecordbyName = gUnitList(n)

End Function

Private Function GetUnitIndexByName(cName As String) As Long
    Dim Found As Boolean
    Dim n As Long
    
    '   Find UnitName in list
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetUnitIndexByName")
    End If
    
    GetUnitIndexByName = -1
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).UnitName = cName Then
            GetUnitIndexByName = n
            Exit For
        End If
    Next n
    
End Function

Private Function GetUnitIndexByVehID(nVehID As Long) As Long
    Dim Found As Boolean
    Dim n As Long
    
    '   Find UnitName in list
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetUnitIndexByVehID")
    End If
    
    GetUnitIndexByVehID = -1
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).VehID = nVehID Then
            GetUnitIndexByVehID = n
            Exit For
        End If
    Next n
    
End Function

Private Function GetCurrentStatus(nUnitID) As Integer
    Dim Found As Boolean
    Dim nCurrentStatus As Integer
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetCurrentStatus")
    End If
    
    Found = False
    nCurrentStatus = -1
    
    '   Find UnitName in list
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).VehID = nUnitID Then
            Found = True
            Exit For
        End If
    Next n
    
    If Found Then
        AddToDebugList "[GetCurrentStatus] Current Status for " & gUnitList(n).UnitName & " is" & Str(gUnitList(n).Status), lstDebug
        nCurrentStatus = gUnitList(n).Status
    Else
        AddToDebugList "[ERROR - GetCurrentStatus] Unable to locate Unit ID:" & Str(nUnitID), lstDebug
    End If

    GetCurrentStatus = nCurrentStatus

End Function

Private Function CalculateChuteTime(nStatus As Integer, Optional FromWhen As Variant) As Variant
    Dim LongChute As Variant
    Dim ShortChute As Variant
    Dim ChuteTime As Double
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function CalculateChuteTime")
    End If
'    gChuteMean = Val(bsiGetSettings("AVL", "ChuteTimeMean", 60, App.Path & "\Simulator.INI"))
'    gChuteRange = Val(bsiGetSettings("AVL", "ChuteTimeRange", 18, App.Path & "\Simulator.INI"))
    
    If IsMissing(FromWhen) Then
        FromWhen = Now
    End If
    
    ChuteTime = gChuteMean - Int(gChuteRange) + Int(Rnd * gChuteRange * 2) '   chute time in seconds for this response
    LongChute = DateAdd("S", ChuteTime, FromWhen)                           '   not mobile, random 60 sec +- 15%
    ShortChute = DateAdd("S", Int(ChuteTime / 15), FromWhen)                '   mobile is much shorter

    '   is the unit mobile or stationary?

    Select Case nStatus
        Case STAT_AtStation
            CalculateChuteTime = LongChute
        Case STAT_ATP
            CalculateChuteTime = ShortChute
        Case STAT_Enr2Post
            CalculateChuteTime = ShortChute
        Case STAT_OnScene
            CalculateChuteTime = ShortChute
        Case STAT_LocalArea
            CalculateChuteTime = ShortChute
        Case STAT_AtDest
            CalculateChuteTime = LongChute
        Case STAT_DelayedAvail
            CalculateChuteTime = LongChute
        Case STAT_Avail
            CalculateChuteTime = ShortChute
        Case Else
            CalculateChuteTime = ShortChute
    End Select
    
End Function

Private Function RefreshUnitInfo(ByVal nUnitID As Long) As Long
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim n As Long
    Dim Found As Boolean
    Dim LocationText As String
    Dim OldSectorDivision As String
    Dim UnitLocation As CADLocation
    
    '   Dim lUnit As MapPointCtl.Location
    Dim nUnit As Integer
    
    Dim cError As String
    
    On Error GoTo ErrHandler:
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function RefreshUnitInfo")
    End If
    
    '   need to include a test here to identify sector/division changes
    
    Found = False
    n = 0
    
    '   Find UnitName in list
    
    For n = 0 To UBound(gUnitList)
        If gUnitList(n).VehID = nUnitID Then
            Found = True
            If gUnitList(n).CurrentLat = 0 Or gUnitList(n).CurrentLon = 0 Then          '   catch a VisiCAD Bug here.
                UnitLocation = GetLocationByName(gUnitList(n).CurrentLoc)
                gUnitList(n).CurrentLat = UnitLocation.Lat
                gUnitList(n).CurrentLon = UnitLocation.Lon
            End If
            Exit For
        End If
    Next n
    
    If Found Then                                       '   test first to see if we've actually found a unit
        If Not gUnitList(n).UpdatePending Then
            
            OldSectorDivision = gUnitList(n).CurrentDivision
        
            cSQL = "Select u.Code, v.ID, v.AVL_ID, v.AVLEnabled, " _
                            & " v.Current_Lat, v.Current_Lon, v.Current_Location, v.Current_City, " _
                            & " v.Destination_Lat, v.Destination_Lon, v.Status_ID, v.MST_MDT_ID, " _
                            & " s.Name, s.Lat, s.Lon, d.DivName, v.master_incident_id, " _
                            & " v.destination_location, v.destination_city, r.description " _
                            & " from Vehicle v Join Unit_Names u " _
                                & " on v.UnitName_ID = u.ID" _
                                & " join Stations s on v.Post_Station_ID = s.ID " _
                                & " join Division d on d.ID = v.CurrentDivision_ID " _
                                & " join Resource r on v.PrimaryResource_ID = r.ID " _
                                & " Where v.ID = " & Str(nUnitID)
        
            nRecs = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
        
            If nRecs > 0 Then
                If Not IsNull(aSQL(10, 0)) Then
                    If gUnitList(n).Status <> aSQL(10, 0) Then              '   status has changed
                        gUnitList(n).LastStatus = gUnitList(n).Status
                    End If
                End If
                gUnitList(n).UnitName = aSQL(0, 0)
                gUnitList(n).AVLID = IIf(IsNull(aSQL(2, 0)), -1, aSQL(2, 0))
                gUnitList(n).AVLEnabled = aSQL(3, 0)
                '    gUnitList(n).CurrentLat = IIf(IsNull(aSQL(4, 0)), -999, aSQL(4, 0) / 1000000) - gLatitudeAdjust
                '    gUnitList(n).CurrentLon = IIf(IsNull(aSQL(5, 0)), -999, aSQL(5, 0) / -1000000) - gLongitudeAdjust
                If Not IsNull(aSQL(4, 0)) Then
                    If aSQL(4, 0) <> 0 Then
                        gUnitList(n).CurrentLat = aSQL(4, 0) / 1000000 - gLatitudeAdjust
                    Else
                        '   Call AddToDebugList("[RefreshUnitInfo] Unit: " & gUnitList(n).UnitName & " - Zero Latitude! Discarding coordinates.")
                    End If
                Else
                    '   Call AddToDebugList("[RefreshUnitInfo] Unit: " & gUnitList(n).UnitName & " - NULL Latitude! Discarding coordinates.")
                End If
                If Not IsNull(aSQL(5, 0)) Then
                    If aSQL(5, 0) <> 0 Then
                        gUnitList(n).CurrentLon = aSQL(5, 0) / -1000000 - gLongitudeAdjust
                    Else
                        '   Call AddToDebugList("[RefreshUnitInfo] Unit: " & gUnitList(n).UnitName & " - Zero Longitude! Discarding coordinates.")
                    End If
                Else
                    '   Call AddToDebugList("[RefreshUnitInfo] Unit: " & gUnitList(n).UnitName & " - NULL Longitude! Discarding coordinates.")
                End If
                
                LocationText = IIf(IsNull(aSQL(6, 0)), "", aSQL(6, 0))
                If LocationText <> "" Then
                    gUnitList(n).CurrentLoc = LocationText
                Else
                    '   Call AddToDebugList("[RefreshUnitInfo] Unit: " & gUnitList(n).UnitName & " - Blank Current Location.")
                End If
                
                '   gUnitList(n).CurrentLoc = IIf(IsNull(aSQL(6, 0)), "", aSQL(6, 0))
                
                gUnitList(n).CurrentCity = IIf(IsNull(aSQL(7, 0)), "", aSQL(7, 0))
                If Not IsNull(aSQL(8, 0)) Then
                    If aSQL(8, 0) <> 0 Then
                        gUnitList(n).DestinationLat = (aSQL(8, 0) / 1000000) - gLatitudeAdjust
                    Else
                        '   Call AddToDebugList("[RefreshUnitInfo] Unit: " & gUnitList(n).UnitName & " - Zero Latitude! Discarding coordinates.")
                    End If
                Else
                    gUnitList(n).DestinationLat = -999
                End If
                '   gUnitList(n).DestinationLat = IIf(IsNull(aSQL(8, 0)), -999, aSQL(8, 0) / 1000000) - gLatitudeAdjust
                If Not IsNull(aSQL(9, 0)) Then
                    If aSQL(9, 0) > 0 Then
                        gUnitList(n).DestinationLon = (aSQL(9, 0) / -1000000) - gLongitudeAdjust
                    Else
                        '   Call AddToDebugList("[RefreshUnitInfo] Unit: " & gUnitList(n).UnitName & " - Zero Longitude! Discarding coordinates.")
                    End If
                Else
                    gUnitList(n).DestinationLon = -999
                End If
                '   gUnitList(n).DestinationLon = IIf(IsNull(aSQL(9, 0)), -999, aSQL(9, 0) / -1000000) - gLongitudeAdjust
                gUnitList(n).Status = IIf(IsNull(aSQL(10, 0)), 0, aSQL(10, 0))
                gUnitList(n).MDTID = IIf(IsNull(aSQL(11, 0)), -1, aSQL(11, 0))
                gUnitList(n).Station = aSQL(12, 0)
                gUnitList(n).StationLat = IIf(IsNull(aSQL(13, 0)), -999, aSQL(13, 0) / 1000000) - gLatitudeAdjust
                gUnitList(n).StationLon = IIf(IsNull(aSQL(14, 0)), -999, aSQL(14, 0) / -1000000) - gLongitudeAdjust
                gUnitList(n).CurrentDivision = IIf(IsNull(aSQL(15, 0)), "", aSQL(15, 0))
                gUnitList(n).RMIID = IIf(IsNull(aSQL(16, 0)), -1, aSQL(16, 0))
                
                LocationText = IIf(IsNull(aSQL(17, 0)), "", aSQL(17, 0))
                If LocationText <> "" Then
                    gUnitList(n).DestinationLoc = LocationText
                Else
                    '   Call AddToDebugList("[RefreshUnitInfo] Unit: " & gUnitList(n).UnitName & " - Blank Destination Location.")
                End If
                
                '   gUnitList(n).DestinationLoc = IIf(IsNull(aSQL(17, 0)), "", aSQL(17, 0))
                
                gUnitList(n).DestinationCity = IIf(IsNull(aSQL(18, 0)), "", aSQL(18, 0))
                gUnitList(n).ResourceType = IIf(IsNull(aSQL(19, 0)), "", aSQL(19, 0))
                gUnitList(n).TXApproved = IsTransportUnit(gUnitList(n).ResourceType)
                
                If gForceAVL Then
                    gUnitList(n).AVLID = gUnitList(n).VehID
                    gUnitList(n).AVLEnabled = True
                End If
                
                If gForceMST Then
                    gUnitList(n).MDTID = gUnitList(n).VehID
                End If
                
                '    If Not gUseESRIResources Then
                '        If gUnitList(n).Status > STAT_OffDuty And gUnitList(n).ShapeID = 0 Then        '   no icon for this unit and visible status
                '
                '            Call CreateUnitIcon(gUnitList(n))
                '
                '        ElseIf gUnitList(n).Status > STAT_OffDuty Then              '   icon exists and unit not off duty
                '
                '            '    Set lUnit = map.ActiveMap.GetLocation(gUnitList(n).CurrentLat, gUnitList(n).CurrentLon)
                '            '
                '            '    Set map.ActiveMap.Shapes.Item(gUnitList(n).UnitName).Location = lUnit
                '            '    map.ActiveMap.Shapes.Item(gUnitList(n).UnitName).FontColor = gStatusList(gUnitList(n).Status).ForeColor
                '            '    map.ActiveMap.Shapes.Item(gUnitList(n).UnitName).Fill.ForeColor = gStatusList(gUnitList(n).Status).BackColor
                '            '    map.ActiveMap.Shapes.Item(gUnitList(n).UnitName).Fill.Visible = True
                '
                '        ElseIf gUnitList(n).Status = STAT_OffDuty And gUnitList(n).ShapeID > 0 Then        '   Off duty so delete icon
                '
                '            gUnitList(n).ShapeID = 0
                '            map.ActiveMap.Shapes.Item(gUnitList(n).UnitName).Delete
                '
                '        End If
                '    End If
                
                Call RefreshUnitQueueRow(gUnitList(n))
                
                gMapRefreshRequired = True
            
                If OldSectorDivision <> gUnitList(n).CurrentDivision Then
                    Call ProcessSectorDivisionChange(gUnitList(n))
                End If
                
            End If
            
        End If
    Else
        
        '   MsgBox "UPDATE PENDING!"
        
        AddToDebugList "[ERROR] RefreshUnitInfo: UNKNOWN UNIT updating Vehicle ID=" & Str(nUnitID), lstDebug          '   & " - Resetting Queue"
        n = -1
        '   Call RefreshUnitQueue
        '   n = RefreshUnitInfo(nUnitID)
    
    End If
    
    RefreshUnitInfo = n
    
    Exit Function
    
ErrHandler:
    On Error Resume Next
    
    Call AddToDebugList("[ERROR] RefreshUnitInfo: Unit=" & gUnitList(n).UnitName & " Err#:" & Str(Err.Number) & " - " & Err.Description & " cSQL = " & cSQL, lstDebug)
    
End Function

Private Function RefreshIncidentInfo(ByRef UnitRecord As Vehicle) As Integer
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Integer, nEmpty As Integer, u As Integer
    Dim Found As Boolean
    Dim cUnitList As String
    Dim cError As String
    Dim cMessage As String
    
'    Private Type Incident
'        SourceID As Long
'        TimeStart As Variant
'        IncID As Long
'        UnitList() As Variant
'        Priority As String
'        PU_Address As String
'        PU_Location As String
'        PU_City As String
'        PU_Lat As Double
'        PU_Lon As Double
'        DE_Address As String
'        DE_Location As String
'        DE_City As String
'        DE_Lat As Double
'        DE_Lon As Double
'        NumPats As Integer
'        ETimeInQueue As Double
'        ETimeChute As Variant
'        ETimeOnScene As Variant
'        ETimeAtDest As Variant
'    End Type

    On Error GoTo RefreshIncidentInfo_ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function RefreshIncidentInfo")
    End If
    
    cMessage = "Start of RefreshIncidentInfo"
    
    If Not UnitRecord.UpdatePending Then

        Found = False
        n = 0
        nEmpty = 0
        cUnitList = ""
        
        '   Find this incident in the list
        
        cMessage = "before Find gIncident record"
        
        For n = 0 To gNumIncidents - 1
            If gIncidents(n).IncId = UnitRecord.RMIID Then
                Found = True
                Exit For
            Else
                If gIncidents(n).IncId = 0 Then         '   keep track of the empty records for re-use
                    nEmpty = n
                End If
            End If
        Next n
        
        cMessage = "before cSQL"
        
        cSQL = "Select  rva.radio_name, " _
                    & " rmi.response_date, " _
                    & " rmi.priority_number, " _
                    & " rmi.priority_description, " _
                    & " rmi.location_name, " _
                    & " rmi.address, " _
                    & " rmi.city, " _
                    & " rmi.latitude, " _
                    & " rmi.longitude, " _
                    & " rt.location_name, " _
                    & " rt.address, " _
                    & " rt.city, " _
                    & " rt.latitude, " _
                    & " rt.longitude, " _
                    & " p.forecolor, " _
                    & " p.backcolor " _
                & " from response_master_incident rmi " _
                    & " join priority p on rmi.priority_number = p.priority " _
                    & " join AgencyTypes a on rmi.Agency_Type = a.Agency_Type " _
                    & " left outer join response_transports rt on rmi.ID = rt.master_incident_id " _
                    & " left outer join response_vehicles_assigned rva on rmi.ID = rva.master_incident_ID " _
                & " where rmi.ID = " & Str(UnitRecord.RMIID) _
                    & " and p.agencytypeid = a.id " _
                & " order by rva.time_assigned "
        
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
        
        cMessage = "before If nRows > 0"
        
        If nRows > 0 Then
            AddToDebugList "[INCIDENT] RMI.ID=" & Str(UnitRecord.RMIID) & "  Found:" & Str(nRows) & " rows returned", lstDebug
            If Not Found Then
                If nEmpty = 0 Then                          '   if no empty records
                    ReDim Preserve gIncidents(0 To n)       '   add record to the list
                    gNumIncidents = gNumIncidents + 1
                Else
                    n = nEmpty                              '   else re-use the empty record
                End If
            End If
            
            cMessage = "build cUnitList"
            
            cUnitList = aSQL(0, 0)
            For u = 1 To nRows - 1
                cUnitList = cUnitList & "|" & aSQL(0, u)
            Next
            
            cMessage = "update gIncident"
            
            gIncidents(n).IncId = UnitRecord.RMIID
            gIncidents(n).UnitList = Split(cUnitList, "|")
            gIncidents(n).TimeStart = IIf(IsNull(aSQL(1, 0)), "", aSQL(1, 0))
            gIncidents(n).Priority = IIf(IsNull(aSQL(3, 0)), "", aSQL(3, 0))
            gIncidents(n).PU_Location = IIf(IsNull(aSQL(4, 0)), "", aSQL(4, 0))
            gIncidents(n).PU_Address = IIf(IsNull(aSQL(5, 0)), "", aSQL(5, 0))
            gIncidents(n).PU_City = IIf(IsNull(aSQL(6, 0)), "", aSQL(6, 0))
            gIncidents(n).PU_Lat = IIf(IsNull(aSQL(7, 0)), -999, aSQL(7, 0) / 1000000) - gLatitudeAdjust
            gIncidents(n).PU_Lon = IIf(IsNull(aSQL(8, 0)), -999, aSQL(8, 0) / -1000000) - gLongitudeAdjust
            gIncidents(n).DE_Location = IIf(IsNull(aSQL(9, 0)), "", aSQL(9, 0))
            gIncidents(n).DE_Address = IIf(IsNull(aSQL(10, 0)), "", aSQL(10, 0))
            gIncidents(n).DE_City = IIf(IsNull(aSQL(11, 0)), "", aSQL(11, 0))
            gIncidents(n).DE_Lat = IIf(IsNull(aSQL(12, 0)), -999, aSQL(12, 0) / 1000000) - gLatitudeAdjust
            gIncidents(n).DE_Lon = IIf(IsNull(aSQL(13, 0)), -999, aSQL(13, 0) / -1000000) - gLongitudeAdjust
            gIncidents(n).ForeColor = IIf(IsNull(aSQL(14, 0)), 1, aSQL(14, 0))
            gIncidents(n).BackColor = IIf(IsNull(aSQL(15, 0)), 1, aSQL(15, 0))
            
            UnitRecord.SceneLoc = IIf(IsNull(aSQL(5, 0)), "", aSQL(5, 0))
            UnitRecord.SceneCity = IIf(IsNull(aSQL(6, 0)), "", aSQL(6, 0))
            UnitRecord.SceneLat = IIf(IsNull(aSQL(7, 0)), -999, aSQL(7, 0) / 1000000) - gLatitudeAdjust
            UnitRecord.SceneLon = IIf(IsNull(aSQL(8, 0)), -999, aSQL(8, 0) / -1000000) - gLongitudeAdjust
            
            '*** CHANGE: ADD in update of the unitlist scene and destination information
            
        Else
            AddToDebugList "[ERROR Reading INCIDENT] UNKNOWN RMI.ID=" & Str(UnitRecord.RMIID), lstDebug
            n = -1
        End If
        
        RefreshIncidentInfo = n
    
    End If
    
    Exit Function
    
RefreshIncidentInfo_ERH:
    AddToDebugList "[ERROR] in RefreshIncidentInfo_ERH: Unit=" & Str(UnitRecord.RMIID) & ", " & Err.Description & " (" & Err.Number & ") - " & cMessage, lstDebug
    RefreshIncidentInfo = -1
End Function

Private Sub CalculateRoute(ByRef UnitRecord As Vehicle, WhichSegment As Integer)
    '    Dim objRoute As MapPointCtl.Route
    '    Dim objLoc As MapPointCtl.Location
    '    Dim objLoc2 As MapPointCtl.Location
    '    Dim objWPT As MapPointCtl.Waypoint
    '    Dim objLine As MapPointCtl.Shape
    Dim NumSegments As Integer
    Dim TotalTime As Double, TotalDist As Double, DriveRate As Double
    Dim nProfile As Integer
    Dim nLine As Integer
    Dim nSpeedFactor As Double
    Dim FromLat As Double
    Dim FromLon As Double
    Dim ToLat As Double
    Dim ToLon As Double
    Dim Lat As Double
    Dim Lon As Double
    Dim cMsg As String
    Dim nFile As Integer
    Dim aRoute As Variant
    
    On Error GoTo RoutingError
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub CalculateRoute")
    End If
    
    '    cMsg = "Entering Routine"
    '
    '    '    gAVLSegmentFactor = Val(bsiGetSettings("AVL", "AVLSegmentDivisor", "8", INIPath & "\Simulator.INI"))
    '    '    gAVLDriveFactor = Val(bsiGetSettings("AVL", "AVLTimeDivisor", "100", INIPath & "\Simulator.INI"))
    '
    '    If gDriveTime(cmbDriveProfile.ListIndex).SpeedScale = "M" Then
    '        nSpeedFactor = 1#
    '    Else
    '        nSpeedFactor = 1.61
    '    End If
    '
    '    Set objRoute = map.ActiveMap.ActiveRoute
    '
    '    cMsg = "Clearing Route"
    '
    '    objRoute.Clear
    '
    '    Select Case WhichSegment
    '        Case ROUTE_ToScene
    '            FromLat = UnitRecord.CurrentLat
    '            FromLon = UnitRecord.CurrentLon
    '            ToLat = UnitRecord.SceneLat
    '            ToLon = UnitRecord.SceneLon
    '        Case ROUTE_ToHosp
    '            FromLat = UnitRecord.SceneLat
    '            FromLon = UnitRecord.SceneLon
    '            ToLat = UnitRecord.DestinationLat
    '            ToLon = UnitRecord.DestinationLon
    '        Case ROUTE_ToStation
    '            FromLat = UnitRecord.CurrentLat
    '            FromLon = UnitRecord.CurrentLon
    '            ToLat = UnitRecord.StationLat
    '            ToLon = UnitRecord.StationLon
    '        Case ROUTE_ToDestination
    '            FromLat = UnitRecord.CurrentLat
    '            FromLon = UnitRecord.CurrentLon
    '            ToLat = UnitRecord.DestinationLat
    '            ToLon = UnitRecord.DestinationLon
    '    End Select
    '
    '    cMsg = "Route type selected"
    '
    '    Set objLoc = map.ActiveMap.GetLocation(FromLat, FromLon)
    '
    '    Set objWPT = objRoute.Waypoints.Add(objLoc, "Start")
    '    objWPT.ListPosition = 1
    '
    '    cMsg = "Route Start Set"
    '
    '    Set objLoc = map.ActiveMap.GetLocation(ToLat, ToLon)
    '
    '    Set objWPT = objRoute.Waypoints.Add(objLoc, "Stop")
    '    objWPT.ListPosition = 2
    '
    '    objRoute.DriverProfile.ResetSpeeds
    '
    '    '   *** This is where we have to do the Drive Profile Modifications - RIGHT HERE!! ***
    '
    '    nProfile = cmbDriveProfile.ListIndex
    '
    '    objRoute.DriverProfile.Speed(geoRoadInterstate) = gDriveTime(nProfile).Interstate / nSpeedFactor
    '    objRoute.DriverProfile.Speed(geoRoadLimitedAccess) = gDriveTime(nProfile).Highway / nSpeedFactor
    '    objRoute.DriverProfile.Speed(geoRoadOtherHighway) = gDriveTime(nProfile).OtherHwy / nSpeedFactor
    '    objRoute.DriverProfile.Speed(geoRoadArterial) = gDriveTime(nProfile).Arterial / nSpeedFactor
    '    objRoute.DriverProfile.Speed(geoRoadStreet) = gDriveTime(nProfile).Street / nSpeedFactor
    '
    '    objRoute.DriverProfile.TimeBetweenRests = 1 * geoOneDay
    '    objRoute.DriverProfile.IncludeRestStops = False
    '
    '    cMsg = "Route Destination Set"
    '
    '    objRoute.Calculate
    '
    '    NumSegments = objRoute.Directions.count
    '
    '    ReDim aRoute(1 To NumSegments, 0 To 1)
    '
    '    cMsg = "First Route Calculated"
    '
    '    map.ItineraryVisible = False
    '
    ''    nFile = FreeFile()
    ''
    ''    Open App.Path & "\RouteLog.log" For Output As #(nFile)
    ''
    ''    Print #(nFile), "Route Coordinates for Original Route (count) =" & Str(objRoute.Directions.Count)
    ''
    ''    Print #(nFile), Format(nLine, "000:") & "Origin:" _
    ''                & ", Lat=" & Format(FromLat, "##0.0000") _
    ''                & ", Lon=" & Format(FromLon, "##0.0000")
    ''
    ''    For nLine = 1 To NumSegments
    ''
    ''        Call CalcPos(map.ActiveMap, objRoute.Directions.Item(nLine).Location, Lat, Lon)
    ''
    ''        If objRoute.Directions.Item(nLine).Distance < 0.1 Then
    ''            aRoute(nLine, 0) = Lat
    ''            aRoute(nLine, 1) = Lon
    ''        Else
    ''            aRoute(nLine, 0) = 9999
    ''            aRoute(nLine, 1) = 9999
    ''        End If
    ''
    ''        Print #(nFile), Format(nLine, "000:") & "Dst=" & Format(objRoute.Directions.Item(nLine).Distance, "##0.000") _
    ''                    & ", Lat=" & Format(Lat, "##0.0000") _
    ''                    & ", Lon=" & Format(Lon, "##0.0000") _
    ''                    & ", EDt=" & Format(objRoute.Directions.Item(nLine).ElapsedDistance, "##0.000") _
    ''                    & ", Tim=" & Format(objRoute.Directions.Item(nLine).StartTime, "##0.000") _
    ''                    & ", Inst=" & objRoute.Directions.Item(nLine).Instruction _
    ''                    & ", Twd=" & objRoute.Directions.Item(nLine).Toward
    ''    Next nLine
    ''
    ''    Print #(nFile), Format(nLine, "000:") & "Destination:" _
    ''                & ", Lat=" & Format(ToLat, "##0.0000") _
    ''                & ", Lon=" & Format(ToLon, "##0.0000")
    ''
    ''    Print #(nFile), "Route Coordinates for Original Calculated."
    ''
    ''    Close #(nFile)
    '
    '    TotalTime = objRoute.DrivingTime / geoOneMinute * 60    '   Total time in Seconds
    '
    '    objRoute.Clear
    '    Set objLoc = map.ActiveMap.GetLocation(FromLat, FromLon)
    '    Set objWPT = objRoute.Waypoints.Add(objLoc, "Origin")
    '
    ''    For nLine = 1 To UBound(aRoute)
    ''        If aRoute(nLine, 0) < 999 Then
    ''            Set objLoc = map.ActiveMap.GetLocation(aRoute(nLine, 0), aRoute(nLine, 1))
    ''            Set objWPT = objRoute.Waypoints.Add(objLoc, "Waypoint" & Str(nLine))
    ''        End If
    ''    Next nLine
    '
    '    Set objLoc = map.ActiveMap.GetLocation(ToLat, ToLon)
    '    Set objWPT = objRoute.Waypoints.Add(objLoc, "Destination")
    '
    ''    objRoute.DriverProfile.Speed(geoRoadInterstate) = ValidateSpeeds(5 + (gDriveTime(nProfile).Interstate / nSpeedFactor) / gAVLSegmentFactor)
    ''    objRoute.DriverProfile.Speed(geoRoadLimitedAccess) = ValidateSpeeds(5 + (gDriveTime(nProfile).Highway / nSpeedFactor) / gAVLSegmentFactor)
    ''    objRoute.DriverProfile.Speed(geoRoadOtherHighway) = ValidateSpeeds(5 + (gDriveTime(nProfile).OtherHwy / nSpeedFactor) / gAVLSegmentFactor)
    ''    objRoute.DriverProfile.Speed(geoRoadArterial) = ValidateSpeeds(5 + (gDriveTime(nProfile).Arterial / nSpeedFactor) / gAVLSegmentFactor)
    ''    objRoute.DriverProfile.Speed(geoRoadStreet) = ValidateSpeeds(5 + (gDriveTime(nProfile).Street / nSpeedFactor) / gAVLSegmentFactor)
    '
    ''   Use Minimum Speed Values to force minimum length segments
    '
    '    objRoute.DriverProfile.Speed(geoRoadInterstate) = 8
    '    objRoute.DriverProfile.Speed(geoRoadLimitedAccess) = 8
    '    objRoute.DriverProfile.Speed(geoRoadOtherHighway) = 7
    '    objRoute.DriverProfile.Speed(geoRoadArterial) = 6
    '    objRoute.DriverProfile.Speed(geoRoadStreet) = 5
    '
    '    objRoute.DriverProfile.TimeBetweenRests = 1 * geoOneMinute
    '    objRoute.DriverProfile.RestStopDuration = 1 * geoOneMinute
    '    objRoute.DriverProfile.IncludeRestStops = True
    '
    '    objRoute.Calculate
    '
    '    cMsg = "Second Route Calculated"
    '
    '    map.ItineraryVisible = False
    '
    '    TotalDist = objRoute.Distance
    '
    ''    MsgBox "Distance:" & Str(TotalDist) & Chr(13) & "Time:" & Str(TotalTime / 60) & Chr(13) & "Rate:" & Str(TotalDist / (TotalTime / 3600)), vbInformation + vbOKOnly, "Route Info"
    '
    '    UnitRecord.Route = CalculateRouteCoordinates(objRoute, NumSegments, TotalDist)
    '    UnitRecord.RouteSegments = NumSegments
    '
    '    cMsg = "Route Coordinates Calculated"
    '
    ''    If TotalDist <> objRoute.Distance Then
    ''        AddToDebugList ("[ERROR in CalculateRoute] Length of routes not equal - 1:" _
    ''                            & Format(TotalDist, "##0.00") & "  2:" _
    ''                            & Format(objRoute.Distance, "##0.00"))
    ''    End If
    '
    '    DriveRate = TotalTime / TotalDist                       '   second per mile
    '
    ''    objRoute.Clear                                         '   leave the most recent calculated route visible
    '
    '    For nLine = 0 To UnitRecord.RouteSegments - 1               '   calculate seconds between updates
    ''        UnitRecord.Route(nLine).Time = UnitRecord.Route(nLine).Distance * DriveRate * (gAVLDriveFactor / 100)
    '        UnitRecord.Route(nLine).Time = UnitRecord.Route(nLine).Distance * DriveRate / gDriveCompression
    '    Next nLine
    '
    '    cMsg = "Route Time Calculated"
    '
    '    '   Call CenterMap
    '
    Exit Sub

RoutingError:

    UnitRecord.RouteSegments = 0

    Call AddToDebugList("[ERROR in CalculateRoute] No.:" & Str(Err.Number) & "  Desc.:" & Err.Description & " msg: " & cMsg, lstDebug)

    Close #(nFile)

End Sub

'Private Function CalculateRouteCoordinates(ByVal objRoute As MapPointCtl.Route, ByRef NumSegments As Integer, ByRef RteDistance As Double) As RtPoint()
'    Dim itemX As ListItem
'    '   Dim objPoint As MapPointCtl.Location
'    Dim aLatLong As Variant
'    Dim rtArray() As RtPoint
'    Dim nFile As Integer
'    Dim Lat As Double, Lon As Double, Dist As Double
'    Dim n As Integer
'    Dim cMsg As String
'
'    On Error GoTo ErrHandler
'
'    '    NumSegments = 0
'    '    RteDistance = 0
'    '
'    '    For n = 1 To objRoute.Directions.count
'    '        RteDistance = RteDistance + objRoute.Directions.Item(n).Distance
'    '        If objRoute.Directions.Item(n).Distance > 0 Then
'    '            NumSegments = NumSegments + 1                       '   Count all the non-zero length route segments
'    '        End If
'    '    Next n
'    '
'    '    ReDim rtArray(0 To NumSegments - 1)
'    '
'    '    nFile = FreeFile()
'    '
'    '    Open App.Path & "\Logs\Route - " & Format(Now, "YYYYMMDD") & ".log" For Append As #(nFile)
'    '
'    '    cMsg = "Route Coordinates for Segmented Route (count) =" & Str(objRoute.Directions.count)
'    '
'    '    NumSegments = 0
'    '    For n = 1 To objRoute.Directions.count
'    '        If objRoute.Directions.Item(n).Distance > 0 Then
'    '
'    '            Set objPoint = objRoute.Directions.Item(n).Location
'    '
'    '            cMsg = "Route Coordinates Calculations Lat/Lon segment: " & Str(n)
'    '
'    '            If CalcPos(map.ActiveMap, objPoint, Lat, Lon) Then
'    '
'    '                Print #(nFile), Format(n, "000:") & "Dst=" & Format(objRoute.Directions.Item(n).Distance, "##0.000") _
'    '                            & ", Lat=" & Format(Lat, "##0.0000") _
'    '                            & ", Lon=" & Format(Lon, "##0.0000") _
'    '                            & ", EDt=" & Format(objRoute.Directions.Item(n).ElapsedDistance, "##0.000") _
'    '                            & ", Tim=" & Format(objRoute.Directions.Item(n).StartTime, "##0.000") _
'    '                            & ", Inst=" & objRoute.Directions.Item(n).Instruction _
'    '                            & ", Twd=" & objRoute.Directions.Item(n).Toward
'    '
'    '                rtArray(NumSegments).Distance = objRoute.Directions.Item(n).Distance
'    '                rtArray(NumSegments).Lat = Lat
'    '                rtArray(NumSegments).Lon = Lon
'    '
'    '                NumSegments = NumSegments + 1
'    '
'    '            Else
'    '                Call AddToDebugList("[ERROR in CalculateRouteCoordinates] Unable to calculate Lat/Lon for segment" & Str(n))
'    '            End If
'    '
'    '        End If
'    '
'    '    Next n
'    '
'    '    Print #(nFile), "Route Calculations Complete"
'    '
'    '    Close #(nFile)
'    '
'    '    cMsg = "Route Coordinates Calculations Assigning Array"
'    '
'    '    CalculateRouteCoordinates = rtArray
'    '
'    Exit Function
'
'ErrHandler:
'
'    CalculateRouteCoordinates = rtArray
'    Call AddToDebugList("[ERROR] CalculateRouteCoordinates - " & Str(Err.Number) & " " & Err.Description & " msg: " & cMsg)
'
'End Function

Private Sub CalculateESRIRoute(ByRef UnitRecord As Vehicle, WhichSegment As Integer, Optional bOneWay As Boolean = False)
    Dim NumSegments As Integer
    Dim TotalTime As Double, TotalDist As Double, DriveRate As Double
    Dim nProfile As Integer
    Dim nLine As Integer
    Dim nSpeedFactor As Double
    Dim FromLat As Double
    Dim FromLon As Double
    Dim ToLat As Double
    Dim ToLon As Double
    Dim Lat As Double
    Dim Lon As Double
    Dim cMsg As String
    Dim nFile As Integer
    
    Dim cError As String
    
    Dim aList() As RtPoint
'    Dim pLine As Polyline
'    Dim aPoints() As esriGeometry.IPoint
    
    Dim RouteDescription As tRoute
    
    On Error GoTo RoutingError
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub CalculateESRIRoute")
    End If
    
    cMsg = "Entering Routine"
    
    '    If gDriveTime(cmbDriveProfile.ListIndex).SpeedScale = "M" Then
    '        nSpeedFactor = 1#
    '    Else
    '        nSpeedFactor = 1.61
    '    End If
    
    Select Case WhichSegment
        Case ROUTE_ToScene
            FromLat = UnitRecord.CurrentLat
            FromLon = UnitRecord.CurrentLon
            ToLat = UnitRecord.SceneLat
            ToLon = UnitRecord.SceneLon
        Case ROUTE_ToHosp
            FromLat = UnitRecord.SceneLat
            FromLon = UnitRecord.SceneLon
            ToLat = UnitRecord.DestinationLat
            ToLon = UnitRecord.DestinationLon
        Case ROUTE_ToStation
            FromLat = UnitRecord.CurrentLat
            FromLon = UnitRecord.CurrentLon
            ToLat = UnitRecord.StationLat
            ToLon = UnitRecord.StationLon
        Case ROUTE_ToDestination
            FromLat = UnitRecord.CurrentLat
            FromLon = UnitRecord.CurrentLon
            ToLat = UnitRecord.DestinationLat
            ToLon = UnitRecord.DestinationLon
    End Select
    
    cMsg = "Route type selected"
    
    ReDim aList(0 To 1)
    
    aList(0).Lat = FromLat
    aList(0).Lon = FromLon
    
    cMsg = "Route Start Set"
    
    aList(1).Lat = ToLat
    aList(1).Lon = ToLon
    
    cMsg = "Route Destination Set"
    
    Debug.Assert ToLat <> 0 And FromLat <> 0
    
    If gLoadTestRunning Then
        If chkLoadTest.Value = vbChecked Then                   '   if running in load test mode AND we want simulator to create the routes
            Call RequestRoute(UnitRecord.VehID, aList)
        End If
    Else                                                        '   if NOT in load test mode, then do the route thing
        Call RequestRoute(UnitRecord.VehID, aList)
    End If
    
    '   RouteDescription = cnESRINARouteCalc(aList, gESRINetworkDataset, gESRINetworkCost, gMapProjection, gAVLUpdateLimit, cError, bOneWay)
    
    '   nProfile = cmbDriveProfile.ListIndex
    
    '   UnitRecord.Route = RouteDescription.Route
    '   UnitRecord.RouteSegments = RouteDescription.RoutePoints

    '   cMsg = "Route Coordinates Calculated"
    
    '    If Not gUseESRIResources Then
    '        DriveRate = RouteDescription.TotalTime / RouteDescription.TotalDistance                       '   second per mile
    '        For nLine = 0 To UnitRecord.RouteSegments - 1               '   calculate seconds between updates
    '            UnitRecord.Route(nLine).Time = UnitRecord.Route(nLine).Distance * DriveRate / gDriveCompression
    '        Next nLine
    '    Else
    '        For nLine = 0 To UnitRecord.RouteSegments - 1
    '            UnitRecord.Route(nLine).Time = UnitRecord.Route(nLine).Time / gDriveCompression * IIf(cmbCostAttribute.Text = "Minutes", 60, 1)
    '        Next nLine
    '    End If
    '
    '    cMsg = "Route Time Calculated"
    
    Exit Sub
    
RoutingError:

    UnitRecord.RouteSegments = 0

    Call AddToDebugList("[ERROR in CalculateRoute] No.:" & Str(Err.Number) & "  Desc.:" & Err.Description & " msg: " & cMsg, lstDebug)

    '   Close #(nFile)

End Sub

Private Function FindClosestHosp(tCadInc As CADIncident, ByRef PickClosest As Boolean) As CADLocation
    Dim n As Long, iClose As Long, iLimit As Long, i As Long
    '   Dim objHere As MapPointCtl.Location
    '   Dim objDest As MapPointCtl.Location
    Dim nClosest As Double
    Dim FoundOne As Boolean
    
    On Error GoTo HospError
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function FindClosestHosp")
    End If
    
    Randomize
    
    gTransportLimit = Val(bsiGetSettings("AVL Settings", "DestinationTransportLimit", "20", gSystemDirectory & "\Simulator.INI"))

    nClosest = Rnd * 100        '   Are we going to transport this time? (subject to overrides, of course)
    
    '   Set objHere = map.ActiveMap.GetLocation(tCadInc.PU_Lat, tCadInc.PU_Lon)
    
    iClose = -1
    iLimit = -1
    
    If nClosest <= gUseClosestPercent Or PickClosest Or gLoadTestRunning Then              '   do we calculate the closest hospital. Always use the closest in a Load Test.
    
        PickClosest = True
    
        nClosest = 999999
        
        For n = 0 To UBound(gHospitals)
            If gHospitals(n).Lat <> 0 And gHospitals(n).TXApproved Then                   '   only include the destination if a valid Lat and long is available and is a valid transport destination
                '   Set objDest = map.ActiveMap.GetLocation(gHospitals(n).Lat, gHospitals(n).Lon)
                '   gHospitals(n).Distance = map.ActiveMap.Distance(objHere, objDest)
                gHospitals(n).Distance = DistanceDeg(tCadInc.PU_Lat, tCadInc.PU_Lon, gHospitals(n).Lat, gHospitals(n).Lon, "KM")
                If gHospitals(n).Distance < nClosest Then    '   keep track of the closest so far
                    nClosest = gHospitals(n).Distance
                    iClose = n                                   '   keep track of the closest so far
                End If
                If gHospitals(n).Distance <= nClosest And gHospitals(n).Distance <= gTransportLimit Then
                    iLimit = n                                   '   keep track of the closest so far within the transport limit (if any)
                End If
            End If
        Next n
        
    Else                                                '   or randomly choose a hospital
    
        PickClosest = False
        FoundOne = False
        
        n = 0
        Do
            i = Int(Rnd * (UBound(gHospitals) + 1))         '   randomly pick a hospital
            If gHospitals(i).Lat <> 0 And gHospitals(i).TXApproved Then    '   that has a valid Lat and Long and is a transport destination
                '   Set objDest = map.ActiveMap.GetLocation(gHospitals(i).Lat, gHospitals(i).Lon)
                '   gHospitals(i).Distance = map.ActiveMap.Distance(objHere, objDest)
                gHospitals(i).Distance = DistanceDeg(tCadInc.PU_Lat, tCadInc.PU_Lon, gHospitals(n).Lat, gHospitals(n).Lon, "KM")
                If gHospitals(i).Distance < gTransportLimit Then         '   make sure it's not too far away
                    FoundOne = True
                End If
            End If
            n = n + 1       '   keep track of how many attempts we've made
            If n > UBound(gHospitals) Then          '   if we've made many attempts
                n = 0
                gTransportLimit = gTransportLimit + 5       '   expand the capture range until we find something
            End If
        Loop Until FoundOne
        
    End If
    
    If iLimit >= 0 Then                             '   if we have a hospital within the designated limit, then we will use that one
        FindClosestHosp = gHospitals(iLimit)
    Else
        FindClosestHosp = gHospitals(iClose)        '   otherwise, we will use the closest hospital to the unit's location
    End If
    
    Exit Function
    
HospError:

    Call AddToDebugList("Error occured in the FindClosestHosp routine: " & Str(Err.Number) & " " & Err.Description, lstDebug)

End Function

Private Function FindSourceHosp(OldIncInfo As tOldIncInfo, ByRef PickedClosest As Boolean, tCadInc As CADIncident) As CADLocation
    Dim n As Integer, i As Integer
    
    On Error GoTo HospError
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function FindSourceHosp")
    End If
    
    Call AddToDebugList("[DEBUG] FindSourceHosp: Looking for " & OldIncInfo.Destination)
    
    i = -1
    
    For n = 0 To UBound(gHospitals)
        If Left(OldIncInfo.Destination, 10) = Left(gHospitals(n).Name, 10) Then          '   use first 6 chars for best chance of match
            i = n
            Exit For
        End If
    Next n
    
    If i < 0 Then                                                '   didn't find anything based on the source information
        FindSourceHosp = FindClosestHosp(tCadInc, True) '   select another destination
        Call AddToDebugList("[DEBUG] FindSourceHosp: Not Found - using " & FindSourceHosp.Name)
    Else
        FindSourceHosp = gHospitals(i)
        Call AddToDebugList("[DEBUG] FindSourceHosp: Found " & FindSourceHosp.Name)
    End If
    
    Exit Function

HospError:

    Call AddToDebugList("Error occured in the FindSourceHosp routine: " & Str(Err.Number) & " " & Err.Description)

End Function

Private Sub RequestRoute(ByVal nUnitID As Long, aRoute() As RtPoint)
    Dim MsgPayload As String
   
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub RequestRoute")
    End If
    
    gUniqueRouteID = gUniqueRouteID + 1
    
    gRouteHashTable(GetUnitIndexByVehID(nUnitID)) = gUniqueRouteID
    
    Call AddToDebugList("[RequestRoute] EntryPoint - Unit: " & nUnitID & ", Route ID: " & gUniqueRouteID, lstDebug)
    
    If gUseShareRoutingFolder Then
        MsgPayload = CN_ROUTE_FILEREQUEST & nUnitID
        MsgPayload = MsgPayload & "|" & Format(gUniqueRouteID, "000000")
        MsgPayload = MsgPayload & "|" & aRoute(0).Lat
        MsgPayload = MsgPayload & "|" & aRoute(0).Lon
        MsgPayload = MsgPayload & "|" & aRoute(1).Lat
        MsgPayload = MsgPayload & "|" & aRoute(1).Lon
        MsgPayload = MsgPayload & "|" & gSharedRoutingFolder
    Else
        MsgPayload = CN_ROUTE_REQUEST & nUnitID
        MsgPayload = MsgPayload & "|" & Format(gUniqueRouteID, "000000")
        MsgPayload = MsgPayload & "|" & aRoute(0).Lat
        MsgPayload = MsgPayload & "|" & aRoute(0).Lon
        MsgPayload = MsgPayload & "|" & aRoute(1).Lat
        MsgPayload = MsgPayload & "|" & aRoute(1).Lon
    End If
    
    Call SendToCADNoWait("9700", MsgPayload)

End Sub

Private Sub ProcessSimRadioMsgRequest(Payload As String)
    Dim aMsg() As String
    Dim UnitRecord As Vehicle
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessSimRadioMsgRequest")
    End If
    
    aMsg = Split(Payload, "|")

    Call AddToDebugList("[ProcessSimRadioMsgRequest]Radio message request from " & aMsg(0) & " >>" & Payload, lstDebug)
    
    UnitRecord = GetUnitRecordbyName(aMsg(1))
    
    Call InsertRadioMessage(UnitRecord, STAT_FREEFORMTEXT, Now, , , aMsg(2))

End Sub

Private Sub ProcessSimSoundByteRequest(Payload As String)
    Dim aMsg() As String
    Dim UnitRecord As Vehicle
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessSimSoundByteRequest")
    End If
    
    aMsg = Split(Payload, "|")

    Call AddToDebugList("[ProcessSimSoundByteRequest]Sound byte request from " & aMsg(0) & " >>" & Payload, lstDebug)
    
    UnitRecord = GetUnitRecordbyName(aMsg(1))
    
    Call InsertSoundEvent(UnitRecord, aMsg(2), Now)

End Sub

Private Sub ProcessSimStatusRequest(Payload As String)
    Dim cMsg As String
    Dim cMsgNum As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessSimStatusRequest")
    End If
    
    cMsgNum = Format(CN_CADNORTH_MSG, "0000")
    
    Call AddToDebugList("[ProcessSimStatusRequest]Status request from " & Payload, lstDebug)

    If gStopProcessing Then
        cMsg = Format(CN_MSG_PAUSED_SIMULATION, "0000") & tcpSock.LocalHostName
    Else
        cMsg = Format(CN_MSG_RUNNING_SIMULATION, "0000") & tcpSock.LocalHostName
    End If
    
    Call SendToCADNoWait(cMsgNum, cMsg)

End Sub

Private Sub ProcessPassthroughRequest(Payload As String)
    Dim aMsg() As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessPassthroughRequest")
    End If
    
    aMsg = Split(Payload, "|")
    
    Call AddToDebugList("[ProcessPassthroughRequest]Passthrough request from " & aMsg(0) & " >>" & Payload, lstDebug)

    Call SendToAVLNoWait(aMsg(1), aMsg(2))
    
End Sub

Private Sub ProcessSpecialRoute(Payload As String)
    Dim aResourceList() As String
    Dim aUnits() As Vehicle
    Dim nThisUnit As Long
    Dim ThisRoute() As RtPoint
    Dim n As Integer
    Dim cName As String
    Dim nError As Integer
    
    Dim cError As String

    On Error GoTo ProcessSpecialRoute_ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessSpecialRoute")
    End If

    Payload = CleanXMLData(Payload)
    
    aResourceList = GetXMLResourceList(Payload, nError)
    
    If nError >= 0 Then
    
        aUnits = ParseXMLUnitRouteInfo(aResourceList)
        
        For n = 0 To UBound(aUnits, 1)
            ReDim ThisRoute(0 To 1)
            
            cName = aUnits(n).UnitName
            
            nThisUnit = GetUnitIndexByName(cName)
            
            If nThisUnit >= 0 Then
            
                Call PurgeEvents(gUnitList(nThisUnit).VehID)
            
                gUnitList(nThisUnit).StatusChangeTime = Now
                
                ThisRoute(0).Lat = aUnits(n).CurrentLat
                ThisRoute(0).Lon = aUnits(n).CurrentLon
                ThisRoute(1).Lat = aUnits(n).DestinationLat
                ThisRoute(1).Lon = aUnits(n).DestinationLon
                
                Call AddToDebugList("[ProcessSpecialRoute] Route request: " & aResourceList(n), lstDebug)
                
                If gLoadTestRunning Then
                    If chkLoadTest.Value = vbChecked Then                   '   if running in load test mode AND we want simulator to create the routes
                        Call RequestRoute(gUnitList(nThisUnit).VehID, ThisRoute)
                    End If
                Else                                                        '   if NOT in load test mode, then do the route thing
                    Call RequestRoute(gUnitList(nThisUnit).VehID, ThisRoute)
                End If
                
                '   DoEvents
                
            Else
            
                Call AddToDebugList("[ProcessSpecialRoute] ERROR: CallSign " & cName & " not found. Skipping route: " & aResourceList(n), lstDebug)
                
            End If
            
        Next n
    Else
        Call AddToDebugList("[ProcessSpecialRoute] ERROR processing route request: " & aResourceList(n), lstDebug)
    End If
    
    On Error GoTo 0
    Exit Sub

ProcessSpecialRoute_ERH:

    Call AddToDebugList("[ERROR] " & Err.Number & " (" & Err.Description & ") in procedure ProcessSpecialRoute of Form frmMain", lstDebug)
    
End Sub

Private Sub ProcessUnitsOffDuty(Payload As String)
    Dim aUnits() As Vehicle
    Dim aResourceList() As String
    Dim n As Integer
    Dim cIDList As String
    Dim cBadIDList As String
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim cError As String
    Dim nUnit As Long
    Dim cName As String
    Dim nError As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessUnitsOffDuty")
    End If
    
    Payload = CleanXMLData(Payload)
    
    aResourceList = GetXMLResourceList(Payload, nError)
    
    If nError >= 0 Then
    
        aUnits = ParseXMLUnitOffDuty(aResourceList)
        
        For n = 0 To UBound(aUnits, 1)
            
            cName = aUnits(n).UnitName
            nUnit = GetUnitIndexByName(cName)
            
            If nUnit >= 0 Then
                gUnitList(nUnit).StatusChangeTime = Now
            
                If n > 0 Then cIDList = cIDList & ","
            
                cIDList = cIDList & gUnitList(nUnit).VehID
                
            Else
            
                If Len(cBadIDList) > 0 Then cBadIDList = cBadIDList & "," & cName Else cBadIDList = cName
                
            End If
            
        Next n
        
        Call AddToDebugList("[ProcessUnitsOffDuty] Request received to OFFDUTY ID(s): " & cIDList, lstDebug)
        
        If Len(cBadIDList) > 0 Then Call AddToDebugList("[ProcessUnitsOffDuty] Unknown unit callsign(s): " & cBadIDList, lstDebug)
        
        cSQL = "SELECT ID FROM Vehicle WHERE Master_Incident_ID is NULL and ID in (" & cIDList & ")"
        
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
        
        If nRows > 0 Then
            cIDList = ""
            For n = 0 To nRows - 1
                If n > 0 Then cIDList = cIDList & ","
                cIDList = cIDList & aSQL(0, n)
            Next n
            
            cSQL = "UPDATE Vehicle SET Status_ID = " & vcStatus.OffDuty & " WHERE ID in (" & cIDList & ")"
            
            nRows = bsiSQLExecute(cSQL, gConnectString, aSQL, , cError)
            
            If nRows > 0 Then
                Call AddToDebugList("[ProcessUnitsOffDuty] Requested units set to OFFDUTY ID(s): " & cIDList & " - remainder were still assigned.", lstDebug)
            Else
                Call AddToDebugList("[ProcessUnitsOffDuty] ERROR setting units OFFDUTY ID(s): " & cIDList & " - " & cError, lstDebug)
            End If
        
            Call SendToCADNoWait(NP_READ_VEHICLE, cIDList)
        
        End If
    Else
        Call AddToDebugList("[ProcessUnitsOffDuty] ERROR setting units OFFDUTY: " & aResourceList(n), lstDebug)
    End If
    
End Sub

Private Function CleanXMLData(ByVal Payload As String) As String
    Dim cTest As String
    
    Dim cError As String

    On Error GoTo CleanXMLData_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function CleanXMLData")
    End If
    
    cError = "Start of CleanXMLData"

    Call AddToDebugList("[CleanXMLData]", lstDebug)
    
    CleanXMLData = Replace(Payload, vbCrLf, "", 1)               '   remove all CR/LF pairs
    
    Do
    
        cTest = CleanXMLData
        
        CleanXMLData = Replace(CleanXMLData, "> ", ">", 1)           '   remove all white space characters
        CleanXMLData = Replace(CleanXMLData, " <", "<", 1)
    
    Loop While Len(cTest) > Len(CleanXMLData)                    '   until we can't find any more

    On Error GoTo 0
    Exit Function

CleanXMLData_ERH:

    Call AddToDebugList("[ERROR] " & Err.Number & " (" & Err.Description & ") in procedure CleanXMLData of Form frmMain", lstDebug)
    
End Function

Private Function GetXMLResourceList(Payload As String, ByRef nErrorCode As Integer) As String()
    Dim cUnits As String
    Dim cUnit As String
    Dim aWork() As String
    Dim aUnits() As Vehicle
    Dim n As Integer
    Dim cSTag As String
    Dim cETag As String
    Dim cEmpty As String
    Dim nEmpty As Long
    Dim nStart As Long
    Dim nStop As Long
    
    Dim cError As String
    
    On Error GoTo GetXMLResourceList_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetXMLResourceList")
    End If
    
    cError = "Start of GetXMLResourceList"

    nErrorCode = -1

    Call AddToDebugList("[GetXMLResourceList]", lstDebug)
    
    cEmpty = "<Resources />"
    cSTag = "<Resources>"
    cETag = "</Resources>"
    
    nEmpty = InStr(1, Payload, cEmpty)
    nStart = InStr(1, Payload, cSTag) + Len(cSTag)
    nStop = InStr(1, Payload, cETag) - nStart
    
    cError = "Parsing XML Payload"
    
    If nEmpty <= 0 Then
        
        cUnits = Mid(Payload, nStart, nStop)
        
        cEmpty = "<Resource />"
        cSTag = "<Resource>"
        cETag = "</Resource>"
        
        nEmpty = InStr(1, Payload, cEmpty)
        nStart = InStr(1, Payload, cSTag) + Len(cSTag)
        nStop = InStr(1, Payload, cETag) - nStart
        
        cError = "Parsing XML Subitems"
        
        If nEmpty <= 0 Then
        
            nErrorCode = 1
            
            cUnits = Replace(cUnits, cETag & cSTag, "|", 1)
            cUnits = Replace(cUnits, cSTag, "", 1)
            cUnits = Replace(cUnits, cETag, "", 1)
            
            GetXMLResourceList = Split(cUnits, "|")
        
        Else
            GetXMLResourceList = Split("Empty List in GetXMLResourceList - " & cSTag & cETag & " NOT FOUND " & cError, "|")
        End If
    
    Else
        GetXMLResourceList = Split("Empty List in GetXMLResourceList - " & cSTag & cETag & " NOT FOUND " & cError, "|")
    End If
    
    On Error GoTo 0
    Exit Function

GetXMLResourceList_ERH:

    Call AddToDebugList("[ERROR] " & Err.Number & " (" & Err.Description & ") in procedure GetXMLResourceList of Form frmMain - " & cError, lstDebug)
    
End Function
    
Private Function ParseXMLUnitRouteInfo(aResourceList() As String) As Vehicle()
    Dim aUnits() As Vehicle
    Dim n As Integer
    Dim Payload As String
    
    Dim cError As String

    On Error GoTo ParseXMLUnitRouteInfo_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ParseXMLUnitRouteInfo")
    End If
    
    cError = "Start of ParseXMLUnitRouteInfo"

    Call AddToDebugList("[ParseXMLUnitRouteInfo] List Length: " & UBound(aResourceList, 1) + 1, lstDebug)
    
    ReDim aUnits(0 To UBound(aResourceList, 1))
    
    For n = 0 To UBound(aResourceList, 1)
    
        Payload = aResourceList(n)
    
        ' Call Sign -------------------------------------------------
        
        aUnits(n).UnitName = GetXMLValue(Payload, "CallSign")

        
        ' Start Latitude --------------------------------------------
        
        aUnits(n).CurrentLat = Val(GetXMLValue(Payload, "StartLat"))
        
        ' Start Latitude --------------------------------------------
        
        aUnits(n).CurrentLon = Val(GetXMLValue(Payload, "StartLong"))
        
        ' Start Latitude --------------------------------------------
        
        aUnits(n).DestinationLat = Val(GetXMLValue(Payload, "EndLat"))
        
        ' Start Latitude --------------------------------------------
        
        aUnits(n).DestinationLon = Val(GetXMLValue(Payload, "EndLong"))
        
    Next n
    
    ParseXMLUnitRouteInfo = aUnits

    On Error GoTo 0
    Exit Function

ParseXMLUnitRouteInfo_ERH:

    Call AddToDebugList("[ERROR] " & Err.Number & " (" & Err.Description & ") in procedure ParseXMLUnitRouteInfo of Form frmMain", lstDebug)

End Function

Private Function ParseXMLUnitOffDuty(aResourceList) As Vehicle()
    Dim aUnits() As Vehicle
    Dim n As Integer
    Dim Payload As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ParseXMLUnitOffDuty")
    End If
    
    ReDim aUnits(0 To UBound(aResourceList, 1))
    
    For n = 0 To UBound(aResourceList, 1)
    
        Payload = aResourceList(n)
    
        ' Call Sign -------------------------------------------------
        
        aUnits(n).UnitName = GetXMLValue(Payload, "CallSign")
        
    Next n
    
    ParseXMLUnitOffDuty = aUnits

End Function
    
Private Function GetXMLValue(cXMLString As String, cTag As String) As String
    Dim cSTag As String
    Dim cETag As String
    Dim nStart As Long
    Dim nStop As Long
    
    Dim cError As String

    On Error GoTo GetXMLValue_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetXMLValue")
    End If
    
    cError = "Start of GetXMLValue"

    Call AddToDebugList("[GetXMLValue] TagName: " & cTag, lstDebug)
    
    cSTag = "<" & cTag & ">"
    cETag = "</" & cTag & ">"
    
    nStart = InStr(1, cXMLString, cSTag) + Len(cSTag)
    nStop = InStr(1, cXMLString, cETag) - nStart
    
    GetXMLValue = Mid(cXMLString, nStart, nStop)

    On Error GoTo 0
    Exit Function

GetXMLValue_ERH:

    Call AddToDebugList("[ERROR] " & Err.Number & " (" & Err.Description & ") in procedure GetXMLValue of Form frmMain", lstDebug)
    
End Function

Private Sub ProcessSetResourceOffduty(aWork() As String)
    Dim aUnits() As Vehicle
    Dim n As Integer
    Dim Payload As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessSetResourceOffduty")
    End If
    
    ReDim aUnits(0 To UBound(aWork, 1))
    
    For n = 0 To UBound(aWork)
    
        Payload = aWork(n)
    
        ' Call Sign -------------------------------------------------
        
        aUnits(n).UnitName = GetXMLValue(Payload, "CallSign")
        
        ' Start Latitude --------------------------------------------
        
        aUnits(n).CurrentLat = Val(GetXMLValue(Payload, "StartLat"))
        
        ' Start Latitude --------------------------------------------
        
        aUnits(n).CurrentLon = Val(GetXMLValue(Payload, "StartLong"))
        
        ' Start Latitude --------------------------------------------
        
        aUnits(n).DestinationLat = Val(GetXMLValue(Payload, "EndLat"))
        
        ' Start Latitude --------------------------------------------
        
        aUnits(n).DestinationLon = Val(GetXMLValue(Payload, "EndLong"))
        
    Next n

End Sub
            
Private Sub ProcessRouteFile(Payload As String)                 '   processes route files in place of route IPC messages - required for VERY LONG routes
    Dim nFile As Integer
    Dim cMsg As String
    Dim cPayload As String
    Dim nTimer As Single
    
    On Error GoTo ProcessRouteFile_ERH
    
    nTimer = Timer          '   seconds before midnight
        
    Do While Dir(Payload) = "" And Timer - nTimer < 30#         '   wait up to thirty seconds for route file to flush to disk (should never be more than 1 or 2)
        DoEvents
        If Timer < nTimer Then nTimer = nTimer - 86400          '   account for the midnight edge condition
    Loop
    
    Call AddToDebugList("[DEBUG] ProcessRouteFile waited " & Format(Timer - nTimer, "#0.00") & " for " & Payload, lstDebug)
    
    nFile = FreeFile
    Open Payload For Input As #nFile                            '   open the route file
    cMsg = Input$(LOF(nFile), nFile)                            '   get the payload from the file
    Close #nFile                                                '   close the file
    
    cPayload = Right(cMsg, Len(cMsg) - 4)                       '   trim off the CN message number
    Call ProcessReceivedRoute(cPayload)                         '   send the remainder of the route payload on for processing
    
    Kill Payload                                                '   Delete the route file after use
    Exit Sub
    
ProcessRouteFile_ERH:
    Call AddToDebugList("Error " & Err.Number & " (" & Err.Description & ") in procedure ProcessRouteFile of Form frmMain", lstDebug)
End Sub
            
Private Sub ProcessReceivedRoute(Payload As String)
    Dim nUnitID As Long
    Dim Route As tRoute
    Dim UnitRecord As Vehicle
    Dim StatusChangeTime As Date
    Dim n As Integer
    Dim bRouteValid As Boolean
    Dim bIDMismatch As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessReceivedRoute")
    End If
    
    Route = UnPackReceivedRoute(Payload, nUnitID, bRouteValid, bIDMismatch)         '   ID Mismatch added to deal with unit swap race condition
    
    '    Call PurgeEvents(nUnitID)
    
    UnitRecord = GetUnitRecord(nUnitID)
    
    Call AddToDebugList("[DEBUG] ProcessReceivedRoute for " & UnitRecord.UnitName & IIf(bRouteValid, ":Valid", ":Not Valid"))
    Call AddToDebugList("[DEBUG]                      from " & UnitRecord.CurrentLoc & " to " & UnitRecord.DestinationLoc)
    
    If bRouteValid Then
    
        UnitRecord.Route = Route.Route
        UnitRecord.RouteSegments = Route.RoutePoints
        
        Call AddToDebugList("[ProcessReceivedRoute] Route for " & UnitRecord.UnitName & " - " & Route.RoutePoints & " AVL points, (Dist: " & Route.TotalDistance & ", Time:" & Route.TotalTime & ")", lstDebug)
        
        For n = 0 To UnitRecord.RouteSegments - 1
            UnitRecord.Route(n).Time = UnitRecord.Route(n).Time / gDriveCompression * IIf(cmbCostAttribute.Text = "Minutes", 60, 1)
        Next n
        
        StatusChangeTime = InsertAVLEvents(UnitRecord, UnitRecord.StatusChangeTime)
        
        If UnitRecord.EndStatus > 0 Then
            Call InsertStatusEvent(UnitRecord, UnitRecord.NextStatus, UnitRecord.EndStatus, StatusChangeTime)    '   insert the termination status change
            Call InsertRadioMessage(UnitRecord, UnitRecord.EndStatus, StatusChangeTime, UnitRecord.DestinationLoc, UnitRecord.NextStatus)
            '   Call InsertRadioMessage(UnitRecord, UnitRecord.EndStatus, StatusChangeTime)
        End If
    
    Else
    
        '   determine here what to do with a unit that received a routing calulation error
        
        If Not bIDMismatch Then
            Call AddToDebugList("[ProcessReceivedRoute] Zero-length Route received for " & UnitRecord.UnitName & " - changing to next status immediately (" & gStatusList(UnitRecord.EndStatus).Description & ")", lstDebug)
            
            StatusChangeTime = Now              '   jump to the next status immediately
            
            If UnitRecord.EndStatus > 0 Then
                Call InsertStatusEvent(UnitRecord, UnitRecord.NextStatus, UnitRecord.EndStatus, StatusChangeTime)    '   insert the termination status change
                Call InsertRadioMessage(UnitRecord, UnitRecord.EndStatus, StatusChangeTime, UnitRecord.DestinationLoc, UnitRecord.NextStatus)
            End If
        Else
            Call AddToDebugList("[ProcessReceivedRoute] ID Mismatched Route received for " & UnitRecord.UnitName & " - waiting for pending route calculation to complete.", lstDebug)
        End If
    
    End If
    
End Sub

Private Function UnPackReceivedRoute(ByVal Payload As String, ByRef nUnitID As Long, ByRef bRouteValid As Boolean, ByRef bIDMismatch As Boolean) As tRoute
    Dim Route As tRoute
    Dim aRoute() As String
    Dim aPoints() As String
    Dim aPt() As String
    Dim n As Long
    Dim cUniqueRouteID As String
    
    Dim cError As String

    On Error GoTo UnPackReceivedRoute_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function UnPackReceivedRoute")
    End If
    
    cError = "Start of UnPackReceivedRoute"

    aRoute = Split(Payload, "|")
    
    nUnitID = aRoute(0)
    cUniqueRouteID = aRoute(1)
    Route.RoutePoints = aRoute(2)
    Route.TotalDistance = aRoute(3)
    Route.TotalTime = aRoute(4)
    
    If Val(cUniqueRouteID) = gRouteHashTable(GetUnitIndexByVehID(nUnitID)) Then
    
        If Route.RoutePoints > 0 Then
        
            bRouteValid = True
            bIDMismatch = False
    
            aPoints = Split(aRoute(5), ";")
        
            ReDim Route.Route(0 To UBound(aPoints, 1))
        
            For n = 0 To UBound(aPoints, 1)
                
                cError = "UnPackReceivedRoute Loop, n = " & n
                
                aPt = Split(aPoints(n), ",")
                Route.Route(n).AccumDist = aPt(0)
                Route.Route(n).AccumTime = aPt(1)
                Route.Route(n).Distance = aPt(2)
                Route.Route(n).Lat = aPt(3)
                Route.Route(n).Lon = aPt(4)
                Route.Route(n).Time = aPt(5)
            Next n
        
        Else                    '   handle an error message route response received from the routing engine - Route.RoutePoints = 0
            
            bRouteValid = False
            bIDMismatch = False
            Call AddToDebugList("[ERROR] Function UnPackReceivedRoute: Zero-length Route Discarded, Received >" & Payload & "< from cnRoutingTG", lstDebug)
        
        End If
    
    Else
    
        bRouteValid = False
        bIDMismatch = True
        Call AddToDebugList("[DEBUG] Function UnPackReceivedRoute: Route Discarded, ID Mismatch = " & cUniqueRouteID & ", expected = " & gRouteHashTable(GetUnitIndexByVehID(nUnitID)), lstDebug)
    
    End If
    
    UnPackReceivedRoute = Route
    
    On Error GoTo 0
    Exit Function

UnPackReceivedRoute_ERH:

    Call AddToDebugList("Error " & Err.Number & " (" & Err.Description & ") in procedure UnPackReceivedRoute of Form frmMain", lstDebug)
    
End Function

Private Function InsertAVLEvents(UnitRecord As Vehicle, StartTime As Variant) As Variant
    Dim n As Integer
    Dim xItem As ListItem
    Dim cMessageBody As String
    Dim SegTime As Variant
    
'    .ColumnHeaders.Add 1, , "Time", 1000
'    .ColumnHeaders.Add 2, , "Vehicle", 1000
'    .ColumnHeaders.Add 3, , "Type", 800
'    .ColumnHeaders.Add 4, , "Interval", 1200
'    .ColumnHeaders.Add 5, , "Distance", 1200
'    .ColumnHeaders.Add 6, , "Message Body", 30000

'                  1      2        3         4
'   cMessageBody = AVL_ID|Latitude|Longitude|UpdateTime
    
    Dim cError As String

    On Error GoTo InsertAVLEvents_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function InsertAVLEvents")
    End If
    
    cError = "Start of InsertAVLEvents"

    Call AddToDebugList("[InsertAVLEvents] EntryPoint", lstDebug)
    
    If IsEmpty(StartTime) Then
        SegTime = Now
    Else
        SegTime = StartTime
    End If
    
    For n = 0 To UnitRecord.RouteSegments - 1
        SegTime = DateAdd("S", IIf(UnitRecord.Route(n).Time >= 1, UnitRecord.Route(n).Time, 1), SegTime)
        
        If SegTime >= gScriptTime Then          '   Eliminate all AVL updates that occur prior to the startup time of the scenario (since they have already occurred)
                                                '   This will save a great deal of time in having the scenario come up to speed. All routes will still
                                                '   have to be calculated, but the interface work of moving the units to their starting locations will
                                                '   be greatly reduced.
                                                
                                                '   gMeridianOffset correction applied here
        
            '                  1      2        3         4          5          6       7
            '   cMessageBody = AVL_ID|Latitude|Longitude|UpdateTime|Vehicle_ID|Heading|Speed
            '
            '   Note: Heading and Speed values are NOT calculated by Simulator are are added for compatibility only
        
            cMessageBody = Trim(Str(UnitRecord.AVLID)) & "|" _
                            & Format(UnitRecord.Route(n).Lat + gLatitudeAdjust, "###0.000000") & "|" _
                            & Format(UnitRecord.Route(n).Lon + gLongitudeAdjust, "###0.000000") & "|" _
                            & Format(SegTime, "MMM DD YYYY HH:NN:SS") & "|" _
                            & Trim(Str(UnitRecord.VehID)) & "|" _
                            & "0|0"                     '   format the AVL message (Heading and Speed fill values added for compatibility with AVLMDT Interface)
            
            Set xItem = lvEvents.ListItems.Add(, , Format(SegTime, "hh:nn:ss"))                     '   insert the AVL update into the Event Queue
            xItem.SubItems(LVUnit) = UnitRecord.UnitName
            xItem.SubItems(LVType) = Format(EV_AVL, "0000")
            xItem.SubItems(LVDur) = Format(UnitRecord.Route(n).Time, "##0.00")
            xItem.SubItems(LVDist) = Format(UnitRecord.Route(n).Distance, "#0.000")
            xItem.SubItems(LVMessage) = cMessageBody
            xItem.SubItems(LVSort) = Format(SegTime, DATE_FORMAT)
            
        End If
        
    Next n
    
    If gDetailedDebugging Then
        Call AddToDebugList("[INFO] InsertAVLEvents - List Length = " & lvEvents.ListItems.count, lstDebug)
    End If
    
    InsertAVLEvents = SegTime                                                                       '   return the ending time of the current route segment

    On Error GoTo 0
    Exit Function

InsertAVLEvents_ERH:

    Call AddToDebugList("[ERROR] " & Err.Number & " (" & Err.Description & ") in procedure InsertAVLEvents of Form frmMain", lstDebug)

End Function

Private Function InsertIncEvents(nIncID As Long, _
                                    Optional ByVal StartTime As Variant, _
                                    Optional ByVal EventTime As Variant, _
                                    Optional ByVal Address As String, _
                                    Optional ByVal City As String, _
                                    Optional ByVal Priority As String, _
                                    Optional ByVal Compression As Double = 1, _
                                    Optional ByVal bCalledFromScript As Boolean = False) As Variant
    
    Dim n As Integer
    Dim xItem As ListItem
    Dim cMessageBody As String
    Dim nInterval As Double
    Dim SegTime As Variant
    Dim cANIALIString As String
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function InsertIncEvents")
    End If
    
'                  1      2          3     4          5
'   cMessageBody = INC_ID|<Address>|<City>|<Priority>|SendTime
    
    If IsMissing(StartTime) Then        '   triggered by command button
        SegTime = Now
        
        '   This is where we will have to insert the history record for the new incident
        
        Call LoadIncidentHistory(SegTime, SegTime, nIncID)
    Else                                '   scripted insertion
        nInterval = DateDiff("S", CDate(StartTime), CDate(EventTime))
        SegTime = DateAdd("S", nInterval / Compression, Now)
    End If
    
    cMessageBody = Trim(Str(nIncID)) & "|" _
                    & IIf(IsMissing(Address), "<Address>", Address) & "|" _
                    & IIf(IsMissing(City), "<City>", City) & "|" _
                    & IIf(IsMissing(Priority), "<Priority>", Priority) & "|" _
                    & Format(SegTime, "MMM DD YYYY HH:NN:SS")
    
    Set xItem = lvEvents.ListItems.Add(, , Format(SegTime, "hh:nn:ss"))
    xItem.SubItems(LVUnit) = ""
    xItem.SubItems(LVType) = Format(EV_INC, "0000")
    xItem.SubItems(LVDur) = "INC"
    xItem.SubItems(LVDist) = "INC"
    xItem.SubItems(LVMessage) = cMessageBody
    xItem.SubItems(LVSort) = Format(SegTime, DATE_FORMAT)
    
    InsertIncEvents = SegTime
    
    If gSendANIALIWithInc And bCalledFromScript Then
        cANIALIString = Trim(Str(nIncID)) & "," & gANIALIWorkstation & "," & IIf(gUseANIALIDialog, "1", "0")
        Call InsertE911Events(cANIALIString, DateAdd("S", -60, SegTime))
    End If
    
    If gDetailedDebugging Then
        Call AddToDebugList("[INFO] InsertINCEvents - List Length = " & lvEvents.ListItems.count)
    End If
    
    Exit Function
    
ERH:

    Call AddToDebugList("ERROR: InsertIncEvents - " & cMessageBody, lstDebug)
    Call AddToDebugList("  ERR: " & Err.Number & "   DESC: " & Err.Description, lstDebug)

End Function

Private Function InsertLogOnEvents(OnTime As Variant, cUnit As String, LogOnMessage As String) As Boolean
    Dim xItem As ListItem
    
'    .ColumnHeaders.Add 1, , "Time", 1000
'    .ColumnHeaders.Add 2, , "Vehicle", 1000
'    .ColumnHeaders.Add 3, , "Type", 800
'    .ColumnHeaders.Add 4, , "Interval", 1200
'    .ColumnHeaders.Add 5, , "Distance", 1200
'    .ColumnHeaders.Add 6, , "Message Body", 30000

    On Error GoTo ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function InsertLogOnEvents")
    End If

    InsertLogOnEvents = False

    Set xItem = lvEvents.ListItems.Add(, , Format(OnTime, "hh:nn:ss"))
    xItem.SubItems(LVUnit) = cUnit
    xItem.SubItems(LVType) = Format(EV_INIT_ON, "0000")
    xItem.SubItems(LVDur) = "LOGON"
    xItem.SubItems(LVDist) = "LOGON"
    xItem.SubItems(LVMessage) = LogOnMessage
    xItem.SubItems(LVSort) = Format(OnTime, DATE_FORMAT)
    
    InsertLogOnEvents = True
    
    Exit Function
    
ERH:

    Call AddToDebugList("ERROR: InsertLogOnEvents - " & LogOnMessage, lstDebug)
    Call AddToDebugList("  ERR: " & Str(Err.Number) & "   DESC: " & Err.Description, lstDebug)

End Function

Private Function InsertContextEvents(StartTime As Variant) As Boolean
    Dim xItem As ListItem

'    .ColumnHeaders.Add 1, , "Time", 1000
'    .ColumnHeaders.Add 2, , "Vehicle", 1000
'    .ColumnHeaders.Add 3, , "Type", 800
'    .ColumnHeaders.Add 4, , "Interval", 1200
'    .ColumnHeaders.Add 5, , "Distance", 1200
'    .ColumnHeaders.Add 6, , "Message Body", 30000

    On Error GoTo ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function InsertContextEvents")
    End If

    InsertContextEvents = False

    Set xItem = lvEvents.ListItems.Add(, , Format(StartTime, "hh:nn:ss"))           '   set the context for the current scenario
    xItem.SubItems(LVUnit) = ""                                                  '   based on the script date
    xItem.SubItems(LVType) = Format(EV_INIT_INC, "0000")
    xItem.SubItems(LVDur) = "INITINC"
    xItem.SubItems(LVDist) = "INITINC"                                              '   we must send both the source datetime and the script run datetime
    xItem.SubItems(LVMessage) = Format(StartTime, "MMM DD YYYY HH:NN:SS") & "|" & Format(gScriptTime, "MMM DD YYYY HH:NN:SS")
    xItem.SubItems(LVSort) = Format(gScriptTime, DATE_FORMAT)                       '   make sure it gets done as part of the script initialization
                                                                                    '   by using 'gScriptTime'
    InsertContextEvents = True
    
    Exit Function
    
ERH:

    Call AddToDebugList("ERROR: InsertContextEvents - " & Format(StartTime, "MMM DD YYYY HH:NN:SS"), lstDebug)
    Call AddToDebugList("  ERR: " & Str(Err.Number) & "   DESC: " & Err.Description, lstDebug)

End Function

Private Sub InsertE911Events(cAniAliMessage As String, SegTime As Variant)
    Dim n As Integer
    Dim xItem As ListItem
    Dim cMessageBody As String
    Dim nInterval As Double

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InsertE911Events")
    End If

''                  1        2             3
''   cMessageBody = nANIALIID,<Workstation Name>,0

    Set xItem = lvEvents.ListItems.Add(, , Format(SegTime, "hh:nn:ss"))
    xItem.SubItems(LVUnit) = ""
    xItem.SubItems(LVType) = Format(EV_E911, "0000")
    xItem.SubItems(LVDur) = "E911"
    xItem.SubItems(LVDist) = "E911"
    xItem.SubItems(LVMessage) = cAniAliMessage
    xItem.SubItems(LVSort) = Format(SegTime, DATE_FORMAT)

    If gDetailedDebugging Then
        Call AddToDebugList("[INFO] InsertE911Events - List Length = " & lvEvents.ListItems.count)
    End If
    
End Sub

Private Sub InsertStatusEvent(UnitRecord As Vehicle, CurrentStatus As Integer, NewStatus As Integer, StatusTime As Variant)
    Dim xItem As ListItem
    Dim cSQL As String
    Dim aSQL As Variant
    Dim Latitude As Double
    Dim Longitude As Double
    Dim cLoc As String
    Dim nRows As Long
    Dim cMessageBody As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InsertStatusEvent")
    End If
    
    cLoc = ""
    
    Select Case NewStatus
        Case STAT_OnScene
            Latitude = UnitRecord.SceneLat
            Longitude = UnitRecord.SceneLon
        Case STAT_AtDest
            Latitude = UnitRecord.DestinationLat
            Longitude = UnitRecord.DestinationLon
            cLoc = UnitRecord.DestinationCode
        Case STAT_AtStation
            If CurrentStatus <> STAT_Avail Then
                Latitude = UnitRecord.DestinationLat
                Longitude = UnitRecord.DestinationLon
                cLoc = UnitRecord.DestinationLoc
            Else
                Latitude = UnitRecord.StationLat
                Longitude = UnitRecord.StationLon
                cLoc = UnitRecord.Station
            End If
        Case STAT_LocalArea
            Latitude = UnitRecord.DestinationLat
            Longitude = UnitRecord.DestinationLon
            cLoc = UnitRecord.DestinationLoc
        Case STAT_Transport
            Latitude = UnitRecord.CurrentLat
            Longitude = UnitRecord.CurrentLon
            cLoc = UnitRecord.DestinationCode
        Case Else
            Latitude = UnitRecord.CurrentLat
            Longitude = UnitRecord.CurrentLon
    End Select

'    .ColumnHeaders.Add 1, , "Time", 1000
'    .ColumnHeaders.Add 2, , "Vehicle", 1000
'    .ColumnHeaders.Add 3, , "Type", 800
'    .ColumnHeaders.Add 4, , "Interval", 1200
'    .ColumnHeaders.Add 5, , "Distance", 1200
'    .ColumnHeaders.Add 6, , "Message Body", 30000

'                  1      2         3         4        5         6          7        8
'   cMessageBody = MDT_ID|OldStatus|NewStatus|Latitude|Longitude|StatusTime|Odometer|DestinationCode
    
    '   gMeridianOffset correction applied here
    
    cMessageBody = Trim(Str(UnitRecord.MDTID)) & "|" _
                            & Format(CurrentStatus, "00") & "|" _
                            & Format(NewStatus, "00") & "|" _
                            & Format(Latitude + gLatitudeAdjust, "###0.000000") & "|" _
                            & Format(Longitude + gLongitudeAdjust, "###0.000000") & "|" _
                            & Format(StatusTime, "MMM DD YYYY HH:NN:SS") & "|" _
                            & cLoc

    Set xItem = lvEvents.ListItems.Add(, , Format(DateAdd("S", 1, StatusTime), "hh:nn:ss"))     '   add one second to status change event to allow for AVL processing queue to catch up
    xItem.SubItems(LVUnit) = UnitRecord.UnitName
    xItem.SubItems(LVType) = Format(EV_STAT, "0000")
    xItem.SubItems(LVDur) = "STAT"
    xItem.SubItems(LVDist) = "STAT"
    xItem.SubItems(LVMessage) = cMessageBody
    xItem.SubItems(LVSort) = Format(StatusTime, DATE_FORMAT)
        
    If gDetailedDebugging Then
        Call AddToDebugList("[INFO] InsertStatusEvent - List Length = " & lvEvents.ListItems.count)
    End If
    
End Sub

Private Sub InsertTransportEvent(UnitRecord As Vehicle, CurrentStatus As Integer, NewStatus As Integer, StatusTime As Variant, UsedClosest As Boolean)
    Dim xItem As ListItem
    Dim cSQL As String
    Dim aSQL As Variant
    Dim Latitude As Double
    Dim Longitude As Double
    Dim cLoc As String
    Dim nRows As Long
    Dim nNumPats As Long
    Dim cProtocol As String
    Dim cPriority As String
    Dim cMessageBody As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InsertTransportEvent")
    End If
    
    Latitude = UnitRecord.CurrentLat
    Longitude = UnitRecord.CurrentLon
    cLoc = UnitRecord.DestinationCode
    
    '    cPriority = bsiGetSettings("SystemDefaults", "DefaultTransportPriority", "", App.Path & "\Simulator.INI")
    '
    '    If Not UsedClosest Then
    '        cProtocol = bsiGetSettings("SystemDefaults", "DefaultTransportProtocol", "", App.Path & "\Simulator.INI")
    '    Else
    '        cProtocol = bsiGetSettings("SystemDefaults", "ClosestTransportProtocol", "", App.Path & "\Simulator.INI")
    '    End If
    
    nNumPats = 1

'    .ColumnHeaders.Add 1, , "Time", 1000
'    .ColumnHeaders.Add 2, , "Vehicle", 1000
'    .ColumnHeaders.Add 3, , "Type", 800
'    .ColumnHeaders.Add 4, , "Interval", 1200
'    .ColumnHeaders.Add 5, , "Distance", 1200
'    .ColumnHeaders.Add 6, , "Message Body", 30000

'                  1      2         3         4        5         6          7        8    9       10
'   cMessageBody = MDT_ID|OldStatus|NewStatus|Latitude|Longitude|StatusTime|Protocol|Mode|NumPats|DestinationCode
    
    '   gMeridianOffset correction applied here
    
    cMessageBody = Trim(Str(UnitRecord.MDTID)) & "|" _
                            & Format(CurrentStatus, "00") & "|" _
                            & Format(NewStatus, "00") & "|" _
                            & Format(Latitude + gLatitudeAdjust, "###0.000000") & "|" _
                            & Format(Longitude + gLongitudeAdjust, "###0.000000") & "|" _
                            & Format(StatusTime, "MMM DD YYYY HH:NN:SS") & "|" _
                            & cProtocol & "|" _
                            & cPriority & "|" _
                            & Format(nNumPats, "0") & "|" _
                            & cLoc

    Set xItem = lvEvents.ListItems.Add(, , Format(StatusTime, "hh:nn:ss"))
    xItem.SubItems(LVUnit) = UnitRecord.UnitName
    xItem.SubItems(LVType) = Format(EV_TRANS, "0000")
    xItem.SubItems(LVDur) = "TRANS"
    xItem.SubItems(LVDist) = "TRANS"
    xItem.SubItems(LVMessage) = cMessageBody
    xItem.SubItems(LVSort) = Format(StatusTime, DATE_FORMAT)
        
    If gDetailedDebugging Then
        Call AddToDebugList("[INFO] InsertTransportEvent - List Length = " & lvEvents.ListItems.count)
    End If
    
End Sub

Private Sub InsertRadioMessage(UnitRecord As Vehicle, NewStatus As Integer, StatusTime As Variant, _
                                    Optional cLocation As String = "", _
                                    Optional ByVal LastStatus As Integer = -1, _
                                    Optional FreeFormText As String)
                                    
    Dim xItem As ListItem
    Dim cRadio As String
    Dim cTalk As String
    Dim cChar As String
    Dim cUnitType As String
    Dim cUnitName As String
    Dim cUnitNumber As String
    Dim RadioChannel As String
    Dim ChannelName As String
    Dim CrewVoice As String
    Dim n As Integer
    Dim SectorDivision As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InsertRadioMessage")
    End If
    
'    .ColumnHeaders.Add 1, , "Time", 1000
'    .ColumnHeaders.Add 2, , "Vehicle", 1000
'    .ColumnHeaders.Add 3, , "Type", 800
'    .ColumnHeaders.Add 4, , "Interval", 1200
'    .ColumnHeaders.Add 5, , "Distance", 1200
'    .ColumnHeaders.Add 6, , "Message Body", 30000

    If gAssignChannelBy = "S" Then
        SectorDivision = GetSectorByDivision(UnitRecord.CurrentDivision)
    Else
        SectorDivision = UnitRecord.CurrentDivision
    End If
    
    Call GetRadioChannel(SectorDivision, RadioChannel, ChannelName)
    
    CrewVoice = Str(UnitRecord.VehID Mod gMaxVoices)

    If NewStatus = STAT_RADIOCHANNELCHANGE Then
        If ChannelName = "" Then
            cTalk = " with you on channel " & RadioChannel & ", "
            cRadio = " with you on channel " & RadioChannel & ", "
        Else
            cTalk = " with you on " & ChannelName & ", "
            cRadio = " with you on " & ChannelName & ", "
        End If
        Call ChangeRadioChannel(UnitRecord, RadioChannel)
        NewStatus = UnitRecord.Status
    ElseIf NewStatus = STAT_FREEFORMTEXT Then
        cTalk = ", " & FreeFormText
        cRadio = ", " & FreeFormText
    ElseIf LastStatus = STAT_Avail Or LastStatus = STAT_Enr2Post Or LastStatus = STAT_LocalArea Then
        '    cTalk = " 10-4, "
        '    cRadio = " 10 4, "
    Else
        cTalk = " is "
        cRadio = " is "
    End If
    
    Select Case NewStatus
        Case STAT_Avail
            cTalk = cTalk & " CLEAR of " & UnitRecord.CurrentLoc
            cRadio = cRadio & " clear of " & TranslateRadioLocation(UnitRecord.CurrentLoc)
        Case STAT_AtStation
            If UnitRecord.DestinationLoc = "" Then
                cTalk = cTalk & " ARRIVING at " & UnitRecord.Station
                cRadio = cRadio & " arriving at " & TranslateRadioLocation(UnitRecord.Station)
            Else
                cTalk = cTalk & " ARRIVING at " & UnitRecord.DestinationLoc
                cRadio = cRadio & " arriving at " & TranslateRadioLocation(UnitRecord.DestinationLoc)
            End If
        Case STAT_LocalArea
                cTalk = cTalk & " in the area of " & UnitRecord.DestinationLoc
                cRadio = cRadio & " in the area of " & TranslateRadioLocation(UnitRecord.DestinationLoc)
        Case STAT_AvailOS
            cTalk = cTalk & " CANCELLED at " & UnitRecord.CurrentLoc
            cRadio = cRadio & " cancelled at " & TranslateRadioLocation(UnitRecord.CurrentLoc)
        Case STAT_OOS
            cTalk = cTalk & " OUT OF SERVICE at " & UnitRecord.CurrentLoc
            cRadio = cRadio & " OUT OF SERVICE at " & TranslateRadioLocation(UnitRecord.CurrentLoc)
        Case STAT_Resp
            cTalk = cTalk & " RESPONDING to " & UnitRecord.DestinationLoc
            cRadio = cRadio & " responding to " & TranslateRadioLocation(UnitRecord.DestinationLoc)
        Case STAT_Enr2Post
            cTalk = cTalk & " ENROUTE to " & UnitRecord.DestinationLoc
            cRadio = cRadio & " enroute to " & TranslateRadioLocation(UnitRecord.DestinationLoc)
        Case STAT_Staged
        Case STAT_OnScene
            If UnitRecord.DestinationLoc <> "" Then
                cTalk = cTalk & " ARRIVING at " & UnitRecord.DestinationLoc
                cRadio = cRadio & " arriving at " & TranslateRadioLocation(UnitRecord.DestinationLoc)
            Else
                cTalk = cTalk & " ARRIVING at " & UnitRecord.CurrentLoc
                cRadio = cRadio & " arriving at " & TranslateRadioLocation(UnitRecord.CurrentLoc)
            End If
        Case STAT_PtContact
        Case STAT_Transport
            If cLocation = "" Then
                cTalk = cTalk & " ENROUTE to " & UnitRecord.DestinationLoc
                cRadio = cRadio & " enroute to " & TranslateRadioLocation(UnitRecord.DestinationLoc)
            Else
                cTalk = cTalk & " ENROUTE to " & cLocation
                cRadio = cRadio & " enroute to " & TranslateRadioLocation(cLocation)
            End If
        Case STAT_AtDest
            If cLocation = "" Then
                cTalk = cTalk & " ARRIVING at " & UnitRecord.DestinationLoc
                cRadio = cRadio & " arriving at " & TranslateRadioLocation(UnitRecord.DestinationLoc)
            Else
                cTalk = cTalk & " ARRIVING at " & cLocation
                cRadio = cRadio & " arriving at " & TranslateRadioLocation(cLocation)
            End If
        Case STAT_DelayedAvail
            If cLocation = "" Then
                cTalk = cTalk & " ready to clear at " & UnitRecord.CurrentLoc
                cRadio = cRadio & " ready to clear at " & TranslateRadioLocation(UnitRecord.CurrentLoc)
            Else
                cTalk = cTalk & " ready to clear at " & cLocation
                cRadio = cRadio & " ready to clear at " & TranslateRadioLocation(cLocation)
            End If
'            If cLocation = "" Then
'                cTalk = cTalk & " AVAILABLE at " & UnitRecord.CurrentLoc
'                cRadio = cRadio & " available at " & TranslateRadioLocation(UnitRecord.CurrentLoc)
'            Else
'                cTalk = cTalk & " AVAILABLE at " & cLocation
'                cRadio = cRadio & " available at " & TranslateRadioLocation(cLocation)
'            End If
        Case STAT_ATP
        Case STAT_D2L
        Case STAT_R2L
        Case STAT_A2L
    End Select
    
    Call SchedUnitLateInStatus(UnitRecord)
    
    If cTalk <> "" Then                 '   if there is to be a message
    
        For n = 1 To Len(UnitRecord.UnitName)
            cChar = Mid(UnitRecord.UnitName, n, 1)
            If bsiIsDigit(cChar) Then
                cUnitNumber = cUnitNumber & cChar
            ElseIf bsiIsAlpha(cChar) Then
                cUnitName = cUnitName & cChar
            End If
        Next n
        
        cRadio = cRadio & " "
        
        cRadio = Replace(cRadio, "\", " and ")
        cRadio = Replace(cRadio, "/", " and ")
        cRadio = Replace(cRadio, "&", " and ")
        
        cRadio = Replace(cRadio, " AV ", " AVENUE ", , vbTextCompare)
        cRadio = Replace(cRadio, " AVE ", " AVENUE ", , vbTextCompare)
        cRadio = Replace(cRadio, " DR ", " DRIVE ", , vbTextCompare)
        cRadio = Replace(cRadio, " RD ", " ROAD ", , vbTextCompare)
        cRadio = Replace(cRadio, " CT ", " COURT ", , vbTextCompare)
        cRadio = Replace(cRadio, " CL ", " CIRCLE ", , vbTextCompare)
        cRadio = Replace(cRadio, " CR ", " CRESCENT ", , vbTextCompare)
        cRadio = Replace(cRadio, " LA ", " LANE ", , vbTextCompare)
        cRadio = Replace(cRadio, " LI ", " LINE ", , vbTextCompare)
        cRadio = Replace(cRadio, " SQ ", " SQUARE ", , vbTextCompare)
        cRadio = Replace(cRadio, " WY ", " WAY ", , vbTextCompare)
        cRadio = Replace(cRadio, " WK ", " WALK ", , vbTextCompare)
        cRadio = Replace(cRadio, " BV ", " BOULEVARD ", , vbTextCompare)

        cRadio = Replace(cRadio, " Av ", " Avenue ", , vbTextCompare)
        cRadio = Replace(cRadio, " Dr ", " Drive ", , vbTextCompare)
        cRadio = Replace(cRadio, " Rd ", " Road ", , vbTextCompare)
        cRadio = Replace(cRadio, " Ct ", " Court ", , vbTextCompare)
        cRadio = Replace(cRadio, " Cl ", " Circle ", , vbTextCompare)
        cRadio = Replace(cRadio, " Cr ", " Crescent  ", , vbTextCompare)
        cRadio = Replace(cRadio, " La ", " Lane ", , vbTextCompare)
        cRadio = Replace(cRadio, " Li ", " Line ", , vbTextCompare)
        cRadio = Replace(cRadio, " Sq ", " Square ", , vbTextCompare)
        cRadio = Replace(cRadio, " Wy ", " Way ", , vbTextCompare)
        cRadio = Replace(cRadio, " Wk ", " Walk ", , vbTextCompare)
        cRadio = Replace(cRadio, " Bv ", " Boulevard ", , vbTextCompare)
        
        cRadio = Replace(cRadio, " E ", " EAST ")
        cRadio = Replace(cRadio, " N ", " NORTH ")
        cRadio = Replace(cRadio, " W ", " WEST ")
        cRadio = Replace(cRadio, " S ", " SOUTH ")
'        cRadio = Replace(cRadio, " ST ", " STREET ")
        
        cRadio = gControlName & ", " & TranslateRadioUnitNumber(UnitRecord) & " " & LCase(cRadio)
        
'        cTalk = "Control (" & Trim(SectorDivision) & "), " & UnitRecord.UnitName & cTalk
        cTalk = gControlName & ", " & UnitRecord.UnitName & cTalk
        
        Set xItem = lvEvents.ListItems.Add(, , Format(StatusTime, "hh:nn:ss"))
        xItem.SubItems(LVUnit) = UnitRecord.UnitName
        xItem.SubItems(LVType) = Format(EV_RADIO, "0000")
        xItem.SubItems(LVDur) = "TALK"
        xItem.SubItems(LVDist) = "TALK"
        xItem.SubItems(LVMessage) = Format(NP_BRIMAC_MESSAGE_CLASS, "0000") & Format(NP_BSI_RADIO_MESSAGE, "0000") & "|" & cTalk & "|" & cRadio & "|" & RadioChannel & "|" & CrewVoice & "|" & Trim(Str(UnitRecord.VehID))
        xItem.SubItems(LVSort) = Format(StatusTime, DATE_FORMAT)
        
    End If
    
    If gDetailedDebugging Then
        Call AddToDebugList("[INFO] InsertRadioEvent - List Length = " & lvEvents.ListItems.count)
    End If
    
End Sub

Private Sub SchedUnitLateInStatus(UnitRecord As Vehicle)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim aParams As Variant
    Dim cErrorMsg As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SchedUnitLateInStatus")
    End If
    
    '   USP_CADMonScheduleVehicleLate @VehicleID, @EmployeeID
    
    cSQL = "USP_CADMonScheduleVehicleLate"
    
    aParams = Array(-1, UnitRecord.VehID, 0)
    
    nRows = bsiSQLExecSP(cSQL, aSQL, gConnectString, , , cErrorMsg, aParams)
    
    If cErrorMsg <> "" Or aParams(0) <> 0 Then
        Call AddToDebugList("ERROR: SchedUnitLateInStatus - " & cErrorMsg & " - Returned: " & aParams(0), lstDebug)
    End If

End Sub


Private Sub InsertSoundEvent(UnitRecord As Vehicle, cSoundFile As String, SoundTime As Variant)
    Dim xItem As ListItem
    Dim SectorDivision As String
    Dim RadioChannel As String
    Dim ChannelName As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InsertSoundEvent")
    End If
    
    If gAssignChannelBy = "S" Then
        SectorDivision = GetSectorByDivision(UnitRecord.CurrentDivision)
    Else
        SectorDivision = UnitRecord.CurrentDivision
    End If

    Call GetRadioChannel(SectorDivision, RadioChannel, ChannelName)
    
    Set xItem = lvEvents.ListItems.Add(, , Format(SoundTime, "hh:nn:ss"))
    xItem.SubItems(LVUnit) = ""
    xItem.SubItems(LVType) = Format(EV_RADIO, "0000")
    xItem.SubItems(LVDur) = "SOUND"
    xItem.SubItems(LVDist) = "SOUND"
    xItem.SubItems(LVMessage) = Format(NP_BRIMAC_MESSAGE_CLASS, "0000") & Format(NP_BSI_RADIO_SOUNDBYTE, "0000") & "|" & cSoundFile & "|" & cSoundFile & "|" & RadioChannel & "|0"
    xItem.SubItems(LVSort) = Format(SoundTime, DATE_FORMAT)

End Sub

Private Function InsertSoundByteEvent(StartTime As Variant, _
                                            ByVal SoundTime As Variant, _
                                            ByVal cSoundFile As String, _
                                            ByVal RadioChannel As String, _
                                            ByVal Compression As Integer) As Variant
    Dim xItem As ListItem
    Dim nInterval As Long
    Dim SegTime As Variant
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function InsertSoundByteEvent")
    End If
    
    If Compression = 0 Then
        Compression = 1
        Call AddToDebugList("Error in InsertSoundByteEvent: Compression was 0 - Resetting to 1", lstDebug)
    End If
    
    nInterval = DateDiff("S", CDate(StartTime), CDate(SoundTime))
    SegTime = DateAdd("S", nInterval / Compression, Now)
    
    Set xItem = lvEvents.ListItems.Add(, , Format(SegTime, "hh:nn:ss"))
    xItem.SubItems(LVUnit) = ""
    xItem.SubItems(LVType) = Format(EV_RADIO, "0000")
    xItem.SubItems(LVDur) = "SOUND"
    xItem.SubItems(LVDist) = "SOUND"
    xItem.SubItems(LVMessage) = Format(NP_BRIMAC_MESSAGE_CLASS, "0000") & Format(NP_BSI_RADIO_SOUNDBYTE, "0000") & "|" & cSoundFile & "|" & cSoundFile & "|" & RadioChannel & "|0"
    xItem.SubItems(LVSort) = Format(SegTime, DATE_FORMAT)

    InsertSoundByteEvent = SegTime

    Exit Function
    
ERH:

    Call AddToDebugList("Error in InsertSoundByteEvent: Error No: " & Str(Err.Number) & ", " & Err.Description, lstDebug)

End Function

Private Sub GetRadioChannel(ByVal Division As String, ByRef RadioChannel As String, ByRef ChannelName As String)
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub GetRadioChannel")
    End If
    
    RadioChannel = 1        '   default to channel 1
    ChannelName = ""        '   default name to blank
    
    For n = 0 To UBound(gRadioChannels)
        If Division = gRadioChannels(n, 0) Then
            RadioChannel = gRadioChannels(n, 1)      '   assign radio channel on basis of division name
            ChannelName = gRadioChannels(n, 2)          '   assign radio channel name on basis of division name
            Exit For
        End If
    Next n
    
End Sub

Private Function GetSectorByDivision(Division As String) As String
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetSectorByDivision")
    End If
    
    cSQL = "Select Description from Sector where SectorID in " _
            & "(Select SectorID from SectorDivision where DivisionID in " _
            & "(Select ID from Division where DivName = '" & Division & "'))"
            
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    If nRows > 0 Then
        GetSectorByDivision = aSQL(0, 0)
    End If
            
End Function

Private Function TranslateRadioLocation(SpeakString As String) As String
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function TranslateRadioLocation")
    End If
    
    TranslateRadioLocation = SpeakString
    
    For n = 0 To UBound(gSpeechLocations, 1)
        If SpeakString = gSpeechLocations(n, 0) Then
            If gSpeechLocations(n, 1) <> "" Then
                TranslateRadioLocation = gSpeechLocations(n, 1)
            End If
            Exit For
        End If
    Next n

    TranslateRadioLocation = TranslateRadioLocation & " "               '   padded to allow for a trailing space on addresses (speach translation thing)

End Function

Private Function TranslateRadioCallSign(ResourceType As String) As String
    Dim n As Integer
    Dim Found As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function TranslateRadioCallSign")
    End If
    
    TranslateRadioCallSign = "Unit"
    Found = False
    
    Do While n <= UBound(gSpeechCallSigns) And Not Found
        If ResourceType = gSpeechCallSigns(n, 0) Then
            Found = True
            TranslateRadioCallSign = gSpeechCallSigns(n, 1)
        Else
            n = n + 1
        End If
    Loop

End Function

Private Function TranslateRadioUnitNumber(xUnitRecord As Vehicle) As String
    Dim n As Integer
    Dim T As Integer
    Dim aNamePart(0 To 15) As String
    Dim aPartType(0 To 15) As String
    Dim SpeachString As String
    Dim nLength As Integer
    Dim cLastType As String
    Dim cThisType As String
    Dim cThisByte As String
    
    On Error GoTo TranslateRadioUnitNumber_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function TranslateRadioUnitNumber")
    End If
    
    '
    '   i want to break the unit name into strings of numbers and letters and then translate the number and letter strings separately
    '   example:    M403 = medic four oh three (medic because of the resource type)
    '               23S2 = twenty-three sierra two
    '
    
    SpeachString = TranslateRadioCallSign(xUnitRecord.ResourceType)
    
    nLength = Len(xUnitRecord.UnitName)
    
    cLastType = ""
    T = -1
    For n = 1 To nLength
        cThisByte = Mid(xUnitRecord.UnitName, n, 1)
        If bsiIsDigit(cThisByte) Then
            cThisType = "D"
        Else
            cThisType = "A"
        End If
                
        '   If n = 1 And cThisType = "D" Or n > 1 Then          '   ignore leading alpha in call sign (designates unit type which is handled with resource type call above)
        
            If cLastType <> cThisType Then
                T = T + 1
                cLastType = cThisType
            End If
            
            aNamePart(T) = aNamePart(T) & cThisByte
            aPartType(T) = cThisType
    
        '   End If
    Next n
    
    SpeachString = SpeachString & " " & CreateSpokenName(aNamePart, aPartType)
    
    TranslateRadioUnitNumber = SpeachString

    Exit Function
    
TranslateRadioUnitNumber_ERH:

    TranslateRadioUnitNumber = "Error"
    Call AddToDebugList("Error in TranslateRadioUnitNumber: Error No: " & Str(Err.Number) & ", " & Err.Description & " >> UnitNumber = " & xUnitRecord.UnitName, lstDebug)

End Function

Private Function CreateSpokenName(aNamePart() As String, aPartType() As String) As String
    Dim n As Integer
    Dim T As Integer
    Dim cSpeachString As String
    Dim aNumber(0 To 15) As String
    
    T = 0
    
    For n = 0 To UBound(aNamePart)
        If aPartType(n) = "D" Then              '   digits
            Select Case Len(aNamePart(n))
                Case 1, 2
                    aNumber(T) = aNamePart(n)
                    T = T + 1
                Case 3
                    aNumber(T) = Left(aNamePart(n), 1)
                    aNumber(T + 1) = Right(aNamePart(n), 2)
                    T = T + 2
                Case 4
                    aNumber(T) = Left(aNamePart(n), 2)
                    aNumber(T + 1) = Right(aNamePart(n), 2)
                    T = T + 2
                Case 5
                    aNumber(T) = Left(aNamePart(n), 2)
                    aNumber(T + 1) = Mid(aNamePart(n), 3, 1)
                    aNumber(T + 2) = Right(aNamePart(n), 2)
                    T = T + 3
            End Select
        ElseIf aPartType(n) = "A" Then          '   alpha
            If (n = 0 And Not gSkipLeadingAlphas) Or n > 0 Then
                aNumber(T) = PhoneticAlpha(aNamePart(n))
                T = T + 1
            End If
        Else                                    '   huh?!?
        
        End If
        
    Next n
    
    n = 0
    
    Do While n <= UBound(aNumber)
        If Left(aNumber(n), 1) = "0" Then
            cSpeachString = cSpeachString & Replace(aNumber(n), "0", "Oh ") & " "
        Else
            cSpeachString = cSpeachString & aNumber(n) & " "
        End If
        
        n = n + 1
    Loop

    CreateSpokenName = Trim(cSpeachString)

End Function

Private Function PhoneticAlpha(cNamePart As String) As String
    Dim n As Integer
    Dim cThisOne As String
    Dim cNameString As String
    
    PhoneticAlpha = ""
    
    For n = 1 To Len(cNamePart)
        cThisOne = Mid(cNamePart, n, 1)
        
        Select Case UCase(cThisOne)
            Case "A"
                cNameString = "Alpha"
            Case "B"
                cNameString = "Bravo"
            Case "C"
                cNameString = "Charlie"
            Case "D"
                cNameString = "Delta"
            Case "E"
                cNameString = "Echo"
            Case "F"
                cNameString = "Foxtrot"
            Case "G"
                cNameString = "Golf"
            Case "H"
                cNameString = "Hotel"
            Case "I"
                cNameString = "India"
            Case "J"
                cNameString = "Juliette"
            Case "K"
                cNameString = "Kilo"
            Case "L"
                cNameString = "Lima"
            Case "M"
                cNameString = "Mike"
            Case "N"
                cNameString = "November"
            Case "O"
                cNameString = "Oscar"
            Case "P"
                cNameString = "Papa"
            Case "Q"
                cNameString = "Quebec"
            Case "R"
                cNameString = "Romeo"
            Case "S"
                cNameString = "Sierra"
            Case "T"
                cNameString = "Tango"
            Case "U"
                cNameString = "Uniform"
            Case "V"
                cNameString = "Victor"
            Case "W"
                cNameString = "Whiskey"
            Case "X"
                cNameString = "X ray"
            Case "Y"
                cNameString = "Yankee"
            Case "Z"
                cNameString = "Zulu"
        End Select
        
        PhoneticAlpha = PhoneticAlpha & cNameString & " "
        
    Next n
End Function

Private Sub ChangeRadioChannel(UnitRecord As Vehicle, Channel As String)
    Dim xItem As ListItem
    Dim n As Long
    Dim D As Long
    Dim atemp As Variant
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ChangeRadioChannel")
    End If
    
    D = 1
    
    Do While D <= lvEvents.ListItems.count
        Set xItem = lvEvents.ListItems.Item(D)
        If Val(xItem.SubItems(LVType)) = EV_RADIO Then
            If xItem.SubItems(LVUnit) = UnitRecord.UnitName Then
                atemp = Split(xItem.SubItems(LVMessage), "|")
                atemp(3) = Channel
                xItem.SubItems(LVMessage) = ""
                For n = 0 To UBound(atemp)
                    If xItem.SubItems(LVMessage) = "" Then
                        xItem.SubItems(LVMessage) = atemp(n)
                    Else
                        xItem.SubItems(LVMessage) = xItem.SubItems(LVMessage) & "|" & atemp(n)
                    End If
                Next n
            End If
        End If
        D = D + 1
    Loop
    
End Sub

Private Sub NewStatus(Vehicle_ID As Long)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim n As Long
    Dim Found As Boolean
    
    On Error GoTo ErrorHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub NewStatus")
    End If
    
    Found = False
    n = 0
    
    While Not Found
        If gUnitList(n).VehID = Vehicle_ID Then
            Found = True
        Else
            n = n + 1
        End If
    Wend
    
    AddToDebugList "[Status Update] " & gUnitList(n).UnitName, lstDebug
    
    cSQL = "Select Current_Lat, Current_Lon, Current_Location, Current_City, " _
                    & " Destination_Lat, Destination_Lon, Status_ID " _
                    & " from Vehicle " _
                    & " Where ID = " & Str(Vehicle_ID)
        
    nRecs = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    If nRecs > 0 Then
        If Not IsNull(aSQL(6, 0)) Then
            If gUnitList(n).Status <> aSQL(6, 0) Then              '   status has changed
                gUnitList(n).LastStatus = gUnitList(n).Status
            End If
        End If
        If Not IsNull(aSQL(0, 0)) Then
            If aSQL(0, 0) <> 0 Then
                gUnitList(n).CurrentLat = aSQL(0, 0) / 1000000 - gLatitudeAdjust
            Else
                Call AddToDebugList("[NewStatus] Zero Latitude! Discarding coordinates.", lstDebug)
            End If
        Else
            Call AddToDebugList("[NewStatus] NULL Latitude! Discarding coordinates.", lstDebug)
        End If
        If Not IsNull(aSQL(1, 0)) Then
            If aSQL(1, 0) <> 0 Then
                gUnitList(n).CurrentLon = aSQL(1, 0) / -1000000 - gLongitudeAdjust
            Else
                Call AddToDebugList("[NewStatus] Zero Longitude! Discarding coordinates.", lstDebug)
            End If
        Else
            Call AddToDebugList("[NewStatus] NULL Longitude! Discarding coordinates.", lstDebug)
        End If
        '    gUnitList(n).CurrentLat = aSQL(0, 0) / 1000000 - gLatitudeAdjust
        '    gUnitList(n).CurrentLon = aSQL(1, 0) / -1000000 - gLongitudeAdjust
        gUnitList(n).CurrentLoc = IIf(IsNull(aSQL(2, 0)), "", aSQL(2, 0))
        gUnitList(n).CurrentCity = IIf(IsNull(aSQL(3, 0)), "", aSQL(3, 0))
        gUnitList(n).DestinationLat = IIf(IsNull(aSQL(4, 0)), 0, aSQL(4, 0) / 1000000) - gLatitudeAdjust
        gUnitList(n).DestinationLon = IIf(IsNull(aSQL(5, 0)), 0, aSQL(5, 0) / -1000000) - gLongitudeAdjust
        gUnitList(n).Status = IIf(IsNull(aSQL(6, 0)), 0, aSQL(6, 0))
        
        Call RefreshUnitQueueRow(gUnitList(n))
        
    Else
        AddToDebugList "[ERROR] Record Not Found updating Vehicle " & gUnitList(n).UnitName & ", Vehicle_ID=" & Str(Vehicle_ID) & " cSQL=" & cSQL, lstDebug
    End If
    
    Exit Sub
    
ErrorHandler:

    AddToDebugList "[ERROR] in NewStatus: Unit=" & gUnitList(n).UnitName & ", Vehicle_ID=" & Str(Vehicle_ID) & " cSQL=" & cSQL, lstDebug
    
End Sub

Private Sub AtStationStatus(nUnitID As Long)
    Dim aIDs As Variant
    Dim nUnit As Long
    Dim CurrentStatus As Integer
    Dim StatusChangeTime As Variant
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub AtStationStatus")
    End If
    
    Call PurgeEvents(nUnitID)                   '   delete events, just in case there are some left over

    Call RefreshUnitInfo(nUnitID)               '   update the unit's info

End Sub

Private Sub IncidentCancelled(IncidentID As String)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim n As Integer
    
    '   This procedure is only required in pre-1.10 versions of Command because an XML unit status message is not sent when it should be
    '   We will send units back to station when their incident gets cancelled
    '   but we need to make sure that the units have not been assigned to any subsequent incidents
    
    On Error GoTo ErrHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub IncidentCancelled")
    End If
    
    If Not gb110Version Then            '   don't need this test for 1.10 or higher
                                        '   because TriTech fixed their bug (i.e. no XML unit status msg on incident cancellation)
    
        cSQL = "Select rv.ID, v.ID, v.status_ID, s.description, v.master_incident_ID from Response_Vehicles_Assigned rv " _
                & " join vehicle v on rv.vehicle_ID = v.ID " _
                & " join status s on v.status_ID = s.ID " _
            & " where rv.Master_Incident_ID = " & IncidentID _
            & " and (v.Master_Incident_ID = " & IncidentID & " or v.Master_Incident_ID IS NULL) " _
            & " and rv.Time_Call_Cleared is NOT NULL"
            
        nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
        
        If nRows > 0 Then
            For n = 0 To nRows - 1
                If aSQL(2, n) = STAT_Avail Then
                    If gReturnToBase Then
                        Call ReturnToStation(Val(aSQL(1, n)))
                    Else
                        Call AwaitInstructionsFromDispatch(Val(aSQL(1, n)))
                    End If
                Else
                    Call AddToDebugList("[INFO] in IncidentCancelled - Unit ID" & Str(aSQL(1, n)) & " is in Status " & aSQL(3, n), lstDebug)
                End If
            Next n
        End If
    
    End If
    
    Exit Sub
    
ErrHandler:

    Call AddToDebugList("[ERROR] in IncidentCancelled - Err #" & Str(Err.Number) & " - " & Err.Description, lstDebug)

End Sub

Private Sub UnitCancelled(Message As String)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim n As Integer
    Dim aMessage As Variant
    
    '   This procedure is only required in pre-1.10 versions of Command because an XML unit status message is not sent when it should be
    '   We will send units back to station when their incident gets cancelled, but no other time. Later versions of Command send the XML
    '   message just like with other status changes.
    
    On Error GoTo ErrHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub UnitCancelled")
    End If
    
    If Not gb110Version Then            '   don't need this test for 1.10 or higher
                                        '   because TriTech fixed their bug (i.e. no XML unit status msg on incident cancellation)
        aMessage = Split(Message, ";")
        
        If aMessage(1) <> "0" Then      '   When no incident exists, skip this because it's not the situation we're trying to handle.
        
            cSQL = "Select * from Vehicle where ID = " & aMessage(0) & " and Master_Incident_ID = " & aMessage(1)
            
            '   We only want the circumstance where the unit and incident are no longer associated in the Vehicle table.
            '   This only occurs immediately following a cancellation.
            
            nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
            
            If nRows = 0 Then               '   if the unit ID and the Master_Incident_ID are no longer associated in Vehicle,
                                            '   then the unit has been cancelled from the incident, so return to station
                If gReturnToBase Then
                    Call ReturnToStation(Val(aMessage(0)))
                Else
                    Call AwaitInstructionsFromDispatch(Val(aMessage(0)))
                End If
            
            End If
    
        End If
    
    End If
    
    Exit Sub
    
ErrHandler:

    Call AddToDebugList("[ERROR] in UnitCancelled - Message: " & Message & "  >>  Err #" & Str(Err.Number) & " - " & Err.Description, lstDebug)

End Sub

Private Sub UnitOutOfService(nUnitID As Long)
    Dim n As Integer
    Dim Message As String
    Dim Found As Boolean
    
    On Error GoTo UnitOutOfService_ErrHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub UnitOutOfService")
    End If
    
    Message = "Start of UnitOutOfService"
    
    Found = False
    n = 0
    
    n = RefreshUnitInfo(nUnitID)

    If gUnitList(n).Status = STAT_OOS Then

        Call AddToDebugList("[Status Update] in module UnitOutOfService (Out of Service) " & gUnitList(n).UnitName, lstDebug)
        
        Message = "UnitOutOfService - Purging Events"
        
        Call PurgeEvents(nUnitID)
        
        Message = "UnitOutOfService - Inserting Radio Message"
        
        Call InsertRadioMessage(gUnitList(n), STAT_OOS, Now, gUnitList(n).CurrentLoc)
        
    Else
    
        Call AddToDebugList("[INFO] in module UnitOutOfService - Unit: " & gUnitList(n).UnitName & " (ID=" & nUnitID & ") actually in Status " & gUnitList(n).Status, lstDebug)
        
    End If
    
    Exit Sub
    
UnitOutOfService_ErrHandler:

    Call AddToDebugList("[ERROR] in UnitOutOfService - Message: " & Message & "  >>  Err #" & Str(Err.Number) & " - " & Err.Description, lstDebug)

End Sub

Private Sub LoadScript()
    Dim Buffer As String
    Dim aHeader As Variant
    Dim aEvent As Variant
    Dim Name As String
    Dim StartTime As Variant
    Dim EndTime As Variant
    Dim Division As String
    Dim Notes As String
    Dim lSync As Boolean
    Dim lHist As Boolean
    Dim IDList As String
    Dim nIDCount As Long

    ' get header record first
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub LoadScript")
    End If
    
    Line Input #gScript, Buffer

    aHeader = Split(Buffer, "|")

    Name = aHeader(0)
    StartTime = CDate(aHeader(1))
    EndTime = CDate(aHeader(2))
    Division = aHeader(3)
    Notes = aHeader(4)
'    If UBound(aHeader) > 4 Then
'        lSync = (aHeader(5) = "Y")
'        lHist = (aHeader(6) = "Y")
'    Else
'        lSync = False
'        lHist = False
'    End If
    
    Call LoadScenarioContext(StartTime, EndTime, Division, gLoadContext, gLoadHistory, gCompression)
    
    IDList = ""
    nIDCount = 0
    
    '   Then get all the event records.
    
    Do While Not EOF(gScript)
    
        Line Input #gScript, Buffer
    
        aEvent = Split("|" + Buffer, "|")
        
        If aEvent(K_FLX_ID) = "SOUND" Then
        
            Call InsertSoundByteEvent(StartTime, aEvent(K_FLX_DateTime), aEvent(K_FLX_Address), aEvent(K_FLX_City), gCompression)
        
        Else
        
            Call InsertIncEvents(Val(aEvent(K_FLX_ID)), StartTime, aEvent(K_FLX_DateTime), aEvent(K_FLX_Address), aEvent(K_FLX_City), aEvent(K_FLX_Quad), gCompression, True)
            IDList = IDList & aEvent(K_FLX_ID) & ","
            nIDCount = nIDCount + 1
        
        End If
    
    Loop
    
    IDList = Left(IDList, Len(IDList) - 1)

    Exit Sub
    
ERH:
    Call AddToDebugList("ERROR: LoadScript", lstDebug)
    Call AddToDebugList("  ERR:" & Str(Err.Number) & "   DESC: " & Err.Description, lstDebug)
End Sub

Private Sub LoadScenarioContext(StartTime As Variant, StopTime As Variant, Division As String, Sync As Boolean, Hist As Boolean, Compress As Double)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cShift As String
    Dim nStart As Integer
    Dim nLen As Integer
    Dim cDiv As String
    Dim nDiv As String
    Dim cTemp As String
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub LoadScenarioContext")
    End If
    
    '    gUseSourceDest = IIf(UCase(bsiGetSettings("AVL", "UseSourceDest", "false", App.Path & "\Simulator.INI")) = "TRUE", True, False)
    
    If Hist Then
        Call PurgeActivityLog                               '   Clean out all of today's activity in preparation for the scenario
    End If
    
    txtRadio.Text = "Scenario Startup: Initialization in progress"
    
    If Sync Then
        Call SetUnitsOffDuty(1)
        Call CreateUnitLogOnEvents(1, StartTime, StopTime)
    End If
    
    If Hist Then
        Call LoadIncidentHistory(StartTime, StopTime)       '   load internal incident history matrix
        Call InsertContextEvents(StartTime)                 '   load incidents into history
    Else
        If gUseSourceDest Then
            Call LoadIncidentHistory(StartTime, StopTime)
        End If
    End If
    
    Exit Sub
    
ERH:
    
    Call AddToDebugList("ERROR: LoadScenarioContext - StartTime=" & Format(StartTime, "mmm dd, yyyy hh:nn:ss") & " Division=" & Division & " Sync=" & IIf(Sync, "TRUE", "FALSE") & " Hist=" & IIf(Sync, "TRUE", "FALSE"), lstDebug)
    Call AddToDebugList("  ERR: " & Str(Err.Number) & " Desc: " & Err.Description & " Source: " & Err.Source, lstDebug)

End Sub

Private Sub PurgeActivityLog()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long

    On Error GoTo ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub PurgeActivityLog")
    End If

    '   Clean out today's activity in preparation for the scenario

    cSQL = "Delete From Activity_Log " _
            & " Where Date_Time between '" & Format(Now, "mmm dd, yyyy") & "' " _
            & " and '" & Format(DateAdd("D", 1, Now), "mmm dd, yyyy") & "' "

    nRows = bsiSQLExecute(cSQL, gConnectString)
    
    Call AddToDebugList("PurgeActivityLog: " & Str(nRows) & " rows purged from Activity_Log", lstDebug)
    
    Exit Sub
    
ERH:

    Call AddToDebugList("ERROR: PurgeActivityLog - StartTime=" & Format(Now, "mmm dd, yyyy hh:nn:ss") & " StopTime=" & Format(Now, "mmm dd, yyyy hh:nn:ss"), lstDebug)
    Call AddToDebugList("  ERR: " & Str(Err.Number) & " Desc: " & Err.Description & " Source: " & Err.Source, lstDebug)

End Sub

Private Function SetUnitsOffDuty(nAgencyID As Integer) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim nDivisionID As Long
    Dim n As Integer
    Dim xItem As ListItem
    
    On Error GoTo ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function SetUnitsOffDuty")
    End If

    '   Set all units off duty in preparation for scenario
    
    SetUnitsOffDuty = False
    
'    cSQL = "Select ID from Vehicle "
'
'    '   cSQL = "Select ID from Vehicle where AgencyTypeID = " & Str(nAgencyID)
'
'    nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
'
''    cSQL = "Update Vehicle " _
''            & " Set Status_ID = " & Str(STAT_OffDuty) _
''            & " Where AgencyTypeID = " & Str(nAgencyID) _
''            & "   and Master_Incident_ID is NULL "
'
'    cSQL = "Update Vehicle " _
'            & " Set Status_ID = " & Str(STAT_OffDuty) _
'            & " Where Master_Incident_ID is NULL "
'
'    nRows = bsiSQLExecute(cSQL, gConnectString)
'
'    If nRows > 0 Then
'        SetUnitsOffDuty = True
'        For n = 0 To nRows - 1
'            Call SendToCADNoWait(NP_READ_VEHICLE, Str(aSQL(0, n)))       '   Got to make sure folks know
'        Next n
'    End If
'
'    Call AddToDebugList("SetUnitsOffDuty: " & Str(nRows) & " Units set OffDuty")

    Set xItem = lvEvents.ListItems.Add(, , Format(gScriptTime, "hh:nn:ss"))
    xItem.SubItems(LVUnit) = ""
    xItem.SubItems(LVType) = Format(EV_INIT_OFF, "0000")
    xItem.SubItems(LVDur) = "LOGOFF"
    xItem.SubItems(LVDist) = "LOGOFF"
    xItem.SubItems(LVMessage) = ""
    xItem.SubItems(LVSort) = Format(gScriptTime, DATE_FORMAT)
    
    SetUnitsOffDuty = True
    
    Exit Function
    
ERH:

    Call AddToDebugList("ERROR: SetUnitsOffDuty - Agency ID = " & Str(nAgencyID), lstDebug)
    Call AddToDebugList("  ERR: " & Str(Err.Number) & " Desc: " & Err.Description & " Source: " & Err.Source, lstDebug)

End Function

Private Sub SetUnitToAvailableStatus(nUnitID As Long)
    Dim xItem As ListItem
    Dim xUnit As Vehicle
    Dim cMessageBody As String
    
    On Error GoTo SetUnitToAvailableStatus_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetUnitToAvailableStatus")
    End If
    
    xUnit = GetUnitRecord(nUnitID)
    
    cMessageBody = Trim(Str(xUnit.VehID))

    Set xItem = lvEvents.ListItems.Add(, , Format(Now, "hh:nn:ss"))
    xItem.SubItems(LVUnit) = xUnit.UnitName
    xItem.SubItems(LVType) = Format(EV_LOADTEST_AVAIL, "0000")
    xItem.SubItems(LVDur) = "LOADSTAT"
    xItem.SubItems(LVDist) = "LOADSTAT"
    xItem.SubItems(LVMessage) = cMessageBody
    xItem.SubItems(LVSort) = Format(Now, DATE_FORMAT)
        
    If gDetailedDebugging Then
        Call AddToDebugList("[INFO] SetUnitToAvailableStatus - List Length = " & lvEvents.ListItems.count)
    End If
    
    Exit Sub
    
SetUnitToAvailableStatus_ERH:
    Call AddToDebugList("[ERROR] in SetUnitToAvailableStatus of frmMain: (" & Err.Number & ") " & Err.Description & " > " & cMessageBody)
End Sub

Private Sub SetUnitAssignToIncident(nUnitID As Long, nUnitLat As Double, nUnitLon As Double)            '   added Lat/Lon of unit so we can select an incident within the set proximity range
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Integer
    Dim n As Long
    Dim cSelectedInc As String
    Dim xItem As ListItem
    Dim cMessageBody As String
    Dim cError As String
    Dim cUnit As String
    Dim aUnits() As String
    Dim nIncLat As Double
    Dim nIncLon As Double
    Dim nDistance As Double
    Dim cIncidents As String
    Dim aQualifyingIncs() As String
    Dim Successful As Boolean
    Dim TriedOnce As Boolean
    
    Static scUnitQueue As String
    Static scUsedIncidents As String
    
    On Error GoTo SetUnitAssignToIncident_ERH
    
    cError = "Parameters = " & CStr(nUnitID) & ", " & CStr(nUnitLat) & ", " & CStr(nUnitLon) & ", scUnitQueue >" & scUnitQueue & "<"
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetUnitAssignToIncident")
    End If
    
    If nUnitID = 0 Then                         '   if we haven't been given a specific unit, so this is the result of an incident creation
        If scUnitQueue <> "" Then               '   if we have any units waiting (we should, after all)
            Call AddToDebugList("[INFO] in SetUnitAssignToIncident of frmMain: " & cError & " responding to incident creation.")
            aUnits = Split(scUnitQueue, ";")
            nUnitID = Val(aUnits(0))                    '   so get the first waiting unit from the static list
            If UBound(aUnits, 1) > 0 Then               '   there are remaining units to assign
                scUnitQueue = aUnits(1)                 '   bump the queue up by one
                For n = 2 To UBound(aUnits, 1)
                    scUnitQueue = scUnitQueue & ";" & aUnits(n)
                Next n
            Else
                scUnitQueue = ""                        '   we are done! All units assigned, so set the queue to empty.
            End If
            If Not IsUnitAssigned(nUnitID) Then   '   make sure we only use unassigned units
                nUnitLat = GetUnitRecord(nUnitID).CurrentLat
                nUnitLon = GetUnitRecord(nUnitID).CurrentLon
            Else
                Call AddToDebugList("[DEBUG] in SetUnitAssignToIncident of frmMain: Unit " & GetUnitRecord(nUnitID).UnitName & " is in pending units list but was assigned to an incident. Skipped.")
                nUnitLat = 0
                nUnitLon = 0
            End If
        Else
            Call AddToDebugList("[ERROR] in SetUnitAssignToIncident of frmMain: " & cError & " Incident created with no unit waiting for assignment.")
            
            Exit Sub            '   nothing to do, so exit now
            
        End If
    Else
        Call UpdateLoadTestUnitList(GetUnitRecord(nUnitID).UnitName)
    End If
    
    cSQL = "SELECT ID, Latitude, Longitude FROM Response_Master_Incident WHERE WhichQueue='W'"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then                   '   if there are waiting incidents, then we'll see if we can assign one of them
        Successful = False
        TriedOnce = False
        While Not Successful
            For n = 0 To UBound(aSQL, 2)
        
                '   check to see that we have not already flagged this incident for assign AND that this incident is within the radius limit for assignment in a load test
                
                If InStr(1, scUsedIncidents, aSQL(0, n)) = 0 _
                        And DistanceDeg(nUnitLat, nUnitLon, CDbl(aSQL(1, n)) / 1000000 - gLatitudeAdjust, CDbl(aSQL(2, n)) / -1000000 - gLongitudeAdjust, "KM") <= CDbl(txtLoadRadius.Text) * IIf(TriedOnce, 3, 1) Then
                    cSelectedInc = aSQL(0, n)
                    Successful = True
                    Exit For
                End If
            Next n
            If Not Successful And Not TriedOnce Then
                Call AddToDebugList("[DEBUG] in SetUnitAssignToIncident of frmMain: Expanding reach to accommodate out of range Unit " & GetUnitRecord(nUnitID).UnitName & ".")
                TriedOnce = True
            ElseIf TriedOnce Then
                Successful = True
            End If
        Wend
    End If
    
    If cSelectedInc <> "" Then                  '   we found a qualifying waiting incident, so we will use that one
        
        scUsedIncidents = scUsedIncidents & cSelectedInc & ";"            '   flag it so we don't try to use it again next time through (before the assignment get's completed)
        
        cMessageBody = Trim(cSelectedInc) & ";" & Trim(Str(nUnitID))
    
        Set xItem = lvEvents.ListItems.Add(, , Format(Now, "hh:nn:ss"))
        xItem.SubItems(LVUnit) = GetUnitRecord(nUnitID).UnitName
        xItem.SubItems(LVType) = Format(EV_LOADTEST_ASSIGN, "0000")
        xItem.SubItems(LVDur) = "LOADSTAT"
        xItem.SubItems(LVDist) = "LOADSTAT"
        xItem.SubItems(LVMessage) = cMessageBody
        xItem.SubItems(LVSort) = Format(Now, DATE_FORMAT)
        
        If nRows > 1 Then       '   there was more than one waiting incident when we ran the query, so we should call this routine right away to assign the next incident
            '   Debug.Assert False
            '   call SetUnitAssignToIncident(0, 0, 0)
        End If
    
    Else                                '   otherwise, we'll need to create one (which will trigger an assignment when the creation is completed)
    
        If nUnitID <> 0 Then
            If scUnitQueue <> "" Then
                scUnitQueue = scUnitQueue & ";" & Trim(Str(nUnitID))        '   no incident to assign, so save the unit id until next time when there is a waiting inc.
            Else
                scUnitQueue = Trim(Str(nUnitID))                            '   no incident to assign, so save the unit id until next time when there is a waiting inc.
            End If
        End If
    
        Successful = False
        TriedOnce = False
        
        While Not Successful
            For n = 0 To UBound(gLoadTestIncident, 1)
                
                nDistance = DistanceDeg(nUnitLat, nUnitLon, gLoadTestIncident(n).Latitude, gLoadTestIncident(n).Longitude, "KM")
                
                If nDistance <= CDbl(txtLoadRadius.Text) * IIf(TriedOnce, 3, 1) Then                  '   create a delimited list of qualifying incidents
                    If cIncidents <> "" Then cIncidents = cIncidents & ";"
                    cIncidents = cIncidents & CStr(gLoadTestIncident(n).RMIID)
                End If
            Next n
            If cIncidents <> "" And TriedOnce Then
                Successful = True
            Else
                '   we can't find a qualifying incident for this unit, so get incidents from around this unit
                If GetLoadTestIncidentsForUnit(nUnitLat, nUnitLon, CDbl(txtLoadRadius.Text) * 1000) > 0 Then
                    '   found some!
                    TriedOnce = True
                    Call AddToDebugList("[INFO] Found qualifying incidents and added them for test unit " & GetUnitRecord(nUnitID).UnitName)
                Else
                    Successful = True
                    Call AddToDebugList("[ERROR] No qualifying incidents found for test unit " & GetUnitRecord(nUnitID).UnitName & " at configured radius of " & txtLoadRadius.Text & "KMs")
                    '   at this point, this unit should drop from those included in the test
                End If
            End If
        Wend
        
        If cIncidents <> "" Then
            aQualifyingIncs = Split(cIncidents, ";")                                    '   split the qualifying list into an array
            cSelectedInc = aQualifyingIncs(Int(Rnd * UBound(aQualifyingIncs)))          '   select a qualifying incident at random
            
            Call InsertIncEvents(Val(cSelectedInc), Now, Now)                           '   and send it to queue
        Else                                                                            '   we didn't find a qualifying incident
            '   the code should NEVER get here.
        End If
        
    End If
    
    If gDetailedDebugging Then
        Call AddToDebugList("[INFO] SetUnitAssignToIncident - List Length = " & lvEvents.ListItems.count)
    End If
    
    Exit Sub
    
SetUnitAssignToIncident_ERH:
    Call AddToDebugList("[ERROR] in SetUnitAssignToIncident of frmMain: (" & Err.Number & ") " & Err.Description & " > " & cError)
End Sub

Private Function IsUnitAssigned(nUnit As Long) As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long
    Dim cError As String
    
    On Error GoTo IsUnitAssigned_ERH
    
    IsUnitAssigned = False          '   Assume unit is not assigned
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function IsUnitAssigned")
    End If

    cSQL = "Select Master_Incident_ID from Vehicle WHERE ID = " & Str(nUnit)

    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, , , cError)
    
    If nRows > 0 Then
        For n = 0 To UBound(aSQL, 1)
            If CLng(IIf(IsNull(aSQL(0, n)), 0, aSQL(0, n))) > 0 Then
                '   non-zero Master_Incident_ID, unit is assigned
                IsUnitAssigned = True
                Exit For
            End If
        Next n
    Else
        '   no rows, so unit is not assigned
    End If

    Exit Function
    
IsUnitAssigned_ERH:
    Call AddToDebugList("[ERROR] in IsUnitAssigned of frmMain: (" & Err.Number & ") " & Err.Description & " > " & cError)
End Function

Private Sub UpdateLoadTestUnitList(cUnitName)
    Dim n As Long
    Dim Test As Boolean
    Dim Buffer As String
    Dim aBuffer() As String
    Dim cBuffer As String
    Dim bFound As Boolean
    
    On Error GoTo UpdateLoadTestUnitList_ERH
    
    bFound = False
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function UpdateLoadTestUnitList")
    End If
    
    Buffer = bsiGetSettings("LOADTEST", "SELECTEDUNITS", "", gSystemDirectory & "\Simulator.INI")
    aBuffer = Split(Buffer, ";")
    cBuffer = ""
    For n = 0 To UBound(aBuffer)
        If cBuffer <> "" Then
            cBuffer = cBuffer & ";" & aBuffer(n)
        Else
            cBuffer = aBuffer(n)
        End If
        
        If aBuffer(n) = cUnitName Then
            bFound = True
            Exit For
        End If
        
    Next n
    
    If Not bFound Then
        cBuffer = cBuffer & ";" & cUnitName
        Test = bsiPutSettings("LOADTEST", "SELECTEDUNITS", cBuffer, gSystemDirectory & "\Simulator.INI")
        lvUnitList.ListItems.Item("K" & cUnitName).Checked = True
    End If
    
    Exit Sub
    
UpdateLoadTestUnitList_ERH:
End Sub

Private Sub SetAssignedToPostStatus(nUnitID As Long)
    Dim xItem As ListItem
    Dim xUnit As Vehicle
    Dim cSQL As String
    Dim aSQL As Variant
    Dim Latitude As Double
    Dim Longitude As Double
    Dim cLoc As String
    Dim nRows As Long
    Dim cMessageBody As String
    
    On Error GoTo SetAssignedToPostStatus_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SetAssignedToPostStatus")
    End If
    
'    .ColumnHeaders.Add 1, , "Time", 1000
'    .ColumnHeaders.Add 2, , "Vehicle", 1000
'    .ColumnHeaders.Add 3, , "Type", 800
'    .ColumnHeaders.Add 4, , "Interval", 1200
'    .ColumnHeaders.Add 5, , "Distance", 1200
'    .ColumnHeaders.Add 6, , "Message Body", 30000

'
'   cMessageBody = nUnitID
    
    cMessageBody = Trim(Str(nUnitID))
    
    xUnit = GetUnitRecord(nUnitID)

    Set xItem = lvEvents.ListItems.Add(, , Format(Now, "hh:nn:ss"))     '   add one second to status change event to allow for AVL processing queue to catch up
    xItem.SubItems(LVUnit) = xUnit.UnitName
    xItem.SubItems(LVType) = Format(EV_LOADTEST_POST, "0000")
    xItem.SubItems(LVDur) = "LOADTEST"
    xItem.SubItems(LVDist) = "LOADTEST"
    xItem.SubItems(LVMessage) = cMessageBody
    xItem.SubItems(LVSort) = Format(Now, DATE_FORMAT)
        
    If gDetailedDebugging Then
        Call AddToDebugList("[INFO] SetAssignedToPostStatus - List Length = " & lvEvents.ListItems.count)
    End If
    
    Exit Sub
    
SetAssignedToPostStatus_ERH:
    Call AddToDebugList("[ERROR] in SetAssignedToPostStatus in frmMain: (" & Err.Number & ") " & Err.Description & " > " & cMessageBody)
End Sub

Private Function CreateUnitLogOnEvents(nAgencyID As Integer, ScriptStartDate As Variant, ScriptStopDate As Variant) As Long
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim cMessage As String
    Dim TimeTag As String
    Dim LogOnTime As Variant
    Dim OrigLogOnTime As Variant
    Dim cUnit As String
    Dim n As Integer
    Dim nDateOffSet As Double
    Dim StartTime As Variant
    
    On Error GoTo ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function CreateUnitLogOnEvents")
    End If

    '   Create Logon Events for the Units in the scenario
    
    CreateUnitLogOnEvents = False
    
    If hour(ScriptStartDate) >= 7 And hour(ScriptStartDate) < 19 Then
        TimeTag = " 00:00:00"
        StartTime = CDate(Format(ScriptStartDate, "MMM DD YYYY") & TimeTag)
    Else
        TimeTag = " 12:00:00"
        If hour(ScriptStartDate) < 7 Then
            StartTime = CDate(Format(DateAdd("D", -1, CDate(ScriptStartDate)), "MMM DD YYYY" & TimeTag))
        Else
            StartTime = CDate(Format(ScriptStartDate, "MMM DD YYYY") & TimeTag)
        End If
    End If
    
    cSQL = "Select Date_Time, Location, Latitude, Longitude, Radio_Name, Vehicle_ID " _
            & " From Activity_Log " _
            & " Where Activity = 'Start_Shift_Record' " _
            & " and Agency_ID = " & Str(nAgencyID) _
            & " and Date_Time between '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "' " _
            & " and '" & Format(ScriptStopDate, "MMM DD YYYY HH:NN:SS") & "' "

    nRows = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    If nRows > 0 Then
        For n = 0 To nRows - 1
        
            OrigLogOnTime = CDate(aSQL(0, n))
            
            '   we must move all logons forward to occur at the same times within each simulation
            '   so that all units will appear in the system at the same time in each simulation
            
            If OrigLogOnTime < ScriptStartDate Then                             '   unit logged on before the start of the simulation (so logon now)
                LogOnTime = CDate(Format(Now, "MMM DD YYYY ") & Format(OrigLogOnTime, "HH:NN:SS"))      '   use original start time

                If LogOnTime > gScriptTime Then         '   the simulation is running earlier than the original script times,
                    LogOnTime = gScriptTime             '   so set the logon time to start of script
                End If                                  '   this makes sure that the fleet remains the same no matter when the script is run
            
                cUnit = aSQL(4, n)
                cMessage = Format(LogOnTime, "MMM DD YYYY HH:NN:SS") & "|" _
                            & aSQL(1, n) & "|" _
                            & aSQL(2, n) & "|" _
                            & aSQL(3, n) & "|" _
                            & aSQL(4, n) & "|" _
                            & aSQL(5, n)
                
                Call InsertLogOnEvents(gScriptTime, cUnit, cMessage)
            
            ElseIf OrigLogOnTime <= ScriptStopDate Then                         '   unit logged on during the simulation        (so adjust)
                LogOnTime = gScriptTime                                         '   move start time so that it occurs at the script time
                
                cUnit = aSQL(4, n)
                cMessage = Format(LogOnTime, "MMM DD YYYY HH:NN:SS") & "|" _
                            & aSQL(1, n) & "|" _
                            & aSQL(2, n) & "|" _
                            & aSQL(3, n) & "|" _
                            & aSQL(4, n) & "|" _
                            & aSQL(5, n)
                
                Call InsertLogOnEvents(LogOnTime, cUnit, cMessage)
            
            Else                                                                '   unit logged on after the simulation         (so ignore)
'                LogOnTime = CDate(Format(Now, "MMM DD YYYY ") & "HH:NN:SS"))      '   use original start time
'
'                If LogOnTime > gScriptTime Then         '   the simulation is running earlier than the original script times,
'                    LogOnTime = gScriptTime             '   so set the logon time to start of script
'                End If                                  '   this makes sure that the fleet remains the same no matter when the script is run
'
'                cUnit = aSQL(4, n)
'                cMessage = Format(LogOnTime, "MMM DD YYYY HH:NN:SS") & "|" _
'                            & aSQL(1, n) & "|" _
'                            & aSQL(2, n) & "|" _
'                            & aSQL(3, n) & "|" _
'                            & aSQL(4, n) & "|" _
'                            & aSQL(5, n)
'
'                Call InsertLogOnEvents(gScriptTime, cUnit, cMessage)
'
            End If
    
        Next n
    End If
    
    Exit Function
    
ERH:

    Call AddToDebugList("ERROR: CreateUnitLogOnEvents = " & cMessage, lstDebug)
    Call AddToDebugList("  ERR: " & Str(Err.Number) & " Desc: " & Err.Description & " Source: " & Err.Source, lstDebug)

End Function

Private Sub LoadIncidentHistory(StartTime As Variant, StopTime As Variant, Optional nNewIncID As Long)
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    Dim n As Long, o As Long
    Dim OldID As String
    Dim TempTime As Long
    Dim cMessage As String

    On Error GoTo ERRHNDLR

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub LoadIncidentHistory")
    End If

    cMessage = ""

    cSQL = "Select  rm.ID, rv.ID, rt.ID, " _
                & " rm.priority_description, " _
                & " rv.radio_name, " _
                & " re.description, " _
                & " rm.address, " _
                & " rt.Location_name, " _
                & " rv.time_assigned, " _
                & " rv.time_enroute, " _
                & " rv.time_ArrivedAtScene, " _
                & " rt.time_Depart_Scene, " _
                & " rt.time_arrive_destination, " _
                & " rv.time_delayed_availability, " _
                & " rv.time_call_cleared, " _
                & " rt.Address, " _
                & " rm.CreatedByPrescheduleModule, " _
                & " CAST(rt.Latitude as FLOAT) / 1000000, " _
                & " CAST(rt.Longitude as FLOAT) / -1000000 " _
        & " from    Response_master_incident rm left outer join " _
                & " Response_Vehicles_assigned rv on rv.master_incident_id = rm.id left outer join " _
                & " Response_Transports rt on rt.vehicle_assigned_id = rv.id left outer join " _
                & " Vehicle ve on ve.ID = rv.Vehicle_ID left outer join " _
                & " Resource re on ve.PrimaryResource_ID = re.ID "
                
    If nNewIncID = 0 Then
        cSQL = cSQL & " Where rm.Time_CallEnteredQueue <= '" & Format(StopTime, "MMM DD YYYY HH:NN:SS") & "' " _
                    & " and rm.Time_CallClosed > '" & Format(StartTime, "MMM DD YYYY HH:NN:SS") & "' "
    Else
        cSQL = cSQL & " Where rm.id = " & Str(nNewIncID)
    End If
    
    cSQL = cSQL & " order by rm.id, rv.time_assigned "

    nRows = bsiSQLGet(cSQL, aSQL, gConnectString)

    If nRows > 0 Then
    
        Call AddToDebugList("LoadIncidentHistory: Loading info on" & Str(nRows) & " Incident Records for RMI.ID=" & IIf(nNewIncID = 0, " Script", Str(nNewIncID)), lstDebug)

        cMessage = "REDIM gOldIncInfo"

        If nNewIncID = 0 Then
            ReDim gOldIncInfo(0 To nRows - 1)                               '   initialization of a script
            n = 0
            o = 0
        Else
            If gOldIncInfo(0).OldID = 0 Then
                ReDim gOldIncInfo(0 To nRows - 1)
            Else
                ReDim Preserve gOldIncInfo(0 To UBound(gOldIncInfo) + 1)        '   expanding the list to add a new incident
            End If
            n = 0
            o = UBound(gOldIncInfo)
        End If

        'Private Type tOldIncInfo
        '    NewID As Long
        '    OldID As Long
        '    Transported As Integer
        '    TransportType As String
        '    Destination As String
        '    DestAddress As String
        '    Cancelled As Boolean
        '    ClearTime As Long
        '    OnSceneTime As Long                     '   onscene to depart scene/cancelled in seconds
        '    AtDestTime As Long                      '   At Destination to Available in seconds
        '    TimeCancelled As Long                   '   time assigned to time cancelled in seconds
        '    StatusCancelled As Integer              '   0, 3, 4 - status at cancellation (not cancelled, cancelled enroute, cancelled at scene)
        '    NumUnitsAssigned As Integer             '   number of units assigned
        '    FirstUnitOnScene As String              '   radio name of the first unit to arrive
        '    TimeFirstUnitAssigned As Variant        '   for use when more than one unit assigned
        '    TimeFirstUnitOnScene As Variant         '   for use when more than one unit assigned
        '    TimeFirstUnitDepartScene As Variant     '   for use when more than one unit assigned
        '    TimeFirstUnitAtDest As Variant          '   for use when more than one unit assigned
        '    PreScheduledIncident As Boolean         '   prescheduled incidents behave differently when selecting a destination
        '    DestLatitude As Double                  '   required for prescheduled incidents
        '    DestLongitude As Double                 '   required for prescheduled incidents
        'End Type
        
        Do While n < nRows
            OldID = aSQL(0, n)
            gOldIncInfo(o).OldID = OldID
            gOldIncInfo(o).Cancelled = True             '   assume cancelled first
            gOldIncInfo(o).ClearTime = 0                '   assume a long time first
            Do While aSQL(0, n) = OldID
            
                cMessage = "Load Destination Info. ID=" & Str(OldID)

                If Not IsNull(aSQL(7, n)) Then                                              '   there a destination
                    If aSQL(7, n) <> " " Then                                               '   there a destination
                        If gOldIncInfo(o).Destination = "" Then                             '   this is the first
                            gOldIncInfo(o).Destination = aSQL(7, n)                         '   save destination name
                            gOldIncInfo(o).DestAddress = aSQL(15, n)                        '   and address
                            gOldIncInfo(o).TransportType = aSQL(5, n)                       '   save transport type
                            gOldIncInfo(o).StatusCancelled = 0                              '   not cancelled
                            gOldIncInfo(o).Cancelled = False
                        End If
                        gOldIncInfo(o).Transported = gOldIncInfo(o).Transported + 1         '   save # transports
                        If Not IsNull(aSQL(10, n)) And Not IsNull(aSQL(11, n)) Then         '   onscene to depart scene
                            TempTime = DateDiff("S", aSQL(10, n), aSQL(11, n))              '   calc onscene time
                            If gOldIncInfo(o).OnSceneTime < TempTime Then                   '   save the longest onscene time
                                gOldIncInfo(o).OnSceneTime = TempTime
                            End If
                        End If
                    End If
                ElseIf IsNull(aSQL(7, n)) Or aSQL(7, n) <> " " Then                         '   no destination in this record
                    If gOldIncInfo(o).Transported > 0 Then                                  '   we already recorded a transport on this incident
                                                                                            '   do nothing
                    Else
                        If Not IsNull(aSQL(10, n)) And Not IsNull(aSQL(14, n)) Then         '   onscene to clear
                            TempTime = DateDiff("S", aSQL(10, n), aSQL(14, n))              '   calc onscene to clear time
                            If gOldIncInfo(o).Transported = 0 _
                                And gOldIncInfo(o).ClearTime < TempTime Then                '   no transports yet, so save the
                                gOldIncInfo(o).ClearTime = TempTime                         '   longest onscene to clear time
                            End If
                        ElseIf Not IsNull(aSQL(8, n)) And Not IsNull(aSQL(14, n)) Then      '   timeAssigned to Clear
                            TempTime = DateDiff("S", aSQL(8, n), aSQL(14, n))               '   calc assigned time to Avail
                            If gOldIncInfo(o).ClearTime < TempTime Then                     '   always save the shortest
                                gOldIncInfo(o).ClearTime = TempTime
                            End If
                        End If
                    End If
                End If

                cMessage = "Calc Dest Time. ID=" & Str(OldID)                               '   bug in here somewhere

                If Not IsNull(aSQL(12, n)) And Not IsNull(aSQL(13, n)) Then                 '   atDest to DelayedAvail
                    TempTime = DateDiff("S", aSQL(12, n), aSQL(13, n))                      '   calc Dest time to Delayed Avail
                    If gOldIncInfo(o).AtDestTime < TempTime Then                            '   always save the longest destination time
                        gOldIncInfo(o).AtDestTime = TempTime
                    End If
                ElseIf Not IsNull(aSQL(12, n)) And Not IsNull(aSQL(14, n)) Then             '   atDest to Clear
                    TempTime = DateDiff("S", aSQL(12, n), aSQL(14, n))                      '   calc Dest time to Avail
                    If gOldIncInfo(o).AtDestTime < TempTime Then                            '   always save the longest
                        gOldIncInfo(o).AtDestTime = TempTime
                    End If
                End If
                
                gOldIncInfo(o).PreScheduledIncident = IIf(IsNull(aSQL(16, n)), False, True)
                gOldIncInfo(o).DestLatitude = CDbl(aSQL(17, n))
                gOldIncInfo(o).DestLongitude = CDbl(aSQL(18, n))

                n = n + 1
                If n >= nRows Then
                    Exit Do
                End If
            Loop
            o = o + 1
        Loop
    
    Else
    
        Call AddToDebugList("ERROR: LoadIncidentHistory: Found" & Str(nRows) & " Incident Records for RMI.ID =" & IIf(nNewIncID = 0, " Script", Str(nNewIncID)), lstDebug)

    End If

    Exit Sub

ERRHNDLR:

    '   Call MsgBox("LoadUnitList Failed" & Chr(13) & "Check ERROR Logs" & Chr(13) & "Err: " & Str(Err.Number) & " " & Err.Description)
    Call AddToDebugList("ERROR: LoadIncidentHistory = " & Err.Description & " (" & Str(Err.Number) & ") - " & cMessage & vbCrLf & "    SQL=" & cSQL, lstDebug)

    Resume Next

End Sub

Private Sub LoadUnitList()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim n As Long
    Dim AccumLat As Double
    Dim AccumLon As Double
    Dim LatLonCount As Long
    
    Dim lWidth As Integer
        
'    Private Type Vehicle
'        UnitName As String
'        VehID As Long
'        AVLID As Long
'        MDTID As Long
'        RMIID As Long
'        AVLEnabled As Boolean
'        CurrentLat As Double
'        CurrentLon As Double
'        CurrentLoc As String
'        CurrentCity As String
'        DestinationCode As String
'        DestinationLat As Double
'        DestinationLon As Double
'        Status As Integer
'        MapLocation As MapPointCtl.Location
'        Route() As RtPoint
'        RouteSegments As Integer
'        QueueRow As Long
'        Station As String
'        StationLat As Double
'        StationLon As Double
'    End Type

    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub LoadUnitList")
    End If
    
        cSQL = "Select u.Code, v.ID, v.AVL_ID, v.AVLEnabled, " _
                        & " v.Current_Lat, v.Current_Lon, v.Current_Location, v.Current_City, " _
                        & " v.Destination_Lat, v.Destination_Lon, v.Status_ID, v.MST_MDT_ID, " _
                        & " s.Name, s.Lat, s.Lon, d.DivName, v.master_incident_id, " _
                        & " v.destination_location, v.destination_city, r.description " _
                        & " from Vehicle v Join Unit_Names u " _
                            & " on v.UnitName_ID = u.ID" _
                            & " join Stations s on v.Post_Station_ID = s.ID " _
                            & " join Division d on d.ID = v.CurrentDivision_ID " _
                            & " join Resource r on v.PrimaryResource_ID = r.ID " _
                        & " Where v.Status_ID <> " & Str(STAT_OffDuty) _
                        & " Order by u.Code"

'                            & " where not (v.Current_Lat is NULL or v.Current_Lon is NULL) " _
'                            & " and not (v.Current_Lat = 0 or v.Current_Lon = 0) " _
'                            & " Order by u.Code"

'                            & " Where v.ID = " & Str(nUnitID)
    
'    cSQL = "Select u.Code, v.ID, v.AVL_ID, v.AVLEnabled, " _
'                    & " v.Current_Lat, v.Current_Lon, v.Current_Location, v.Current_City, " _
'                    & " v.Destination_Lat, v.Destination_Lon, v.Status_ID, v.MST_MDT_ID, " _
'                    & " s.Name, s.Lat, s.Lon, d.DivName " _
'                    & " from Vehicle v Join Unit_Names u " _
'                        & " on v.UnitName_ID = u.ID" _
'                        & " join Stations s on v.Post_Station_ID = s.ID " _
'                        & " join Division d on d.ID = v.CurrentDivision_ID " _
'                        & " where not (v.Current_Lat is NULL or v.Current_Lon is NULL) " _
'                        & " and not (v.Current_Lat = 0 or v.Current_Lon = 0) " _
'                        & " Order by u.Code"
'
'                        & " Where v.Status_ID <> " & Str(STAT_OffDuty) _    '   removed from query - include all units
    
    nRecs = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    AddToDebugList "[Loading] " & Str(nRecs) & " Units Found", lstDebug
    
    If nRecs > 0 Then

        ReDim gUnitList(0 To nRecs)
        For n = 0 To nRecs - 1
            gUnitList(n).UnitName = aSQL(0, n)
            gUnitList(n).VehID = aSQL(1, n)
            gUnitList(n).AVLID = IIf(IsNull(aSQL(2, n)), -1, aSQL(2, n))
            gUnitList(n).AVLEnabled = aSQL(3, n)
            If Not IsNull(aSQL(4, n)) Then
                If aSQL(4, n) <> 0 Then
                    gUnitList(n).CurrentLat = aSQL(4, n) / 1000000 - gLatitudeAdjust
                Else
                    '   Call AddToDebugList("[LoadUnitList] Zero Latitude! Discarding coordinates.")
                End If
            Else
                '   Call AddToDebugList("[LoadUnitList] NULL Latitude! Discarding coordinates.")
            End If
            If Not IsNull(aSQL(5, n)) Then
                If aSQL(5, n) <> 0 Then
                    gUnitList(n).CurrentLon = aSQL(5, n) / -1000000 - gLongitudeAdjust
                Else
                    '   Call AddToDebugList("[LoadUnitList] Zero Longitude! Discarding coordinates.")
                End If
            Else
                '   Call AddToDebugList("[LoadUnitList] NULL Longitude! Discarding coordinates.")
            End If
            gUnitList(n).CurrentLoc = IIf(IsNull(aSQL(6, n)), "", aSQL(6, n))
            gUnitList(n).CurrentCity = IIf(IsNull(aSQL(7, n)), "", aSQL(7, n))
            gUnitList(n).DestinationLat = IIf(IsNull(aSQL(8, n)), -999, aSQL(8, n) / 1000000) - gLatitudeAdjust
            gUnitList(n).DestinationLon = IIf(IsNull(aSQL(9, n)), -999, aSQL(9, n) / -1000000) - gLongitudeAdjust
            gUnitList(n).Status = IIf(IsNull(aSQL(10, n)), 0, aSQL(10, n))
            gUnitList(n).MDTID = IIf(IsNull(aSQL(11, n)), -1, aSQL(11, n))
            gUnitList(n).Station = aSQL(12, n)
            gUnitList(n).StationLat = IIf(IsNull(aSQL(13, n)), gUnitList(n).CurrentLat, aSQL(13, n) / 1000000) - gLatitudeAdjust     '   if station lat/lon is unknown, use the unit's current location
            gUnitList(n).StationLon = IIf(IsNull(aSQL(14, n)), gUnitList(n).CurrentLon, aSQL(14, n) / -1000000) - gLongitudeAdjust   '   if station lat/lon is unknown, use the unit's current location
            gUnitList(n).CurrentDivision = IIf(IsNull(aSQL(15, n)), "", aSQL(15, n))
            gUnitList(n).RMIID = IIf(IsNull(aSQL(16, n)), -1, aSQL(16, n))
            gUnitList(n).DestinationLoc = IIf(IsNull(aSQL(17, n)), "", aSQL(17, n))
            gUnitList(n).DestinationCity = IIf(IsNull(aSQL(18, n)), "", aSQL(18, n))
            gUnitList(n).ResourceType = IIf(IsNull(aSQL(19, n)), "", aSQL(19, n))
            gUnitList(n).TXApproved = IsTransportUnit(gUnitList(n).ResourceType)
            
            If gForceAVL Then
                gUnitList(n).AVLID = gUnitList(n).VehID
                gUnitList(n).AVLEnabled = True
            End If
            
            If gForceMST Then
                gUnitList(n).MDTID = gUnitList(n).VehID
            End If
            
            If Abs(gUnitList(n).CurrentLat) <= 90 And gUnitList(n).CurrentLat <> 0 Then
                AccumLat = AccumLat + gUnitList(n).CurrentLat
                AccumLon = AccumLon + gUnitList(n).CurrentLon
                LatLonCount = LatLonCount + 1
            End If
            
            '    If gUnitList(n).Status > STAT_OffDuty Then
            '
            '        Call CreateUnitIcon(gUnitList(n))
            '
            '    End If
            
        Next n
        
        If gUseESRIResources Then
            Call cnUpdateUnitLocations
        End If
        
        If gLicensedVersion Then            '   perform secondary validation of license based on geography covered by fleet
            
            Debug.Print AccumLat / LatLonCount, AccumLon / LatLonCount, DistanceDeg(AccumLat / LatLonCount, AccumLon / LatLonCount, gClientLat, gClientLong, "SM")
            
            If DistanceDeg(AccumLat / LatLonCount, AccumLon / LatLonCount, gClientLat, gClientLong, "SM") > gClientRadius Then
                Call MsgBox("Sorry, but the key provided for this application appears to be corrupt." _
                & Chr(13) & "                 Connection to VisiCAD has been disabled." & Chr(13) _
                & Chr(13) & "                    Please contact CAD North Inc." _
                & Chr(13) & "                        support@cadnorth.com" _
                & Chr(13) & "              or 1 (905) 646-5172 for further assistance.", vbOKOnly + vbCritical, "Invalid Key")
                gLicensedVersion = False
                sBar.Panels(2).Text = "Error in Keyfile"
                Call AddToDebugList("[License Region Test]   : Key is not for this region", lstDebug)
                Call AddToDebugList("[License] Key Lat/Lon   : " & Format(gClientLat, "###0.000000 ") & Format(gClientLong, "###0.000000"), lstDebug)
                Call AddToDebugList("[License] Key Radius    : " & Format(gClientRadius, "###0"), lstDebug)
                Call AddToDebugList("[License] Fleet Lat/Lon : " & Format(AccumLat / LatLonCount, "###0.000000 ") & Format(AccumLon / LatLonCount, "###0.000000"), lstDebug)
                Call AddToDebugList("[License] Distance      : " & Format(DistanceDeg(AccumLat / LatLonCount, AccumLon / LatLonCount, gClientLat, gClientLong, "SM"), "####0.000"), lstDebug)
            End If
        
        End If
    
    End If
    
    DoEvents
    
    Exit Sub

ERH:
    
    AddToDebugList ("[ERROR in LoadUnitList] No.:" & Str(Err.Number) & " " & Err.Description), lstDebug
    
End Sub

Private Sub cnUpdateUnitLocations(Optional ByRef cError As String = "")
    Dim n As Long
    
    Dim nHnd As Long
    
    On Error GoTo cnUpdateUnitLocationsERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cnUpdateUnitLocations")
    End If
    
    If gDrawingLayerHandle >= 0 Then                    '   if we have one, clear it first
        Call winMap1.ClearDrawing(gDrawingLayerHandle)
    End If
    
    gDrawingLayerHandle = winMap1.NewDrawing(dlSpatiallyReferencedList)
    winMap1.DrawingLabelsShadow(gDrawingLayerHandle) = False
    Call winMap1.DrawingFont(gDrawingLayerHandle, "Calibri", 9)
    '   winMap1.DrawingLabelsOffset(gDrawingLayerHandle) = 14
        
    For n = 0 To UBound(gUnitList, 1)
        
        If gUnitList(n).CurrentLat <> 0 And gUnitList(n).Status > vcStatus.OffDuty Then
        
            Call winMap1.DrawPointEx(gDrawingLayerHandle, gUnitList(n).CurrentLon, gUnitList(n).CurrentLat, 20, gStatusList(gUnitList(n).Status).BackColor)
            '   Call winMap1.AddDrawingLabel(gDrawingLayerHandle, gUnitList(n).UnitName, RGB(0, 0, 0), gUnitList(n).CurrentLon, gUnitList(n).CurrentLat, laCenter)
            Call winMap1.AddDrawingLabel(gDrawingLayerHandle, gUnitList(n).UnitName, gStatusList(gUnitList(n).Status).ForeColor, gUnitList(n).CurrentLon, gUnitList(n).CurrentLat, hjCenter)
            
        End If
            
    Next n
    
    gMapRefreshRequired = False
        
    Exit Sub
cnUpdateUnitLocationsERH:
    cError = "Error creating shapefile in cnUpdateUnitLocations: " & Err.Description & " (" & Str(Err.Number) & ") "
End Sub

Private Function IsTransportUnit(ResourceType As String) As Boolean
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function IsTransportUnit")
    End If
    
    IsTransportUnit = False
    
    For n = 0 To UBound(gSpeechCallSigns)
        If gSpeechCallSigns(n, 0) = ResourceType Then
            IsTransportUnit = gSpeechCallSigns(n, 2)
            Exit For
        End If
    Next n

End Function

Private Sub CreateUnitIcon(ByRef Unit As Vehicle)
    Dim lWidth As Integer

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub CreateUnitIcon")
    End If

    '    If Abs(Unit.CurrentLat) <= 90 And Abs(Unit.CurrentLon) <= 180 Then
    '        If Unit.CurrentLat <> 0 And Unit.CurrentLon <> 0 Then
    '            Set lUnit = map.ActiveMap.GetLocation(Unit.CurrentLat, Unit.CurrentLon)
    '
    '            map.ActiveMap.MapFont = geoMapFontSmallest
    '
    '            Select Case Len(Unit.UnitName)
    '                Case 3
    '                    lWidth = 38
    '                Case 4
    '                    lWidth = 45
    '                Case 5
    '                    lWidth = 53
    '                Case Else
    '                    lWidth = 62
    '            End Select
    '
    '            map.ActiveMap.Shapes.AddTextbox lUnit, lWidth, 5
    '
    '            Unit.ShapeID = map.ActiveMap.Shapes.count
    '
    '            map.ActiveMap.Shapes.Item(Unit.ShapeID).Text = Unit.UnitName
    '            map.ActiveMap.Shapes.Item(Unit.ShapeID).Name = Unit.UnitName
    '            map.ActiveMap.Shapes.Item(Unit.ShapeID).Line.Weight = 0
    '            map.ActiveMap.Shapes.Item(Unit.ShapeID).Line.Visible = True
    '            map.ActiveMap.Shapes.Item(Unit.ShapeID).FontColor = gStatusList(Unit.Status).ForeColor
    '            map.ActiveMap.Shapes.Item(Unit.ShapeID).Fill.ForeColor = gStatusList(Unit.Status).BackColor
    '            map.ActiveMap.Shapes.Item(Unit.ShapeID).Fill.Visible = True
    '        End If
    '    End If
End Sub

Private Sub CenterMap()
    Dim n As Long
    Dim MaxLat As Double, MaxLon As Double
    Dim MinLat As Double, MinLon As Double
    '    Dim objLoc1 As MapPointCtl.Location
    '    Dim objLoc2 As MapPointCtl.Location
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub CenterMap")
    End If
    
    '    MaxLat = -99999999
    '    MaxLon = -99999999
    '    MinLat = 99999999
    '    MinLon = 99999999
    '
    '    For n = 0 To UBound(gUnitList)
    '        If gUnitList(n).Status <> STAT_OffDuty Then             '   don't find map center using OffDuty units
    '            If Abs(gUnitList(n).CurrentLat) <= 90 And Abs(gUnitList(n).CurrentLon) <= 180 _
    '                And Abs(gUnitList(n).CurrentLat) > 0 And Abs(gUnitList(n).CurrentLon) > 0 Then      '   and skip the center of the earth
    '                If MaxLat <= gUnitList(n).CurrentLat Then
    '                    MaxLat = gUnitList(n).CurrentLat
    '                End If
    '                If MaxLon <= gUnitList(n).CurrentLon Then
    '                    MaxLon = gUnitList(n).CurrentLon
    '                End If
    '                If MinLat >= gUnitList(n).CurrentLat Then
    '                    MinLat = gUnitList(n).CurrentLat
    '                End If
    '                If MinLon >= gUnitList(n).CurrentLon Then
    '                    MinLon = gUnitList(n).CurrentLon
    '                End If
    '            End If
    '        End If
    '    Next n
    '
    '    If Abs(MaxLat) <= 90 And Abs(MinLat) <= 90 And Abs(MaxLon) <= 180 And Abs(MinLon) <= 180 Then
    '        '   i.e.: if we've actually got real earth coordinates
    '    '        Set objLoc1 = map.ActiveMap.GetLocation(MaxLat, MaxLon)
    '    '        Set objLoc2 = map.ActiveMap.GetLocation(MinLat, MinLon)
    '    '
    '    '        map.ActiveMap.Union(Array(objLoc1, objLoc2)).GoTo
    '
    '    End If

    Exit Sub

ERH:
    
    AddToDebugList ("[ERROR in CenterMap] No.:" & Str(Err.Number) & " " & Err.Description), lstDebug
    AddToDebugList ("                       n: " & Str(n)), lstDebug
    AddToDebugList ("                  maxLat:" & Str(MaxLat)), lstDebug
    AddToDebugList ("                  maxLon:" & Str(MaxLon)), lstDebug
    AddToDebugList ("                  minLat:" & Str(MinLat)), lstDebug
    AddToDebugList ("                  minLon:" & Str(MinLon)), lstDebug
End Sub

Private Sub RefreshUnitQueue()
    Dim n As Long, c As Long
    Dim nRow As Long
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub RefreshUnitQueue")
    End If
    
    With flxVehicle
        .Rows = UBound(gUnitList) + 2
        .Cols = 7
        .ColWidth(3) = .ColWidth(2) * 3
        .ColAlignment(0) = flexAlignLeftCenter
        .ColAlignment(1) = flexAlignRightCenter
        .ColAlignment(2) = flexAlignRightCenter
        .ColAlignment(3) = flexAlignLeftCenter
        .ColAlignment(4) = flexAlignLeftCenter
        .ColAlignment(5) = flexAlignRightCenter
        .ColAlignment(6) = flexAlignRightCenter
        .TextMatrix(0, 0) = "Unit"
        .TextMatrix(0, 1) = "CurrLat"
        .TextMatrix(0, 2) = "CurrLon"
        .TextMatrix(0, 3) = "CurrLoc"
        .TextMatrix(0, 4) = "CurrCity"
        .TextMatrix(0, 5) = "DestLat"
        .TextMatrix(0, 6) = "DestLon"
        .Row = 0
        .Col = 1
        .CellAlignment = flexAlignLeftCenter   '   correct header alignment
        .Col = 2
        .CellAlignment = flexAlignLeftCenter   '   correct header alignment
        .Col = 5
        .CellAlignment = flexAlignLeftCenter   '   correct header alignment
        .Col = 6
        .CellAlignment = flexAlignLeftCenter   '   correct header alignment
    End With
    
    nRow = 1
    
    For n = 0 To UBound(gUnitList)
'        If gUnitList(n).Status <> STAT_OffDuty Then                '   we're including off duty units now
            With flxVehicle
                .TextMatrix(nRow, 0) = gUnitList(n).UnitName
                .TextMatrix(nRow, 1) = gUnitList(n).CurrentLat
                .TextMatrix(nRow, 2) = gUnitList(n).CurrentLon
                .TextMatrix(nRow, 3) = gUnitList(n).CurrentLoc
                .TextMatrix(nRow, 4) = gUnitList(n).CurrentCity
                .TextMatrix(nRow, 5) = gUnitList(n).DestinationLat
                .TextMatrix(nRow, 6) = gUnitList(n).DestinationLon
                
                gUnitList(n).QueueRow = nRow
                
                .Row = nRow
                For c = 0 To .Cols - 1
                    .Col = c
                    .CellForeColor = gStatusList(gUnitList(n).Status).ForeColor
                    .CellBackColor = gStatusList(gUnitList(n).Status).BackColor
                Next c
            End With
            nRow = nRow + 1
'        End If
    Next n
    
    Exit Sub

ERH:
    
    AddToDebugList ("[ERROR in RefreshUnitQueue] No.:" & Str(Err.Number) & " " & Err.Description & " - nRow = " & Str(nRow)), lstDebug
    
End Sub

Private Sub ProcessSectorDivisionChange(UnitList As Vehicle)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessSectorDivisionChange")
    End If
'    MsgBox "Processing Sector/Division change for unit " & UnitList.UnitName, vbOKOnly + vbExclamation, "Testing and Coding to do here"
    Call InsertRadioMessage(UnitList, STAT_RADIOCHANNELCHANGE, Now)
End Sub

Private Sub RefreshUnitQueueRow(UnitList As Vehicle)
    Dim n As Long, c As Long
    Dim nRow As Long
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub RefreshUnitQueueRow")
    End If
    
    nRow = UnitList.QueueRow
    
    With flxVehicle
        .TextMatrix(nRow, 0) = UnitList.UnitName
        .TextMatrix(nRow, 1) = UnitList.CurrentLat
        .TextMatrix(nRow, 2) = UnitList.CurrentLon
        .TextMatrix(nRow, 3) = UnitList.CurrentLoc
        .TextMatrix(nRow, 4) = UnitList.CurrentCity
        .TextMatrix(nRow, 5) = UnitList.DestinationLat
        .TextMatrix(nRow, 6) = UnitList.DestinationLon
        
        '   gUnitList(n).QueueRow = nRow
        
        .Row = nRow
        
        For c = 0 To .Cols - 1
            .Col = c
            .CellForeColor = gStatusList(UnitList.Status).ForeColor
            .CellBackColor = gStatusList(UnitList.Status).BackColor
        Next c
    
    End With

    Exit Sub

ERH:
    
    AddToDebugList ("[ERROR in RefreshUnitQueueRow] No.:" & Str(Err.Number) & " " & Err.Description), lstDebug
    
End Sub

Private Sub InitStatus()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim n As Long
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InitStatus")
    End If
    
'    Private Type Status
'        ID as Long
'        Description As String
'        ForeColor As Long
'        BackColor As Long
'    End Type

    cSQL = "Select ID, Description, ForeColor, BackColor " _
                    & " from Status " _
                    & " Order by ID "

    nRecs = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    AddToDebugList "[Loading] " & Str(nRecs) & " Status Records Found", lstDebug
    
    If nRecs > 0 Then
        ReDim gStatusList(0 To nRecs - 1)
        For n = 0 To nRecs - 1
            gStatusList(n).Id = aSQL(0, n)
            If n <> aSQL(0, n) Then
                AddToDebugList "[OOPS!] Status ID not Sequential n=" & Str(n) & " Status ID=" & aSQL(0, n), lstDebug
            End If
            gStatusList(n).Description = aSQL(1, n)
            gStatusList(n).ForeColor = IIf(aSQL(2, n) > 0, aSQL(2, n), 1)       '   eliminate non-black blacks
            gStatusList(n).BackColor = IIf(aSQL(3, n) > 0, aSQL(3, n), 1)       '   eliminate non-black blacks
        Next n
    End If
    
    Exit Sub
    
ERH:
    
    AddToDebugList ("[ERROR in InitStatus] No.:" & Str(Err.Number) & " " & Err.Description), lstDebug

End Sub

Private Sub InitHospitals()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim n As Long
    Dim i As Integer
    '   Dim tLocation As MapPointCtl.Location
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InitHospitals")
    End If
    
'    Private Type CadLocation
'        Code As String
'        Name As String
'        Lat As Double
'        Lon As Double
'    End Type
     
    '    gHospitalTypeIDs = bsiGetSettings("AVL", "HospitalTypeID", "", App.Path & "\Simulator.INI")
    '    gUseClosestPercent = Val(bsiGetSettings("AVL", "UseClosestPercent", "100", App.Path & "\Simulator.INI"))
    '    gTransportPercent = Val(bsiGetSettings("AVL", "TransportPercent", "100", App.Path & "\Simulator.INI"))

    '    cSQL = "Select ID, Code, Name, Address, City, Latitude, Longitude " _
    '                    & " from Locations " _
    '                    & " Where LocationType_ID in (" & gHospitalTypeIDs & ")"

    cSQL = "Select ID, Code, Name, Address, City, Latitude, Longitude " _
                    & " from Locations "
    
    '   We will need to implement something that respects the LocationType_ID for permitted transport destination types. It looks like we used to do it this way.
    
    nRecs = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    AddToDebugList "[Loading Hospitals] " & Str(nRecs) & " Records Found", lstDebug
    
    If nRecs > 0 Then
        ReDim gHospitals(0 To nRecs - 1)
        For n = 0 To nRecs - 1
            gHospitals(n).Id = aSQL(0, n)
            gHospitals(n).Code = aSQL(1, n)
            gHospitals(n).Name = aSQL(2, n)
            gHospitals(n).Address = IIf(IsNull(aSQL(3, n)), "", aSQL(3, n))
            gHospitals(n).City = IIf(IsNull(aSQL(4, n)), "", aSQL(4, n))
            gHospitals(n).Lat = (aSQL(5, n) / 1000000) - gLatitudeAdjust
            gHospitals(n).Lon = (aSQL(6, n) / -1000000) - gLongitudeAdjust
            gHospitals(n).TXApproved = IsTXApprovedHospital(gHospitals(n).Name)
            '   need to add reference to destination agency type here
        Next n
    End If
    
    Exit Sub
    
ERH:
    
    AddToDebugList ("[ERROR in InitHospital] No.:" & Str(Err.Number) & " " & Err.Description), lstDebug

End Sub

Private Function GetLocationByName(cName As String) As CADLocation
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetLocationByName")
    End If
    
    For n = 0 To UBound(gHospitals, 1)
        If gHospitals(n).Name = cName Then
            GetLocationByName = gHospitals(n)
            Exit For
        End If
    Next n
    
End Function

Private Sub InitMapWindow()
    Dim sf As New MapWinGIS.Shapefile
    Dim nHnd As Long

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InitMapWindow")
    End If
    
    winMap1.RemoveAllLayers
    
    If gESRINetworkDataset <> "" Then
        sf.Open (gESRINetworkDataset)
        nHnd = winMap1.AddLayer(sf, True)
        winMap1.ShapeLayerLineColor(nHnd) = RGB(120, 120, 120)
        gMapProjection = sf.Projection
    End If

End Sub

Private Sub InitBrowseMapWindow()
    Dim sf As New MapWinGIS.Shapefile
    Dim nHnd As Long

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InitBrowseMapWindow")
    End If
    
    incMap.RemoveAllLayers
    
    If gESRINetworkDataset <> "" Then
        sf.Open (gESRINetworkDataset)
        nHnd = incMap.AddLayer(sf, True)
        incMap.ShapeLayerLineColor(nHnd) = RGB(120, 120, 120)
    End If

End Sub

Private Function IsTXApprovedHospital(cHospital As String) As Boolean
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function IsTXApprovedHospital")
    End If
    
    IsTXApprovedHospital = False
        
    For n = 0 To UBound(gSpeechLocations)
        If cHospital = gSpeechLocations(n, 0) Then
            IsTXApprovedHospital = gSpeechLocations(n, 2)
        End If
    Next n
    
End Function

Private Sub InitPosts()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    Dim n As Long
'
'    Private Type tPost
'        Code As String
'        Name As String
'        IsPost As Boolean
'    End Type
'
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InitPosts")
    End If
    
    cSQL = "Select Code, Name, StationFlag from Stations "      '   modified to actually work

    nRecs = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    AddToDebugList "[Loading Posts] " & Str(nRecs) & " Records Found", lstDebug
    
    If nRecs > 0 Then
        ReDim gPostList(0 To nRecs - 1)
        For n = 0 To nRecs - 1
            gPostList(n).Code = aSQL(0, n)
            gPostList(n).Name = aSQL(1, n)
            gPostList(n).IsPost = IIf(aSQL(2, n) = 0, True, False)
        Next n
    End If

    Exit Sub

ERH:
    
    AddToDebugList ("[ERROR in InitPosts] No.:" & Str(Err.Number) & " " & Err.Description), lstDebug
    
End Sub

Private Sub InitRoutingHashTable()
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRecs As Long
    
    On Error GoTo InitRoutingHashTable_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub InitRoutingHashTable")
    End If
    
    cSQL = "SELECT Count(*) from Vehicle"
    
    nRecs = bsiSQLGet(cSQL, aSQL, gConnectString)
    
    AddToDebugList "[Setting up Route Hash Table] " & Str(aSQL(0, 0)) & " Records Found", lstDebug
    
    If nRecs > 0 Then
        ReDim gRouteHashTable(0 To Val(aSQL(0, 0)) - 1)
    Else
        AddToDebugList "[ERROR] Setting up Route Hash Table - No Vehicle Records found", lstDebug
    End If
    
    Exit Sub

InitRoutingHashTable_ERH:
    
    AddToDebugList ("[ERROR in InitPosts] No.:" & Str(Err.Number) & " " & Err.Description), lstDebug
    
End Sub

Private Function IsPostOrStation(PostName As String, ByRef IsPost As Boolean) As Boolean
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function IsPostOrStation")
    End If
    
    IsPost = False
    IsPostOrStation = False
    
    For n = 0 To UBound(gPostList)
        If PostName = gPostList(n).Name Then
            IsPost = gPostList(n).IsPost
            IsPostOrStation = True
            Exit For
        End If
    Next n
    
End Function

Private Function ConnectToServer() As Boolean
    Dim LocalName As String
    Dim TimeOut As Variant
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ConnectToServer")
    End If
    
    LocalName = UCase(tcpSock.LocalHostName)
    txtIPCServer.Text = UCase(txtIPCServer.Text)
    tcpSock.RemoteHost = txtIPCServer.Text
    
    If gUseXMLPort Then
        tcpSock.RemotePort = 122
    Else
        tcpSock.RemotePort = 121
    End If
    
    AddToDebugList "[Connecting to IPC Server] " & tcpSock.RemoteHost & ":" & tcpSock.RemotePort, lstDebug
    
    If tcpSock.State = sckClosed Then
        tcpSock.Connect
        AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock), lstDebug
        AddToDebugList "[Local port]" & Str(tcpSock.LocalPort), lstDebug
        
        TimeOut = Now
        
        While tcpSock.State <> sckConnected And DateDiff("S", TimeOut, Now) < 2
            DoEvents
        Wend
        
        If tcpSock.State <> sckConnected Then        '   Timeout
            AddToDebugList "[Error] Failed to connect", lstDebug
            AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock), lstDebug
            AddToDebugList "[Closing]", lstDebug
            tcpSock.Close
            AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock), lstDebug
            ConnectToServer = False
        Else                                        '   Success!
            AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock), lstDebug
            AddToDebugList "[Registering] " & LocalName & "_cnSimulator", lstDebug
            tcpSock.SendData Chr(2) & Chr(200) & LocalName & "_cnSimulator" & Chr(3)
            DoEvents
            ConnectToServer = True
        End If
    Else
        AddToDebugList "[Error] Cannot connect - invalid state" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock), lstDebug
        AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock), lstDebug
        ConnectToServer = False
    End If
End Function

Private Function ConnectToAVLInterface(Optional nRetry As Integer = 3) As Boolean
    Dim LocalName As String
    Dim TimeOut As Variant
    Dim nAttempt As Integer
    Dim success As Boolean
    Dim cShareString As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ConnectToAVLInterface")
    End If
    
    cShareString = " SHARE=" & Left(gSystemDirectory, 2)
    
    nAttempt = 0
    success = False
    
    On Error GoTo ErrorHandler
    
TryConnectingAnyway:

    gAVLMDTAppName = bsiGetSettings("AVL Settings", "AVLAPPName", "SimulatorAVL-MDT.exe", gSystemDirectory & "\Simulator.INI")
    
    If Dir(gAVLPath & "\" & gAVLMDTAppName) = "" And gAVLIPAddress = "127.0.0.1" Then        '   Cannot find AVLMDT Interface Application on the selected path on the local machine
        
        success = False
    
    Else                                                    '   File was found, so let's launch if necessary and connect
    
        While nAttempt < nRetry And Not success
        
            If gAVLIPAddress = "127.0.0.1" Then         '   interface is on this machine
            
                If gAVLTaskID < 0 Then
                    gAVLTaskID = Shell(gAVLPath & "\" & gAVLMDTAppName & cShareString, vbMinimizedNoFocus)      '   launch the interface
                    
                    If gAVLTaskID > 0 Then
                        AddToDebugList "[Connecting to AVL] Launching AVL Interface on local machine.", lstDebug
                    Else
                        AddToDebugList "[Connecting to AVL] Cannot Launch AVL Interface on local machine.", lstDebug
                    End If
                
                End If
                
            End If
                
            nAttempt = nAttempt + 1
            
            LocalName = UCase(tcpAVL.LocalHostName)
            tcpAVL.RemoteHost = gAVLIPAddress
            tcpAVL.RemotePort = gAVLPort
            
            AddToDebugList "[Connecting to AVL Interface] " & tcpAVL.RemoteHost & ":" & tcpAVL.RemotePort & "   Attempt =" & Str(nAttempt), lstDebug
            
            If tcpAVL.State = sckClosed Then
                tcpAVL.Connect
                AddToDebugList "[State]" & Str(tcpAVL.State) & ", " & GetWinSockState(tcpAVL), lstDebug
                AddToDebugList "[Local port]" & Str(tcpAVL.LocalPort), lstDebug
                
                TimeOut = Now
                
                While tcpAVL.State <> sckConnected And DateDiff("S", TimeOut, Now) < 5
                    DoEvents
                Wend
                
                If tcpAVL.State <> sckConnected Then        '   Timeout
                    AddToDebugList "[Error] Failed to connect", lstDebug
                    AddToDebugList "[State]" & Str(tcpAVL.State) & ", " & GetWinSockState(tcpAVL), lstDebug
                    AddToDebugList "[Closing]", lstDebug
                    tcpAVL.Close
                    AddToDebugList "[State]" & Str(tcpAVL.State) & ", " & GetWinSockState(tcpAVL), lstDebug
                Else                                        '   Success!
                    AddToDebugList "[State]" & Str(tcpAVL.State) & ", " & GetWinSockState(tcpAVL), lstDebug
                    AddToDebugList "[Registering] " & LocalName & "_cnSimulator", lstDebug
                    success = True
                End If
            Else
                AddToDebugList "[Error] Cannot connect - invalid state" & Str(tcpAVL.State) & ", " & GetWinSockState(tcpAVL), lstDebug
                AddToDebugList "[State]" & Str(tcpAVL.State) & ", " & GetWinSockState(tcpAVL), lstDebug
                tcpAVL.Close
            End If
        
        Wend
    
    End If

CleanUP:

    If Not success Then
        MsgBox "Unable to connect to AVL/MST Interface on " & gAVLIPAddress & Chr(13) & "Path: " & gAVLPath, vbCritical + vbOKOnly
        AddToDebugList "[Connecting to AVL] Unable to connect to AVL Interface on " & gAVLIPAddress & " Path: " & gAVLPath, lstDebug
    End If

    ConnectToAVLInterface = success
    
    Exit Function
    
ErrorHandler:

    AddToDebugList ("[ERROR in ConnectToAVLServer] Try Connecting Anyway. Err:" & Err.Number & " " & Err.Description), lstDebug
    If nAttempt = 0 Then
        nAttempt = nAttempt + 1
        Resume TryConnectingAnyway
    Else
        Resume CleanUP
    End If
    ConnectToAVLInterface = success
End Function

Private Sub DisconnectServer()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub DisconnectServer")
    End If
    AddToDebugList "[Disconnecting IPC] " & tcpSock.RemoteHost & ":" & tcpSock.RemotePort, lstDebug
    tcpSock.Close
    AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock), lstDebug
    sBar.Panels(2).Text = "Disconnected"
End Sub

Private Sub DisconnectAVL()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub DisconnectAVL")
    End If
    AddToDebugList "[Disconnecting AVL] " & tcpAVL.RemoteHost & ":" & tcpAVL.RemotePort, lstDebug
    tcpAVL.Close
    AddToDebugList "[State]" & Str(tcpAVL.State) & ", " & GetWinSockState(tcpAVL), lstDebug
    gAVLTaskID = -1
End Sub

Private Function SendToAVL(MessageType As String, Optional MessageString As String = "", Optional nTimeOut As Integer = 1) As Boolean
    Dim TimeToGo As Variant
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function SendToAVL")
    End If
    
    AddToDebugList "[Send Data to AVL] Type=" & Format(MessageType, "0000") & " >" & MessageString & "<", lstDebug
    
    If tcpAVL.State = sckConnected Then
        If gLicensedVersion Or MessageType = EV_KILL Then
            tcpAVL.SendData Chr(2) & Format(MessageType, "0000") & "|" & MessageString & Chr(3)
            DoEvents
            gSendComplete = False
            TimeToGo = Now
            
            While Not gSendComplete And DateAdd("S", nTimeOut, TimeToGo) > Now
                DoEvents
            Wend
            
            SendToAVL = gSendComplete
        End If
    Else
        AddToDebugList "[ERROR] Not Ready to SEND", lstDebug
    End If

End Function

Private Sub SendToCADNoWait(MessageType As String, Optional MessageString As String = "")
    
    On Error GoTo SendToCADNoWait_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SendToCADNoWait")
    End If
    
    AddToDebugList "[Send Data to CAD (NoWait)] Type=" & Format(MessageType, "0000") & " >" & MessageString & "<", lstDebug
    
    If tcpSock.State = sckConnected Then
        Call SendToInterface(Format(MessageType, "0000") & "|" & MessageString)
        If gLicensedVersion Or MessageType = EV_KILL Then
            tcpSock.SendData Chr(2) & Format(MessageType, "0000") & MessageString & Chr(3)
            '   DoEvents
        End If
    Else
        Call SendToInterface("[ERROR - Sending] " & Format(MessageType, "0000") & "|" & MessageString)
        AddToDebugList "[ERROR] Not Ready to SEND", lstDebug
    End If
    
    Exit Sub
    
SendToCADNoWait_ERH:
    AddToDebugList ("[ERROR] SendToCADNoWait - Type:" & MessageType & "<  Message:" & MessageString & "<  Err#: " & Str(Err.Number) & " " & Err.Description), lstDebug
End Sub

Private Sub SendToAVLNoWait(MessageType As String, Optional MessageString As String = "")
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SendToAVLNoWait")
    End If
    
    If tcpAVL.State = sckConnected Then
        If MessageType = EV_INIT_DONE Then
            Call AddToDebugList("[SCRIPT] Synchronization Complete message received from Interface", lstDebug)
            gStopProcessing = True                      '   pause simulation when init is complete
            Call NewRadioMessage(MessageString, MessageType)
            Call SendToInterface("[INITDONE] >" & MessageString & "<")
        ElseIf MessageType <> EV_RADIO Then
            Call SendToInterface(Format(MessageType, "0000") & "|" & MessageString)
            If gLicensedVersion Then
                AddToDebugList "[Send Data to AVL (NoWait)] Type=" & Format(MessageType, "0000") & " >" & MessageString & "<", lstDebug
                tcpAVL.SendData Chr(2) & Format(MessageType, "0000") & "|" & MessageString & Chr(3)
                DoEvents
            End If
        Else
            Call NewRadioMessage(MessageString, MessageType)
            Call SendToInterface("[RADIO] >" & MessageString & "<")
            If gLicensedVersion Then
                AddToDebugList "[RADIO Message] " & Format(MessageType, "0000") & " >" & MessageString & "<", lstDebug
                tcpSock.SendData Chr(2) & MessageString & Chr(3)
                DoEvents
            End If
        End If
    Else
        Call SendToInterface("[ERROR - Sending] " & Format(MessageType, "0000") & "|" & MessageString)
        AddToDebugList "[ERROR] Not Ready to SEND", lstDebug
    End If
    
End Sub

Private Sub NewRadioMessage(NewMessage As String, MessageType As String)
    Static LastRadioMessageList As String
    Dim RadioMessage As String
    Dim aMessages As Variant
    Dim n As Integer
    Dim Dupe As Boolean
    
    On Error GoTo ErrHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub NewRadioMessage")
    End If
    
    If Val(MessageType) = EV_INIT_DONE Then             '   When initialization is complete,
        txtRadio.Text = ""                              '   clear the radio window and
        lstRadio.Clear                                  '   purge the message list
        gPhase2Complete = True
    End If
    
    aMessages = Split(LastRadioMessageList, "|")                        '   split the list back into an array
    
    Dupe = False
    
    RadioMessage = Split(NewMessage, "|")(1)
    
    For n = 0 To UBound(aMessages)                                      '   look for the current message in the list of previous messages
        If aMessages(n) = RadioMessage Then
            Dupe = True                                                 '   if found, mark it as a duplicate
            Exit For
        End If
    Next n
    
    If Not Dupe Then                                                    '   sometimes we get duplicate radio messages because we have to run some
                                                                        '   routines twice to get around a VisiCAD bug. This section
        lstRadio.AddItem RadioMessage                                   '   checks to see if the last message we sent was the same as this one
                                                                        '   and skips adding it to the radio list if it's a dupe.
        If txtRadio.Text = "" Then
            txtRadio.Text = lstRadio.List(0)
            lstRadio.RemoveItem 0
            cmdMessage.Caption = "Clear Message"
        Else
            cmdMessage.Caption = "Queued Messages =" & Str(lstRadio.ListCount)
        End If
        
        If gDingWithMessage Then Beep
        
    End If
    
    If UBound(aMessages) > 10 Then                                              '   only keep the last 10 messages
        LastRadioMessageList = aMessages(1)
        For n = 2 To UBound(aMessages)
            LastRadioMessageList = LastRadioMessageList & "|" & aMessages(n)    '   Keep track of the last radio message in this static variable so that we can
        Next n                                                                  '   check for dupes next time around.
        LastRadioMessageList = LastRadioMessageList & "|" & RadioMessage
    Else
        LastRadioMessageList = LastRadioMessageList & "|" & RadioMessage        '   Keep track of the last radio message in this static variable so that we can
    End If
    
    Exit Sub
    
ErrHandler:
    AddToDebugList ("[ERROR] NewRadioMessage - msg:" & RadioMessage & "<  typ:" & MessageType & "<  Err#: " & Str(Err.Number) & " " & Err.Description), lstDebug
End Sub

Private Sub cmdMessage_Click()

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdMessage_Click")
    End If
    
    If lstRadio.ListCount > 0 Then
        txtRadio.Text = lstRadio.List(0)
        lstRadio.RemoveItem 0
        If lstRadio.ListCount = 0 Then
            cmdMessage.Caption = "Clear Message"
        Else
            cmdMessage.Caption = "Queued Messages =" & Str(lstRadio.ListCount)
        End If
        If gDingWithMessage Then Beep
    Else
        txtRadio.Text = ""
        cmdMessage.Caption = "No Messages"
    End If
    
End Sub

Private Function GetWinSockState(oWinSock As Winsock) As String
    ' sckClosed             0 Default. Closed
    ' sckOpen               1 Open
    ' sckListening          2 Listening
    ' sckConnectionPending  3 Connection pending
    ' sckResolvingHost      4 Resolving host
    ' sckHostResolved       5 Host resolved
    ' sckConnecting         6 Connecting
    ' sckConnected          7 Connected
    ' sckClosing            8 Peer is closing the connection
    ' sckError              9 Error
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetWinSockState")
    End If
    
    Select Case oWinSock.State
        Case sckClosed                  ' 0
            GetWinSockState = "Closed"
        Case sckOpen                    ' 1
            GetWinSockState = "Open"
        Case sckListening               ' 2
            GetWinSockState = "Listening"
        Case sckConnectionPending       ' 3
            GetWinSockState = "Connection pending"
        Case sckResolvingHost           ' 4
            GetWinSockState = "Resolving host"
        Case sckHostResolved            ' 5
            GetWinSockState = "Host resolved"
        Case sckConnecting              ' 6
            GetWinSockState = "Connecting"
        Case sckConnected               ' 7
            GetWinSockState = "Connected"
        Case sckClosing                 ' 8
            GetWinSockState = "Peer is closing the connection"
        Case sckError                   ' 9
            GetWinSockState = "Error"
    End Select
    
End Function

Private Sub tcpAVL_SendComplete()
'    AddToDebugList ("[Send Complete]")
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tcpAVL_SendComplete")
    End If
    gSendComplete = True
End Sub

Private Sub tcpSock_Close()
    '
    ' This should only get called when the connection to IPC service is closed from by the IPC service.
    '
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tcpSock_Close")
    End If
    AddToDebugList "[Event] Socket Closed", lstDebug
    
    AddToDebugList "[Debug] IPC Service Closed Connection. Resetting Connection to IPC Service and AVL/MDT Interface", lstDebug
    Call cmdConnect_Click
    
    AddToDebugList "[Debug] Re-Connecting to IPC Service and AVL/MDT Interface", lstDebug
    Call cmdConnect_Click

End Sub

Private Sub tcpSock_Connect()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tcpSock_Connect")
    End If
    AddToDebugList "[Event] Connected", lstDebug
End Sub

Private Sub tcpSock_ConnectionRequest(ByVal requestID As Long)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tcpSock_ConnectionRequest")
    End If
    AddToDebugList "[Event] Connection Request Received", lstDebug
End Sub

Private Sub OLD_tcpSock_DataArrival(ByVal bytesTotal As Long)
    Dim sTemp As String
    Dim aMessage As Variant
    Dim msgLen As Integer
    Dim sReceived As String
    Dim Buffer As String
    Dim cASCII As String
    Dim n As Integer, i As Integer
    Dim cMessage As String
    
    On Error GoTo tcpSock_DataArrival_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tcpSock_DataArrival")
    End If
    
'    AddToDebugList "[Event] DataArrival" & Str(bytesTotal)

    cMessage = "GetData"

    tcpSock.GetData sReceived
    
    cMessage = "Merging with Previous Message"

    Buffer = gPartialMessage & sReceived
    
    cMessage = "Splitting Buffer"

    aMessage = Split(Buffer, Chr(3))      '   split buffer into separate messages at the termination character ASCII 3
    
    cMessage = "Messages = " & UBound(aMessage)

    For n = 0 To UBound(aMessage) - 1
    
        cMessage = "Processing Message " & n

        msgLen = Len(aMessage(n))
        If msgLen > 3 Then
        
            cMessage = "Complete Message " & n

            aMessage(n) = Right(aMessage(n), msgLen - 1)
            If Left(aMessage(n), 4) <> "0181" Then                  '   don't log or further process hearbeat messages
                Call AddToTCPList(aMessage(n), lstTCP, , True)
                Call PutCADMessage(aMessage(n))
            End If
            ' Call AddToCADMsgQueue(aMessage(n))
            ' Call ProcessCADMessage(aMessage(n))               '   bad thing to call the process routine inside an event handler
        Else
            
            cMessage = "ODD Message " & n

            aMessage(n) = Right(aMessage(n), msgLen - 1)
            cASCII = ""
            For i = 1 To Len(aMessage(n))
                cASCII = cASCII & "[" & Trim(Str(Asc(Mid(aMessage(n), i, 1)))) & "]"
            Next i
            AddToDebugList "[ODD THING] >" & aMessage(n) & "< ASCII:" & cASCII, lstDebug
        End If
    Next n
    
    cMessage = "Saving Partian Message " & n

    gPartialMessage = aMessage(n)   '   aMessage(n) will either be empty or contain a partial message because we split the buffer at the chr(3) msg terminator
    
    Exit Sub
    
tcpSock_DataArrival_ERH:
    
    Call AddToDebugList("[ERROR] in event tcpSock_DataArrival: (" & Err.Number & ") " & Err.Description & " @ " & cMessage & " - >" & aMessage(n) & "<", lstDebug)
    
End Sub

Private Sub tcpSock_DataArrival(ByVal bytesTotal As Long)
    Dim sTemp As String
    Dim aMessage As Variant
    Dim msgLen As Long
    Dim sReceived As String
    Dim Buffer As String
    Dim cASCII As String
    Dim n As Integer, i As Integer
    Dim cMessage As String
    Dim MsgNum As String
    Dim MsgBody As String
    
    On Error GoTo tcpSock_DataArrival_ERH
    
    tcpSock.GetData sReceived
    
    gReceivedWhileProcessing = gReceivedWhileProcessing & sReceived
    
    If Not gIPCProcessing Then
    
        gIPCProcessing = True
        
        cMessage = "Splitting Buffer"
        Buffer = gReceivedWhileProcessing
        gReceivedWhileProcessing = ""
        
        aMessage = Split(Buffer, Chr(vcIP_EOM))      '   split buffer into separate messages at the termination character ASCII 3
        
        cMessage = "Messages = " & UBound(aMessage)
        
        If gDetailedDebugging Then
            Call AddToDebugList("[TRACE] Sub tcpSock_DataArrival. Main Loop.")
            Call AddToDebugList("[DEBUG] Bytes Received in tcpSock_DataArrival = " & Len(Buffer) & " Messages: = " & cMessage)
        End If
    
        '    AddToDebugList "[Event] DataArrival" & Str(bytesTotal)
          
        Do While UBound(aMessage) > 0
        
            If gDetailedDebugging Then
                Call AddToDebugList("[IPC LOOP] Messages pending = " & UBound(aMessage))
            End If
    
            For n = 0 To UBound(aMessage) - 1       '   UBound()-1 -- because when we split on vcIP_EOM, the last element of aMessage will always be either blank/NULL or a partial message
            
                cMessage = "Processing Message " & n
                
                sBar.Panels(3).Text = UBound(aMessage, 1) - n
        
                msgLen = Len(aMessage(n))
                If msgLen > 3 Then
                
                    cMessage = "Complete Message " & n
                    
                    If Left(aMessage(n), 1) = Chr(vcIP_BOM) Then
                        aMessage(n) = Right(aMessage(n), msgLen - 1)        '   strip off the leading Start_of_message marker
                    End If
                    
                    Call AddToTCPList(aMessage(n), lstTCP, , True)
                    
                    MsgNum = Left(aMessage(n), 4)
                    MsgBody = Right(aMessage(n), Len(aMessage(n)) - 4)
                    
                    Call ProcessCADMsg(MsgNum, MsgBody)
                    
                Else
                    
                    cMessage = "ODD Message " & n
                    
                    '   aMessage(n) = Right(aMessage(n), msgLen - 1)
                    
                    cASCII = ""
                    For i = 1 To Len(aMessage(n))
                        cASCII = cASCII & "[" & Trim(Str(Asc(Mid(aMessage(n), i, 1)))) & "]"
                    Next i
                    AddToDebugList "[ODD THING] >" & aMessage(n) & "< ASCII:" & cASCII, lstDebug
                End If
                
                DoEvents
                
            Next n
            
            If gDetailedDebugging Then
                Call AddToDebugList("Remainder Message at end of For loop: >" & aMessage(n) & "<")
                Call AddToDebugList("Bytes received during main For loop: " & Len(gPartialMessage))
            End If
        
            '   aMessage(n) will either be empty or contain a partial message because we split the buffer at the chr(3) msg terminator
            
            gPartialMessage = aMessage(n) & gReceivedWhileProcessing
            
            aMessage = Split(gPartialMessage, Chr(vcIP_EOM))
            
            gReceivedWhileProcessing = ""
            
        Loop
        
        If gDetailedDebugging Then
            If UBound(aMessage, 1) >= 0 Then
                Call AddToDebugList("Remainder Message at end of Do loop: >" & aMessage(UBound(aMessage, 1)) & "<")
            Else
                Call AddToDebugList("Remainder Message at end of Do loop: ><")
            End If
        End If
        
        If UBound(aMessage, 1) >= 0 Then
            gReceivedWhileProcessing = aMessage(UBound(aMessage, 1))
        Else
            gReceivedWhileProcessing = ""
        End If
        
        gIPCProcessing = False
        
        sBar.Panels(3).Text = 0
        
    Else
    
        If gDetailedDebugging Then
            Call AddToDebugList("[TRACE] Sub tcpSock_DataArrival. Received while processing - " & Len(gReceivedWhileProcessing) & " bytes.")
        End If
    
        '   tcpSock.GetData sReceived
        
        '   gReceivedWhileProcessing = gReceivedWhileProcessing & sReceived
            
        '   If gDetailedDebugging Then
        '       Call AddToDebugList("[DEBUG] Receiving data in tcpSock_DataArrival while gIPCProcessing is TRUE. Buffer size = " & Len(gReceivedWhileProcessing))
        '   End If
        
    End If
    
    Exit Sub
    
tcpSock_DataArrival_ERH:

    gIPCProcessing = False      '   in the event of an error, turn on processing of IPC messages back on
    
    Call AddToDebugList("[ERROR] in event tcpSock_DataArrival: (" & Err.Number & ") " & Err.Description & " @ " & cMessage & " - >" & aMessage(n) & "<", lstDebug)
    
End Sub

Private Sub tcpSock_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tcpSock_Error")
    End If
    
    AddToDebugList "[Event IPC] Error" & Str(Number) & ": " & Description, lstDebug
    AddToDebugList "[Info]  Source: " & Source & " Scode:" & Str(Scode), lstDebug
    AddToDebugList "[Info]  HelpFile: " & HelpFile & " HelpContext:" & Str(HelpContext), lstDebug
    AddToDebugList "[Info]  CancelDisplay: " & Switch(CancelDisplay, "TRUE", Not CancelDisplay, "FALSE"), lstDebug
    AddToDebugList "[State]" & Str(tcpSock.State) & ", " & GetWinSockState(tcpSock), lstDebug

End Sub

Private Sub tcpAVL_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tcpAVL_Error")
    End If
    
    AddToDebugList "[Event AVL] Error" & Str(Number) & ": " & Description, lstDebug
    AddToDebugList "[Info]  Source: " & Source & " Scode:" & Str(Scode), lstDebug
    AddToDebugList "[Info]  HelpFile: " & HelpFile & " HelpContext:" & Str(HelpContext), lstDebug
    AddToDebugList "[Info]  CancelDisplay: " & Switch(CancelDisplay, "TRUE", Not CancelDisplay, "FALSE"), lstDebug
    AddToDebugList "[State]" & Str(tcpAVL.State) & ", " & GetWinSockState(tcpAVL), lstDebug

End Sub

'Public Sub AddToDebugList(sString As String)
'    Dim LogItem As String
'    Dim n As Long
'
'    On Error GoTo AddToDebugList_ERH
'
'    LogItem = Format(Now, "hh:nn:ss ") & sString
'
'    If lstDebug.ListCount > 30000 Then      '   check for list overflow
'        For n = 0 To 1000
'            lstDebug.RemoveItem 0           '   removing '0' promotes '1' to '0'
'        Next n
'    End If
'
'    lstDebug.AddItem LogItem
'    If gbLogging Then
'        gDebugLog = FreeFile()
'        If Dir(App.Path & "\Logs", vbDirectory) = "" Then
'            MkDir (App.Path & "\Logs")
'        End If
'
'        Open App.Path & "\Logs\Debuglog - " & Format(Now, "YYYYMMDD") & ".log" For Append As #gDebugLog
'
'        Print #gDebugLog, LogItem
'
'        Close #gDebugLog
'    End If
'    lstDebug.ListIndex = lstDebug.ListCount - 1
'    lstDebug.Selected(lstDebug.ListIndex) = False
'
'    Exit Sub
'
'AddToDebugList_ERH:
'
'    Call MsgBox("Error in AddToDebugList writing to log: " & Err.Description & " (" & Err.Number & ")", vbOKOnly, "Logging Error")
'
'End Sub

Public Sub AddToTraceLog(sString As String)
    Dim nLogFile As Integer
    Dim LogItem As String
    Dim n As Long

    On Error GoTo AddToTraceLog_ERH

    LogItem = Format(Now, "hh:nn:ss ") & sString

    nLogFile = FreeFile()
    If Dir(App.Path & "\Logs", vbDirectory) = "" Then
        MkDir (App.Path & "\Logs")
    End If

    Open App.Path & "\Logs\TraceLog - " & Format(Now, "YYYYMMDD") & ".log" For Append As #nLogFile

    Print #nLogFile, LogItem

    Close #nLogFile

    Exit Sub

AddToTraceLog_ERH:

    Call AddToDebugList("[TRACE ERROR] in AddToTraceLog writing to log: " & Err.Description & " (" & Err.Number & ")", lstDebug)

End Sub

'Private Sub AddToTCPList(ByVal sString As String, Optional nBytes As Long = 0)
'    Dim lvwItem As Object
'    Dim msgType As String
'    Dim msgTypeString As String
'    Dim MsgData As String
'    Dim LogItem As String
'    Dim cByteString As String
'
'    On Error GoTo AddToTCPList_ERH
'
'    If gDetailedDebugging Then
'        Call AddToDebugList("[TRACE] Sub AddToTCPList")
'    End If
'
'    msgType = Left(sString, 4)
'    msgTypeString = VisiCADMessageType(msgType)
'    MsgData = Right(sString, Len(sString) - 4)
'
'    cByteString = ConvertToByteString(sString)
'
'    If gbLogging Then
'        If Dir(App.Path & "\Logs", vbDirectory) = "" Then
'            MkDir (App.Path & "\Logs")
'        End If
'
'        LogItem = Format(Now, "hh:nn:ss ")
'        LogItem = LogItem & "[" & msgType & "]"
'        LogItem = LogItem & "[" & Format(Len(MsgData), "000") & "]"
'        LogItem = LogItem & "[" & msgTypeString & Space(IIf(Len(msgTypeString) < 40, 40, Len(msgTypeString)) - Len(msgTypeString)) & "]"
'        LogItem = LogItem & ">" & MsgData & "<"
'
'        LogItem = LogItem & cByteString
'
'        gTCPLog = FreeFile()
'        Open App.Path & "\Logs\TCPlog - " & Format(Now, "YYYYMMDD") & ".log" For Append As #gTCPLog
'
'        Print #gTCPLog, LogItem
'
'        Close #gTCPLog
'    End If
'
'    Set lvwItem = lvMain.ListItems.Add()
'    lvwItem.Text = Format(Now, "hh:mm:ss")
'    lvwItem.SubItems(1) = msgType
'    lvwItem.SubItems(2) = Len(MsgData)
'    lvwItem.SubItems(3) = msgTypeString
'    lvwItem.SubItems(4) = MsgData
'
'    lvMain.ListItems.Item(lvMain.ListItems.count).Selected = True
'    lvMain.SelectedItem.EnsureVisible
'
'    Exit Sub
'
'AddToTCPList_ERH:
'
'    Call AddToDebugList("[ERROR] in AddToTCPList writing to log: " & Err.Description & " (" & Err.Number & ") Received Message = >" & sString & "<")
'
'End Sub
'
Private Sub SendToInterface(sString As String)
    Dim LogItem As String
    
    On Error GoTo SendToInterface_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SendToInterface")
    End If
    
    If gbLogging Then
        If Dir(App.Path & "\Logs", vbDirectory) = "" Then
            MkDir (App.Path & "\Logs")
        End If
            
        LogItem = Format(Now, "hh:nn:ss >") & sString & "<"
        
        gInterfaceLog = FreeFile()
        Open App.Path & "\Logs\InterfaceLog - " & Format(Now, "YYYYMMDD") & ".log" For Append As #gInterfaceLog

        Print #gInterfaceLog, LogItem
        
        Close #gInterfaceLog
    End If
    
    Exit Sub
    
SendToInterface_ERH:

    Call MsgBox("Error in SendToInterface writing to log: " & Err.Description & " (" & Err.Number & ")", vbOKOnly, "Logging Error")

End Sub

Private Function ParsePath(cFilename As String) As String
    Dim cPath As String
    Dim n As Integer
    Dim aPath() As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ParsePath")
    End If
    
    aPath = Split(cFilename, "\")
    
    cPath = aPath(0)
    For n = 1 To UBound(aPath, 1) - 1
        cPath = cPath & "\" & aPath(n)
    Next n
    
    ParsePath = cPath
    
End Function

Private Function ParseXMLStatus(Message As String) As XMLStatus
    Dim result As XMLStatus
    Dim nStart As Integer
    Dim nStop As Integer
    
    '00:36:41 [0244][214][NP_READ_VEHICLE_STATUS                  ]
    '<Vehicle ID="166"  ReadVehicleSent= "-1">
    '    <FromStatus>13</FromStatus>
    '    <ToStatus>14</ToStatus>
    '    <Master_Incident_ID>2291</Master_Incident_ID>
    '    <TimeStamp>Apr 17 2003 00:36:38</TimeStamp>
    '    <StationID>34</StationID>
    '</Vehicle>
    
    'Beginning with v5.6, this is the new structure of the message      2014.07.17 BM
    '0244
    '<?xml version="1.0" encoding="utf-16"?>
    '<Vehicle xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" ID="155">
    '   <FromStatus>1</FromStatus>
    '   <ToStatus>6</ToStatus>
    '   <Master_Incident_ID>192642</Master_Incident_ID>
    '   <StationID>0</StationID>
    '</Vehicle>
    
    'Type XMLStatus
    '    UnitID As Integer
    '    FromStat As Integer
    '    ToStat As Integer
    '    CallID As Integer
    '    StatTime As Variant
    '    StationID As Integer
    'End Type
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ParseXMLStatus")
    End If
    
    nStart = InStr(1, Message, " ID=" & Chr(34)) + Len(" ID=" & Chr(34))                '   Look for the string [ ID="] - required for v5.6 and thankfully, it's backwards-compatible :)
    nStop = InStr(nStart + 1, Message, Chr(34))
    result.UnitID = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<FromStatus>") + Len("<FromStatus>")
    nStop = InStr(nStart, Message, "</FromStatus>")
    result.FromStat = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<ToStatus>") + Len("<ToStatus>")
    nStop = InStr(nStart, Message, "</ToStatus>")
    result.ToStat = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<Master_Incident_ID>") + Len("<Master_Incident_ID>")
    nStop = InStr(nStart, Message, "</Master_Incident_ID>")
    result.CallID = Val(Mid(Message, nStart, nStop - nStart))
    
    nStart = InStr(1, Message, "<TimeStamp>") + Len("<TimeStamp>")
    nStop = InStr(nStart, Message, "</TimeStamp>")
    If nStop = 0 Then              '   no time stamp in message for some funky reason ... needs to be fixed
        result.StatTime = Now
    Else
        result.StatTime = CDate(Mid(Message, nStart, nStop - nStart))
    End If
    
    nStart = InStr(1, Message, "<StationID>") + Len("<StationID>")
    nStop = InStr(nStart, Message, "</StationID>")
    result.StationID = Val(Mid(Message, nStart, nStop - nStart))

    ParseXMLStatus = result
    
End Function

Private Function ParseXMLPosition(ByVal Message As String) As XMLPosition
    Dim result As XMLPosition
    Dim nStart As Integer
    Dim nStop As Integer
    Dim aMsg() As String
    
'    00:38:36 [0245][314][NP_READ_VEHICLE_POSITION                ]
'    >
'    <Vehicle ID= "164" ReadVehicleSent= "-1">
'        <Current_Lat>30229930</Current_Lat>
'        <Current_Lon>97767324</Current_Lon>
'        <Current_Location>EMS Station 1</Current_Location>
'        <Current_City>AUSTIN</Current_City>
'        <LastAVLUpdate>Apr 17 2003 00:38:33</LastAVLUpdate>
'        <Speed>0</Speed>
'        <Heading></Heading>
'        <Altitude>0</Altitude>
'    </Vehicle>
'    <
    
'    Type XMLPosition
'        UnitID As Integer
'        CurrLat As Double
'        CurrLon As Double
'        CurrLoc As String
'        CurrCity As String
'        LastAVL As Variant
'        Speed As Single
'        Heading As Single
'        Altitude As Long
'    End Type

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ParseXMLPosition")
    End If
    
    If Left(Message, 1) <> "<" Then
        
        aMsg = Split(Message, ",")
        
        result.UnitID = Val(aMsg(0))
        result.CurrLat = (Val(aMsg(1)) / 1000000) - gLatitudeAdjust
        result.CurrLon = (Val(aMsg(2)) / -1000000) - gLongitudeAdjust
            
    Else

        result.UnitID = Val(GetXMLAttributeByTag("Vehicle", "ID", Message))
        
        result.CurrLat = (Val(GetXMLValueByTag("Current_Lat", Message)) / 1000000) - gLatitudeAdjust
        
        result.CurrLon = (Val(GetXMLValueByTag("Current_Lon", Message)) / -1000000) - gLongitudeAdjust
        
        result.CurrLoc = GetXMLValueByTag("Current_Location", Message)
        
        result.CurrCity = GetXMLValueByTag("Current_City", Message)
        
        result.LastAVLUpdate = CDate(GetXMLValueByTag("LastAVLUpdate", Message))
        
        result.Speed = Val(GetXMLValueByTag("Speed", Message))
        
        result.Heading = Val(GetXMLValueByTag("Heading", Message))
    
        result.Altitude = Val(GetXMLValueByTag("Altitude", Message))
        
    End If

    ParseXMLPosition = result
    
End Function

Private Function GetXMLValueByTag(ByVal cXMLTag As String, ByVal cXMLString As String) As String
    Dim nStart As Integer
    Dim nStop As Integer
    
    On Error GoTo GetXMLValueByTag_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetXMLValueByTag: Node = <" & cXMLTag & "></" & cXMLTag & ">")
    End If
    
    GetXMLValueByTag = ""
    
    If InStr(1, cXMLString, "<" & cXMLTag & ">") > 0 Then
        nStart = InStr(1, cXMLString, "<" & cXMLTag & ">") + Len("<" & cXMLTag & ">")
        nStop = InStr(nStart, cXMLString, "</" & cXMLTag & ">")
        GetXMLValueByTag = Mid(cXMLString, nStart, nStop - nStart)
    End If
    
    Exit Function
    
GetXMLValueByTag_ERH:
    Call AddToDebugList("[ERROR] in GetXMLValueByTag: " & Err.Description & " (" & Err.Number & ") XMLString = " & cXMLString, lstDebug)
End Function

Private Function GetXMLAttributeByTag(ByVal cXMLTag As String, ByVal cXMLAttrib As String, ByVal cXMLString As String) As String
    Dim nStart As Integer
    Dim nStop As Integer
    Dim cAttrib As String
    Dim aAttribList() As String
    Dim aAttrib() As String
    Dim n As Integer
    
    On Error GoTo GetXMLValueByTag_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function GetXMLAttributeByTag: Node = <" & cXMLTag & "></" & cXMLTag & ">")
    End If
    
    GetXMLAttributeByTag = ""
    
    If InStr(1, cXMLString, "<" & cXMLTag & " ") > 0 Then
        nStart = InStr(1, cXMLString, "<" & cXMLTag & " ") + Len("<" & cXMLTag & " ")
        nStop = InStr(nStart, cXMLString, ">")
        cAttrib = Mid(cXMLString, nStart, nStop - nStart)
        If InStr(1, cAttrib, "=") > 0 Then
            cAttrib = FixXMLAttribString(cAttrib)
            aAttribList = Split(cAttrib, " ")
            For n = 0 To UBound(aAttribList, 1)
                If InStr(1, aAttribList(n), cXMLAttrib) > 0 Then
                    aAttrib = Split(aAttribList(n), "=")
                    GetXMLAttributeByTag = Replace(aAttrib(1), """", "")
                    Exit For
                End If
            Next n
        End If
    End If
    
    Exit Function
    
GetXMLValueByTag_ERH:
    Call AddToDebugList("[ERROR] in GetXMLAttributeByTag: " & Err.Description & " (" & Err.Number & ") XMLString = " & cXMLString, lstDebug)
End Function

Private Function GetBoundingBox(nLatitude As Double, nLongitude As Double, nRadius As Double) As tBoundingBox
    Dim nLatDistPerDegree As Double
    Dim nLonDistPerDegree As Double
    
    nLatDistPerDegree = Abs(DistanceDeg(nLatitude, nLongitude, 0, nLongitude) / nLatitude)           '   distance in meters per degree of latitude (calculated based on position to equator)
    nLonDistPerDegree = Abs(DistanceDeg(nLatitude, nLongitude, nLatitude, 0) / nLongitude)           '   distance in meters per degree of longitude (calculated based on position to prime meridian)
    
    GetBoundingBox.FromLat = nLatitude + (Sgn(nLatitude) * nRadius / nLatDistPerDegree)             '   adjust the lat/lon by +- the number of degrees in the nRadius distance requested
    GetBoundingBox.FromLon = nLongitude + (Sgn(nLongitude) * nRadius / nLonDistPerDegree)
    GetBoundingBox.ToLat = nLatitude - (Sgn(nLatitude) * nRadius / nLatDistPerDegree)
    GetBoundingBox.ToLon = nLongitude - (Sgn(nLongitude) * nRadius / nLonDistPerDegree)
    
    Debug.Print Chr(34) & "POLYGON ((" & GetBoundingBox.FromLon & " " & GetBoundingBox.FromLat & ", " _
                                        & GetBoundingBox.ToLon & " " & GetBoundingBox.FromLat & ", " _
                                        & GetBoundingBox.ToLon & " " & GetBoundingBox.ToLat & ", " _
                                        & GetBoundingBox.FromLon & " " & GetBoundingBox.ToLat & ", " _
                                        & GetBoundingBox.FromLon & " " & GetBoundingBox.FromLat & "))" & Chr(34)

End Function

Private Function FixXMLAttribString(ByVal cAttrib As String) As String

    On Error GoTo FixXMLAttribString_ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function FixXMLAttribString")
    End If

    Do While InStr(1, cAttrib, " =") > 0                '   trim off leading spaces
        cAttrib = Replace(cAttrib, " =", "=")
    Loop

    Do While InStr(1, cAttrib, "= ") > 0                '   trim off trailing spaces
        cAttrib = Replace(cAttrib, "= ", "=")
    Loop

    FixXMLAttribString = cAttrib
    
Exit Function

FixXMLAttribString_ERH:
    Call AddToDebugList("[ERROR] in FixXMLAttribString: " & Err.Description & " (" & Err.Number & ") cAttrib = " & cAttrib, lstDebug)
End Function

Private Function VisiCADMessageType(ByVal txtMSG As String)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function VisiCADMessageType")
    End If
    Select Case txtMSG
        Case "8000"
            VisiCADMessageType = "NP_READ_SQLTABLE"
        Case "9000"
            VisiCADMessageType = "NP_USER_DEFINED"
        Case "0001"
            VisiCADMessageType = "NP_TIME_UPDATE"
        Case "0002"
            VisiCADMessageType = "NP_READ_RESOURCETYPE"
        Case "0094"
            VisiCADMessageType = "NP_READ_RESOURCEGROUPS"
        Case "0121"
            VisiCADMessageType = "NP_READ_RESOURCEGROUPLIST"
        Case "0003"
            VisiCADMessageType = "NP_READ_SYSTEM_PREFS"
        Case "0005"
            VisiCADMessageType = "NP_AVL_UPDATE"
        Case "0006"
            VisiCADMessageType = "NP_MESSAGE_BROADCAST"
        Case "0009"
            VisiCADMessageType = "NP_MAIL_WAITING"
        Case "0010"
            VisiCADMessageType = "NP_SWITCH_MIRROREDSERVER"
        Case "0011"
            VisiCADMessageType = "NP_MAP_TO_MIRROR"
        Case "0012"
            VisiCADMessageType = "NP_READ_PAGER_GROUPS"
        Case "0014"
            VisiCADMessageType = "NP_READ_AGENCYTYPES"
        Case "0015"
            VisiCADMessageType = "NP_READ_JURISDICTIONS"
        Case "0016"
            VisiCADMessageType = "NP_READ_DIVISIONS"
        Case "0017"
            VisiCADMessageType = "NP_READ_BATTALIONS"
        Case "0018"
            VisiCADMessageType = "NP_READ_RESPONSEAREAS"
        Case "0027"
            VisiCADMessageType = "NP_READ_PRIORITY"
        Case "0064"
            VisiCADMessageType = "NP_READ_STATUS"
        Case "0065"
            VisiCADMessageType = "NP_READ_STATUS_ACTION"
        Case "0028"
            VisiCADMessageType = "NP_READ_PAGERMESSAGE_TYPE"
        Case "0029"
            VisiCADMessageType = "NP_READ_PAGING_SERVICE"
        Case "0030"
            VisiCADMessageType = "NP_READ_MACHINESETTINGS"
        Case "0034"
            VisiCADMessageType = "NP_READ_LOCATIONTYPE"
        Case "0069"
            VisiCADMessageType = "NP_READ_SHIFTTYPE"
        Case "0070"
            VisiCADMessageType = "NP_READ_CALLDISPOSITION"
        Case "0071"
            VisiCADMessageType = "NP_READ_CANCELREASON"
        Case "0072"
            VisiCADMessageType = "NP_READ_FACILITYCHANGE"
        Case "0073"
            VisiCADMessageType = "NP_READ_INVENTORY"
        Case "0074"
            VisiCADMessageType = "NP_READ_FTETYPES"
        Case "0075"
            VisiCADMessageType = "NP_READ_FIRSTRESPONSE"
        Case "0076"
            VisiCADMessageType = "NP_READ_INCIDENTTYPE"
        Case "0077"
            VisiCADMessageType = "NP_READ_DELAY"
        Case "0068"
            VisiCADMessageType = "NP_READ_OUTOFSERVICEREASONS"
        Case "0078"
            VisiCADMessageType = "NP_READ_METHODOFCALLRCVD"
        Case "0079"
            VisiCADMessageType = "NP_READ_RADIOCHANNELS"
        Case "0080"
            VisiCADMessageType = "NP_READ_ROLODEXCATEGORIES"
        Case "0081"
            VisiCADMessageType = "NP_READ_PROTOCOL"
        Case "0082"
            VisiCADMessageType = "NP_READ_UPDNGRADE"
        Case "0083"
            VisiCADMessageType = "NP_READ_FUNCTIONALITYGROUPS"
        Case "0084"
            VisiCADMessageType = "NP_READ_VEHICLEBODYTYPE"
        Case "0085"
            VisiCADMessageType = "NP_READ_VEHICLECERTIFICATION"
        Case "0086"
            VisiCADMessageType = "NP_READ_UNITNAMES"
        Case "0087"
            VisiCADMessageType = "NP_READ_HYDRANTCLASS"
        Case "0088"
            VisiCADMessageType = "NP_READ_BURNPERMITTYPE"
        Case "0089"
            VisiCADMessageType = "NP_READ_ALARMTYPES"
        Case "0090"
            VisiCADMessageType = "NP_READ_CALLERTYPES"
        Case "0091"
            VisiCADMessageType = "NP_READ_CAPABILITIES"
        Case "0092"
            VisiCADMessageType = "NP_READ_EMPLOYEECERT"
        Case "0055"
            VisiCADMessageType = "NP_READ_EMPPOSITION"
        Case "0093"
            VisiCADMessageType = "NP_READ_AUDITREASONS"
        Case "0095"
            VisiCADMessageType = "NP_READ_TRANSPORTMODES"
        Case "0097"
            VisiCADMessageType = "NP_READ_USERTIMESTAMP"
        Case "0163"
            VisiCADMessageType = "NP_READ_USERDATESTAMP"
        Case "0100"
            VisiCADMessageType = "NP_READ_BURN_PERMITS"
        Case "0101"
            VisiCADMessageType = "NP_READ_SHIFTNAME"
        Case "0106"
            VisiCADMessageType = "NP_READ_LISTBOXCOLUMNSETUP"
        Case "0111"
            VisiCADMessageType = "NP_READ_LAYOUTASSIGNMENTS"
        Case "0117"
            VisiCADMessageType = "NP_READ_PROBLEMS"
        Case "0118"
            VisiCADMessageType = "NP_READ_MODULES"
        Case "0120"
            VisiCADMessageType = "NP_READ_INCIDENTNUMBERSETUP"
        Case "0130"
            VisiCADMessageType = "NP_READ_SHORTHAND_COMMENTS"
        Case "0213"
            VisiCADMessageType = "NP_READ_MA911PROBLEM"
        Case "0214"
            VisiCADMessageType = "NP_READ_MA911PROBXREF"
        Case "0212"
            VisiCADMessageType = "NP_READ_MULTIAGENCY"
        Case "0218"
            VisiCADMessageType = "NP_READ_TRANSPRIORITY"
        Case "0022"
            VisiCADMessageType = "NP_READ_LOCATIONS"
        Case "0033"
            VisiCADMessageType = "NP_CLEAR_LOCATIONS"
        Case "0023"
            VisiCADMessageType = "NP_READ_LOCATIONPERSONNEL"
        Case "0024"
            VisiCADMessageType = "NP_READ_LOCATIONRESPONSEPLAN"
        Case "0026"
            VisiCADMessageType = "NP_READ_LOCATIONHAZMAT"
        Case "0216"
            VisiCADMessageType = "NP_READ_LOCATIONRESPONSEAREAS"
        Case "0032"
            VisiCADMessageType = "NP_READ_RESPONSEPLAN"
        Case "0107"
            VisiCADMessageType = "NP_READ_RESPONSEPLAN_STATIC"
        Case "0108"
            VisiCADMessageType = "NP_READ_RESPONSEPLAN_STATICALARM"
        Case "0021"
            VisiCADMessageType = "NP_READ_ALLVEHICLEINFO"
        Case "0020"
            VisiCADMessageType = "NP_READ_VEHICLE"
        Case "0244"
            VisiCADMessageType = "NP_READ_VEHICLE_STATUS"
        Case "0245"
            VisiCADMessageType = "NP_READ_VEHICLE_POSITION"
        Case "0007"
            VisiCADMessageType = "NP_VEHICLE_LATE"
        Case "0165"
            VisiCADMessageType = "NP_READ_VEHICLEOUTOFSERVICE"
        Case "0176"
            VisiCADMessageType = "NP_READ_VEHICLERADIOS"
        Case "0109"
            VisiCADMessageType = "NP_NEW_VEHICLE_SHIFT"
        Case "0110"
            VisiCADMessageType = "NP_REMOVE_VEHICLE_SHIFT"
        Case "0102"
            VisiCADMessageType = "NP_READ_VEHICLE_SHIFT"
        Case "0103"
            VisiCADMessageType = "NP_READ_VEHICLE_SHIFT_PERSONNEL"
        Case "0035"
            VisiCADMessageType = "NP_NEW_RESPONSE"
        Case "0036"
            VisiCADMessageType = "NP_REMOVE_RESPONSE"
        Case "0123"
            VisiCADMessageType = "NP_READ_RESPONSE_FIELDS"
        Case "0037"
            VisiCADMessageType = "NP_READ_RESPONSE_MASTER_INCIDENT"
        Case "0038"
            VisiCADMessageType = "NP_READ_RESPONSE_ALARMS"
        Case "0039"
            VisiCADMessageType = "NP_READ_RESPONSE_USER_TIMESTAMPS"
        Case "0040"
            VisiCADMessageType = "NP_READ_RESPONSE_PRESCHEDULED"
        Case "0152"
            VisiCADMessageType = "NP_READ_RESPONSE_DESTINATION"
        Case "0041"
            VisiCADMessageType = "NP_READ_RESPONSE_AUDIT"
        Case "0042"
            VisiCADMessageType = "NP_READ_RESPONSE_CALLBACKS"
        Case "0043"
            VisiCADMessageType = "NP_READ_RESPONSE_CHANGEDADDRESS"
        Case "0001"
            VisiCADMessageType = "RESPONSE_CHANGEDADDRESS_PICK"
        Case "0002"
            VisiCADMessageType = "RESPONSE_CHANGEDADDRESS_DEST"
        Case "0003"
            VisiCADMessageType = "RESPONSE_CHANGEDADDRESS_BOTH"
        Case "0044"
            VisiCADMessageType = "NP_READ_RESPONSE_PRIORITYCHANGE"
        Case "0045"
            VisiCADMessageType = "NP_READ_RESPONSE_VEHICLESASSIGN"
        Case "0046"
            VisiCADMessageType = "NP_READ_RESPONSE_TRANSPORT"
        Case "0047"
            VisiCADMessageType = "NP_READ_RESPONSE_TRANSPORTLEGS"
        Case "0048"
            VisiCADMessageType = "NP_READ_RESPONSE_PATIENT"
        Case "0066"
            VisiCADMessageType = "NP_READ_RESPONSE_COMMENTS"
        Case "0150"
            VisiCADMessageType = "NP_READ_RESPONSE_COMMENTS_NOTIFICATION"
        Case "0153"
            VisiCADMessageType = "NP_REMOVE_COMMENTS_NOTIFICATION"
        Case "0162"
            VisiCADMessageType = "NP_READ_RESPONSE_USERDATAFIELDS"
        Case "0122"
            VisiCADMessageType = "NP_RESPONSE_LATE"
        Case "0125"
            VisiCADMessageType = "NP_READ_RESPONSE_EDIT"
        Case "0131"
            VisiCADMessageType = "NP_READ_RESPONSE_ANIALI"
        Case "0126"
            VisiCADMessageType = "NP_READ_COMMAND_CONFIG"
        Case "0235"
            VisiCADMessageType = "NP_CHANGED_RESPONSE_DESTINATION"
        Case "0236"
            VisiCADMessageType = "NP_CHANGED_RESPONSE_PROBLEMNATURECHANGE"
        Case "0049"
            VisiCADMessageType = "UPDATE_CONTROLLER_VIEW"
        Case "0098"
            VisiCADMessageType = "VISICAD_IS_TERMINATING"
        Case "0067"
            VisiCADMessageType = "NP_READ_ALLPERSONNEL"
        Case "0031"
            VisiCADMessageType = "NP_READ_PERSONNEL"
        Case "0050"
            VisiCADMessageType = "NP_READ_EMPALTPOSITION"
        Case "0051"
            VisiCADMessageType = "NP_READ_EMPBATTALION"
        Case "0053"
            VisiCADMessageType = "NP_READ_EMPDIVISION"
        Case "0054"
            VisiCADMessageType = "NP_READ_EMPPAGEGROUP"
        Case "0056"
            VisiCADMessageType = "NP_READ_EMPPHONE"
        Case "0057"
            VisiCADMessageType = "NP_READ_EMPPASSWORD"
        Case "0058"
            VisiCADMessageType = "NP_READ_EMPHISTORY"
        Case "0052"
            VisiCADMessageType = "NP_READ_EMPCERTIFICATION"
        Case "0061"
            VisiCADMessageType = "GIS_ZOOM_ON_VEHICLE"
        Case "0062"
            VisiCADMessageType = "GIS_ZOOM_ON_INCIDENT"
        Case "0063"
            VisiCADMessageType = "GIS_ZOOM_TO_CLOSEST_VEHICLES"
        Case "0096"
            VisiCADMessageType = "GIS_ZOOM_ON_LATLON"
        Case "0159"
            VisiCADMessageType = "GIS_CLEAR_ON_LATLON"
        Case "0210"
            VisiCADMessageType = "GIS_ZOOM_ON_SSM_PLAN"
        Case "0119"
            VisiCADMessageType = "GIS_CLEAR_ADDRESS_FLAGS"
        Case "0226"
            VisiCADMessageType = "GIS_FAX_PRINT"
        Case "0019"
            VisiCADMessageType = "NP_READ_STATIONS"
        Case "0059"
            VisiCADMessageType = "NP_READ_STATIONBATTALIONS"
        Case "0060"
            VisiCADMessageType = "NP_READ_STATIONPOSITIONS"
        Case "0177"
            VisiCADMessageType = "NP_READ_STATIONRADIOS"
        Case "0112"
            VisiCADMessageType = "NP_READ_RA_SEGMENTS"
        Case "0113"
            VisiCADMessageType = "NP_READ_PIORITY_RESPONSETIMES"
        Case "0114"
            VisiCADMessageType = "NP_READ_RESPONSE_PLAN_LINK"
        Case "0115"
            VisiCADMessageType = "NP_READ_REGION_TYPE"
        Case "0116"
            VisiCADMessageType = "NP_READ_REGION_SEGEMENTS"
        Case "0124"
            VisiCADMessageType = "NP_READ_GA_STREETS"
        Case "0127"
            VisiCADMessageType = "NP_READ_NEW_CENTROID"
        Case "0128"
            VisiCADMessageType = "NP_READ_RA_CHIEFLINK"
        Case "0129"
            VisiCADMessageType = "EDITMGR_VIEW_RESPONSE"
        Case "0133"
            VisiCADMessageType = "NP_READ_SSMGROUPS"
        Case "0134"
            VisiCADMessageType = "NP_READ_SSMPLANS"
        Case "0198"
            VisiCADMessageType = "NP_READ_SSMCHANGEPLAN"
        Case "0147"
            VisiCADMessageType = "NP_REMOVE_SSMGROUP"
        Case "0148"
            VisiCADMessageType = "NP_REMOVE_SSMPLAN"
        Case "0145"
            VisiCADMessageType = "NP_REMOVE_ACTIVE_SSMPLAN"
        Case "0146"
            VisiCADMessageType = "NP_READ_ACITVE_SSMPLAN"
        Case "0135"
            VisiCADMessageType = "NP_READ_RESETTIMERREASONS"
        Case "0136"
            VisiCADMessageType = "NP_READ_ALARM_ZONES"
        Case "0137"
            VisiCADMessageType = "NP_READ_CAUTION_NOTES"
        Case "0138"
            VisiCADMessageType = "NP_RESET_LATE_TIMER"
        Case "0139"
            VisiCADMessageType = "NP_READ_COMMENTS"
        Case "0141"
            VisiCADMessageType = "NP_READ_MGMT_RULES"
        Case "0142"
            VisiCADMessageType = "NP_READ_MGMT_PRIORITIES"
        Case "0143"
            VisiCADMessageType = "NP_READ_MGMT_STATUS"
        Case "0144"
            VisiCADMessageType = "NP_READ_MGMT_AREA"
        Case "0149"
            VisiCADMessageType = "NP_READ_UNSENT_RESOURCES"
        Case "0151"
            VisiCADMessageType = "NP_AVAILABLE_RESOURCE"
        Case "0154"
            VisiCADMessageType = "NP_READ_MAPBOOK"
        Case "0155"
            VisiCADMessageType = "NP_ADD_DISPLAY_COMMENT_NOTE"
        Case "0156"
            VisiCADMessageType = "NP_REMOVE_DISPLAY_COMMENT_NOTE"
        Case "0157"
            VisiCADMessageType = "NP_ADD_VEHICLE_DISPLAY_COMMENT_NOTE"
        Case "0158"
            VisiCADMessageType = "NP_REMOVE_VEHICLE_DISPLAY_COMMENT_NOTE"
        Case "0160"
            VisiCADMessageType = "NP_READ_CROSS_STREET"
        Case "0161"
            VisiCADMessageType = "NP_READ_RESPONSE_AREA"
        Case "0164"
            VisiCADMessageType = "NP_READ_RESPONSELEVELASSIGNMENT"
        Case "0167"
            VisiCADMessageType = "NP_READ_START_DETECT_VERSION"
        Case "0168"
            VisiCADMessageType = "NP_READ_END_DETECT_VERSION"
        Case "0169"
            VisiCADMessageType = "NP_READ_UPDATE_VERSION"
        Case "0170"
            VisiCADMessageType = "NP_READ_MUST_RESTART"
        Case "0171"
            VisiCADMessageType = "NP_READ_DISPATCHER_NOTES"
        Case "0172"
            VisiCADMessageType = "NP_READ_GRIDPACK"
        Case "0173"
            VisiCADMessageType = "NP_READ_GRIDNAMES"
        Case "0174"
            VisiCADMessageType = "NP_CADMONITOR_RUNNING"
        Case "0181"
            VisiCADMessageType = "NP_APP_HEARTBEAT"
        Case "0175"
            VisiCADMessageType = "NP_READ_GRID"
        Case "0178"
            VisiCADMessageType = "NP_READ_MESSAGE_CHANGE"
        Case "0179"
            VisiCADMessageType = "NP_REMOVE_MESSAGE_CHANGE"
        Case "0180"
            VisiCADMessageType = "NP_READ_TIMEBLOCK"
        Case "0182"
            VisiCADMessageType = "NP_REMOVE_PATIENT"
        Case "0183"
            VisiCADMessageType = "NP_ACTIVATE_VISICAD_MAIN"
        Case "0184"
            VisiCADMessageType = "NP_ACTIVATE_WAITING_QUEUE"
        Case "0185"
            VisiCADMessageType = "NP_ACTIVATE_ACTIVE_QUEUE"
        Case "0186"
            VisiCADMessageType = "NP_ACTIVATE_UNIT_QUEUE"
        Case "0187"
            VisiCADMessageType = "NP_ACTIVATE_INCIDENT_EDITOR"
        Case "0188"
            VisiCADMessageType = "NP_ACTIVATE_EMERGENCY_CALL"
        Case "0189"
            VisiCADMessageType = "NP_ACTIVATE_SCHEDULED_CALL"
        Case "0190"
            VisiCADMessageType = "NP_ACTIVATE_JUR_DIVISION_SELECT"
        Case "0191"
            VisiCADMessageType = "NP_ACTIVATE_PAGING"
        Case "0192"
            VisiCADMessageType = "NP_ACTIVATE_HELP"
        Case "0193"
            VisiCADMessageType = "NP_ACTIVATE_TOOLS_LAUNCHER"
        Case "0194"
            VisiCADMessageType = "NP_ACTIVATE_UNSENT_RESOURCES"
        Case "0267"
            VisiCADMessageType = "NP_ACTIVATE_REPORTS_LAUNCHER"
        Case "0268"
            VisiCADMessageType = "NP_ACTIVATE_CALL_LOADING"
        Case "0201"
            VisiCADMessageType = "NP_ACTIVATE_CANCEL_RESPONSE"
        Case "0202"
            VisiCADMessageType = "NP_ACTIVATE_DUPLICATE_CALL"
        Case "0204"
            VisiCADMessageType = "NP_ACTIVATE_GIS"
        Case "0205"
            VisiCADMessageType = "NP_ACTIVATE_STATION_VIEWER"
        Case "0206"
            VisiCADMessageType = "NP_ACTIVATE_CARDFILE"
        Case "0207"
            VisiCADMessageType = "NP_ACTIVATE_ACTIVITY_LOG_VIEWER"
        Case "0208"
            VisiCADMessageType = "NP_ACTIVATE_PERSONNEL_MANAGER"
        Case "0209"
            VisiCADMessageType = "NP_ACTIVATE_VIEW_CONTROLLER"
        Case "0215"
            VisiCADMessageType = "NP_ACTIVATE_MULTIAGENCY"
        Case "0193"
            VisiCADMessageType = "NP_READ_OOS_LOG"
        Case "0194"
            VisiCADMessageType = "NP_READ_DIVERTREASONS"
        Case "0195"
            VisiCADMessageType = "NP_READ_SNAPSHOT_DISPATCH"
        Case "0196"
            VisiCADMessageType = "NP_READ_SNAPSHOT_CLOCK_START"
        Case "0197"
            VisiCADMessageType = "NP_READ_SHAPSHOT_CLOCK_START_END"
        Case "0199"
            VisiCADMessageType = "NP_PAINT"
        Case "0217"
            VisiCADMessageType = "NP_SECONDARY_ROUTE_RESULTS"
        Case "0218"
            VisiCADMessageType = "NP_READ_ROSTER_EXCEPTION"
        Case "0219"
            VisiCADMessageType = "NP_CHATMESSAGE"
        Case "0220"
            VisiCADMessageType = "NP_READ_FACILITY_REASONS"
        Case "0221"
            VisiCADMessageType = "NP_READ_FACILITY_STATUS"
        Case "0222"
            VisiCADMessageType = "NP_UPDATE_FACILITY_STATUS"
        Case "0223"
            VisiCADMessageType = "NP_READ_MACHINEMODULE"
        Case "0224"
            VisiCADMessageType = "NP_SEND_MSGBOX_TO_MACHINE"
        Case "0225"
            VisiCADMessageType = "NP_READ_ROSTERSCHEDULE"
        Case "0230"
            VisiCADMessageType = "NP_READ_EMPLOYEESCHEDULE"
        Case "0227"
            VisiCADMessageType = "NP_ACTIVATE_ROSTER_SYSTEM"
        Case "0192"
            VisiCADMessageType = "NP_ACTIVATE_SHIFT_MANAGER"
        Case "0228"
            VisiCADMessageType = "NP_READ_ROSTERTEMPLATE"
        Case "0229"
            VisiCADMessageType = "NP_RECORDCHECK"
        Case "0231"
            VisiCADMessageType = "NP_TRANSFER_INCIDENT"
        Case "0232"
            VisiCADMessageType = "NP_TRANSFER_CONFIRM"
        Case "0233"
            VisiCADMessageType = "NP_READ_TRANSFERREASON"
        Case "0234"
            VisiCADMessageType = "NP_READ_MULTI_ASSIGN_VEHICLE"
        Case "0237"
            VisiCADMessageType = "NP_READ_ROSTER_EXCEPTION"
        Case "0238"
            VisiCADMessageType = "NP_FAILOVER_IN_PROGRESS"
        Case "0239"
            VisiCADMessageType = "NP_FAILOVER_COMPLETED"
        Case "0240"
            VisiCADMessageType = "NP_READ_RESPONSEPLANSTATION"
        Case "0241"
            VisiCADMessageType = "NP_CADMON_SCHEDULE_VEHICLE_LATE_OS"
        Case "0242"
            VisiCADMessageType = "GIS_ZOOM_AT_INITIAL_ASSIGN"
        Case "0243"
            VisiCADMessageType = "NP_READ_RESPONSE_LICENSE_INFO"
        Case "0246"
            VisiCADMessageType = "NP_ZOOM_GIS"
        Case "0247"
            VisiCADMessageType = "NP_TESTSTATIONTONE"
        Case "0248"
            VisiCADMessageType = "NP_READ_XML"
        Case "0249"
            VisiCADMessageType = "NP_DATABASE_FREE_SPACE"
        Case "0250"
            VisiCADMessageType = "NP_INERFACE_MANUAL_SEND"
        Case "0251"
            VisiCADMessageType = "NP_ACTIVATE_DEPARTSCENE"
        Case "0252"
            VisiCADMessageType = "NP_ACTIVATE_STATIONASSIGN"
        Case "0253"
            VisiCADMessageType = "NP_READ_RESPONSE_USERCUSTOMDATA"
        Case "0254"
            VisiCADMessageType = "NP_ACTIVATE_CHG_TRANSPORT"
        Case "0255"
            VisiCADMessageType = "NP_READ_GENERIC_XML"
        Case "0256"
            VisiCADMessageType = "NP_SHOW_NOTES_INCIDENT"
        Case "0257"
            VisiCADMessageType = "NP_SHOW_NOTES_VEHICLE"
        Case "0258"
            VisiCADMessageType = "NP_ACTIVATE_COMMANDLINEACTION"
        Case "0259"
            VisiCADMessageType = "NP_PLAY_INCIDENT_LATE_SOUND"
        Case "0260"
            VisiCADMessageType = "NP_RECORDCHECK_RESULT"
        Case "0261"
            VisiCADMessageType = "NP_RECORDCHECK_CLIENT_RESULT"
        Case "0262"
            VisiCADMessageType = "NP_READ_VEHICLE_PERSONNEL_XML"
        Case "0263"
            VisiCADMessageType = "NP_TESTSTATIONPRINT"
        Case "0264"
            VisiCADMessageType = "NP_READ_ACTIVATE_EXIT_CAD"
        Case "0265"
            VisiCADMessageType = "NP_READ_ACTIVATE_LOGOFF_CAD"
        Case "0266"
            VisiCADMessageType = "NP_READ_ACTIVATE_LOGOFFON_CAD"
        Case "0267"
            VisiCADMessageType = "NP_ACTIVATE_REPORTS_LAUNCHER"
        Case "0268"
            VisiCADMessageType = "NP_ACTIVATE_CALL_LOADING"
        Case "0269"
            VisiCADMessageType = "NP_READ_GPSHEADINGSETUP"
        Case "0270"
            VisiCADMessageType = "NP_UPDATE_CURRENT_VIEW"
        Case "0271"
            VisiCADMessageType = "NP_UPDATE_MESSAGE"
        Case "0272"
            VisiCADMessageType = "NP_GROUP_VEHICLES_DISPATCHED"
        Case "0273"
            VisiCADMessageType = "NP_SEND_PAGE"
        Case "0274"
            VisiCADMessageType = "NP_SELF_INIT_DISPATCH"
        Case "0275"
            VisiCADMessageType = "NP_UPDATE_CALL_SCREEN"
        Case "0276"
            VisiCADMessageType = "NP_UPDATE_ADDRESS_INDICATOR"
        Case "0277"
            VisiCADMessageType = "NP_UPDATE_ADDRESS"
        Case "0278"
            VisiCADMessageType = "NP_REQUEST_TO_PULL"
        Case "0279"
            VisiCADMessageType = "NP_REQUEST_TO_PUSH"
        Case "0280"
            VisiCADMessageType = "NP_PERMISSION_TO_PULL"
        Case "0281"
            VisiCADMessageType = "NP_PERMISSION_TO_PUSH"
        Case "0282"
            VisiCADMessageType = "NP_DENY_PERMISSION_TO_PULL"
        Case "0283"
            VisiCADMessageType = "NP_DENY_PERMISSION_TO_PUSH"
        Case "0284"
            VisiCADMessageType = "NP_CDSU_SA_LOGGED_IN"
        Case "0285"
            VisiCADMessageType = "NP_IPC_CONNECTION_STATUS"
        Case "0286"
            VisiCADMessageType = "NP_UNIT_SECTOR_CHANGED"
        Case "0287"
            VisiCADMessageType = "NP_UNIT_DIVISION_CHANGED"
        Case "0288"
            VisiCADMessageType = "NP_MESSAGING_SERVER_START"
        Case "0289"
            VisiCADMessageType = "NP_MESSAGING_SERVER_STOP"
        Case "0290"
            VisiCADMessageType = "NP_APPLY_IMPEDANCEUPDATE"
        Case "0291"
            VisiCADMessageType = "NP_READ_RESPONSE_UNIT_PRIORITYCHANGE"
        Case "0292"
            VisiCADMessageType = "NP_UPDATE_LOWER_PRIORITY_RULES"
        Case "0293"
            VisiCADMessageType = "GIS_UNIT_CENTERING"
        Case "0294"
            VisiCADMessageType = "NP_COMMANDLINE_QUICKMESSAGE"
        Case "0295"
            VisiCADMessageType = "NP_MESSAGING_NOTIFY_RECONNECT"
        Case "0296"
            VisiCADMessageType = "NP_CLOSE_GIS"
        Case "0297"
            VisiCADMessageType = "NP_READ_SECTOR_PERSONNEL"
        Case "0298"
            VisiCADMessageType = "NP_CLOSE_GIS_FOR_LOGOFF"
        Case "0299"
            VisiCADMessageType = "NP_RESPONSE_REASSIGN"
        Case "0300"
            VisiCADMessageType = "NP_UNIT_HOMESECTOR_CHANGED"
        Case "0301"
            VisiCADMessageType = "NP_LOAD_TIMESTAMP_VALUES"
        Case "0302"
            VisiCADMessageType = "NP_READ_HYDRANT_INFO"
        Case "0303"
            VisiCADMessageType = "NP_WIQ_INCIDENT_LIST"
        Case "0304"
            VisiCADMessageType = "NP_WIQ_RECONCILE_LIST"
        Case "0305"
            VisiCADMessageType = "NP_WIQ_ADDINCIDENT"
        Case "0306"
            VisiCADMessageType = "NP_VEHICLE_EMERGENCY_ACTIVATION"
        Case "0307"
            VisiCADMessageType = "NP_SET_NOTIFICATION_STATUS"
        Case "0308"
            VisiCADMessageType = "NP_READ_STATION_CROSS_STAFF"
        Case "0309"
            VisiCADMessageType = "NP_UPDATE_STATION_CROSS_STAFF"
        Case "0310"
            VisiCADMessageType = "NP_READ_RESPONSE_CASENUMBERS"
        Case "0311"
            VisiCADMessageType = "NP_UPDATE_DISPATCH_LEVEL"
        Case "0312"
            VisiCADMessageType = "NP_READ_RESPONSE_VEHICLESSTACK"
        Case "0313"
            VisiCADMessageType = "NP_READ_VEHICLE_STACKEDRESPONSES"
        Case "0314"
            VisiCADMessageType = "NP_READ_AGENCY_SETUPVALUE"
        Case "0315"
            VisiCADMessageType = "NP_READ_RESPONSE_FOR_VEHICLE"
        Case "0316"
            VisiCADMessageType = "NP_UPDATE_POWERLINE_COMMAND"
        Case "0317"
            VisiCADMessageType = "NP_UPDATE_POWERLINE_PERMISSION"
        Case "0318"
            VisiCADMessageType = "NP_READ_DISPATCH_LEVEL"
        Case "0319"
            VisiCADMessageType = "NP_READ_MESSAGING_ICON_FLASH_SETUP"
        Case "0320"
            VisiCADMessageType = "NP_READ_DASHBOARD_SETUP"
        Case "0321"
            VisiCADMessageType = "NP_READ_PSAPJURISDICTIONS"
        Case "0322"
            VisiCADMessageType = "NP_READ_PSAP_CODES"
        Case "0323"
            VisiCADMessageType = "NP_EXTERNALLY_UPDATE_SHOWRIP"
        Case "0324"
            VisiCADMessageType = "NP_READ_VEHICLE_CONFIGURATION_TYPES"
        Case "0325"
            VisiCADMessageType = "NP_READ_POWERLINE_SHORT_CUT_KEYS"
        Case "0326"
            VisiCADMessageType = "NP_READ_CAUTIONNOTECATEGORIES"
        Case "0327"
            VisiCADMessageType = "NP_READ_CAUTIONNOTESOURCES"
        Case "0328"
            VisiCADMessageType = "NP_UPDATE_AUTOMATIC_DISPATCH_STATUS"
        Case "0329"
            VisiCADMessageType = "NP_UPDATE_AUTOMATIC_DISPATCH_ICON"
        Case "0330"
            VisiCADMessageType = "NP_READ_RESPONSE_CHANGED_RESPONSEPLAN"
        Case "0331"
            VisiCADMessageType = "NP_UPDATE_SSMPLAN_OVERLOAD"
        Case "0332"
            VisiCADMessageType = "NP_READ_LOCAL_MAILROOM_MESSAGE_COUNTS"
        Case "0333"
            VisiCADMessageType = "NP_READ_UNIT_SWAP_RECOMMENDATION"
        Case "3001"
            VisiCADMessageType = "NP_MESSAGESWITCH_ANIALI"
        Case "3002"
            VisiCADMessageType = "NP_MESSAGESWITCH_UPDATELATLON"
        Case "3003"
            VisiCADMessageType = "NP_MESSAGESWITCH_ROUTING_REQUEST"
        Case "3004"
            VisiCADMessageType = "NP_MESSAGESWITCH_ROUTING_UPDATE"
        Case "3005"
            VisiCADMessageType = "NP_MESSAGESWITCH_CALLALERT"
        Case "3006"
            VisiCADMessageType = "NP_MESSAGESWITCH_VOCALARM_STATIONUPDATE"
        Case "3007"
            VisiCADMessageType = "NP_MESSAGESWITCH_AUTODIAL"
        Case "3008"
            VisiCADMessageType = "NP_MESSAGESWITCH_OPENRADIOCHANNEL"
        Case "3009"
            VisiCADMessageType = "NP_MESSAGESWITCH_RADIOMESSAGE** DUPLICATE WITH NP_REQUEST_ANIALI"
        Case "3009"
            VisiCADMessageType = "NP_REQUEST_ANIALI** DUPLICATE WITH  NP_MESSAGESWITCH_RADIOMESSAGE"
        Case "3010"
            VisiCADMessageType = "NP_MESSAGESWITCH_MANUALAVLPOLL"
        Case "3011"
            VisiCADMessageType = "NP_MESSAGESWITCH_RECORDSCHECK"
        Case "3012"
            VisiCADMessageType = "NP_MESSAGESWITCH_RECORDSCHECK_LOGON"
        Case "3013"
            VisiCADMessageType = "NP_MESSAGESWITCH_RECORDSCHECK_LOGOFF"
        Case "3014"
            VisiCADMessageType = "NP_MESSAGESWITCH_RECORDSCHECK_CHG_PASSWORD"
        Case "3015"
            VisiCADMessageType = "NP_MESSAGESWITCH_ANIALI_COMMENT"
        Case "3016"
            VisiCADMessageType = "NP_MESSAGESWITCH_SENDSTATIONDATA"
        Case "3017"
            VisiCADMessageType = "NP_MESSAGESWITCH_MANUALPRINTFORSTATIONALERT"
        Case "8000"
            VisiCADMessageType = "NP_READ_SQLTABLE"
        Case "9000"
            VisiCADMessageType = "NP_USER_DEFINED"
        Case "9700"                                                 '   9700 - 9749 are for Frozen Frog
            VisiCADMessageType = "NP_FROZENFROG_MESSAGE_CLASS"
        Case "9750"                                                 '   9750 - 9799 are for Brimac Systems
            VisiCADMessageType = "NP_BRIMAC_MESSAGE_CLASS"
        Case Else
            VisiCADMessageType = "UNDOCUMENTED VISICAD MESSAGE TYPE"
    End Select
End Function

Private Sub ExitApplication(Optional ByVal bSaveSettings As Boolean = True)
    Dim Test As Boolean
    
    On Error Resume Next
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ExitApplication")
    End If
    
    '    map.ActiveMap.Saved = True
    '    mapInc.ActiveMap.Saved = True
    If bSaveSettings Then
        If gSavePosition = "Y" Then
            If Me.WindowState <> vbMinimized Then
                Test = bsiPutSettings("SystemDefaults", "StartupPosition", Trim(Str(frmMain.Top)) & "|" & Trim(Str(frmMain.Left)), gSystemDirectory & "\Simulator.INI")
            Else
                Call AddToDebugList("[INFO] Form MINIMIZED at Exit. Screen position (" & Trim(Str(frmMain.Top)) & "|" & Trim(Str(frmMain.Left)) & ") NOT SAVED.", lstDebug)
            End If
        End If
        
        If tcpSock.State <> sckClosed Then
            Call DisconnectServer
            Call DisconnectAVL
        End If
        If gbLogging Then
            AddToDebugList "[NORMAL SHUTDOWN]", lstDebug
            AddToTCPList "9000CAD North Application Shutdown", lstTCP
    '        Close #gTCPLog
    '        Close #gDebugLog
    '        Close #gInterfaceLog
        End If
    End If
    
    End
End Sub

Private Sub tcpSock_SendComplete()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tcpSock_SendComplete")
    End If
    '   AddToDebugList "[tcpsock: Send complete]"
End Sub

Private Sub tcpSock_SendProgress(ByVal bytesSent As Long, ByVal bytesRemaining As Long)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tcpSock_SendProgress")
    End If
    '   AddToDebugList "[tcpsock: Sending] Sent=" & bytesSent & "  Remaining=" & bytesRemaining
End Sub

'Private Function bsiGetSettings(Section As String, Item As String, Default As String, INIFile As String) As String
'    Dim ParamString As String
'    Dim nBytes As Integer
'
'    ParamString = Space(2000)
'
'    nBytes = GetPrivateProfileString(Section, Item, Default, ParamString, 2000, INIFile)
'
'    If nBytes > 0 Then
'        bsiGetSettings = Left(ParamString, nBytes)
'    End If
'
'End Function
'
'Private Function bsiPutSettings(Section As String, KeyName As String, Value As String, INIFile As String) As Boolean
'    Dim nBytes As Long
'
'    bsiPutSettings = False
'
'    nBytes = WritePrivateProfileString(Section, KeyName, Value, INIFile)
'
'    If nBytes <> 0 Then
'        bsiPutSettings = True
'    End If
'
'End Function
'
Private Sub tmCADMsg_Timer()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tmCADMsg_Timer")
    End If
    '   Call ProcessCADMessages
    '   Call NewProcessCADMessages
End Sub

Private Sub tmMapRefresh_Timer()
    Dim cError As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tmMapRefresh_Timer")
    End If
    
    If gMapRefreshRequired Then
        Call cnUpdateUnitLocations(cError)
        If cError <> "" Then
            AddToDebugList ("[ERROR in tmMapRefresh_Timer()] Err: " & cError), lstDebug
        End If
    End If
    
End Sub

Private Sub tmUpdateAVL_Timer()
    Dim CurrentTime As String
    Dim oItem As ListItem
    Dim nIndex As Integer
    Dim AllDone As Boolean
    Dim cError As String
    
    On Error GoTo ErrorHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tmUpdateAVL_Timer")
    End If
    
    tmUpdateAVL.Enabled = False                     '   prevent being called by the timer while we're still here
    
    If Not gStopProcessing Then
        gScriptTime = Now
    End If
    
    CurrentTime = Format(gScriptTime, DATE_FORMAT)
    sBar.Panels(1).Text = Format(gScriptTime, "hh:nn:ss")
    
    Do While lvEvents.ListItems.count > 0                                       '   if there are items still in the list
        If lvEvents.ListItems.Item(1).SubItems(LVSort) <= CurrentTime Then      '   see if any meet the current time criteria
        
            Call SendToAVLNoWait(lvEvents.ListItems.Item(1).SubItems(LVType), _
                                        lvEvents.ListItems.Item(1).SubItems(LVMessage))         '   send them
            lvEvents.ListItems.Remove (1)                                                       '   then remove them
            
        Else
            Exit Do                                 '   exit if no more records meet the current time criteria
        End If
    Loop
    
    tmUpdateAVL.Enabled = True                      '   re-enable the time so we can be called again later
    
    sBar.Panels(4).Text = lvEvents.ListItems.count
    
'    If gStopProcessing Then                         '   *** NEEDS WORK *** there needs to be a better way to do this. Pausing the application should not
'        Call cmdPause_Click                         '   stop all messages from being sent. It should stop the clock so that any lag
'    End If                                          '   in processing messages can still be dealt with.
    
    DoEvents
    
    Exit Sub
    
ErrorHandler:

    AddToDebugList ("[ERROR in tmUpdateAVL_Timer()] Err:" & Str(Err.Number) & ", " & Err.Description), lstDebug
    
    tmUpdateAVL.Enabled = True                      '   re-enable the time so we can be called again later

End Sub

Private Sub bsiProcessBrimacMessage(sMessage As String)
    Dim aMsg As Variant
    Dim xItem As ListItem
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub bsiProcessBrimacMessage")
    End If
    
    aMsg = Split(sMessage, "|")
    
    On Error GoTo ErrHand
    
    Select Case aMsg(0)
        Case Val(NP_BSI_OLDINC_NEWINC_XREF)
            For n = 0 To UBound(gOldIncInfo)
                If gOldIncInfo(n).OldID = Val(aMsg(1)) And gOldIncInfo(n).NewID = 0 Then
                    gOldIncInfo(n).NewID = Val(aMsg(2))
                    Exit For
                End If
            Next n
        Case Val(NP_BSI_INITIALIZATION_COMPLETE)
        
            Set xItem = lvEvents.ListItems.Add(, , Format(gScriptTime, "hh:nn:ss"))
            xItem.SubItems(LVUnit) = ""
            xItem.SubItems(LVType) = Format(EV_INIT_DONE, "0000")
            xItem.SubItems(LVDur) = "INITDONE"
            xItem.SubItems(LVDist) = "INITDONE"
            xItem.SubItems(LVMessage) = "Scenario Startup: Initialization Complete. Waiting for interface to catch up"
            xItem.SubItems(LVSort) = Format(gScriptTime, DATE_FORMAT)
        Case Val(NP_BSI_RADIO_MESSAGE)
    
    End Select
    
    Exit Sub
    
ErrHand:
    AddToDebugList ("ERROR in bsiProcessBrimacMessage: n=" & Str(n) & ", sMessage=>" & sMessage & "<"), lstDebug
    AddToDebugList ("ERROR in bsiProcessBrimacMessage: " & Str(Err.Number) & ", " & Err.Description), lstDebug
End Sub

Private Function bsiFindOldIncID(nNewIncID As Long) As Long
    Dim n As Long
    
    On Error GoTo ERHand
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function bsiFindOldIncID")
    End If
    
    bsiFindOldIncID = -1        '   assume we won't find it for now
    
    For n = 0 To UBound(gOldIncInfo)
        If gOldIncInfo(n).NewID = nNewIncID Then
            bsiFindOldIncID = n
            Exit For
        End If
    Next n
    
    Exit Function
    
ERHand:

    AddToDebugList ("ERROR in bsiFindOldIncID: " & Str(Err.Number) & ", " & Err.Description), lstDebug
    bsiFindOldIncID = -1

End Function

Private Function bsiValidateTime(ByRef TimeString As String) As Boolean
    Dim hour As Integer
    Dim minute As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function bsiValidateTime")
    End If
    
    bsiValidateTime = False
    
    If TimeString <> "" Then
        Select Case Len(TimeString)
            Case 2
                If IsNumeric(TimeString) Then
                    hour = Val(TimeString)
                    If hour >= 0 And hour < 24 Then
                        TimeString = Format(hour, "00") & ":00"
                        bsiValidateTime = True
                    Else
                        MsgBox "Invalid Time (use 24-hour clock)", vbCritical + vbOKOnly, "Time Error"
                    End If
                Else
                    MsgBox "Invalid Time (use 24-hour clock)", vbCritical + vbOKOnly, "Time Error"
                End If
            Case 4
                If IsNumeric(TimeString) Then
                    hour = Val(Left(TimeString, 2))
                    If hour >= 0 And hour < 24 Then
                        minute = Val(Right(TimeString, 2))
                        If minute >= 0 And minute < 60 Then
                            TimeString = Format(hour, "00:") & Format(minute, "00")
                            bsiValidateTime = True
                        Else
                            MsgBox "Invalid Time (use 24-hour clock)", vbCritical + vbOKOnly, "Time Error"
                        End If
                    End If
                Else
                    MsgBox "Invalid Time (use 24-hour clock)", vbCritical + vbOKOnly, "Time Error"
                End If
            Case 5
                If Mid(TimeString, 3, 1) = ":" Then
                    If IsNumeric(Left(TimeString, 2)) Then
                        hour = Val(Left(TimeString, 2))
                        If hour >= 0 And hour < 24 Then
                            minute = Val(Right(TimeString, 2))
                            If minute >= 0 And minute < 60 Then
                                TimeString = Format(hour, "00:") & Format(minute, "00")
                                bsiValidateTime = True
                            Else
                                MsgBox "Invalid Time (use 24-hour clock)", vbCritical + vbOKOnly, "Time Error"
                            End If
                        End If
                    Else
                        MsgBox "Invalid Time (use 24-hour clock)", vbCritical + vbOKOnly, "Time Error"
                    End If
                Else
                    MsgBox "Invalid Time (use 24-hour clock)", vbCritical + vbOKOnly, "Time Error"
                End If
            Case Else
                MsgBox "Invalid Time (use 24-hour clock)", vbCritical + vbOKOnly, "Time Error"
        End Select
    End If

End Function

Private Function bsiGetPriority(ByVal nPriority As Long, ByVal AgencyID As Long) As tPriority
    Dim n As Integer
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function bsiGetPriority")
    End If
    
    bsiGetPriority.BackColor = 1
    bsiGetPriority.ForeColor = 1
    
    For n = 0 To UBound(gPriority)
        If gPriority(n).PNumber = nPriority And gPriority(n).AgencyID = AgencyID Then
            bsiGetPriority = gPriority(n)
            Exit For
        End If
    Next n
    
    Exit Function

ERH:
    AddToDebugList "Error(GetPriority): " & Str(Err.Number) & "  " & Err.Description, lstDebug
End Function

Private Function Check110Version() As Boolean
    Dim cSQL As String
    Dim aSQL As Variant
    Dim nRows As Long
    
    On Error GoTo ErrHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function Check110Version")
    End If
    
    Check110Version = True
    
    cSQL = "select COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH " _
            & " From INFORMATION_SCHEMA.Columns " _
            & " WHERE TABLE_NAME = 'response_master_incident' " _
            & " and Column_Name = 'UnReadIncident' "
    
    '    cSQL = "Select UnreadIncident from Response_Master_Incident"
    
    nRows = bsiSQLGet(cSQL, aSQL, gConnectString, 1)
    
    If nRows <= 0 Then
        Check110Version = False
        Call AddToDebugList("CheckVersion: Pre-1.10 Non-Controlling Dispatch System detected", lstDebug)
    Else
        Call AddToDebugList("CheckVersion: Command 1.10 Dispatch System detected", lstDebug)
    End If
    
    Exit Function

ErrHandler:
        Check110Version = False
        Call AddToDebugList("ERROR in CheckVersion: Pre-1.10 Non-Controlling Dispatch System detected", lstDebug)
End Function

Private Function ValidateSpeeds(nSpeed As Integer) As Integer
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ValidateSpeeds")
    End If
    
    If nSpeed < 5 Then
        nSpeed = 5
    End If
    If nSpeed > 200 Then
        nSpeed = 200
    End If
    
    ValidateSpeeds = nSpeed
    
End Function

Private Sub txtAVLUpdateLimit_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtAVLUpdateLimit_Change")
    End If
    cmdSaveESRI.Enabled = True
End Sub

Private Sub txtChannelName_LostFocus()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtChannelName_LostFocus")
    End If
    If lvRadioChannel.ListItems.count > 0 Then
        lvRadioChannel.SelectedItem.SubItems(2) = txtChannelName.Text
    End If
End Sub

Private Sub txtControlName_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtControlName_Change")
    End If
    cmdRadioSave.Enabled = True
    cmdRadioReset.Enabled = True
End Sub

Private Sub txtESRINetworkDataset_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtESRINetworkDataset_Change")
    End If
    cmdSaveESRI.Enabled = True
End Sub

Private Sub txtFrom_GotFocus()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtFrom_GotFocus")
    End If
    txtFrom.SelStart = 0
    txtFrom.SelLength = Len(txtFrom.Text)
End Sub

Private Sub txtFrom_LostFocus()
    Dim TimeString As String
        
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtFrom_LostFocus")
    End If
        
    If txtFrom.Text <> "" Then
        TimeString = txtFrom.Text
        
        If bsiValidateTime(TimeString) Then
            txtFrom.Text = TimeString
        End If
    End If
End Sub

Private Sub txtTo_GotFocus()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtTo_GotFocus")
    End If
    txtTo.SelStart = 0
    txtTo.SelLength = Len(txtTo.Text)
End Sub

Private Sub txtTo_LostFocus()
    Dim TimeString As String
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtTo_LostFocus")
    End If
    
    If txtTo.Text <> "" Then
        TimeString = txtTo.Text
        
        If bsiValidateTime(TimeString) Then
            txtTo.Text = TimeString
        End If
    End If
End Sub

Private Sub txtProfile_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtProfile_Change")
    End If
    If txtInterstate.Text <> "" And _
        txtHighway.Text <> "" And _
        txtLimAccess.Text <> "" And _
        txtArterial.Text <> "" And _
        txtStreet.Text <> "" And _
        txtProfile.Text <> "" Then
        
        cmdSaveProfile.Enabled = True
        
    End If
End Sub

Private Sub txtTransmit_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtTransmit_Change")
    End If
    If lblRadioUnit.Caption <> "" Then
        If txtTransmit.Text <> "" Then
            cmdTransmit.Enabled = True
        Else
            cmdTransmit.Enabled = False
        End If
    Else
        cmdTransmit.Enabled = False
    End If
End Sub

Private Sub txtWSName_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub txtWSName_Change")
    End If
    cmdWSSave.Enabled = True
    cmdWSCancel.Enabled = True
End Sub

Private Function ValidateLicense() As Boolean
    Dim KeyInfo As tLicenseInfo
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ValidateLicense")
    End If
    
    gLicensedVersion = cnValidateLicense(KeyInfo, "bsiSimulator")
    
    If gLicensedVersion Then
    
        gClientInfo = KeyInfo.ClientName & ", " & KeyInfo.ClientCity & " " & KeyInfo.ClientProv
        gSiteInfo = KeyInfo.ClientSite
        gLicenseExpires = KeyInfo.AppExpires
        gSerialNumber = KeyInfo.AppSerial
        gLicenseMode = KeyInfo.AppMode
        gClientLogo = KeyInfo.ClientLogo
        gClientLat = KeyInfo.SiteLat
        gClientLong = KeyInfo.SiteLong
        gClientRadius = KeyInfo.Radius
        gClientGeoBase = KeyInfo.GeoBase

        Call AddToDebugList("[Licensed to] " & gClientInfo, lstDebug)

    End If

    ValidateLicense = gLicensedVersion

End Function

'Private Function ValidateLicense() As Boolean
'    Dim KeyInfo As LicenseInfo
'    Dim LicenseBuffer As Variant
'
'    If Dir(App.Path & "\cnLicenseKey.bin") <> "" Then
'        gLicensedVersion = GetKey(LicenseBuffer, App.Path & "\cnLicenseKey.bin")
'    Else
'        gLicensedVersion = GetKey(LicenseBuffer, App.Path & "\bsiKey.bin")
'    End If
'
'    If gLicensedVersion Then
'        KeyInfo = DecryptKey(LicenseBuffer)
'
'        gClientInfo = KeyInfo.ClientName & ", " & KeyInfo.ClientCity & " " & KeyInfo.ClientProv
'        gSiteInfo = KeyInfo.ClientSite
'        gLicenseExpires = KeyInfo.AppExpires
'        gSerialNumber = KeyInfo.AppSerial
'        gLicenseMode = KeyInfo.AppMode
'        gClientLogo = KeyInfo.ClientLogo
'        gClientLat = KeyInfo.SiteLat
'        gClientLong = KeyInfo.SiteLong
'        gClientRadius = KeyInfo.Radius
'        gClientGeoBase = KeyInfo.GeoBase
'
'        Call AddToDebugList("[Licensed to] " & gClientInfo)
'
'        If KeyInfo.AppName = "bsiSimulator" Then
'            If KeyInfo.AppMode = "DEMO" Then
'                If Now > KeyInfo.AppExpires Then
'                    Call MsgBox("Sorry, the evaluation period for cnSimulator has expired." _
'                    & Chr(13) & "           Connection to VisiCAD has been disabled." & Chr(13) _
'                    & Chr(13) & "              Please contact CAD North Inc." _
'                    & Chr(13) & "                   support@cadnorth.com" _
'                    & Chr(13) & "                 for further information.", vbOKOnly + vbCritical, "Expired DEMO")
'                    gLicensedVersion = False
'                    Call AddToDebugList("[License] DEMO Expired - " & gLicenseExpires)
'                End If
'            End If
'        Else
'            Call MsgBox("Sorry, but the key provided for this application is invalid." _
'            & Chr(13) & "           Connection to VisiCAD has been disabled." & Chr(13) _
'            & Chr(13) & "              Please contact CAD North Inc." _
'            & Chr(13) & "                   support@cadnorth.com" _
'            & Chr(13) & "                 for further information.", vbOKOnly + vbCritical, "Expired DEMO")
'            gLicensedVersion = False
'            Call AddToDebugList("[License] Key is NOT for this product: " & KeyInfo.AppName)
'        End If
'    Else
'        Call MsgBox("Sorry, the key provided for this application is missing or invalid." _
'        & Chr(13) & "           Connection to VisiCAD has been disabled." & Chr(13) _
'        & Chr(13) & "              Please contact CAD North Inc." _
'        & Chr(13) & "                   support@cadnorth.com" _
'        & Chr(13) & "                 for further information.", vbOKOnly + vbCritical, "Expired DEMO")
'        gLicensedVersion = False
'        Call AddToDebugList("[License] Key is invalid or missing")
'    End If
'
'    ValidateLicense = gLicensedVersion
'
'End Function
'
Private Function bsiIsDigit(cChar As String) As Boolean
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function bsiIsDigit")
    End If
    bsiIsDigit = False
    
    If InStr(1, "0123456789", cChar) > 0 Then
        bsiIsDigit = True
    End If
        
End Function

Private Function bsiIsAlpha(cChar As String) As Boolean
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function bsiIsAlpha")
    End If
    bsiIsAlpha = False
    
    If Asc(cChar) >= Asc("A") And Asc(cChar) <= Asc("z") Then
        bsiIsAlpha = True
    End If
        
End Function

Private Sub udInterMessage_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub udInterMessage_Change")
    End If
    txtInterMessage.Text = udInterMessage.Value
End Sub

Private Sub udSpeechRate_Change()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub udSpeechRate_Change")
    End If
    txtSpeechRate.Text = udSpeechRate.Value
End Sub


'---------------------------------------------------------------------------------------
' Procedure :   SearchFile
' DateTime  :   1/9/2007 13:51
' Author    :   Brian McGrath
' Copyright :   CAD North Inc. 2006
' Purpose   :
'---------------------------------------------------------------------------------------
'
Private Function SearchFile(cPath As String, cFileMask As String, ByRef ReturnPath As String, ByRef ReturnDate As Variant) As Variant
    Dim Temp As String
    Dim Done As Boolean
    Dim Folders() As String
    Dim Files() As String
    Dim nDir As Long
    Dim n As Long
    Dim nFile As Long
    Dim nAttrib As Integer
    Dim dFileDate As Variant
    Dim localDate As Variant
    Dim cFilePath As String
    
    Dim ErrorMsg As String

    On Error GoTo SearchFile_ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function SearchFile")
    End If

    ErrorMsg = "Start of SearchFile"

    If Right(cPath, 1) <> "\" Then cPath = cPath & "\"

    Temp = Dir(cPath & "*.*", vbDirectory + vbArchive + vbNormal + vbReadOnly)
    
    While Temp <> ""
        If Temp <> "." And Temp <> ".." Then
            nAttrib = GetAttr(cPath & "\" & Temp)
            If (nAttrib And vbDirectory) <> 0 Then
                ReDim Preserve Folders(0 To nDir)
                Folders(nDir) = cPath & Temp
                nDir = nDir + 1
            Else
                ReDim Preserve Files(0 To nFile)
                Files(nFile) = cPath & Temp
                If UCase(Temp) = UCase(cFileMask) Then
                    ReturnDate = FileDateTime(Files(nFile))
                    ReturnPath = Files(nFile)
                    Call AddToDebugList("[SUCCESS] '" & ReturnPath & "' Dated: " & Format(ReturnDate, "YYYY-MM-DD HH:NN:SS"), lstDebug)
                    SearchFile = ReturnDate
                End If
                nFile = nFile + 1
            End If
        End If
        Temp = Dir
    Wend
    
    If nDir > 0 Then
        For n = 0 To UBound(Folders)
                
            ErrorMsg = "Recursing " & Folders(n)
        
            localDate = SearchFile(Folders(n), cFileMask, cFilePath, dFileDate)
            If localDate > ReturnDate Then
                ReturnDate = dFileDate
                ReturnPath = cFilePath
                SearchFile = ReturnDate
            End If
        
        Next n
    End If

    On Error GoTo 0
    Exit Function

SearchFile_ERH:

    Call AddToDebugList("[ERROR] in procedure SearchFile of Form frmMain:" & Err.Number & ", " & Err.Description _
                            & " - " & ErrorMsg, lstDebug)

End Function

'---------------------------------------------------------------------------------------
' Procedure :   GetGeographicOffsets
' DateTime  :   1/17/2007 10:27
' Author    :   Brian McGrath
' Copyright :   CAD North Inc. 2006
' Purpose   :
'---------------------------------------------------------------------------------------
'
Private Sub GetGeographicOffsets(cOffsetFile As String, ByRef dLatitude As Double, ByRef dLongitude As Double)
    Dim nFile As Integer
    Dim dSomeDate As Variant
    Dim OffLat As Long
    Dim OffLon As Long
    
    Dim ErrorMsg As String

    On Error GoTo GetGeographicOffsets_ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub GetGeographicOffsets")
    End If

    ErrorMsg = "Start of GetGeographicOffsets"

    dSomeDate = FileDateTime(cOffsetFile)
    Call AddToDebugList("[GEOFIX] Offset File: " & cOffsetFile & " - " & Format(dSomeDate, "YYYY-MM-DD HH:NN:SS"), lstDebug)
    nFile = FreeFile
    Open cOffsetFile For Binary Access Read Shared As #nFile
    Get #nFile, , OffLon
    Get #nFile, , OffLat
    Close #nFile
    dLatitude = OffLat / 1000000
    dLongitude = OffLon / -1000000

    On Error GoTo 0
    Exit Sub

GetGeographicOffsets_ERH:

    Call AddToDebugList("[ERROR] in procedure GetGeographicOffsets of Form frmMain:" & Err.Number & ", " & Err.Description _
                            & " - " & ErrorMsg, lstDebug)

End Sub

Private Sub tbTools_ButtonClick(ByVal Button As MSComctlLib.Button)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub tbTools_ButtonClick")
    End If
    Select Case Button.Key
        Case "ZoomIn"     '   zoom in
            winMap1.CursorMode = cmZoomIn
        Case "ZoomOut"    '   zoom out
            winMap1.CursorMode = cmZoomOut
        Case "Pan"      '   pan
            winMap1.CursorMode = cmPan
        Case "ZoomPrev"   '   zoom last
            winMap1.ZoomToPrev
        Case "ZoomFull"   '   zoom full
            winMap1.ZoomToMaxExtents
        Case "Identify"      '   identify
            winMap1.CursorMode = cmSelection
        Case "ClearHighlight"
            Call winMap1.ClearDrawing(gHighLightLayerHandle)
    End Select
End Sub

Private Function ConvertToByteString(sString As String)
    Dim n As Long
    Dim aString() As Byte
    Dim cReturn As String
    
    aString = StrConv(sString, vbFromUnicode)
    
    cReturn = ""
    For n = 0 To UBound(aString)
        cReturn = cReturn & "[" & Hex(aString(n)) & "]"
    Next n
    
    ConvertToByteString = cReturn
    
End Function
