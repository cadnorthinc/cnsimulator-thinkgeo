VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{C1A8AF28-1257-101B-8FB0-0020AF039CA3}#1.1#0"; "mci32.ocx"
Begin VB.Form frmSoundByte 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   1755
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   6135
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1755
   ScaleWidth      =   6135
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.ComboBox cmbSoundChannel 
      Height          =   315
      Left            =   4200
      TabIndex        =   1
      Text            =   "Combo1"
      Top             =   360
      Width           =   615
   End
   Begin VB.CommandButton cmdSoundPlay 
      Caption         =   "&Play"
      Height          =   315
      Left            =   2520
      TabIndex        =   4
      Top             =   1320
      Width           =   1095
   End
   Begin MSComDlg.CommonDialog dlgSoundByte 
      Left            =   120
      Top             =   1200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdSoundCancel 
      Caption         =   "&Cancel"
      Height          =   315
      Left            =   3720
      TabIndex        =   6
      Top             =   1320
      Width           =   1095
   End
   Begin VB.CommandButton cmdSaveSoundByte 
      Caption         =   "&Save"
      Height          =   315
      Left            =   4920
      TabIndex        =   5
      Top             =   1320
      Width           =   1095
   End
   Begin VB.CommandButton cmdSoundBrowse 
      Caption         =   "&Browse"
      Height          =   315
      Left            =   4920
      TabIndex        =   3
      Top             =   840
      Width           =   1095
   End
   Begin VB.TextBox txtSoundFile 
      Height          =   315
      Left            =   1200
      TabIndex        =   2
      Text            =   "txtSoundFile"
      Top             =   840
      Width           =   3615
   End
   Begin VB.TextBox txtSoundTime 
      Height          =   315
      Left            =   1200
      TabIndex        =   0
      Text            =   "txtSoundTime"
      Top             =   360
      Width           =   1815
   End
   Begin MCI.MMControl mmSoundByte 
      Height          =   375
      Left            =   2400
      TabIndex        =   9
      Top             =   1320
      Width           =   3540
      _ExtentX        =   6244
      _ExtentY        =   661
      _Version        =   393216
      DeviceType      =   ""
      FileName        =   ""
   End
   Begin VB.Label Label3 
      Caption         =   "Channel:"
      Height          =   255
      Left            =   3360
      TabIndex        =   10
      Top             =   480
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Sound File:"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   960
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Sound Time:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   480
      Width           =   1095
   End
End
Attribute VB_Name = "frmSoundByte"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdSaveSoundByte_Click()
    Dim r As Integer
    Dim Test As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSaveSoundByte_Click of frmSoundByte")
    End If
    
    If Left(frmSoundByte.Caption, 1) = "I" Then                             '   an insert
    Else                                                                    '   an edit
        frmMain.flxBrowse.RemoveItem frmMain.flxBrowse.Row
    End If
    
    For r = 1 To frmMain.flxBrowse.Rows - 1
        If frmMain.flxBrowse.TextMatrix(r, K_FLX_DateTime) > txtSoundTime.Text _
            Or frmMain.flxBrowse.TextMatrix(r, K_FLX_DateTime) = "" Then
            Exit For
        End If
    Next r
    
    frmMain.flxBrowse.AddItem "", r
    frmMain.flxBrowse.TextMatrix(r, K_FLX_ID) = "SOUND"
    frmMain.flxBrowse.TextMatrix(r, K_FLX_DateTime) = txtSoundTime.Text
    frmMain.flxBrowse.TextMatrix(r, K_FLX_Address) = txtSoundFile.Text
    frmMain.flxBrowse.TextMatrix(r, K_FLX_City) = cmbSoundChannel.Text
    frmMain.flxBrowse.Row = r
    frmMain.flxBrowse.Col = 0
    frmMain.flxBrowse.RowSel = frmMain.flxBrowse.Row
    frmMain.flxBrowse.ColSel = frmMain.flxBrowse.Cols - 1
    frmMain.flxBrowse.CellAlignment = flexAlignRightCenter
    frmMain.cmdSaveScript.Enabled = True
    
    frmSoundByte.Hide

End Sub

Private Sub cmdSoundBrowse_Click()
    Dim FileName As String
    Dim aPath As Variant
    Dim n As Integer
    
    On Error GoTo ErrorHandler
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSoundBrowse_Click of frmSoundByte")
    End If
    
    dlgSoundByte.InitDir = gSoundByteDir
    dlgSoundByte.FileName = ""
    dlgSoundByte.Filter = "Wave Files (*.wav)|*.wav"
    dlgSoundByte.DialogTitle = "Select SoundByte File"
    dlgSoundByte.CancelError = True
    
    dlgSoundByte.ShowOpen
    
    frmSoundByte.txtSoundFile.Text = dlgSoundByte.FileName
    
    aPath = Split(dlgSoundByte.FileName, "\")
    
    gSoundByteDir = aPath(0)
    For n = 1 To UBound(aPath) - 1
        gSoundByteDir = gSoundByteDir & "\" & aPath(n)
    Next n
    
    Test = bsiPutSettings("SystemDefaults", "SoundDirectory", gSoundByteDir, gSystemDirectory & "\Simulator.INI")
    
    Exit Sub
    
ErrorHandler:
    '   MsgBox "Cancel selected", vbOKOnly + vbInformation, "Dialog trap"
End Sub

Private Sub cmdSoundCancel_Click()
    frmSoundByte.Hide
End Sub

Private Sub cmdSoundPlay_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSoundPlay_Click of frmSoundByte")
    End If
    If cmdSoundPlay.Caption = "&Play" Then
        cmdSoundPlay.Caption = "&Stop"
        mmSoundByte.DeviceType = "WaveAudio"
        mmSoundByte.Notify = False
        mmSoundByte.FileName = frmSoundByte.txtSoundFile.Text
        mmSoundByte.Command = "Open"
        
        mmSoundByte.Notify = True
        mmSoundByte.Wait = False
        mmSoundByte.Command = "Play"
    Else
        cmdSoundPlay.Caption = "&Play"
        mmSoundByte.Command = "Stop"
        DoEvents
    End If
End Sub

Private Sub Form_Load()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub Form_Load of frmSoundByte")
    End If
    mmSoundByte.Visible = False
    
    cmbSoundChannel.Clear                    '   reload the channel pick list. In a later version,
    cmbSoundChannel.AddItem " "              '   we'll check to see how many channels are needed
    cmbSoundChannel.AddItem "1"
    cmbSoundChannel.AddItem "2"
    cmbSoundChannel.AddItem "3"
    cmbSoundChannel.AddItem "4"
    cmbSoundChannel.AddItem "5"
    cmbSoundChannel.AddItem "6"
    cmbSoundChannel.AddItem "7"
    cmbSoundChannel.AddItem "8"

End Sub

Private Sub mmSoundByte_Done(NotifyCode As Integer)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mmSoundByte_Done of frmSoundByte")
    End If
    Select Case NotifyCode
        Case 1      '   Successful
        Case 2      '   Superceded
        Case 3      '   Aborted
        Case 4      '   Failure
    End Select
    mmSoundByte.Command = "Close"
    cmdSoundPlay.Caption = "&Play"
End Sub
