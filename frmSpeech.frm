VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmSpeech 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Simulator Speech Configuration"
   ClientHeight    =   5850
   ClientLeft      =   150
   ClientTop       =   540
   ClientWidth     =   9750
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5850
   ScaleWidth      =   9750
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox cbStations 
      Height          =   315
      Left            =   7560
      TabIndex        =   12
      Text            =   "cbStations"
      Top             =   5520
      Width           =   1695
   End
   Begin VB.ComboBox cbUnits 
      Height          =   315
      Left            =   5760
      TabIndex        =   11
      Text            =   "cbUnits"
      Top             =   5520
      Width           =   1695
   End
   Begin VB.ComboBox cbAgencies 
      Height          =   315
      Left            =   4680
      TabIndex        =   1
      Text            =   "cbAgencies"
      Top             =   0
      Width           =   3255
   End
   Begin MSComctlLib.Slider slVoiceRate 
      Height          =   255
      Left            =   720
      TabIndex        =   7
      Top             =   5520
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   450
      _Version        =   393216
   End
   Begin VB.ComboBox cbVoices 
      Height          =   315
      Left            =   720
      TabIndex        =   6
      Text            =   "cbVoices"
      Top             =   5160
      Width           =   4695
   End
   Begin VB.ComboBox cbLocations 
      Height          =   315
      Left            =   0
      TabIndex        =   0
      Text            =   "cbLocations"
      Top             =   0
      Width           =   4575
   End
   Begin VB.TextBox txtSayIt 
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Text            =   "txtSayIt"
      Top             =   4680
      Width           =   8415
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      Height          =   375
      Left            =   7200
      TabIndex        =   8
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Height          =   375
      Left            =   8520
      TabIndex        =   5
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton cmdSpeak 
      Caption         =   "Say It!"
      Height          =   375
      Left            =   8520
      TabIndex        =   4
      Top             =   4680
      Width           =   1215
   End
   Begin MSComctlLib.ListView lvTalk 
      Height          =   4215
      Left            =   0
      TabIndex        =   2
      Top             =   360
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   7435
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label Label2 
      Caption         =   "Rate:"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   5520
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Voice:"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   5160
      Width           =   495
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuSave 
         Caption         =   "&Save"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "&Close"
      End
   End
   Begin VB.Menu mnuOptions 
      Caption         =   "&Options"
      Begin VB.Menu mnuLocations 
         Caption         =   "&Locations"
      End
      Begin VB.Menu mnuStations 
         Caption         =   "&Stations and Posts"
      End
      Begin VB.Menu mnuUnits 
         Caption         =   "&Unit Type Callsigns"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "&About"
      End
   End
End
Attribute VB_Name = "frmSpeech"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Voice As SpeechLib.SpVoice
Dim Speaker As SpeechLib.ISpeechObjectTokens

Dim gItemsChanged As Boolean
Dim gOldSelection As String
Dim gCommandLine As Variant
Dim gDebugMode As Boolean
Dim gSpeechInstalled As Boolean

Dim gConnectString As String

'   Const INIPATH = "Q:\Tritech\VisiCAD\Data\System"
Const DEBUGINIPATH = ".."

Private Sub cbLocations_Click()
    Dim cSQL As String
    Dim aArray As Variant
    Dim rNum As Integer
    Dim cFixedName As String
    Dim n As Integer
    Dim lItem As ListItem
    Dim aINIList As Variant
    Dim TXApproved As Boolean
    Dim aSpeechList() As Variant
    Dim DebugMsg As String
    Dim aErrLog(0 To 20) As String
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cbLocations_Click of frmSpeech")
    End If
    
    aErrLog(0) = "cbLocations_Click"
    
    If gItemsChanged Then
        aErrLog(3) = "Calling SaveSpeechText"
        Call SaveSpeechText(gOldSelection)
    End If
    
    cSQL = "Select Name from Locations " _
            & "where LocationType_ID in " _
            & "(Select ID from LocationTypes where Description = '" _
            & cbLocations.Text _
            & "') order by Name"
    
    If Not gDebugMode Then
        aErrLog(3) = "Calling bsiSQLGet"
        aErrLog(4) = cSQL
        rNum = bsiSQLGet(cSQL, aArray, gConnectString)
    End If
    
    If rNum > 0 Then
    
        cFixedName = Replace(cbLocations.Text, " ", "_")
    
        aINIList = Split(bsiGetSettings("SPEECH.LOCATIONS", cFixedName, "", IIf(gDebugMode, DEBUGINIPATH, gSystemDirectory) & "\Simulator.INI"), "|")
        
        If UBound(aINIList) >= 0 Then
            ReDim aSpeechList(0 To UBound(aINIList))
            For n = 0 To UBound(aINIList)
                If UBound(Split(aINIList(n), ";")) = 1 Then
                    aINIList(n) = aINIList(n) & ";0"    '   correct for TXApproved Modification
                End If
                aSpeechList(n) = Split(aINIList(n), ";")
            Next n
        
            lvTalk.ListItems.Clear
        
            For n = 0 To rNum - 1
                TXApproved = False
                Set lItem = lvTalk.ListItems.Add(n + 1, , aArray(0, n))
                lItem.SubItems(1) = ScanArray(aSpeechList, aArray(0, n), TXApproved)
                lItem.Checked = TXApproved
            Next
            
            lvTalk.ListItems.Item(1).Selected = True
            
        Else
        
            lvTalk.ListItems.Clear
        
            For n = 0 To rNum - 1
                Set lItem = lvTalk.ListItems.Add(n + 1, , aArray(0, n))
            Next
        
        End If
        
    Else
        MsgBox "Error! No Locations found for type: " & UCase(cbLocations.Text) _
                & Chr(13) & "Check with the System Administrator!" _
                , vbCritical + vbOKOnly
    End If
    
    gItemsChanged = False
    gOldSelection = cbLocations.Text
    
    Exit Sub
    
ERH:

    aErrLog(1) = "Error:  " & Err.Description
    aErrLog(2) = "Number: " & Err.Number
    
    Call SpeechLog(aErrLog)
    
    MsgBox "Error occurred in cbLocations_Click for: " & UCase(cbLocations.Text) _
            & Chr(13) & "Please report this error to the System Administrator!" _
            , vbCritical + vbOKOnly

End Sub

Private Sub cbAgencies_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cbAgencies_Click of frmSpeech")
    End If
    
    If gItemsChanged Then
        Call SaveSpeechText(gOldSelection)
        gItemsChanged = False
    End If
    
    If mnuStations.Checked Then
    
        Call ProcessStations(cbAgencies.Text)
    
    ElseIf mnuUnits.Checked Then
    
        Call ProcessUnitCallSigns(cbAgencies.Text)
        
    End If
    
    gOldSelection = cbAgencies.Text
    
End Sub

Private Sub ProcessStations(AgencyName As String)
    Dim cSQL As String
    Dim aArray As Variant
    Dim rNum As Integer
    Dim cFixedName As String
    Dim n As Integer
    Dim lItem As ListItem
    Dim aINIList As Variant
    Dim TXApproved As Boolean
    Dim aSpeechList() As Variant
    Dim aErrLog(0 To 20) As String
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessStations of frmSpeech")
    End If
        
    aErrLog(0) = "ProcessStations(AgencyName As String) with '" & AgencyName & "'"
    
    cSQL = "select name from stations where id in " _
            & "(select stationid from stationbattalionlist where battalionid in " _
            & "(select id from battalion where divisionid in " _
            & "(select id from division where jurisdictionid in " _
            & "(select id from jurisdiction where agencyid in " _
            & "(select id from agencytypes where agency_type = '" & AgencyName & "') ) ) ) ) " _
            & "order by Name"
    
    If Not gDebugMode Then
        aErrLog(3) = "Calling bsiSQLGet"
        aErrLog(4) = cSQL
        rNum = bsiSQLGet(cSQL, aArray, gConnectString)
    End If
    
    If rNum > 0 Then
    
        cFixedName = Replace(AgencyName, " ", "_")
    
        aINIList = Split(bsiGetSettings("SPEECH.STATIONS", cFixedName, "", IIf(gDebugMode, DEBUGINIPATH, gSystemDirectory) & "\Simulator.INI"), "|")
        
        If UBound(aINIList) >= 0 Then
            ReDim aSpeechList(0 To UBound(aINIList))
            For n = 0 To UBound(aINIList)
                If UBound(Split(aINIList(n), ";")) = 1 Then
                    aINIList(n) = aINIList(n) & ";0"    '   correct for TXApproved Modification
                End If
                aSpeechList(n) = Split(aINIList(n), ";")
            Next n
        
            lvTalk.ListItems.Clear
        
            aErrLog(3) = "Calling ScanArray"
            aErrLog(4) = ""
            
            For n = 0 To rNum - 1
                Set lItem = lvTalk.ListItems.Add(n + 1, , aArray(0, n))
                lItem.SubItems(1) = ScanArray(aSpeechList, aArray(0, n), TXApproved)
                lItem.Checked = False
            Next
        Else
        
            lvTalk.ListItems.Clear
        
            For n = 0 To rNum - 1
                Set lItem = lvTalk.ListItems.Add(n + 1, , aArray(0, n))
            Next
        
        End If
        
    Else
        MsgBox "Error! No Stations found for Agency: " & UCase(cbAgencies.Text) _
                & Chr(13) & "Check with the System Administrator!" _
                , vbCritical + vbOKOnly
    End If
    
    Exit Sub
    
ERH:

    aErrLog(1) = "Error:  " & Err.Description
    aErrLog(2) = "Number: " & Err.Number
    
    Call SpeechLog(aErrLog)
    
    MsgBox "Error occurred in ProcessStations for: " & UCase(cbAgencies.Text) _
            & Chr(13) & "Please report this error to the System Administrator!" _
            , vbCritical + vbOKOnly
            
End Sub

Private Sub ProcessUnitCallSigns(AgencyName As String)
    Dim cSQL As String
    Dim aArray As Variant
    Dim rNum As Integer
    Dim cFixedName As String
    Dim n As Integer
    Dim lItem As ListItem
    Dim aINIList As Variant
    Dim TXApproved As Boolean
    Dim aSpeechList() As Variant
    Dim aErrLog(0 To 20) As String
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub ProcessUnitCallSigns of frmSpeech")
    End If
    
    aErrLog(0) = "ProcessUnitCallSigns(AgencyName As String) with '" & AgencyName & "'"
    
    cSQL = "select description from resource where agencytypeid in " _
            & "(select id from agencytypes where agency_type = '" & cbAgencies.Text & "') " _
            & "order by Description"
    
    aErrLog(3) = "Calling bsiSQLGet"
    aErrLog(4) = cSQL
    
    rNum = bsiSQLGet(cSQL, aArray, gConnectString)
    
    If rNum > 0 Then
    
        cFixedName = Replace(AgencyName, " ", "_")
    
        aINIList = Split(bsiGetSettings("SPEECH.UNITS", cFixedName, "", IIf(gDebugMode, DEBUGINIPATH, gSystemDirectory) & "\Simulator.INI"), "|")
        
        If UBound(aINIList) >= 0 Then
            ReDim aSpeechList(0 To UBound(aINIList))
            For n = 0 To UBound(aINIList)
                If UBound(Split(aINIList(n), ";")) = 1 Then
                    aINIList(n) = aINIList(n) & ";0"    '   correct for TXApproved Modification
                End If
                aSpeechList(n) = Split(aINIList(n), ";")
            Next n
        
            lvTalk.ListItems.Clear
        
            aErrLog(3) = "Calling ScanArray"
            aErrLog(4) = ""
            
            For n = 0 To rNum - 1
                TXApproved = False
                Set lItem = lvTalk.ListItems.Add(n + 1, , aArray(0, n))
                lItem.SubItems(1) = ScanArray(aSpeechList, aArray(0, n), TXApproved)
                lItem.Checked = TXApproved
            Next
        Else
        
            lvTalk.ListItems.Clear
        
            For n = 0 To rNum - 1
                Set lItem = lvTalk.ListItems.Add(n + 1, , aArray(0, n))
            Next
        
        End If
        
    Else
        MsgBox "Error! No Stations found for Agency: " & UCase(cbAgencies.Text) _
                & Chr(13) & "Check with the System Administrator!" _
                , vbCritical + vbOKOnly
    End If
    
    Exit Sub
    
ERH:

    aErrLog(1) = "Error:  " & Err.Description
    aErrLog(2) = "Number: " & Err.Number
    
    Call SpeechLog(aErrLog)
    
    MsgBox "Error occurred in ProcessUnitCallSigns for: " & UCase(cbAgencies.Text) _
            & Chr(13) & "Please report this error to the System Administrator!" _
            , vbCritical + vbOKOnly

End Sub

Private Function ScanArray(aArray As Variant, ByVal SearchString As String, ByRef TXApproved As Boolean) As String
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function ScanArray of frmSpeech")
    End If
    
    ScanArray = ""
    
    If IsArray(aArray) Then
        For n = 0 To UBound(aArray)
            If aArray(n)(0) = SearchString Then
                ScanArray = aArray(n)(1)
                TXApproved = IIf(aArray(n)(2) = "1", True, False)
                Exit For
            End If
        Next
    End If
    
End Function

Private Sub SaveSpeechText(LocationType As String)
    Dim PropertyString As String
    Dim Test As Boolean
    Dim n As Integer
    Dim SettingSection As String
    Dim SettingItem As String
    Dim aErrLog(0 To 20) As String
    
    On Error GoTo ERH
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SaveSpeechText of frmSpeech")
    End If
    
    aErrLog(0) = "SaveSpeechText(LocationType As String) with '" & LocationType & "'"
    
    If mnuLocations.Checked Then
        SettingSection = "SPEECH.LOCATIONS"
        SettingItem = UCase(Replace(LocationType, " ", "_"))
    ElseIf mnuStations.Checked Then
        SettingSection = "SPEECH.STATIONS"
        SettingItem = UCase(Replace(cbAgencies.Text, " ", "_"))     '   no blanks allowed in INI files
    ElseIf mnuUnits.Checked Then
        SettingSection = "SPEECH.UNITS"
        SettingItem = UCase(Replace(cbAgencies.Text, " ", "_"))
    End If
    
    For n = 1 To lvTalk.ListItems.count
        If lvTalk.ListItems.Item(n).SubItems(1) <> "" Or lvTalk.ListItems.Item(n).Checked Then
            If PropertyString = "" Then
                PropertyString = lvTalk.ListItems.Item(n).Text & ";" _
                                    & lvTalk.ListItems.Item(n).SubItems(1) & ";" _
                                    & IIf(lvTalk.ListItems.Item(n).Checked, "1", "0")
            Else
                PropertyString = PropertyString & "|" _
                                    & lvTalk.ListItems.Item(n).Text & ";" _
                                    & lvTalk.ListItems.Item(n).SubItems(1) & ";" _
                                    & IIf(lvTalk.ListItems.Item(n).Checked, "1", "0")

            End If
        End If
    Next
    
    If Not bsiPutSettings(SettingSection, SettingItem, PropertyString, IIf(gDebugMode, DEBUGINIPATH, gSystemDirectory) & "\Simulator.INI", True) Then
        MsgBox "Could not save settings to Simulator.INI in SaveSpeechText for: " & LocationType _
                & Chr(13) & "Please report this error to the System Administrator!" _
                , vbCritical + vbOKOnly
    End If
    
    Exit Sub

ERH:

    aErrLog(1) = "Error:  " & Err.Description
    aErrLog(2) = "Number: " & Err.Number
    aErrLog(3) = "SettingSection: " & SettingSection
    aErrLog(4) = "PropertyString: " & PropertyString
    
    Call SpeechLog(aErrLog)
    
    MsgBox "Error occurred in SaveSpeechText for: " & LocationType _
            & Chr(13) & "Please report this error to the System Administrator!" _
            , vbCritical + vbOKOnly

End Sub

Private Sub cmdReset_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdReset_Click of frmSpeech")
    End If
    lvTalk.SelectedItem.SubItems(1) = ""
    txtSayIt.Text = lvTalk.SelectedItem.Text
End Sub

'---------------------------------------------------------------------------------------
' Procedure :   cmdSave_Click
' DateTime  :   4/4/2007 18:32
' Author    :   Brian McGrath
' Copyright :   CAD North Inc. 2006
' Purpose   :
'---------------------------------------------------------------------------------------
'
Private Sub cmdSave_Click()
    Dim ErrorMsg As String

    On Error GoTo cmdSave_Click_ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSave_Click of frmSpeech")
    End If

    ErrorMsg = "Start of cmdSave_Click"

    If Not gDebugMode And lvTalk.ListItems.count > 0 Then
        lvTalk.SelectedItem.SubItems(1) = txtSayIt.Text
    End If
    gItemsChanged = True

    On Error GoTo 0
    Exit Sub

cmdSave_Click_ERH:

    Call AddToDebugList("[ERROR] in procedure cmdSave_Click of Form frmSpeech:" & Err.Number & ", " & Err.Description _
                            & " - " & ErrorMsg, frmMain.lstDebug)
End Sub

'---------------------------------------------------------------------------------------
' Procedure :   cmdSpeak_Click
' DateTime  :   4/30/2007 16:35
' Author    :   Brian McGrath
' Copyright :   CAD North Inc. 2006
' Purpose   :
'---------------------------------------------------------------------------------------
'
Private Sub cmdSpeak_Click()
    Dim ErrorMsg As String

    On Error GoTo cmdSpeak_Click_ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub cmdSpeak_Click of frmSpeech")
    End If

    ErrorMsg = "Start of cmdSpeak_Click"

    If txtSayIt.Text <> "" Then
        Call cmdSave_Click
        If gSpeechInstalled Then
            ErrorMsg = "Setting Voice"
            Set Voice.Voice = Speaker.Item(cbVoices.ListIndex)
            ErrorMsg = "Setting Rate"
            Voice.Rate = slVoiceRate.Value
            ErrorMsg = "Speaking Phrase: '" & txtSayIt.Text & "'"
            Voice.Speak txtSayIt.Text, SVSFlagsAsync
        End If
    End If

    On Error GoTo 0
    Exit Sub

cmdSpeak_Click_ERH:

    Call AddToDebugList("[ERROR] in procedure cmdSpeak_Click of Form frmSpeech:" & Err.Number & ", " & Err.Description _
                            & " - " & ErrorMsg, frmMain.lstDebug)

End Sub

'---------------------------------------------------------------------------------------
' Procedure :   Form_Load
' DateTime  :   4/4/2007 12:05
' Author    :   Brian McGrath
' Copyright :   CAD North Inc. 2006
' Purpose   :
'---------------------------------------------------------------------------------------
'
Private Sub Form_Load()
    Dim aVoices() As String
    Dim n As Integer
    Dim cCommand As String
    
    Dim ErrorMsg As String

    On Error GoTo Form_Load_ERH

    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub Form_Load of frmSpeech")
    End If

    ErrorMsg = "Start of Form_Load"
    
    gSpeechInstalled = True

    cCommand = Command()
    
    If cCommand = "" Then
        cCommand = "/"
    End If
    
    gCommandLine = Split(cCommand)
    
    ErrorMsg = "Creating SpVoice Speech Object"
    Set Voice = New SpVoice
    
ResumeFromHere:

    If Not gSpeechInstalled Then
        cmdSpeak.Enabled = False
        cbVoices.Enabled = False
        slVoiceRate.Enabled = False
        cbVoices.Text = ""
        txtSayIt.Text = ""
    End If
    
    With frmSpeech
'        .Top = Screen.Height / 2 - .Height / 2          '   center form
'        .Left = Screen.Width / 2 - .Width / 2           '   center form
'        If gCommandLine(0) = "/debug" Then
'            gDebugMode = True
'        End If
    End With
        
    With lvTalk
        .MultiSelect = False
        .View = lvwReport
        .Checkboxes = True
        .ColumnHeaders.Add , , "Location Name (Check box if Transport Destination)", (.Width * 0.4)
        .ColumnHeaders.Add , , "Speak String", (.Width * 0.59)
        .GridLines = True
        .FullRowSelect = True
        .HideSelection = False
    End With
    
    gConnectString = FixConnectString()
    
    ErrorMsg = "Loading Locations"
    Call LoadLocationsCombo
    ErrorMsg = "Loading Agencies"
    Call LoadAgenciesCombo
    
    With cbUnits
        .Width = cbLocations.Width
        .Top = cbLocations.Top
        .Left = cbLocations.Left
        .Visible = False
    End With
    
    With cbStations
        .Width = cbLocations.Width
        .Top = cbLocations.Top
        .Left = cbLocations.Left
        .Visible = False
    End With
    
    With cbAgencies
        .Width = cbLocations.Width
        .Top = cbLocations.Top
        .Left = cbLocations.Left
        .Visible = False
    End With
    
    mnuLocations.Checked = True
    frmSpeech.Caption = "Speech Configuration: Locations" & IIf(gDebugMode, " - Debug Mode", "")
    
    If gSpeechInstalled Then
        ErrorMsg = "Creating Speaker Objects"
        Set Speaker = Voice.GetVoices()
        
        frmMain.gMaxVoices = Speaker.count
        
        For n = 0 To Speaker.count - 1
            cbVoices.AddItem Speaker.Item(n).GetDescription
        Next
        
        cmdSpeak.Default = True
        
        slVoiceRate.Max = 10
        slVoiceRate.Min = -10
        slVoiceRate.TickFrequency = 1
        slVoiceRate.LargeChange = 2
        slVoiceRate.Value = 0
        '   slVoiceRate.Value = Voice.Rate
        
        cbVoices.ListIndex = 0
        txtSayIt.Text = ""
    End If
    
    gItemsChanged = False

    On Error GoTo 0
    Exit Sub

Form_Load_ERH:
    Dim ErrorNum As Integer
    Dim ErrorDesc As String
    
    ErrorNum = Err.Number
    ErrorDesc = Err.Description
    
    Call AddToDebugList("[ERROR] in procedure Form_Load of Form frmSpeech:" & ErrorNum & ", " & ErrorDesc _
                            & " - " & ErrorMsg, frmMain.lstDebug)
   
    Select Case ErrorNum
        Case 429
            gSpeechInstalled = False
            Resume ResumeFromHere
        Case other
    End Select

End Sub

Private Function FixConnectString() As String
    Dim Temp As Variant
    Dim sTemp As String
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Function FixConnectString of frmSpeech")
    End If
    
    sTemp = bsiGetSettings("SystemPreferences", "ODBCPassword", "UID=tritech;PWD=europa;DSN=system;", IIf(gDebugMode, DEBUGINIPATH, gSystemDirectory) & "\System.INI")
    Temp = Split(sTemp, ";")
    sTemp = ""
    
    For n = 0 To UBound(Temp)
        If Temp(n) <> "ODBC" Then
            sTemp = sTemp & Temp(n) & ";"
        End If
    Next n
        
    FixConnectString = sTemp
    
End Function

Private Sub LoadLocationsCombo()
    Dim cSQL As String
    Dim aArray As Variant
    Dim rNum As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub LoadLocationsCombo of frmSpeech")
    End If
    
    cSQL = "Select Description from LocationTypes"
    
    If Not gDebugMode Then
        rNum = bsiSQLGet(cSQL, aArray, gConnectString)
    End If
    
    If rNum > 0 Then
        For n = 0 To rNum - 1
            cbLocations.AddItem aArray(0, n)
        Next
        cbLocations.ListIndex = -1
        cbLocations.Text = "<Select Location Type>"
    Else
        MsgBox "Error! No Locations Found!" _
                & Chr(13) & "Check ODBC Settings and TriTech System Drive Mapping!" _
                , vbCritical + vbOKOnly
    End If

End Sub

Private Sub LoadAgenciesCombo()
    Dim cSQL As String
    Dim aArray As Variant
    Dim rNum As Integer
            
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub LoadAgenciesCombo of frmSpeech")
    End If
            
    cSQL = "Select Agency_Type from AgencyTypes"
    
    If Not gDebugMode Then
        rNum = bsiSQLGet(cSQL, aArray, gConnectString)
    End If
    
    If rNum > 0 Then
        For n = 0 To rNum - 1
            cbAgencies.AddItem aArray(0, n)
            cbAgencies.Text = "<Select Agency>"
        Next
    Else
        MsgBox "Error! No Agencies Found!" _
                & Chr(13) & "This should NEVER happen!" _
                & Chr(13) & "Check ODBC Settings and TriTech System Drive Mapping!" _
                , vbCritical + vbOKOnly
    End If

End Sub
Private Sub Form_Unload(Cancel As Integer)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub Form_Unload of frmSpeech")
    End If
    Call mnuExit_Click
End Sub

Private Sub lvTalk_BeforeLabelEdit(Cancel As Integer)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lvTalk_BeforeLabelEdit of frmSpeech")
    End If
    Cancel = 1
End Sub

Private Sub lvTalk_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim Test As Boolean
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lvTalk_ItemCheck of frmSpeech")
    End If
    
    Test = Item.Checked
    
    gItemsChanged = True
    
End Sub

Private Sub lvTalk_ItemClick(ByVal Item As MSComctlLib.ListItem)
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub lvTalk_ItemClick of frmSpeech")
    End If
    If Item.SubItems(1) = "" Then
        txtSayIt.Text = Item.Text
    Else
        txtSayIt.Text = Item.SubItems(1)
    End If
End Sub

Private Sub mnuExit_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuExit_Click of frmSpeech")
    End If
    If gItemsChanged Then
        Call SaveSpeechText(gOldSelection)
        gItemsChanged = False
    End If
    
    Call frmMain.SetupSpeechLists
    
    Call frmSpeech.Hide
    
    '   Call Form_Unload(0)
End Sub

Private Sub mnuLocations_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuLocations_Click of frmSpeech")
    End If
    If gItemsChanged Then
        Call SaveSpeechText(gOldSelection)
        gItemsChanged = False
    End If
    mnuLocations.Checked = True
    mnuStations.Checked = False
    mnuUnits.Checked = False
    cbLocations.Visible = True
    cbAgencies.Visible = False
    cbUnits.Visible = False
    cbStations.Visible = False
    lvTalk.ListItems.Clear
    cbLocations.ListIndex = -1
    
    lvTalk.ColumnHeaders.Item(1).Text = "Locations (Check box if Transport Destination)"
    
    cbLocations.Text = "<Select Location Type>"
    frmSpeech.Caption = "Speech Configuration: Locations" & IIf(gDebugMode, " - Debug Mode", "")
End Sub

Private Sub mnuStations_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuStations_Click of frmSpeech")
    End If
    If gItemsChanged Then
        Call SaveSpeechText(gOldSelection)
        gItemsChanged = False
    End If
    mnuLocations.Checked = False
    mnuStations.Checked = True
    mnuUnits.Checked = False
    cbLocations.Visible = False
    cbAgencies.Visible = True
    cbStations.Visible = False
    cbUnits.Visible = False
    lvTalk.ListItems.Clear
    '   cbAgencies.ListIndex = -1
    
    lvTalk.ColumnHeaders.Item(1).Text = "Stations"
    
    cbAgencies.Text = "<Select Agency>"
    frmSpeech.Caption = "Speech Configuration: Stations and Posts" & IIf(gDebugMode, " - Debug Mode", "")
End Sub

Private Sub mnuUnits_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuUnits_Click of frmSpeech")
    End If
    If gItemsChanged Then
        Call SaveSpeechText(gOldSelection)
        gItemsChanged = False
    End If
    mnuLocations.Checked = False
    mnuStations.Checked = False
    mnuUnits.Checked = True
    cbLocations.Visible = False
    cbAgencies.Visible = True
    cbStations.Visible = False
    cbUnits.Visible = False
    lvTalk.ListItems.Clear
    '   cbAgencies.ListIndex = -1
    
    lvTalk.ColumnHeaders.Item(1).Text = "Unit Callsigns (Check box if Transport-capable)"
    
    cbAgencies.Text = "<Select Agency>"
    frmSpeech.Caption = "Speech Configuration: Unit Type Callsigns" & IIf(gDebugMode, " - Debug Mode", "")
End Sub

Private Sub mnuSave_Click()
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub mnuSave_Click of frmSpeech")
    End If
    If gItemsChanged Then
        Call SaveSpeechText(gOldSelection)
        gItemsChanged = False
    End If
End Sub

Private Sub SpeechLog(aString() As String)
    Dim FileNum As Integer
    Dim FileName As String
    Dim n As Integer
    
    If gDetailedDebugging Then
        Call AddToDebugList("[TRACE] Sub SpeechLog of frmSpeech")
    End If
    
    FileNum = FreeFile
    
    If FileNum > 0 Then
        FileName = App.Path & "\SpeechError.Log"
        
        Open (FileName) For Append As #(FileNum)
        
        Print #(FileNum), Format(Now, "MMM DD, YYYY HH:NN:SS ") & "Process: " & aString(0)
        For n = 1 To UBound(aString)
            If aString(n) <> "" Then
                Print #(FileNum), "                        " & aString(n)
            End If
        Next n
        
        Close #(FileNum)
    End If
    
End Sub
