VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSplashOLD 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5730
   ClientLeft      =   255
   ClientTop       =   1410
   ClientWidth     =   7380
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmSplashOLD.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5730
   ScaleWidth      =   7380
   ShowInTaskbar   =   0   'False
   Begin VB.Frame frmKeyInfo 
      Caption         =   "Frame1"
      Height          =   1215
      Left            =   0
      TabIndex        =   2
      Top             =   4500
      Width           =   7275
      Begin VB.Label lblSerial 
         Caption         =   "Label2"
         Height          =   195
         Left            =   4140
         TabIndex        =   7
         Top             =   900
         Width           =   1515
      End
      Begin VB.Label lblSite 
         Caption         =   "Label2"
         Height          =   195
         Left            =   1200
         TabIndex        =   6
         Top             =   600
         Width           =   4515
      End
      Begin VB.Label lblClient 
         Caption         =   "Label2"
         Height          =   195
         Left            =   1200
         TabIndex        =   5
         Top             =   300
         Width           =   4395
      End
      Begin VB.Label lblExpiry 
         Caption         =   "Label2"
         Height          =   195
         Left            =   180
         TabIndex        =   4
         Top             =   900
         Width           =   3855
      End
      Begin VB.Label Label1 
         Caption         =   "Licensed to:"
         Height          =   195
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   915
      End
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   720
      Top             =   2940
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   400
      ImageHeight     =   300
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   4
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSplashOLD.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSplashOLD.frx":13EE2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSplashOLD.frx":14BD6
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSplashOLD.frx":19711
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblProgress 
      BackStyle       =   0  'Transparent
      Caption         =   "lblProgress"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   4200
      Width           =   3615
   End
   Begin VB.Image imgClient 
      Height          =   1035
      Left            =   2640
      Top             =   1620
      Width           =   1395
   End
   Begin VB.Label lblCompany 
      BackStyle       =   0  'Transparent
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Garamond"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   675
      Left            =   3060
      TabIndex        =   1
      Top             =   480
      Width           =   4275
   End
   Begin VB.Label lblVersion 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Version"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   285
      Left            =   3060
      TabIndex        =   0
      Top             =   3840
      Width           =   3345
   End
   Begin VB.Image imgSplash 
      Height          =   1335
      Left            =   1320
      Top             =   1020
      Width           =   1875
   End
End
Attribute VB_Name = "frmSplashOLD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Dim ClientRatio As Double
    
    imgSplash.Stretch = False
    imgSplash.Top = 0
    imgSplash.Left = 0
    Set imgSplash.Picture = ImageList1.ListImages(1).Picture
    frmSplash.Height = imgSplash.Height
    frmSplash.Width = imgSplash.Width
    imgClient.Stretch = False
    
    If frmMain.gClientLogo > 0 Then
        Set imgClient.Picture = ImageList1.ListImages(frmMain.gClientLogo).Picture
        ClientRatio = imgClient.Picture.Height / imgClient.Picture.Width
        imgClient.Top = frmSplash.Height / 2 - imgClient.Height / 2
        imgClient.Left = frmSplash.Width - imgClient.Width - 150
    End If
    
    lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
    lblVersion.Left = frmSplash.Width - lblVersion.Width - 150
    lblVersion.Top = frmSplash.Height - lblVersion.Height - 150
    lblCompany.Visible = False
    
    frmSplash.Height = frmSplash.Height + frmKeyInfo.Height + 80
    frmKeyInfo.Width = frmSplash.ScaleWidth
    
    If frmMain.gLicensedVersion Then
        frmKeyInfo.Caption = "License Information"
        lblClient.Caption = frmMain.gClientInfo
        lblSite.Caption = frmMain.gSiteInfo
        lblSerial.Caption = "Serial: " & frmMain.gSerialNumber
        If KeyInfo.AppMode = "DEMO" Then
            lblExpiry.Caption = "Demonstation Version - Expires " & Format(frmMain.gLicenseExpires, "MMM DD, YYYY")
        Else
            lblExpiry.Caption = "Licensed Version"
        End If
    Else
        frmKeyInfo.Caption = "License Information"
        lblClient.Caption = "Unlicensed Version"
        lblExpiry.Caption = ""
        lblSite.Caption = "Contact Brimac Systems Inc. for licensing information"
        lblSerial.Caption = "Serial: *Unlicensed*"
    End If

End Sub

Private Sub Frame1_Click()
    Unload Me
End Sub
